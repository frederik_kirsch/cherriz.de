<?php

ini_set("display_errors", 1);
require '../sources/basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../sources/");

LibImporter::import("basic/util/class.Picture.php");
LibImporter::import("basic/util/class.DBConnect.php");
LibImporter::import("basic/util/class.Filter.php");
LibImporter::import("basic/util/class.Settings.php");
LibImporter::import("basic/util/class.CherrizSessionHandler.php");

CherrizSessionHandler::init();

$id = $_REQUEST['id'];
$size = $_REQUEST['size'];

$pic = new Picture();
switch($_REQUEST['type']){
	case Picture::PICTURE:
		$pic->showPicture($id, $size);
		break;
	case Picture::USER:
		$pic->showUser($id, $size);
		break;
	case Picture::GRUPPE:
		$pic->showGruppe($id, $size);
		break;
}