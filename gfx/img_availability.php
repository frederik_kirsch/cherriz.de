<?php
ini_set("display_errors", 1);
require '../sources/basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../sources/");
LibImporter::import("basic/globals.php");

//Tag ermitteln
$day = FILTER::text($_REQUEST['day']);
if (strlen($day) != 10) {
    $day = date("d.m.Y");
}

AvailabilityImage::draw(AvailabilityImage::DAY, $day);

class AvailabilityImage
{

    //KONSTANTEN
    const DAY = "day";

    const WEEK = "week";

    const MONTH = "month";

    //SONSTIGES
    private $user = null;

    private $db = null;

    private $date = null;

    public static function draw($type, $startDate)
    {
        $instance = new AvailabilityImage($startDate);
        if (!$instance->checkPermission()) {
            die("Access denied");
        }
        switch ($type) {
            case AvailabilityImage::DAY:
                $instance->drawDay();
                break;
            case AvailabilityImage::WEEK:
                break;
            case AvailabilityImage::MONTH:
                break;
        }
    }

    private function __construct($date)
    {
        $this->user = Login::getInstance()->getCurrentUserObject();
        $this->db = DBConnect::getDBConnection();
        $this->date = $this->getDBDate($date);
    }

    private function drawDay()
    {
        header("Content-type: image/png");

        $image = $this->getDayBasic();

        $image = $this->drawDayStatistic($image);

        Imagepng($image);
    }

    private function getDBDate($date)
    {
        $date = DateTime::createFromFormat("d.m.Y", $date);
        return $date->format("Y-m-d");
    }

    private function drawDayStatistic($image)
    {
        //Farben
        $red = ImageColorAllocate($image, 255, 0, 0);
        $green = ImageColorAllocate($image, 0, 255, 0);
        //Intervalle
        $width = 5.48;
        $interv = 864;

        Imagefilledrectangle($image, 26, 0, 574, 50, $green);

        $query = "SELECT ROUND(TIME_TO_SEC(Time), 60) AS Sec, Count(*) AS Anz
                  FROM Request
                  WHERE Time BETWEEN '" . $this->date . " 00:00:00' AND '" . $this->date . " 23:59:59'
                  GROUP BY ROUND(TIME_TO_SEC(TIME) / $interv)
                  ORDER BY TIME";
        $result = $this->db->query($query);

        //Ausfälle ermitteln
        $item = $result->fetch_object();
        $sec = 0;
        for ($x = 26; $x < 569; $x += $width) {
            if ($item->Sec - $sec > $interv) {
                Imagefilledrectangle($image, $x, 0, $x + $width, 50, $red);
            } else {
                if ($item->Anz < 2) {
                    Imagefilledrectangle($image, $x, 0, $x + $width, 50, $red);
                }
                $item = $result->fetch_object();
            }
            $sec += $interv;
        }

        return $image;
    }

    private function getDayBasic()
    {
        $bild = Imagecreatetruecolor(600, 100);

        $white = ImageColorAllocate($bild, 255, 255, 255);
        $black = ImageColorAllocate($bild, 0, 0, 0);

        ImageFill($bild, 0, 0, $white);
        Imageline($bild, 25, 80, 575, 80, $black);

        Imageline($bild, 25, 0, 25, 80, $black);
        ImageString($bild, 3, 10, 85, "0 Uhr", $black);

        Imageline($bild, 48, 70, 48, 80, $black);
        Imageline($bild, 71, 70, 71, 80, $black);
        Imageline($bild, 94, 70, 94, 80, $black);
        Imageline($bild, 117, 70, 117, 80, $black);
        Imageline($bild, 140, 70, 140, 80, $black);

        Imageline($bild, 163, 60, 163, 80, $black);
        ImageString($bild, 3, 148, 85, "6 Uhr", $black);

        Imageline($bild, 185, 70, 185, 80, $black);
        Imageline($bild, 210, 70, 210, 80, $black);
        Imageline($bild, 233, 70, 233, 80, $black);
        Imageline($bild, 256, 70, 256, 80, $black);
        Imageline($bild, 279, 70, 279, 80, $black);

        Imageline($bild, 300, 50, 300, 80, $black);
        ImageString($bild, 3, 280, 85, "12 Uhr", $black);

        Imageline($bild, 323, 70, 323, 80, $black);
        Imageline($bild, 346, 70, 346, 80, $black);
        Imageline($bild, 369, 70, 369, 80, $black);
        Imageline($bild, 392, 70, 392, 80, $black);
        Imageline($bild, 415, 70, 415, 80, $black);

        Imageline($bild, 438, 60, 438, 80, $black);
        ImageString($bild, 3, 423, 85, "18 Uhr", $black);

        Imageline($bild, 460, 70, 460, 80, $black);
        Imageline($bild, 483, 70, 483, 80, $black);
        Imageline($bild, 506, 70, 506, 80, $black);
        Imageline($bild, 529, 70, 529, 80, $black);
        Imageline($bild, 552, 70, 552, 80, $black);

        Imageline($bild, 575, 0, 575, 80, $black);
        ImageString($bild, 3, 555, 85, "24 Uhr", $black);

        return $bild;
    }

    private function checkPermission()
    {
        if ($this->user->hasRight(Recht::ACCESS_ADMIN) && $this->user->hasRight(Recht::CONSTRUCTION_MODE)) {
            return true;
        }
        return false;
    }

}