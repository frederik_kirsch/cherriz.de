//@ sourceURL=wysiwyg_utility.js

/** Enumaration for Features that have to be implemented platformdependent. */
var FeatureSupport = {

	/** Exec Command Insert HTML. */
	INSERT_HTML: 0,

	/** Linebreaks in Textselection. */
	SELECTION: 1

};

/** Enumerations for ExecCommands that are not Browser Indipendent. */
var IndependendExecCommand = {

	/** Command for inserting HTML at caret position. */
	INSERT_HTML: "insertHTML"

};

/**
 * Creates a WYSIWYG Editor, based on the given Textarea.
 *
 * @param {string} textAreaID The ID of the Textarea.
 * @param {int} width The Width the editor should have.
 * @param {int} height The Height, the editor should have.
 * @param {boolean} beta <code>true</code> if Beta Features should be activated.
 * @returns {jQuery} The Editor as a jQuery Object.
 */
function createEditor(textAreaID, width, height, beta) {
	var controls = "bold italic underline cheader | bullets numbering | alignleft center alignright | undo redo | clink unlink cimage cimagegal cvideo | source";
	if (beta) {
		controls = "bold italic underline cheader | bullets numbering | alignleft center alignright | undo redo | clink unlink ccode cimage cimagegal cvideo | source";
	}

	var textArea = $("#" + textAreaID);
	var editor = textArea.cleditor({
		width: width, // width not including margins, borders or padding
		height: height, // height not including margins, borders or padding
		controls: controls, // controls to add to the toolbar
		useCSS: false, // use CSS to style HTML when possible (not supported in ie)
		docType: '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">', // Document type contained within the editor
		docCSSFile: "styles/cleditor/content.php?width=" + width, // CSS file used to style the document contained within the editor
		bodyStyle: "" // style to assign to document body contained within the editor
	});

	// Klick auf Buttons zerstoert Selektion in iframe nicht.
	$(".cleditorButton").attr("unselectable", "on");

	// Interceptor fuer Past-Event einbinden
	var doc = $(textArea.parent().find("iframe")[0].contentDocument);
	doc.find("body").on('paste', function(e) {insertAsPlaneText(e, doc, editor[0])});

	// Cursor setzem
	editor.focus();

	return editor;
}

/**
 * Unterbricht das Pasteevent und transformiert den Inhalt der Zwischenablage als PlainText um ihn manuell in den Editor einzufuegen.
 * @param {Event} e Das Event.
 * @param {jQuery} doc Das HTML Dokument des iframes.
 * @param {jQuery} editor
 */
function insertAsPlaneText(e, doc, editor) {
	// cancel paste
	e.preventDefault();

	var text;
	if (typeof clipboardData !== 'undefined') {
		// get text representation of clipboard
		text = clipboardData.getData('Text');
		var tmpImage = "!!!tmp!!!";
		doc[0].execCommand("insertImage", false, tmpImage);
		doc.find("img[src='" + tmpImage + "']").replaceWith(convertCodeToHTML(text));
	} else if (e.originalEvent) {
		// get text representation of clipboard
		text = e.originalEvent.clipboardData.getData('text/plain');
		doc[0].execCommand('insertHTML', false, convertCodeToHTML(text));
		editor.updateTextArea();
	}
}

/**
 * Transformiert den Quellcode nach HTML.
 * @param {string} code Der Quellcode.
 * @returns {string} Der Quellcode als HTML.
 */
function convertCodeToHTML(code) {
	$.each([[/</g, "&lt;"],
			[/>/g, "&gt;"],
			[/"/g, "&quot;"],
			[/([^ ])( )([^ ])/g, "$1!!!space!!!$3"],
			[/ /g, "&nbsp;"],
			[/(!!!space!!!)/g, " "],
			[/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;"],
			[/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, "$1<br />$2"]],
		function(index, item) {
			code = code.replace(item[0], item[1]);
		});
	return code;
}

/**
 * Determines the Browsertype.
 *
 * @returns {boolean} The Feature.
 */
function isSupported(feature) {
	switch (feature) {
		case FeatureSupport.INSERT_HTML:
			if (document.execCommandShowHelp == null) {
				return true;
			}
			break;
		case FeatureSupport.SELECTION:
			if (document.execCommandShowHelp == null) {
				return true;
			}
			break;
	}
	return false;
}

/**
 * Determines the browserspecific command for the requested functionality.
 *
 * @param {string} command The platform indipenden command.
 * @returns {string|null} The platform dependend command.
 */
function getExecCommand(command) {
	switch (command) {
		case IndependendExecCommand.INSERT_HTML:
			if (isSupported(FeatureSupport.INSERT_HTML)) {
				return "insertHTML";
			}
			return "insertImage";
			break;
	}
	return null;
}

/**
 * Executes a specific command, that is platform dependent.
 *
 * @param {string} command The indipendent command.
 * @param {string} para The Parameter for the command.
 * @param {object} eventData The object representing the event.
 */
function execCommand(command, para, eventData) {
	switch (command) {
		case IndependendExecCommand.INSERT_HTML:
			insertHTML(para, eventData);
			break;
	}
}

/**
 * Insert HTML into the Editor. Supports all major browsers.
 *
 * @param {string} html The html to insert.
 * @param {object} eventData The object representing the event.
 */
function insertHTML(html, eventData) {
	if (isSupported(FeatureSupport.INSERT_HTML)) {
		eventData.editor.execCommand(eventData.command, html, eventData.useCSS, eventData.button);
	} else {
		console.log("IE hack");
		var tmpImage = "!!!tmp!!!";
		eventData.editor.execCommand(eventData.command, tmpImage, eventData.useCSS, eventData.button);
		eventData.editor.$frame.contents().find("img[src='" + tmpImage + "']").replaceWith(html);
	}
}