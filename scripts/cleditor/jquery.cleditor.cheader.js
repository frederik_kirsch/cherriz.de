//@ sourceURL=scripts/cleditor/jquery.cleditor.cheader.js

/**
 * Plugin zur Erzeugung einer Ueberschrift im CLEditor.
 * @author Frederik Kirsch
 */
(function ($) {

    // Define the hello button
    $.cleditor.buttons.cheader = {
        name: "cheader",
        image: "cheader.gif",
        title: "Unterüberschrift einfügen",
        command: "formatblock",
        buttonClick: makeHeader
    };

    var HEADER_FORMAT = "<h2>";

    var STANDARD_FORMAT = "<p>";

    function makeHeader(event, data) {
        var format = data.editor.doc.queryCommandValue(data.command);
        if (format != HEADER_FORMAT) {
            data.editor.execCommand(data.command, HEADER_FORMAT, data.useCSS, data.button);
        } else {
            data.editor.execCommand(data.command, STANDARD_FORMAT, data.useCSS, data.button);
        }
	    data.editor.updateTextArea();
        data.editor.focus();
        return false;
    }

})(jQuery);