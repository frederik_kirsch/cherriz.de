//@ sourceURL=scripts/cleditor/jquery.cleditor.cimagegal.js

/**
 * Plugin zum durchsuchen hochgeladener Grafiken.
 * @author Frederik Kirsch
 */
(function($) {

	var lastRequestData;

	var clickData;

	var dialog;

	/**
	 * Definiert den Button im WYSIWYG
	 * @type {{name: string, image: string, title: string, command: string, buttonClick: showGalleryDialog}}
	 */
	$.cleditor.buttons.cimagegal = {
		name: "cimagegal",
		image: "cimage_gal.gif",
		title: "Gallerie durchsuchen",
		command: "insertImage",
		buttonClick: showGalleryDialog
	};

	/**
	 * Zeigt den Uploaddialog.
	 * @param event Das Event.
	 * @param data Die Daten der Benutzeraktion.
	 */
	function showGalleryDialog(event, data) {
		var content = "<div class='meldungen_form_data'>\
					   <span class='meldungen_highlight'>Suche</span> <input id='search' type='text' size='27' /><br />\
					   <div id='meldungen_hits' style='padding-top: 5px'>Suche anhand von Schlagwörter nach entsprechenden Bildern.</div>";
		dialog = new Dialog("Gallerie durchsuchen", content);
		dialog.setProperties({buttons: {}, width: 500});
		dialog.open();

		clickData = data;
		$('#search').keyup(doRequest);
		return false;
	}

	var DELAY = 1000;

	var requestTime;

	/**
	 * Fuehrt den Aufruf durch.
	 * @param event Das ausloesende Event.
	 */
	function doRequest(event) {
		requestTime = new Date().getTime();
		setTimeout(function() {
			if (new Date().getTime() - DELAY > requestTime) {
				if(event == null){
					lastRequestData.start += 5;
				}else{
					lastRequestData = {
						search: event.currentTarget.value,
						start: 0,
						max: 5,
						orderBy: "relevance",
						sortOrder: "desc"
					};
				}
				$.ajax({
					url: 'sources/api/image/search_image.php',
					type: 'GET',
					data: lastRequestData,
					dataType: 'json',
					success: searchSuccessful,
					error: searchFailed,
					cache: false
				});
			}
		}, (DELAY + 50));
	}

	/**
	 * Verarbeitet die Ergebnisse einer erfolgreichen Suche.
	 * @param response Das Ergebnis des Servers.
	 */
	function searchSuccessful(response) {
		//Ergebnis validieren
		if (response.result.error !== undefined) {
			searchFailed(response.result.error);
			return;
		}

		var meldungen_hits = $("#meldungen_hits");
		meldungen_hits.empty();
		if (response.result.data.items.length == 0) {
			meldungen_hits.html("Keine Bilder gefunden!");
		}

		response.result.data.items.forEach(function(entry) {
			var content = "<img src='gfx/img_generator.php?type=Picture&id=" + entry.id + "&size=Small' style='float:right'/>\
						   <span class='meldungen_highlight'> Uploader:</span><a href='index.php?page=Profile&user=" + entry.userid + "' alt='Zum Profil von " + entry.usernick + "'>" + entry.usernick + "</a><br />\
						   <span class='meldungen_highlight'> Datum:</span>" + entry.date + "<br />\
						   <span class='meldungen_highlight'> Tags:</span>" + entry.tags;
			var div = $("<div>").css("cursor","pointer").addClass("meldungen_element_box").html(content).appendTo(meldungen_hits).click(function(){insertPicture(entry.id)});
		});

		if(response.result.data.items.length == 5){
			$("<span>").addClass("icon iconRefresh").html(" Weitere Einträge laden ...").appendTo(meldungen_hits).click(function(){doRequest(null)});
		}

		dialog.adjustPosition();
	}

	/**
	 * Verarbeitet Technische oder Fachliche Fehler bei ausfuehren der Suche.
	 * @param message Die Fehlermeldung.
	 */
	function searchFailed(message) {
		if (typeof message != "string") {
			message = "Vorgang fehlgeschlagen<br />Die Verbindung zum Server dauerte zu lange.";
		}
		$("#meldungen_hits").html("Fehler:<br />" + message).show();
	}

	/**
	 * Fuegt die Grafik ins WYSIWYG ein.
	 * @param pictureID ID der Grafik
	 */
	function insertPicture(pictureID) {
		dialog.close();
		var imgUrl = "gfx/img_generator.php?type=Picture&id=" + pictureID + "&size=Standard";
		clickData.editor.execCommand(clickData.command, imgUrl, clickData.useCSS, clickData.button);
		clickData.editor.focus();
	}

})(jQuery);