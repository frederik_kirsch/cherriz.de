//@ sourceURL=scripts/cleditor/jquery.cleditor.clink.js

/**
 * Plugin zur Linkerzeugung des CLEditors
 * @author Frederik Kirsch
 */
(function($) {

	var eventData;

	// Define the hello button
	$.cleditor.buttons.clink = {
		name: "clink",
		image: "clink.gif",
		title: "Link einfügen",
		command: "createLink",
		buttonClick: showLinkDialog
	};

	/**
	 * Oeffnet den Dialog zur Eingabe eines Links.
	 * @param event
	 * @param data
	 * @returns {boolean}
	 */
	function showLinkDialog(event, data) {
		var content = "<div class='meldungen_form_data'>\
                           <span class='meldungen_highlight'>Link:</span> <input name='url' type='text' size='27' />\
		               </div>";
		eventData = data;
		eventData.dialog = new Dialog("Link einfügen", content);
		eventData.dialog.setProperties({buttons: {"Einfügen": insertLink}, width: 350});
		eventData.dialog.open();

		eventData.dialog.disableButton("Einfügen", true);
		$("input[name=url]").first().keyup(handleLinkValidation);

		return false;
	}

	/**
	 * Fuegt den Link ein.
	 */
	function insertLink() {
		//Formulardaten abrufen
		var link = $("input[name=url]").first().val();
		if (link.substring(0, 4) !== "http") {
			link = "http://" + link;
		}
		var target = "_blank";

		//Dialog schließen
		eventData.dialog.close();

		//Link einfügen
		eventData.editor.execCommand(eventData.command, link, eventData.useCSS, eventData.button);
		eventData.editor.$frame.contents().find("a[href='" + link + "']").attr('target', target);
		eventData.editor.updateTextArea();
		eventData.editor.focus();
	}

	function handleLinkValidation() {
		eventData.dialog.disableButton("Einfügen", $(this).val().length == 0);
	}

})(jQuery);