//@ sourceURL=scripts/cleditor/jquery.cleditor.ccode.js

/**
 * Plugin zur Codeformatierung im CLEditors.
 *
 * @author Frederik Kirsch
 */
(function($) {

	// Define the hello button
	$.cleditor.buttons.ccode = {
		name: "ccode",
		image: "ccode.gif",
		title: "Code formatieren",
		command: getExecCommand(IndependendExecCommand.INSERT_HTML),
		buttonClick: handleFormat
	};

	var languages = {
		"Apache": "apache",
		"Bash": "bash",
		"C#": "cs",
		"C++": "cpp",
		"Clojure": "clojure",
		"CSS": "css",
		"Dart": "dart",
		"Diff": "diff",
		"Gradle": "gradle",
		"HTTP": "http",
		"INI": "ini",
		"Java": "java",
		"Javascript": "javascript",
		"JSON": "json",
		"Makefile": "makefile",
		"Markdown": "markdown",
		"Objective C": "objectivec",
		"Perl": "perl",
		"PHP": "php",
		"Python": "python",
		"Ruby": "ruby",
		"Scala": "scala",
		"SQL": "sql",
		"XML": "xml"
	};

	/**
	 * Formatiert einen ausgewaehlten Text als Codeblock oder entfernt ihn.
	 *
	 * @param {object} event Die ausloesende Aktion.
	 * @param {object} data Das Datenmodell des Editors.
	 * @returns {boolean} Liefert immer <code>false</code> um die Folgeverarbeitung zu unterbinden.
	 */
	function handleFormat(event, data) {
		if (containsCode(data)) {
			removeCodeFormat(data);
		} else {
			showLanguageDialog(data);
		}
		return false;
	}

	/**
	 * Gibt an ob ein Textblock bereits formatierten Code enthaelt.
	 *
	 * @param {object} data Das Datenmodel des Editors
	 * @returns {boolean} <code>true</code> wenn er Textblock Code enthaelt, sonst <code>false</code>.
	 */
	function containsCode(data) {
		var node;
		if (isSupported(FeatureSupport.SELECTION)) {
			node = data.editor.doc.getSelection().anchorNode;
		} else {
			var sel = data.editor.doc.getSelection();
			if (sel.rangeCount == 1) {
				node = sel.getRangeAt(0).startContainer.parentNode;
			}
		}
		if (node) {
			while (node) {
				if (node.tagName && node.tagName.toLowerCase() == "pre") {
					return true;
				}
				node = node.parentNode;
			}
		}
		return false;
	}

	/**
	 * Entfernt den Codeblock von der Textauswahl.
	 *
	 * @param {object} data Das Datenmodel des Editors
	 */
	function removeCodeFormat(data) {
		var sel = data.editor.doc.getSelection();

		var node = sel.anchorNode;
		var code, surrounding;
		while (node) {
			if (node.tagName && node.tagName.toLowerCase() == "pre") {
				surrounding = node;
			} else if (node.tagName && node.tagName.toLowerCase() == "code") {
				surrounding = node.parentNode;
			}
			if (surrounding) {
				if (node.innerText && isSupported(FeatureSupport.SELECTION)) {
					code = node.innerText;
				} else {
					code = node.innerHTML;
				}
				break;
			}
			node = node.parentNode;
		}

		$(surrounding).replaceWith(convertCodeToHTML(code));
		data.editor.updateTextArea();
		data.editor.focus();
	}

	/**
	 * Stellt den Dialog zur Sprachauswahl fuer einen Codeblock dar.
	 *
	 * @param {object} data Das Datenmodell des Editors.
	 * @returns {boolean} Immer <code>false</code> um die Folgeverarbeitung abzubrechen.
	 */
	function showLanguageDialog(data) {
		var selections = "";
		$.each(languages, function(value, key) {
			selections += "<option value='" + key + "'>" + value + "</option>";
		});

		var content = "<div class='meldungen_form_data'>\
						   <span class='meldungen_highlight'>Sprache:</span> \
						   <select name='language'>" + selections + "</select>\
					   </div>";

		var dialog = new Dialog("Sprache wählen", content);
		dialog.setProperties({
			buttons: {
				"Formatieren": function() {
					addCodeFormat(data);
					dialog.close();
				}
			},
			width: 350
		});
		dialog.open();
		return false;
	}

	/**
	 * Fuegt einen Codeblock um den ausgewaehlten Text hinzu.
	 *
	 * @param {object} data Das Datenmodell des Editors.
	 */
	function addCodeFormat(data) {
		var language = $("select[name=language]").first().val();
		var code;
		if (isSupported(FeatureSupport.SELECTION)) {
			code = data.editor.selectedText();
		} else {
			code = selectedTextWithLineBreaks(data.editor);
		}

		execCommand(IndependendExecCommand.INSERT_HTML, "<pre><code class=\"" + language + "\"><span id=\"replacecode\"></span></pre>", data);
		var div = $(data.editor.doc).find("#replacecode").parent();
		div.removeAttr("id");
		div.text(code);
		data.editor.updateTextArea();
		data.editor.focus();
	}

	/**
	 * Reads the text from the Editor with linebreaks.
	 *
	 * @param {object} editor The Editor.
	 * @returns {string} The extracted Text with linebreaks.
	 */
	function selectedTextWithLineBreaks(editor) {
		var code = editor.selectedHTML();
		$.each([[/<\/p><p>/g, "\n\n"],
				[/<p>/g, ""],
				[/<\/p>/g, ""],
				[/<br>/g, "\n"],
				[/&lt;/g, "<"],
				[/&gt;/g, ">"],
				[/&quot;/g, "\""],
				[/&nbsp;/g, " "]], function(index, item) {
			code = code.replace(item[0], item[1]);
		});
		return code;
	}

})(jQuery);