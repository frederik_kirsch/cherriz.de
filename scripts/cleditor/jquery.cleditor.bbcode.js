//@ sourceURL=scripts/cleditor/jquery.cleditor.bbcode.js
/**
 @preserve CLEditor BBCode Plugin v1.0.0
 http://premiumsoftware.net/cleditor
 requires CLEditor v1.3.0 or later

 Copyright 2010, Chris Landowski, Premium Software, LLC
 Dual licensed under the MIT or GPL Version 2 licenses.
 */
(function ($) {
    // Save the previously assigned callback handlers
    var oldAreaCallback = $.cleditor.defaultOptions.updateTextArea;
    var oldFrameCallback = $.cleditor.defaultOptions.updateFrame;

    // Wireup the updateTextArea callback handler
    $.cleditor.defaultOptions.updateTextArea = function (html) {
        // Fire the previously assigned callback handler
        if (oldAreaCallback) {
            html = oldAreaCallback(html);
        }

        // Convert the HTML to BBCode
        return $.cleditor.convertHTMLtoBBCode(html);
    };

    // Wireup the updateFrame callback handler
    $.cleditor.defaultOptions.updateFrame = function (code) {
        // Fire the previously assigned callback handler
        if (oldFrameCallback) {
            code = oldFrameCallback(code);
        }

        // Convert the BBCode to HTML
        return $.cleditor.convertBBCodeToHTML(code);
    };

    // Expose the convertHTMLtoBBCode method
    $.cleditor.convertHTMLtoBBCode = function (html) {
        html = " " + html + " ";

        // Codebloecke vor konvertierung zwischenspeichern
        var matches =  html.match(/<pre><code class=".*?">([\S\s.]*?)<\/code><\/pre>/gi);

        // Allgemeine Konvertierung
        $.each([
			//Vorverarbeitung
			[/(<div>)(<[a-zA-Z/]*>)+(<\/div>)/gi, "[br]"],
			[/<br.*?>/gi, "[br]"],
			[/<p>(.*?)<\/p>/gi, "$1"],
			//BBElemente
			[/<b>(.*?)<\/b>/gi, "[b]$1[/b]"],
			[/<strong>(.*?)<\/strong>/gi, "[b]$1[/b]"],
			[/<i>(.*?)<\/i>/gi, "[i]$1[/i]"],
			[/<em>(.*?)<\/em>/gi, "[i]$1[/i]"],
			[/<u>(.*?)<\/u>/gi, "[u]$1[/u]"],
			[/<ins>(.*?)<\/ins>/gi, "[u]$1[/u]"],
			[/<strike>(.*?)<\/strike>/gi, "[s]$1[/s]"],
			[/<del>(.*?)<\/del>/gi, "[s]$1[/s]"],
			[/<a href="(.*?)".*?target="(.*?)".*?>(.*?)<\/a>/gi, "[url=$1|$2]$3[/url]"],
			[/<a target="(.*?)".*?href="(.*?)".*?>(.*?)<\/a>/gi, "[url=$2|$1]$3[/url]"],
			[/<img.*?src="gfx\/img_generator\.php\?type=Picture&amp;id=(\d*?)&amp;size=Standard".*?>/gi, "[img]$1[/img]"],
			[/<img.*?src="(.*?)".*?>/gi, "[img]$1[/img]"],
			[/<iframe.*?src=".*?youtube.com\/embed\/([\w-]*)".*?>.*?<\/iframe>/gi, "[video=youtube]$1[/video]"],
	        [/<iframe.*?src=".*?youtube.com\/embed\/([\w-]*)".*? \/>/gi, "[video=youtube]$1[/video]"],
			[/<ul>/gi, "[list=0]"],
			[/<\/ul>/gi, "[/list]"],
			[/<ol>/gi, "[list=1]"],
			[/<\/ol>/gi, "[/list]"],
			[/<li[^>]*>/gi, "[*]"],
			[/<\/li>/gi, "[/*]"],
			//Erweiterungen
			[/<h2>(.*?)<\/h2>/gi, "[head]$1[/head]"],
			[/<h2[^>]*align[^>]*(right|center)[^>]*>(.*?)<\/h2>/gi, "[$1][head]$2[/head][/$1]"],
			[/<div[^>]*align[^>]*(right|center)[^>]*>(.*?)<\/div>/gi, "[$1]$2[/$1]"],
			[/<p[^>]*align[^>]*(right|center)[^>]*>(.*?)<\/p>/gi, "[$1]$2[/$1]"],
			[/<div.*?>(.*?)<\/div>/gi, "[br]$1"],
			[/<pre><code class="(.*?)">[\S\s.]*?<\/code><\/pre>/gi, "[code=$1]rplc[/code]"],
			[/<.*?>/gi, ""],
			//Nachverarbeitung
			[/\[\/center]\[br]/gi, "[/center]"],
			[/\[\/right]\[br]/gi, "[/right]"],
			[/\[\/head]\[br]/gi, "[/head]"],
			[/\[\/code]\[br]/gi, "[/code]"],
			[/\[br] &/gi, " "], // Leerzeichen notwendig, da oben angefuegt
			[/^ \[br]/gi, " "] // Leerzeichen notwendig, da oben angefuegt
        ], function (index, item) {
            html = html.replace(item[0], item[1]);
        });

		//Codebloecke wieder einfuegen
	    if(matches){
		    for(var i = 0;i<matches.length;i++){
			    html = html.replace("rplc", convertEntitys(matches[i]));
		    }
	    }

        return $.trim(html);
    };

	/**
	 * Uebersetzt einen in HTML dargestellten Quellcode so, das er als BBCode normal dargestellt werden kann.
	 *
	 * @param text Der HTML-Text.
	 * @returns string Der konvertierte Text.
	 */
	function convertEntitys(text){
		$.each([
			[/&nbsp;/g, " "],
			[/&lt;/g, "<"],
			[/&gt;/g, ">"],
			[/&auml;/g, "ä"],
			[/&uuml;/g, "ü"],
			[/&ouml;/g, "ö"],
			[/&Auml;/g, "Ä"],
			[/&Uuml;/g, "Ü"],
			[/&Ouml;/g, "Ö"],
			[/&szlig;/g, "ß"],
			[/&quot;/g, "\""],
			[/&amp;/g, "&"],
			[/&apos;/g, "'"],
			[/<pre><code class=".*?">([\S\s.]*?)<\/code><\/pre>/i, "$1"]], function (index, item) {
			text = text.replace(item[0], item[1]);
		});
		return text;
	}

	/**
	 * Uebersetzt einen in BB-Code dargestellten Quellcode so, das er in HTML korrekt dargestellt wird.
	 *
	 * @param text Der BB-Code.
	 * @returns Der konvertierte Text.
	 */
	function escapeEntitys(text){
		$.each([
			[/</g, "&lt;"],
			[/>/g, "&gt;"],
			[/"/g, "&quot;"],
			[/\[code=.*?]([\S\s.]*?)\[\/code]/i, "$1"]], function (index, item) {
			text = text.replace(item[0], item[1]);
		});
		return text;
	}

    // Expose the convertBBCodeToHTML method
    $.cleditor.convertBBCodeToHTML = function (code) {
	    // Codebloecke vor konvertierung zwischenspeichern
	    var matches =  code.match(/\[code=.+?]([\S\s.]*?)\[\/code]/gi);

        $.each([
            [/\[br]/g, "<br />"],
            [/\[b](.*?)\[\/b]/gi, "<b>$1</b>"],
            [/\[i](.*?)\[\/i]/gi, "<i>$1</i>"],
            [/\[u](.*?)\[\/u]/gi, "<u>$1</u>"],
            [/\[s](.*?)\[\/s]/gi, "<strike>$1</strike>"],
            [/\[url=(http:\/\/|https:\/\/|mailto:|.{0})(.*?)\|(.*?)](.*?)\[\/url]/gi, "<a href=\"$1$2\" target=\"$3\">$4</a>"],
            [/\[img](\d*?)\[\/img]/gi, "<img src=\"gfx/img_generator.php?type=Picture&amp;id=$1&amp;size=Standard\">"],
            [/\[img](.*?)\[\/img]/gi, "<img src=\"$1\">"],
            [/\[video=youtube](.*?)\[\/video]/gi, "<iframe src=\"https:\/\/www.youtube.com/embed/$1\" allowfullscreen />"],
            [/\[list=0](.*?)\[\/list]/gi, "<ul>$1</ul>"],
            [/\[list=1](.*?)\[\/list]/gi, "<ol>$1</ol>"],
            [/\[\*](.*?)\[\/\*]/gi, "<li>$1</li>"],
            //Erweiterungen
            [/\[head](.*?)\[\/head]/gi, "<h2>$1</h2>"],
            [/\[right](.*?)\[\/right]/gi, "<div style=\"text-align: right;\">$1</div>"],
            [/\[center](.*?)\[\/center]/gi, "<div style=\"text-align: center;\">$1</div>"],
            [/\[left](.*?)\[\/left]/gi, "$1"],
            [/\[code=(.*?)][\S\s.]*?\[\/code]/gi, "<pre><code class=\"$1\">rplc</code></pre>"]
            //Erweiterungen ENDE
        ], function (index, item) {
            code = code.replace(item[0], item[1]);
        });

	    //Codebloecke wieder einfuegen
	    if(matches){
		    for(var i = 0;i<matches.length;i++){
			    code = code.replace("rplc", escapeEntitys(matches[i]));
		    }
	    }

        return code;
    };

})(jQuery);