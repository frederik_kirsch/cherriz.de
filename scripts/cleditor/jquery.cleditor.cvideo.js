//@ sourceURL=scripts/cleditor/jquery.cleditor.cvideo.js

/**
 * Plugin fuer den CLEditor. Dient dem einfuegen von Videos.
 * Unterstuetzte Platformen:
 * <ul>
 *     <li>YouTube</li>
 * </ul>
 * @author Frederik Kirsch
 */
(function($) {

	$.cleditor.buttons.cvideo = {
		name: "cvideo",
		image: "cvideo.gif",
		title: "Video einfügen",
		command: getExecCommand(IndependendExecCommand.INSERT_HTML),
		buttonClick: showVideoDialog
	};

	var ytRegex = /youtube\.com\/watch\?.*v=([\w_-]+)/;

	var eventData;

	/**
	 * Stellt den Dialog zur Eingabe einer VideoURL dar.
	 * @param event
	 * @param data
	 * @returns {boolean}
	 */
	function showVideoDialog(event, data) {
		var content = "<div class='meldungen_form_data'>\
						<span class='meldungen_highlight'>URL:</span><input name='url' type='text' size='27' />\
					   </div>\
					   <div class='meldungen_form_data'>\
					    <span class='meldungen_highlight'>Typ:</span><input type='radio' name='type' value='youtube' checked> Youtube\
					   </div>";
		eventData = data;
		eventData.dialog = new Dialog("Video einfügen", content);
		eventData.dialog.setProperties({
			buttons: {"Einfügen": insertVideo},
			width: 350
		});
		eventData.dialog.open();

		eventData.dialog.disableButton("Einfügen", true);
		$("input[name=url]").first().keyup(handleVideoURLValidation);

		// Direkte Weiterverarbeitung zunaechst deaktivieren
		return false;
	}

	/**
	 * Validiert die Video URL.
	 * Valider Link: https://www.youtube.com/watch?v=xkavLcAUTkg
	 */
	function handleVideoURLValidation() {
		var type = $("input[name='type']:checked").val();
		var result = null;

		switch (type) {
			case "youtube":
				result = $(this).val().match(ytRegex);
				break;
		}

		eventData.dialog.disableButton("Einfügen", result == null);
	}

	/**
	 * Fuegt ein Video ein.
	 */
	function insertVideo() {
		//Formulardaten abrufen
		var type = $("input[name='type']:checked").val();
		var link = $("input[name=url]").first().val();
		var id = null;

		switch (type) {
			case "youtube":
				id = link.match(ytRegex)[1];
				break;
		}

		//Dialog schließen
		eventData.dialog.close();

		//Video einfügen
		var html = "<iframe src='https://www.youtube.com/embed/" + id + "' allowfullscreen />";
		execCommand(IndependendExecCommand.INSERT_HTML, html, eventData);
		eventData.editor.updateTextArea();
		eventData.editor.focus();
	}

})(jQuery);