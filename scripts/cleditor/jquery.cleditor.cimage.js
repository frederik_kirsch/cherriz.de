//@ sourceURL=scripts/cleditor/jquery.cleditor.cimage.js

/**
 * Plugin zum hochladen von Grafiken.
 * @author Frederik Kirsch
 */
(function($) {

	/**
	 * Definiert den Button im WYSIWYG.
	 * @type {{name: string, image: string, title: string, command: string, buttonClick: showImageUploadDialog}}
	 */
	$.cleditor.buttons.cimage = {
		name: "cimage",
		image: "cimage.gif",
		title: "Bild einfügen",
		command: "insertImage",
		buttonClick: showImageUploadDialog
	};

	var clickData = null;

	var dialog = null;

	/**
	 * Zeigt den Uploaddialog.
	 * @param event Das Event.
	 * @param data Die Daten der Nutzeraktion.
	 */
	function showImageUploadDialog(event, data) {
		clickData = data;

		var content = "<form enctype='multipart/form-data' id='img_upload'>\
							<div class='meldungen_form_data'>\
								<span  class='meldungen_highlight'>Bild:</span><input name='picture' type='file' size='27'><br />\
								<span class='meldungen_highlight'>Tags:</span><input name='tags' type='text' title='z.B. \"Party, Freunde, Grillen\"' size='27' /><br />\
								<span class='meldungen_highlight'>Sichtbarkeit:</span>\
								<span title='Jeder darf diesen Beitrag sehen.'><input type='radio' name='visibility' value='Public' checked/> Öffentlich</span> \
								<span title='Der Beitrag ist für meine Gruppen sichtbar.'><input type='radio' name='visibility' value='Umfeld'/> Mein Umfeld</span> \
								<span title='Der Beitrag ist für spezifische Gruppen sichtbar. Mehrfachauswahl über Strg + Click.'><input type='radio' name='visibility' value='Spezifisch'/> Benutzerdefiniert</span><br />\
								<ol id='lst' class='dialog_group' style='display: none; padding-left: 105px'></ol>\
								<input id='dialog_groups' name='groups' type='text' style='display: none' />\
							</div>\
						</form>";

		dialog = new Dialog("Bild einfügen", content);
		dialog.setProperties({
			buttons: {
				"Einfügen": function() {
					$("#img_upload").submit();
					setLoading();
				}
			},
			width: 500
		});
		dialog.open();

		$("[name=picture]").val("");
		$("#img_upload").ajaxForm({
			url: 'sources/api/image/upload_image.php',
			type: 'POST',
			dataType: 'json',
			data: {action: "upload"},
			success: uploadSuccessful,
			error: uploadFailed
		}).submit(setLoading);

		$(".meldungen_form_data").find("[name=visibility]").click(function() {
			if ($(this).val() == "Spezifisch") {
				$("#lst").show("slow");
			} else {
				$("#lst").hide("slow");
			}
		});

		$.ajax({
			url: 'sources/api/user/user_operation.php',
			type: 'POST',
			data: {
				'action': 'getGroups',
				'user': STORE.get("UserID")
			},
			dataType: 'json',
			success: setGroups,
			error: errorLoadGroups
		});
		return false;
	}

	function setGroups(data) {
		var groupArr = data.result.data.groups;
		var groups = "";
		for (var i = 0; i < groupArr.length; i++) {
			groups += "<li class='ui-state-default'><img src='gfx/img_generator.php?type=Gruppe&id=" + groupArr[i].id + "&size=Small' /> " + groupArr[i].name + "</li>";
		}
		var ol = $("#lst");
		ol.html(groups);

		ol.selectable({
			stop: function() {
				var selected = [];
				$("#lst").find(".ui-selected", this).each(function() {
					for (var i = 0; i < groupArr.length; i++) {
						if (groupArr[i].name == $(this).text().trim()) {
							selected.push(groupArr[i].id);
							break;
						}
					}
				});
				$("#dialog_groups").val(selected.join(","));
			}
		});
	}

	function errorLoadGroups() {
		$("#lst").html("Gruppen konnten nicht geladen werden.")
	}

	/**
	 * Stellt die Ladeanimation dar.
	 */
	function setLoading() {
		dialog.setContent("Laden...<br />Bitte warten<br /><img src='gfx/ladebalken.gif' alt='Ladebalken'>");
		dialog.setToClosableOnly();
	}

	/**
	 * Verarbeitet das erfolgreiche laden.
	 * @param response Ergebnis des Servers.
	 */
	function uploadSuccessful(response) {
		//Ergebnis validieren
		if (response.result.error !== undefined) {
			uploadFailed(response.result.error);
			return;
		}

		dialog.close();
		var imgUrl = "gfx/img_generator.php?type=Picture&id=" + response.result.data.insert_id + "&size=Standard";
		clickData.editor.execCommand(clickData.command, imgUrl, clickData.useCSS, clickData.button);
		clickData.editor.focus();
	}

	/**
	 * Verarbeitet einen Fehler beim Laden.
	 * @param message Die Fehlermeldung
	 */
	function uploadFailed(message) {
		if (typeof message != "string") {
			message = "Vorgang fehlgeschlagen<br />Die Verbindung zum Server dauerte zu lange.";
		}
		dialog.setTitle("Fehler");
		dialog.setContent(message);
	}

})(jQuery);