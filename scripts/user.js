//@ sourceURL=user.js

/**
 * Repraesentiert die Entitaet eines Benutzers.
 */
var USER = {

	retFunction: null,

	/**
	 * @type {string}
	 */
	type: null,

	URL_UPDATE: "sources/api/user/user_operation.php",

	/**
	 * Definiert die Rueckgabefunktion durchgefuehrten asynchroner Aktivitaeten.
	 * @param func Die Rueckgabefunktion.
	 */
	setReturnFunction: function(func) {
		this.retFunction = func;
	},

	setAvatar: function(avatar) {
		this.type = "Avatar";
		avatar.ajaxForm({
			url: USER.URL_UPDATE,
			type: 'POST',
			data: {
				'action': 'change' + USER.type,
				'user': STORE.get("UserID")
			},
			dataType: 'json',
			success: USER.validationSuccess,
			error: USER.validationFailure
		}).submit();
	},

	setSignature: function(signature) {
		this.type = "Signatur";
		this.performRequest(signature);
	},

	setName: function(name) {
		this.type = "Name";
		this.performRequest(name);
	},

	setGeburtstag: function(geburtsdatum) {
		this.type = "Geburtsdatum";
		this.performRequest(geburtsdatum);
	},

	setEmail: function(email) {
		this.type = "Email";
		this.performRequest(email);
	},

	setTwitter: function(twitter) {
		this.type = "Twitter";
		this.performRequest(twitter);
	},

	setTelefon: function(telefon) {
		this.type = "Telefon";
		this.performRequest(telefon);
	},

	setHandy: function(handx) {
		this.type = "Handy";
		this.performRequest(handx);
	},

	setStrasse: function(strasse) {
		this.type = "Strasse";
		this.performRequest(strasse);
	},

	setOrt: function(stadt) {
		this.type = "Ort";
		this.performRequest(stadt);
	},

	setPrivatsphaere: function(privatsphaere) {
		this.type = "Privatsphaere";
		this.performRequest(privatsphaere);
	},

	setUseExperimental: function(experimental) {
		this.type = "UseExperimental";
		this.performRequest(experimental);
	},

	setNotificationTypeEmail: function(type) {
		this.type = "NotificationTypeEmail";
		this.performRequest(type);
	},

	setNotificationTypeTwitter: function(type) {
		this.type = "NotificationTypeTwitter";
		this.performRequest(type);
	},

	setNotificationNews: function(notification) {
		this.type = "NotificationNews";
		this.performRequest(notification);
	},

	setNotificationComment: function(notification) {
		this.type = "NotificationComment";
		this.performRequest(notification);
	},

	setNotificationThreadpost: function(notification) {
		this.type = "NotificationThreadpost";
		this.performRequest(notification);
	},

	/**
	 * Fuehrt die eigentliche Anfrage an den Server durch.
	 * @param value an den Server uebertragener Wert.
	 */
	performRequest: function(value) {
		$.ajax({
			url: USER.URL_UPDATE,
			type: 'POST',
			data: {
				'action': 'change' + USER.type,
				'user': STORE.get("UserID"),
				'value': value
			},
			dataType: 'json',
			success: USER.validationSuccess,
			error: USER.validationFailure
		});
	},

	/**
	 * Verarbeitet Erfolgreiche Antworten des Servers.
	 * @param response Ausgabe des Servers.
	 */
	validationSuccess: function(response) {
		if (response.result.error !== undefined) {
			USER.validationFailure(response.result.error);
			return;
		}
		if (USER.retFunction == null) {
			USER.validationFailure("Keine Rückgabefunktion definiert");
			return;
		}

		//Verarbeitung
		USER.retFunction(USER.type, "success", response.result.query.value, response.result.data.info);
	},

	/**
	 * Verarbeitet ein Problem bei der asynchronen Serverkommunikation.
	 * @param meldung Ausgabe des Servers, falls vorhanden.
	 */
	validationFailure: function(meldung) {
		if (typeof meldung != "string") {
			meldung = "Fehler bei der Kommunikation mit dem Webserver.";
		}
		new InfoDialog("Fehler", meldung).open();

		USER.retFunction(USER.type, "failure");
	}

};