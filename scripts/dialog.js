//# sourceURL=dialog.js

/**
 * Stellt einen einfachen Dialog dar.
 * @param title Der Titel.
 * @param content Der Inhalt.
 * @constructor
 */
function Dialog(title, content) {

	var properties = {
		resizable: false,
		width: 600,
		modal: true,
		buttons: {
			"Ok": function() {
				$(this).dialog("close");
			}
		},
		close: function() {uiDialog.remove()}
	};

	var uiDialog = null;

	var dialogTitle = title;

	var dialogContent = content;

	/**
	 * Erlaubt das konfigurieren spezifischer Eigenschaften des Dialogs.
	 * @param specificProps Property Objekt.
	 */
	this.setProperties = function(specificProps) {
		//Dialogeigenschaften setzen
		for (var attrname in specificProps) {
			properties[attrname] = specificProps[attrname];
		}
	};

	/**
	 * Zentriert den Dialog vertikal.
	 */
	this.adjustPosition = function(){
		var dialog = $(".ui-dialog");

		// Groesse ermitteln
		var height = dialog.height();
		var winHeight = $(window).height();
		var winTop = $(window).scrollTop();

		dialog.css("top", winTop + (winHeight-height)/2);
	};

	/**
	 * Oeffnet den Dialog.
	 */
	this.open = function() {
		// Dialog Container anlegen
		uiDialog = $("<div>");
		uiDialog.addClass("meldungen");
		$("#publish").append(uiDialog);

		//Inhalt setzen
		this.setTitle(dialogTitle);
		this.setContent(dialogContent);

		//Dialog aufrufen
		uiDialog.dialog(properties);
	};

	this.setTitle = function(title) {
		dialogTitle = title;
		if (uiDialog != null) {
			uiDialog.attr("title", dialogTitle);
		}
	};

	/**
	 * Aendert den Inhalt des Dialoges.
	 * @param content Der neue Inhalt.
	 */
	this.setContent = function(content) {
		dialogContent = content;
		if (uiDialog != null) {
			uiDialog.html(dialogContent);
		}
	};

	/**
	 * Entfernt die Buttons des Dialogs und sorgt dadurch dafuer das dieser vom Benutzer nur noch geschlossen werden kann.
	 */
	this.setToClosableOnly = function() {
		uiDialog.parent().find(".ui-dialog-buttonpane").hide();
	};

	/**
	 * (De)Aktiviert einen bestimmten Button.
	 * @param button Der Text des betreffenden Buttons.
	 * @param disabled <tt>true</tt> wenn der Button deaktiviert werden soll, sonst <tt>false</tt>.
	 */
	this.disableButton = function(button, disabled) {
		$(".ui-dialog-buttonset").find("button:contains('" + button + "')").attr("disabled", disabled)
	};

	/**
	 * Schliest den Dialog.
	 */
	this.close = function() {
		uiDialog.dialog("close");
		uiDialog.remove();
	};

}

/**
 * Einfacher Dialog zur Darstellung einer Meldung.
 * @param title Der Titel
 * @param text Der Meldungstext.
 * @constructor
 */
function InfoDialog(title, text) {

	var dialog = new Dialog(title, "<div class='meldungen_form_data'>" + text + "</div>");

	/**
	 * Oeffnet den Dialog.
	 */
	this.open = function() {
		dialog.open();
	};

}

/**
 * Bestaetigungsdialog.
 * @param title Der Dialogtitel.
 * @param text Der Text.
 * @param confirmFunc Die Funktion die bei Bestaetigung aufgerufen wird.
 * @constructor
 */
function ConfirmationDialog(title, text, confirmFunc) {

	var dialog = new Dialog(title, "<div class='meldungen_form_data'>" + text + "</div>");
	dialog.setProperties({
		buttons: {
			"Ja": function() {
				$(this).dialog("close");
				confirmFunc()
			},
			"Nein": function() {$(this).dialog("close")}
		}
	});

	/**
	 * Oeffnet den Dialog.
	 */
	this.open = function() {
		dialog.open();
	};

}

/**
 * Oeffnet ein Dialog zur Suche nach Nutzern.
 * @param callBack Rueckgabefunktion für den gewaehlten Eintrag. Uebergibt ein User-Objekt.
 * @constructor
 */
function UserSearchDialog(callBack) {

	/**
	 * Definiert die Zeitverzögerung für die Suche
	 */
	const SEARCH_DELAY = 1000;

	var searchRequestTime;

	var content = "<div id='search_form'>\
						<div class='meldungen_form_data'>\
							Suche nach einem Nutzer.<br />\
							<input id='search' type='text' size='27' />\
						</div>\
						<div id='meldungen_hits'></div>\
					</div>";
	var dialog = new Dialog("Nutzer suchen", content);
	dialog.setProperties({buttons: {}, width: 300});

	/**
	 * Oeffnet den Dialog.
	 */
	this.open = function() {
		dialog.open();
		$('#search').keyup(search);
	};

	/**
	 * Führt die Ajax Suche gegen den Server aus.
	 * @param {Object} event Das Event mit den Daten zur Suche.
	 */
	function search(event) {
		var searchVal = event.currentTarget.value;
		if(searchVal.trim().length>2){
			searchRequestTime = new Date().getTime();
			setTimeout(function(successFunc, failFunc) {
				return function() {
					if (new Date().getTime() - SEARCH_DELAY > searchRequestTime)
						$.ajax({
							url: 'sources/api/user/search.php',
							type: 'GET',
							data: {
								search: searchVal,
								type: "User",
								anz: 5
							},
							dataType: 'json',
							success: successFunc,
							error: failFunc
						})
				};
			}(searchSuccessful, searchFailed), (SEARCH_DELAY + 50));
		}else{
			$("#meldungen_hits").empty();
		}
	}

	/**
	 * Verarbeitet das Suchergebnis.
	 * @param {Object} response Das Suchergebnis in XML Form.
	 * @namespace response.result.error
	 * @namespace response.result.data.items.id
	 * @namespace response.result.data.items.nick
	 * @namespace response.result.data.items.vorname
	 * @namespace response.result.data.items.nachname
	 */
	function searchSuccessful(response) {
		if (response.result.error !== undefined) {
			searchFailed(response.result.error);
			return;
		}
		var hitDiv = $("#meldungen_hits");
		hitDiv.empty();
		if (response.result.data.items.length == 0) {
			hitDiv.html("Keine Treffer!");
		}

		//Verarbeitung von Usern
		response.result.data.items.forEach(function(entry) {
			var usr = new User(entry.id, entry.nick, entry.vorname, entry.nachname);
			var hit = $("<div>").addClass("meldungen_element_box");
			hit.html("<img src='gfx/img_generator.php?type=User&id=" + usr.getID() + "&size=Small' style='float:left'/> <a href='index.php?page=Profile&User=" + usr.getID() + "' alt='Zum Profil von " + usr.getNick() + "'>" + usr.getNick() + "</a> " + usr.getName());
			hitDiv.append(hit);
			hit.click(function() {
				dialog.close();
				callBack(usr);
			});
		});
	}

	/**
	 * Gibt eine Fehlermeldung aus, falls die Suche nicht funktioniert.
	 * @param {Object} message Die Fehlermeldung.
	 */
	function searchFailed(message) {
		if (typeof message != "string") {
			message = "Vorgang fehlgeschlagen<br />Die Verbindung zum Server dauerte zu lange.";
		}
		$("#meldungen_hits").html("Fehler:<br />" + message);
	}

}

/**
 * Pojo zu einem Nutzer.
 * @param id ID des Nutzers.
 * @param nick Nich des Nutzers.
 * @param vorname Vorname des Nutzers.
 * @param nachname Nachname des Nutzers.
 * @constructor
 */
function User(id, nick, vorname, nachname) {

	this.getID = function() {
		return id;
	};

	this.getNick = function() {
		return nick;
	};

	this.getName = function() {
		if (nachname != "" && vorname != "") {
			return nachname + ", " + vorname;
		}
		return nachname + vorname;
	};

	this.getVorname = function() {
		return vorname;
	};

	this.getNachname = function() {
		return nachname;
	};

}