//# sourceURL=sites/news_content_loader.js

function initControl(userID) {
	var blockSize = 2;
	var idURL = "/sources/api/news/search_news.php?author=" + userID;
	var contentURL = "/sources/api/load_content.php";
	var displayType = new FixDisplayType("newsControl");

	var radioBtns = $('input[type=radio][name=type]');
	radioBtns.change(function() {
		var addition = "";
		if(this.value == "open"){
			addition = "&released=notYet";
		}else if(this.value == "entwurf") {
			addition = "&entwurf=yes";
		}
		$(".news_control_item").parent().remove();
		var contentLoader = new ContentLoader(blockSize, idURL+addition, contentURL, displayType, null);
		var appender = new BottomAppender();
		var trigger = new ScrollTrigger(0.95, 1000, contentLoader, appender, null);
		trigger.addToPage();
	});
	radioBtns.first().change();
}

function initOverview(genre) {
	var blockSize = 2;
	var idURL = "/sources/api/news/search_news.php?genre=" + genre;
	var contentURL = "/sources/api/load_content.php";
	var displayType = new FixDisplayType("news");
	var contentLoader = new ContentLoader(blockSize, idURL, contentURL, displayType, null);
	var appender = new BottomAppender();
	var trigger = new ScrollTrigger(0.95, 1000, contentLoader, appender, null);
	trigger.addToPage();
}