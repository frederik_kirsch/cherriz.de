//# sourceURL=sites/admin_user.js

function AdminUserControl(user) {

	var userDiv = $("#userDiv");

	var allRights = null;

	var userRights = null;

	init();

	function init() {
		userDiv.html("<h3>Berechtigungen verwalten</h3>\
					  Berechtigungen für <b>" + user.getNick() + "</b> anpassen.\
					  <div class='description_box'></div>");

		// Verfuegbare Rechte laden
		$.ajax({
			url: 'sources/api/settings/settings_operation.php',
			type: 'GET',
			data: {
				'action': 'getAllRights'
			},
			dataType: 'json',
			success: function(response) {
				allRights = response.result.data.rechte;
				drawControls()
			},
			error: printError
		});

		// Rechte des Benutzers laden
		$.ajax({
			url: "sources/api/user/user_operation.php",
			type: 'GET',
			data: {
				'action': 'getRights',
				'user': user.getID()
			},
			dataType: 'json',
			success: function(response) {
				userRights = response.result.data.rechte;
				drawControls()
			},
			error: printError
		});
	}

	function drawControls() {
		// Warten bis alle Rechte geladen wurden.
		if (allRights == null || userRights == null) {
			return;
		}

		userDiv.find(".description_box").html("<div class='description_attribute_box'>Rechte</div>\
					                           <div class='description_value_box'></div>");

		for (var i = 0; i < allRights.length; i++) {
			createRightButton(allRights[i], user.getID(), userRights.indexOf(allRights[i].recht)!=-1);
		}
	}

	function createRightButton(recht, userID, hasRight) {
		var icon = "switch_off.png";
		if (hasRight) {
			icon = "switch_on.png";
		}
		userDiv.find(".description_value_box").append(
			"<img id='recht" + recht.id + "' src='gfx/button/" + icon + "' style='margin-bottom: -5px'/> [" + recht.recht + "]<br />");
		$("#recht" + recht.id).click(function() {modifyRight($(this), userID, recht)});
	}

	function printError() {
		userDiv.find(".description_value_box").html("Es gab eine Problem beim Laden der Daten.");
	}

	function modifyRight(btn, userID, recht) {
		var action = "removeRight";
		var imgNew = "gfx/button/switch_off.png";
		if (btn.attr("src").indexOf("off") != -1) {
			action = "addRight";
			imgNew = "gfx/button/switch_on.png";
		}

		// Rechte des benutzers laden
		$.ajax({
			url: "sources/api/user/user_operation.php",
			type: 'POST',
			data: {
				'action': action,
				'user': userID,
				'value': recht.recht
			},
			dataType: 'json',
			success: function() {$("#recht" + recht.id).attr("src", imgNew)},
			error: printError
		});
	}

}