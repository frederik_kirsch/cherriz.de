//@ sourceURL=sites/profile_utility.js

/**
 * Ergaenzt die Benutzerattributfelder um Steuerungselemente, wenn die Eingaben geaendert worden.
 */
function handleChange() {
    if ($(this).hasClass("input_passive") || $(this).hasClass("input_changed") || $(this).hasClass("input_error")) {
        var uebernehmen = document.createElement("img");
        uebernehmen.setAttribute("src", "gfx/icon/haken.png");
        $(this).parent().append(uebernehmen);

        $(this).removeClass();
        $(this).addClass("input_active");
        if ($(this).attr("id") == "Geburtsdatum") {
            $(this).addClass("calendar");
        }

        var type = $(this).attr('id');
        $(uebernehmen).mousedown({element: $(this), type: type}, changeVal);
        $(this).blur({element: $(this), type: type}, cancelEditing);
    }
}

/**
 * Verarbeitet eine Aenderung in den Einstellungen.
 */
function handleSettingsChange() {
    var event = {data: {element: $(this), type: $(this).attr('id')}};
    changeVal(event);
}

/**
 * Verarbeitet das abbrechen der Verarbeitung und setzt die Steuerungselemente sowie die Eingabe zurueck.
 * @param event Ausloesendes Event.
 */
function cancelEditing(event) {
    var field = event.data.element;
    var type = event.data.type;

    if (field.hasClass("input_active")) {
        $(field).unbind('blur');
        field.val(STORE.get(type));
        field.parent().find("img").remove();

        field.removeClass();
        field.addClass("input_passive");
    }
}

/**
 * Loest die Kommunikation die Aenderung eines Attributs aus.
 * @param event Ausloesendes Event.
 */
function changeVal(event) {
    var field = event.data.element;
    var type = event.data.type;

    switch (type) {
        case "Avatar":
            USER.setAvatar(field.parent());
            break;
        case "Signatur":
            USER.setSignature(field.val());
            break;
        case "Name":
            USER.setName(field.val());
            break;
        case "Geburtsdatum":
            USER.setGeburtstag(field.val());
            break;
        case "Email":
            USER.setEmail(field.val());
            break;
        case "Twitter":
            USER.setTwitter(field.val());
            break;
        case "Telefon":
            USER.setTelefon(field.val());
            break;
        case "Handy":
            USER.setHandy(field.val());
            break;
        case "Strasse":
            USER.setStrasse(field.val());
            break;
        case "Ort":
            USER.setOrt(field.val());
            break;
        case "Privatsphaere":
            USER.setPrivatsphaere(field.find("option:selected").text());
            break;
        case "NotificationTypeEmail":
            if (field.is(':checked')) {
                USER.setNotificationTypeEmail(true);
            } else {
                USER.setNotificationTypeEmail(false);
            }
            break;
        case "NotificationTypeTwitter":
            if (field.is(':checked')) {
                USER.setNotificationTypeTwitter(true);
            } else {
                USER.setNotificationTypeTwitter(false);
            }
            break;
        case "NotificationNews":
            if (field.is(':checked')) {
                USER.setNotificationNews(true);
            } else {
                USER.setNotificationNews(false);
            }
            break;
	    case "NotificationComment":
		    if (field.is(':checked')) {
			    USER.setNotificationComment(true);
		    } else {
			    USER.setNotificationComment(false);
		    }
		    break;
	    case "NotificationThreadpost":
		    if (field.is(':checked')) {
			    USER.setNotificationThreadpost(true);
		    } else {
			    USER.setNotificationThreadpost(false);
		    }
		    break;
        case "UseExperimental":
            if (field.is(':checked')) {
                USER.setUseExperimental(true);
            } else {
                USER.setUseExperimental(false);
            }
            break;
    }
}

/**
 * Verarbeitet das Ergebnis einer Attributsaenderung.
 *
 * @param type Das geaenderte Attribut.
 * @param status Der Status der Aenderung.
 * @param value Der neue Wert.
 * @param info Meldung der des Servers.
 */
function processResult(type, status, value, info) {
    if (status == "success") {
        if (type == "Avatar") {
	        var avtrBig = $("#Avatar_Big");
	        var avtrMed = $("#Avatar_Medium");
	        var avtrSma = $("#Avatar_Small");
	        avtrBig.attr("src", enhanceImageURL(avtrBig.attr("src")));
	        avtrMed.attr("src", enhanceImageURL(avtrMed.attr("src")));
	        avtrSma.attr("src", enhanceImageURL(avtrSma.attr("src")));
        } else if (type == "Email") {
	        new InfoDialog("Info", info).open();
        } else {
            STORE.saveAssoc(type, value);
        }
    }
    value = STORE.get(type);
    $("#" + type).val(value);
}

/**
 * Erweitert eine Grafikurl um einen Parameter wodurch das neuladen der Grafik erzwungen wird.
 * @param url Die URL.
 * @returns {string} Die angepasste URL.
 */
function enhanceImageURL(url){
    url = url.split("&t=")[0];
    url += "&t=" + new Date().getTime();
    return url;
}

/**
 * Verarbeitet die Antwort des Servers bei einer Einstellungsaenderung im Profil.
 * @param type Geaenderte Einstellung-
 * @param status Status der Aenderung.
 * @param value Neuer Wert.
 * @param info Info vom Server.
 */
function processSettingResult(type, status, value, info) {
    if (status == 'success') {
        STORE.saveAssoc(type, value);
    }
    value = STORE.get(type);


    switch (type) {
        case "NotificationTypeEmail":
        case "NotificationTypeTwitter":
        case "NotificationNews":
	    case "NotificationComment":
	    case "NotificationThreadpost":
        case "UseExperimental":
            if (value == "true") {
                $("#" + type).attr("checked", "checked");
            } else {
                $("#" + type).removeAttr("checked");
            }
            break;
        case "Privatsphaere":
            $("#" + type).val(value);
            break;
    }
}