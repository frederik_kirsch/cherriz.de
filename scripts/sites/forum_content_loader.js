//# sourceURL=sites/forum_content_loader.js

function init() {
	var blockSize = 2;
	var idURL = "/sources/api/forum/search_forum.php";
	var contentURL = "/sources/api/load_content.php";
	var displayType = new FixDisplayType("forum");
	var contentLoader = new ContentLoader(blockSize, idURL, contentURL, displayType, null);
	var appender = new BottomAppender();
	var trigger = new ScrollTrigger(0.95, 1000, contentLoader, appender, null);
	trigger.addToPage();
}