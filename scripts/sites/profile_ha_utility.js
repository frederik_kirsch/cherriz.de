//# sourceURL=sites/profile_ha_utility.js

/**
 * Bindet eine Checkbox als Schalter ein um eine Komponente anzuzeigen oder zu verstecken.
 * @param cbx Die Combobox.
 * @param element Die anzuzeigenden / versteckenden Container.
 */
function addVisibilityToggleToCbx(cbx, element) {
    cbx.change(function(){
        if(cbx.is(':checked')){
            element.show();
        }else{
            element.hide();
        }
    });
}

/**
 * Initialisiert die HomeAutomation Configuration.
 * @param btn Der Button zum aktivieren.
 * @param setupBox Der Container mit Dialog zur Initialisierung.
 * @param configBox Der Container mit Dialog zur Konfiguration.
 * @param userID Die UserID.
 * @param config Das Konfigurationsobjekt.
 */
function setUpHA(btn, setupBox, configBox, userID, config) {
    if(btn.size() != 1){
        return;
    }

    btn.click(function(){
        config.setUp(userID, function(){
            setupBox.hide();
            configBox.show();
        })});
}

/**
 * Bindet den Button zum Neuerzeugen des HA Schluessels an das Konfig Objekt an.
 * @param btn Der Button.
 * @param config Das Konfig Objekt.
 */
function addRegenerateHandler(btn, config) {
    btn.click(function () {
        config.regenerateAuthKey();
    })
}

/**
 * Bindet den Button zum Speichern der Netatmo Konfiguration  an das Konfig Objekt an.
 * @param btn Der Button.
 * @param config Das Konfig Objekt.
 */
function saveNetatmoData(btn, config) {
    btn.click(function () {
        config.updateNetatmoData();
    })
}