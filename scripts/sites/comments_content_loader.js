//@ sourceURL=sites/comments_content_loader.js

function init(newsID) {
	var blockSize = 5;
	var idURL = "/sources/api/comments/search_comments.php?news=" + newsID;
	var contentURL = "/sources/api/load_content.php";
	var displayType = new FixDisplayType("comment");
	var contentLoader = new ContentLoader(blockSize, idURL, contentURL, displayType, null);
	var appender = new BottomAppender();
	var trigger = new ScrollTrigger(0.95, 1000, contentLoader, appender, null);
	trigger.addToPage();
}