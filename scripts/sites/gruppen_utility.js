//@ sourceURL=sites/gruppen_utility.js
var dialog;
/**
 * Oeffnet den Dialog zur Beitrittsanfrage einer Gruppe.
 *
 * @param userID ID des anfragenden Nutzers.
 * @param nick Nick des anfragenden Nutzers.
 * @param groupID ID der Gruppe.
 * @param groupName Name der Gruppe.
 */
function openAccessRequestDialog(userID, nick, groupID, groupName) {
	var content = "<div class='meldungen_form_data'>\
				       Hier kannst du deine Beitrittsanfrage formulieren. Diese wird dann den Verwaltern der Gruppe zugestellt.\
				   </div>\
				   <textarea cols='50' rows='3' id='anfrage'>Hallo,\nich möchte gerne Ihrer Gruppe " + groupName + " beitreten.\nLG " + nick + "</textarea>";
	dialog = new Dialog("Beitrittsanfrage", content);
	dialog.setProperties({
		buttons: {"Senden": processAccessRequest(userID, groupID)}, width: 400
	});
	dialog.open();
}

/**
 * Verarbeitet eine abgeschickte Beitrittsanfrage.
 * @param userID ID des anfragenden Users.
 * @param gruppeID ID der Gruppe.
 */
function processAccessRequest(userID, gruppeID) {
	return function() {
		$.ajax({
			url: 'sources/api/group/group_operation.php',
			type: 'POST',
			data: {
				'user': userID,
				'group': gruppeID,
				'anfrage': $("#anfrage").val(),
				'action': 'REQUEST'
			},
			dataType: 'json',
			success: requestSucceeded,
			error: requestFailed
		});
	}
}

/**
 * Verarbeitet eine erfolgreiche Beitrittsanfrage.
 * @param response Ergebnis der Ajaxanfrage.
 */
function requestSucceeded(response) {
	if (response.result.error != null) {
		requestFailed(response.result.error);
	} else {
		$("#gar" + response.result.query.gruppe).remove();
		dialog.close();
	}
}

/**
 * Verarbeitet eine fehlgeschlagene Beitrittsanfrage.
 */
function requestFailed(error) {
	dialog.close();
	if (typeof error != "string") {
		error = "Die Beitrittsanfrage ist aus technischen Gründen fehlgeschlagen. Bitte versuchen Sie es erneut.";
	}
	new InfoDialog("Fehler", error).open();
}