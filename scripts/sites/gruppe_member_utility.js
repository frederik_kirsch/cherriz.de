//@ sourceURL=sites/gruppe_member_utility.js
/**
 * Script: <b>gruppe_member_utility.js</b>
 * Bildet alle Funktionen zur Clientmanipulation der Unterseite Gruppenmitglieder ab.
 */

/**
 * Ein Gruppenmitglied befoerdern.
 * @param user Die UserID.
 */
function upgrade(user) {
	var userStatusChange = $("#user_" + user).parent().parent().attr("id");
	if (userStatusChange == GROUP.TYPE_VERTRETER) {
		new ConfirmationDialog("Bestätigung",
			"Sind Sie sicher das Sie das Mitglied zum Vertreter befördern wollen? Sie werden dann zum Verwalter herabgestuft.", function() {
				GROUP.upgrade(user);
			}).open();
	} else {
		GROUP.upgrade(user);
	}
}

/**
 * Ein Gruppenmitglied herabstufen.
 * @param user Die UserID.
 */
function downgrade(user) {
	GROUP.downgrade(user);
}

/**
 * Ein Mitglied aus der Gruppe entfernen.
 * @param user Dier UserID.
 */
function removeMember(user) {
	new ConfirmationDialog("Bestätigung", "Sind Sie sicher das Sie das Mitglied entfernen wollen?", function() {
		GROUP.remove(user);
	}).open();
}

/**
 * Ein Benutzer wurde im Suchedialog ausgewaehlt um ihn zur Gruppe hinzuzufuegen.
 * @param user Die UserID.
 */
function newUserSelected(user) {
	createUserDiv(user);
	GROUP.add(user.getID());
}

/**
 * Verarbeitet die Zustimmung zu einer Beitritsanfrage.
 * @param user Der User.
 */
function acceptRequest(user) {
	createUserDiv(user);
	GROUP.acceptRequest(user.getID());
}

/**
 * Verarbeitet die Ablehnung einer Beitritsanfrage.
 * @param userID Die UserID.
 */
function declineRequest(userID) {
	GROUP.declineRequest(userID);
}

/**
 * Funktionen an alle Gruppenmitglieder anhängen.
 */
function appendUserAction() {
	$("#Verwalter").find(".description_value_box").find("span").each(appendControls);
	$("#Vertreter").find(".description_value_box").find("span").each(appendControls);
	$("#Mitglied").find(".description_value_box").find("span").each(appendControls);
}

/**
 * Funktinen an einen einzelnen Nutzer anhängen.
 */
function appendControls() {
	var userStatus = STORE.get("userStatus");
	var span = $(this);
	var type = span.parent().parent().attr("id");

	// Alte Controls entfernen
	span.find("a:not(:first-child)").detach();

	var controls = "";
	if (type == GROUP.TYPE_VERTRETER && userStatus == GROUP.TYPE_VERWALTER || type == GROUP.TYPE_MITGLIED && (userStatus == GROUP.TYPE_VERWALTER || userStatus == GROUP.TYPE_VERTRETER)) {
		var userID = span.attr("id").substr(5);

		if (type != GROUP.TYPE_VERWALTER) {
			controls += "<a title='Mitglied befördern'>[Upgrade]</a> ";
		}
		if (type != GROUP.TYPE_MITGLIED) {
			controls += "<a title='Mitglied herabstufen'>[Downgrade]</a> ";
		}
		controls += "<a title='Mitglied aus der Gruppe entfernen'>[Delete]</a>";
	}
	span.html(span.html() + controls);
	span.find(":contains('Upgrade')").click(function() {upgrade(userID)});
	span.find(":contains('Downgrade')").click(function() {downgrade(userID)});
	span.find(":contains('Delete')").click(function() {removeMember(userID)});
}

/**
 * Funktionen zur Ergebnisverarbeitung der Ajaxanfrage.
 */
function adaptMemberChange(result) {
	switch (result.action) {
		case "Accept":
			if (result.type == "success") {
				adaptAccept(result.userID);
			} else {
				new InfoDialog("Fehler",
					"Die Beitrittsanfrage konnte aus technischen Gründen nicht angenommen werden. Bitte versuchen Sie es erneut.").open();
			}
			break;
		case "Decline":
			if (result.type == "success") {
				$("#anfrage_" + result.userID).remove();
			} else {
				new InfoDialog("Fehler",
					"Die Beitrittsanfrage konnte aus technischen Gründen nicht angenommen werden. Bitte versuchen Sie es erneut.").open();
			}
			break;
		default :
			var div = $("#user_" + result.userID);
			var verwalter = $("#Verwalter").find(".description_value_box").children().first();

			div.detach();
			$("#" + result.status).find(".description_value_box").append(div);
			div.each(appendControls);

			//Sonderbehandlung falls es einen Verwalterwechsel gegeben hat.
			if (result.status == GROUP.TYPE_VERWALTER) {
				adaptVerwalterChange(verwalter);
			}
	}
}

/**
 * Verarbeitet das Ergebnis der Ajaxanfrage zur Annahme einer Beitrittsanfrage.
 * @param userID Die UserID.
 */
function adaptAccept(userID) {
	$("#anfrage_" + userID).remove();
	var div = $("#user_" + userID);
	$("#Mitglied").find(".description_value_box").append(div);
	div.each(appendControls);
}

/**
 * Sonderverarbeitung für die Anpassungen der UI nach einem erfolgreichen Verwalterwechsel.
 * @param verwalter DIV des aktuellen Verwalters.
 */
function adaptVerwalterChange(verwalter) {
	//Aktuellen verwalter downgraden
	verwalter.detach();
	$("#Vertreter").find(".description_value_box").append(verwalter);

	//Aktuellen Userstatus reduzieren
	STORE.update("userStatus", "Vertreter");

	//Alle Controls neu berechnen
	appendUserAction();
}

/**
 * Fehlermeldung bei fehlgeschlagener Memberänderung.
 * @param result Ergebnis der Ajaxanfrage.
 */
function adaptMemberChangeFailure(result) {
	new InfoDialog("Fehler", result.message).open();
}

/**
 * Neuer Benutzer aus Vorlage aufbauen.
 *
 * @param user Das UserObjekt.
 */
function createUserDiv(user) {
	//Falls Objekt bereits existiert vorher entfernen
	$("#user_" + user.getID()).remove();

	var span = $("#user_copy").clone();
	span.attr("id", "user_" + user.getID());
	var a = span.find("a");
	a.html(user.getNick());
	a.attr("href", a.attr("href") + user.getID());
	a.attr("title", a.attr("title") + user.getNick());

	$("#temp").append(span);

	return span;
}

