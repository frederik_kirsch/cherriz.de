//@ sourceURL=sites/gruppe_settings_utility.js
/**
 * Script: <b>gruppe_settings_utility.js</b>
 * Bildet alle Funktionen zur Clientmanipulation der Unterseiten Gruppeeinstellungen ab.
 */

/**
 * Erzeugt einen Validator für ein Gruppenformular.
 * @param create Beschreibt ob der Validator ein Erzeugungs oder Änderungsformular validieren soll.
 */
function createValidator(create) {
    function validate() {
        var name = $("#grp_name").val();
        var pic = $("#grp_pic").val();
        var desc = $("#grp_desc").val();

        if (name.length < 3) {
	        showErrorDialog("Es ist kein Name angegeben,<br />oder der angegebene Name ist zu kurz.");
            return false;
        } else if (create && pic.length < 1) {
	        showErrorDialog("Es ist keine Grafik angegeben.");
            return false;
        } else if (desc.length < 50) {
	        showErrorDialog("Es ist keine Gruppenbeschreibung angegeben,<br />oder die angegebene Beschreibung ist zu kurz.");
            return false;
        }

        return true;
    }

    return validate;
}

function showErrorDialog(text){
	new InfoDialog("Fehler", text).open();
}