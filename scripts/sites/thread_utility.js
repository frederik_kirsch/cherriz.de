//@ sourceURL=sites/thread_utility.js

/**
 * URL für die Thread Ajax API
 * @type {string}
 */
var URL_UPDATE = "sources/api/thread/thread_operation.php";

/**
 * Loescht einen Thread wenn die Sicherheitsabfrage mit OK bestaetigt wurde.
 *
 * @param threadID Die ID des zu loeschenden Threads.
 */
function deleteThread(threadID) {
	$.ajax({
		url: URL_UPDATE,
		type: 'POST',
		data: {
			'action': 'delete',
			'thread': threadID
		},
		dataType: 'json',
		success: validationSuccess,
		error: validationFailure
	});
}

/**
 * Verarbeitet die Rueckmeldung des Ajaxaufrufes für das Loeschen eines Threads.
 *
 * @param response Die Antwort vom Server.
 */
function validationSuccess(response) {
	if (response.result.error !== undefined) {
		validationFailure(response.result.error);
		return;
	}
	//Verarbeitung
	$('#thread_' + response.result.query.thread).remove();
}

/**
 * Verarbeitet eine Fehlermeldung des Ajaxaufrufes für das Loeschen eines Threads.
 *
 * @param meldung Die Fehlermeldung oder null.
 */
function validationFailure(meldung) {
	if (typeof meldung != "string") {
		meldung = "Fehler bei der Kommunikation mit dem Webserver.";
	}
	new InfoDialog("Fehler", meldung).open();
}