//# sourceURL=sites/thread_content_loader.js

function initThread(threadID) {
	var blockSize = 2;
	var idURL = "/sources/api/thread/search_threadpost.php?thread=" + threadID;
	var contentURL = "/sources/api/load_content.php";
	var displayType = new FixDisplayType("threadpost");
	var contentLoader = new ContentLoader(blockSize, idURL, contentURL, displayType, new GapConfig(10, 10));
	var appender = new BottomAppender();
	var changer = new TriggerChanger(threadID, contentLoader, appender);
	var trigger = new ScrollTrigger(0.95, 1000, contentLoader, appender, changer.changeLoaderTrigger);
	trigger.addToPage();
}

function initThreadOverview(forumID) {
	var blockSize = 2;
	var idURL = "/sources/api/thread/search_thread.php?forum=" + forumID;
	var contentURL = "/sources/api/load_content.php";
	var displayType = new FixDisplayType("thread");
	var contentLoader = new ContentLoader(blockSize, idURL, contentURL, displayType, null);
	var appender = new BottomAppender();
	var trigger = new ScrollTrigger(0.95, 1000, contentLoader, appender, null);
	trigger.addToPage();
}

/**
 * Schaltet von Scrolltrigger auf SSETrigger um
 * @param threadID Die ThreadID fuer den Posts geladen werden sollen.
 * @param contentLoader Der Contentloader.
 * @param appender Der Appender.
 * @constructor
 */
function TriggerChanger(threadID, contentLoader, appender) {

	this.changeLoaderTrigger = function(loaderResult) {
		if (loaderResult.hasMore) {
			return;
		}
		$('#post_box').fadeIn();
		var sseTrigger = new SSETrigger("/sources/api/thread/check_threadpost.php?thread=" + threadID, contentLoader, appender);
		sseTrigger.activateServerSentEvents();
	}

}