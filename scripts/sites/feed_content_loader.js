//# sourceURL=sites/feed_content_loader.js

function initFeed() {
	var blockSize = 2;
	var idURL = "/sources/api/feed/search.php";
	var contentURL = "/sources/api/load_content.php";
	var displayType = new GenericDisplayType();
	var contentLoader = new ContentLoader(blockSize, idURL, contentURL, displayType, null);
	var appender = new BottomAppender();
	var trigger = new ScrollTrigger(0.95, 1000, contentLoader, appender, null);
	trigger.addToPage();
}

function GenericDisplayType(){

	this.getType = function(entry){
		return entry.type.toLowerCase();
	}

}