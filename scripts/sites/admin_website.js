//@ sourceURL=sites/admin_website.js

/**
 * Fuehrt Aenderung der Wartungsseite asynchron aus.
 * @param toggleButton Der Toogle Button der Wartungsseite.
 * @constructor
 */
function AdminWebsiteControl(toggleButton) {

	var api = "sources/api/settings/settings_operation.php";

	var wartung;

	init();

	function init() {
		toggleButton.click(function() {
			wartung = toggleButton.attr("src").indexOf("off") == -1;
			changeMaintenance(!wartung);
		});
	}

	/**
	 * Aendert den Wartungsstatus der Webseite.
	 *
	 * @param wartung <code>true</code> fuer Wartungsseite oder <code>false</code>
	 */
	function changeMaintenance(wartung) {
		var icon = "switch_off.png";
		if (wartung) {
			icon = "switch_on.png";
		}

		$.ajax({
			url: api,
			type: 'POST',
			data: {
				'action': 'changeWartung',
				'wartung': wartung
			},
			dataType: 'json',
			success: function() {
				wartung = !wartung;
				if (toggleButton == null) {
					return;
				}
				toggleButton.attr("src", 'gfx/button/' + icon)
			},
			error: printError
		});
	}

	/**
	 * Aendert den Text der Wartungsseite.
	 * @param text Der neue Text
	 */
	this.changeWartungText = function(text) {
		performChange("changeWartungText", text);
	};

	/**
	 * Aendert den Text des Impressums.
	 * @param text Der neue Text
	 */
	this.changeImpressumText = function(text) {
		performChange("changeImpressum", text);
	};

	/**
	 * Aendert den Text der Datenschutzerklaerung.
	 * @param text Der neue Text
	 */
	this.changeDatenschutzText = function(text) {
		performChange("changeDatenschutz", text);
	};

	function performChange(action, text) {
		$.ajax({
			url: api,
			type: 'POST',
			data: {
				'action': action,
				'text': text
			},
			dataType: 'json',
			error: printError
		});
	}

	function printError() {
		new InfoDialog("Fehler", "Es gab Probleme beim Ändern der Daten.").open();
	}

}