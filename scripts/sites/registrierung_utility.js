function validateInput() {
	var info = [];
	if ($("#registration_nick").val().length < 3) {
		info.push("Der Nick muss mindestens 3 Stellen haben.");
	} else if (!/^[a-zA-Z]+[a-zA-Z0-9_]*$/.test($("#registration_nick").val())) {
		info.push("Der Nick ist nicht valide.");
		info.push("Nur Buchstaben, Zahlen sowie Unterstriche sind erlaubt.");
		info.push("Der Nick muss mit einem Buchstaben beginnen.");
	}
	if ($("#registration_passwort1").val().length < 8) {
		info.push("Das Passwort muss mindestens 8 Stellen lang sein.");
	} else if ($("#registration_passwort1").val() != $("#registration_passwort2").val()) {
		info.push("Die eingegebenen Passwörter stimmen nicht überein.");
	}
	if (!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($("#registration_email").val())) {
		info.push("Die angegebene E-Mail ist nicht korrekt.");
	}
	if (info.length == 0) {
		return true;
	}
	new InfoDialog("Hinweis", "Die eingegebenen Daten sind fehlerhaft:<ul><li>" + info.join("</li><li>") + "</li></ul>").open();
	return false;
}