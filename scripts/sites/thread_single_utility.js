//@ sourceURL=sites/thread_single_utility.js

/**
 * URL für die Thread Ajax API
 * @type {string}
 */
var URL_UPDATE = "sources/api/thread/thread_operation.php";

/**
 * Veroeffentlicht einen Post..
 *
 * @param text Der Text.
 * @param threadID Die ID des Threads.
 */
function submitPost(text, threadID) {
	if (text.trim().length == 0) {
		new InfoDialog("Fehler", "Der Beitrag darf nicht leer sein.").open();
		return;
	}
	$.ajax({
		url: URL_UPDATE, type: 'POST', data: {
			'action': 'post', 'thread': threadID, 'text': text
		}, dataType: 'json', success: validationSuccess, error: validationFailure
	});
}

/**
 * Verarbeitet die Rueckmeldung des Ajaxaufrufes für das Posten eines Beitrags.
 *
 * @param response Die Antwort vom Server.
 */
function validationSuccess(response) {
	if (response.result.error !== undefined) {
		validationFailure(response.result.error);
		return;
	}

	//Verarbeitung
	var editor = $('#Post').cleditor()[0];
	editor.$area.val("");
	editor.updateFrame();
}


/**
 * Verarbeitet eine Fehlermeldung des Ajaxaufrufes für das Posten eines Beitrags.
 *
 *  @param meldung Die Fehlermeldung oder null.
 */
function validationFailure(meldung) {
	if (typeof meldung != "string") {
		meldung = "Fehler bei der Kommunikation mit dem Webserver.";
	}
	new InfoDialog("Fehler", meldung).open();
}