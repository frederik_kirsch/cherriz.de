//@ sourceURL=sites/forum_utility.js

/**
 * URL für die Forum Ajax API
 * @type {string}
 */
var URL_UPDATE = "sources/api/forum/forum_operation.php";

/**
 * Loescht ein Forum wenn die Sicherheitsabfrage mit OK bestaetigt wurde.
 *
 * @param forumID Die ID des zu loeschenden Forums.
 */
function deleteForum(forumID) {
	$.ajax({
		url: URL_UPDATE,
		type: 'POST',
		data: {
			'action': 'delete',
			'forum': forumID
		},
		dataType: 'json',
		success: validationSuccess,
		error: validationFailure
	});
}

/**
 * Verarbeitet die Rueckmeldung des Ajaxaufrufes für das Loeschen eines Forums.
 *
 * @param response Die Antwort vom Server.
 */
function validationSuccess(response) {
	if (response.result.error !== undefined) {
		validationFailure(response.result.error);
		return;
	}

	//Verarbeitung
	var id = response.result.query.forum;
	$('#forum_' + id).remove();
}

/**
 * Verarbeitet eine Fehlermeldung des Ajaxaufrufes für das Loeschen eines Forums.
 *
 * @param meldung Die Antwort des Servers.
 */
function validationFailure(meldung) {
	if (typeof meldung != "string") {
		meldung = "Fehler bei der Kommunikation mit dem Webserver.";
	}
	new InfoDialog("Fehler", meldung).open();
}

/**
 * Submittet das Formular, wenn  die Eingaben valide sind.
 */
function createForum() {
	if (submitForum()) {
		$('#forum_form').submit();
	}
}

/**
 * Prueft ob das Formular abgesendet werden kann.
 *
 * @returns {boolean} Validitaet des Formulars.
 */
function submitForum() {
	if ($('#name').val().length == 0) {
		new InfoDialog("Info", "Die Forumsbezeichnung fehlt.<br />Das Forum kann nicht erstellt werden.").open();
		return false;
	} else if ($('#shortdesc').val().length == 0) {
		new InfoDialog("Info", "Dieses Forum enthält keine Beschreibung.<br />Das Forum kann nicht erstellt werden.").open();
		return false;
	}
	return true;
}

/**
 * Setzt das Formular zum Erzeugen des Forums zurueck.
 */
function resetForum() {
	$('#name').val("");
	$('#shortdesc').val("");
}