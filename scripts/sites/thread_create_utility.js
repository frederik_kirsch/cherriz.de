//@ sourceURL=sites/thread_create_utility.js

/**
 * Submittet das Formular, wenn  die Eingaben valide sind.
 */
function createThread() {
    if (submitThread()) {
        $('#thread_form').submit();
    }
}

/**
 * Prueft ob das Formular abgesendet werden kann.
 *
 * @returns {boolean} Validitaet des Formulars.
 */
function submitThread() {
    if ($('#title').val().length == 0) {
	    new InfoDialog("Info", "Der Threadtitel fehlt.<br />Der Thread kann nicht erstellt werden.").open();
        return false;
    } else if ($('#post').val().length == 0) {
	    new InfoDialog("Info", "Dieser Thread enthält keine Beschreibung.<br />Der Thread kann nicht erstellt werden.").open();
        return false;
    }
    return true;
}

/**
 * Setzt das Formular zum Erzeugen des Threads zurueck.
 */
function resetThread() {
    $('#title').val("");
    var editor = $('#post').cleditor()[0];
    editor.$area.val("");
    editor.updateFrame();
}