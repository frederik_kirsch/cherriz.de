//@ sourceURL=sites/news_utility.js

/**
 * Uebernimmt das Handling fuer das Datumsfeld.
 */
function handlePublishDate() {
	var cbx = $('#publish_date_activated');
	var input = $('#publish_date');
	if (cbx.is(':checked')) {
		input.prop('disabled', false);
	} else {
		input.prop('disabled', true);
	}
}

/**
 * Veroeffentlicht eine News wenn sie erfolgreich validiert werden kann.
 *
 * @returns {boolean} Ob die News veroeffentlicht wurde.
 */
function publishNews() {
	var statusField = $('#status');
	if (statusField.val() == "") {
		statusField.val("published");
	}
	return submitNews();
}

/**
 * Speichert eine News im Status Entwurf.
 */
function saveNews() {
	$('#status').val("entwurf");
	$('#news_form').submit();
}

/**
 * Aendert die Seite.
 * @param page Der Schliessen fuer die neue Seite.
 * @param id Die ID einer News falls erorderlich.
 */
function redirect(page, id) {
	var idPar = "";
	if (id != -1) {
		idPar = "&news=" + id;
	}
	self.location.href = ("index.php?page=" + page + idPar);
}

/**
 * Validiert das Formular einer News vor dem Absenden.
 * @returns {boolean} Ergebnis der Validierung.
 */
function submitNews() {
	if ($('[name=headline]').val().length == 0) {
		new InfoDialog("Info", "Die Überschrift fehlt.<br />Die News kann nicht veröffentlicht werden.").open();
		return false;
	} else if ($('#genres_id').val().length == 0) {
		new InfoDialog("Info", "Es sind keine Genres ausgewählt.<br />Die News kann nicht veröffentlicht werden.").open();
		return false;
	} else if ($('#publish_date_activated').prop('checked') && $('#publish_date').val().length != 16) {
		new InfoDialog("Info", "Fehlerhaftes Veröffentlichungsdatum.<br />Die News kann nicht veröffentlicht werden.").open();
		return false;
	} else if ($('#news_text').val().length == 0) {
		new InfoDialog("Info", "Diese News enthält keinen Text.<br />Die News kann nicht veröffentlicht werden.").open();
		return false;
	}
	return true;
}

/**
 * URL für die News Ajax API
 * @type {string}
 */
var URL_UPDATE = "sources/api/news/news_operation.php";

/**
 * Veroeffentlicht eine News die sich im Status entwurf befindet.
 *
 * @param newsID Die ID zu veroeffentlichende News.
 * @param successPage Die Zielseite auf die bei erfolgriecher veroeffentlichung umgelenkt werden soll.
 */
function setNewsPublished(newsID, successPage) {
	STORE.saveAssoc("successPage", successPage);
	$.ajax({
		url: URL_UPDATE,
		type: 'POST',
		data: {
			'action': 'makePublic',
			'news': newsID
		},
		dataType: 'json',
		success: validationSuccess,
		error: validationFailure
	});
}

/**
 * Verarbeitet die Antwort des Servers bei asynchronen Aufrufen.
 *
 * @param response Die Antwort vom Server.
 */
function validationSuccess(response) {
	if (response.result.error !== undefined) {
		validationFailure(response.result.error);
		return;
	}

	//Verarbeitung
	var page = STORE.get("successPage");
	var id = response.result.query.news;
	redirect(page, id);
}

/**
 * Verarbeitet eine Fehlermeldung beim asynchronen Serveraufruf.
 *
 * @param meldung Die Fehlermeldung.
 */
function validationFailure(meldung) {
	if (typeof meldung != "string") {
		meldung = "Fehler bei der Kommunikation mit dem Webserver.";
	}
	new InfoDialog("Fehler", meldung).open();
}