//@ sourceURL=sites/admin_utility.js

function handleDateChange(event) {
    var img = event.data.img;
    var imgLoad = event.data.img_load;
    var text = event.data.date_text;
    var baseURL = event.data.imgUrl;
    var date = $(this).val();

    //Event validieren
    if(date == text.html().substr(0,10)){
        return;
    }

    text.html(date + " 00:00:00 - " + date + " 23:59:59");
    img.hide();
    imgLoad.show();
    img.attr("src", "");

    img.load(function () {
        imgLoad.hide();
        img.show();
    }).attr("src", baseURL + "?day=" + date)
}