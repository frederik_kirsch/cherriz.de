//@ sourceURL=sites/wiederherstellung_utility.js

function validateInput() {
	var info = [];
	var variante = $("input[name='variante']:checked").val();
	if (variante == "nick") {
		if ($("#wiederherstellung_nick").val().length < 3) {
			info.push("Der Nick muss mindestens 3 Stellen haben.");
		} else if (!/^[a-zA-Z]+[a-zA-Z0-9_]*$/.test($("#wiederherstellung_nick").val())) {
			info.push("Der Nick ist nicht valide.");
			info.push("Nur Buchstaben, Zahlen sowie Unterstriche sind erlaubt.");
			info.push("Der Nick muss mit einem Buchstaben beginnen.");
		}
	} else {
		if (!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($("#wiederherstellung_mail").val())) {
			info.push("Die angegebene E-Mail ist nicht korrekt.");
		}
	}
	if (info.length == 0) {
		return true;
	}
	new InfoDialog("Hinweis", "Die eingegebenen Daten sind fehlerhaft:<ul><li>" + info.join("</li><li>") + "</li></ul>").open();
	return false;
}

function validatePwd() {
	var info = null;
	if ($("#wiederherstellung_passwort1").val().length < 8) {
		info = "Das Passwort muss mindestens 8 Stellen lang sein.";
	} else if ($("#wiederherstellung_passwort1").val() != $("#wiederherstellung_passwort2").val()) {
		info = "Die eingegebenen Passwörter stimmen nicht überein.";
	}
	if (info != null) {
		new InfoDialog("Hinweis", "Die eingegebenen Daten sind fehlerhaft:<ul><li>" + info + "</li></ul>").open();
		return false;
	}
	return true;
}

function processVarianteChange() {
	var variante = $("input[name='variante']:checked").val();
	if (variante == "nick") {
		$("#wiederherstellung_nick").prop('disabled', false);
		$("#wiederherstellung_mail").prop('disabled', true);
	} else {
		$("#wiederherstellung_nick").prop('disabled', true);
		$("#wiederherstellung_mail").prop('disabled', false);
	}
}