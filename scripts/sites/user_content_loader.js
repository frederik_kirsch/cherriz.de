//# sourceURL=sites/user_content_loader.js

function initUserSearch() {
	var blockSize = 2;
	var idURL = "/sources/api/user/search.php";
	var contentURL = "/sources/api/load_content.php";
	var displayType = new FixDisplayType("userSmall");
	var contentLoader = new ContentLoader(blockSize, idURL, contentURL, displayType, null);
	var appender = new BottomAppender();
	var trigger = new ScrollTrigger(0.95, 1000, contentLoader, appender, null);
	trigger.addToPage();
}