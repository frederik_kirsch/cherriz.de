//@ sourceURL=groupselector_utility.js

function GroupSelector(rdoType, lstGroups, saveField, initGrps) {

	init();

	function init() {
		rdoType.change(handleSelect);

		$.ajax({
			url: 'sources/api/user/user_operation.php',
			type: 'POST',
			data: {
				'action': 'getGroups',
				'user': STORE.get("UserID")
			},
			dataType: 'json',
			success: setGroups,
			error: errorLoadGroups
		});
	}

	function handleSelect() {
		switch ($(this).val()) {
			case "Public":
				refreshSelecIDs(lstGroups.find(".none"));
				lstGroups.hide("slow");
				break;
			case "Umfeld":
				refreshSelecIDs(lstGroups.find("li"));
				lstGroups.hide("slow");
				break;
			case "Spezifisch":
				refreshSelecIDs(lstGroups.find(".ui-selected"));
				lstGroups.show("slow");
				break;
		}
	}

	function refreshSelecIDs(grpLstItems) {
		var selected = [];
		grpLstItems.each(function() {
			selected.push($(this).attr("id").substr(4));
		});
		saveField.val(selected.join(","));
	}

	function setGroups(data) {
		var groupArr = data.result.data.groups;
		var groups = "";
		for (var i = 0; i < groupArr.length; i++) {
			var selected = "";
			if (initGrps.indexOf(groupArr[i].id) >= 0) {
				selected = " ui-selected";
			}
			groups += "<li class='ui-state-default" + selected + "' id='grp_" + groupArr[i].id + "'>\
						<img src='gfx/img_generator.php?type=Gruppe&id=" + groupArr[i].id + "&size=Small' /> " + groupArr[i].name + "</li>";
		}

		lstGroups.html(groups);
		lstGroups.selectable({
			stop: function() { refreshSelecIDs(lstGroups.find(".ui-selected"))}
		});

		if (initGrps.length > 0 && groupArr.length == initGrps.length) {
			rdoType.eq(1).trigger("click");
		} else if (initGrps.length > 0) {
			rdoType.eq(2).trigger("click");
		}
	}

	function errorLoadGroups() {
		lstGroups.val("<li class='ui-state-default'>Gruppen konnten nicht geladen werden.</li>");
	}
}