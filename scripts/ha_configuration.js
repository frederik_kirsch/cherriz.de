//# sourceURL=ha_util.js

/**
 * Repraesentiert die Konfiguration fuer CherrizHA.
 */
function HAConfiguration(authKey) {

    const MODULE_NETATMO = "Netatmo";

    const MODULE_TIMER = "Timer";

    const URL_UPDATE = "sources/api/ha/ha_operation.php";

    var userID;

    var modules = [];

    /**
     *Laed die initial die Konfiguration wenn diese einmalig angelegt wurde.
     */

    (function () {
        if (authKey == null) {
            return;
        }
        loadConfiguration();
    })();

    /**
     * Generiert eienen Zugangsschluesse und legt fuer den Benutzer die Initialkonfiguration an.
     *
     * @param userID Die User ID.
     * @param cb Die Callbackfunktion.
     */
    this.setUp = function (userID, cb) {
        $.ajax({
            url: URL_UPDATE,
            type: 'POST',
            data: {
                'action': 'setUp',
                'user': userID
            },
            dataType: 'json',
            success: function (response) {
                validationSuccess(response, function () {
                    authKey = response.result.data.authKey;
                    loadConfiguration();
                    cb();
                })
            },
            error: validationFailure
        });
    };

    /**
     * Erzeugt einen neuen Authentifizierungsschluessel fuer Cherriz HA.
     */
    this.regenerateAuthKey = function () {
        $.ajax({
            url: URL_UPDATE,
            type: 'POST',
            data: {
                'action': 'regenerateAuthKey',
                'authKey': authKey
            },
            dataType: 'json',
            success: function (response) {
                validationSuccess(response, function (query, data) {
                    setAuthKey(data.authKey);
                })
            },
            error: validationFailure
        });
    };

    /**
     * Aktualisiert die Konfigurationsdaten fuer das Netatmo Modul.
     */
    this.updateNetatmoData = function () {
        var user = modules[MODULE_NETATMO].getProperty("user");
        var pass = modules[MODULE_NETATMO].getProperty("pass");
        $.ajax({
            url: URL_UPDATE,
            type: 'POST',
            data: {
                'action': 'setModuleProperty',
                'authKey': authKey,
                'value': {'module': MODULE_NETATMO, 'properties': {'User': user, 'Pass': pass}}
            },
            dataType: 'json',
            success: function (response) {
                validationSuccess(response, null);
            },
            error: validationFailure
        });
    };

    /**
     * Laed die Konfiguration vom Server.
     */
    function loadConfiguration() {
        $.ajax({
            url: URL_UPDATE,
            type: 'GET',
            data: {
                'action': 'getHAData',
                'authKey': authKey
            },
            dataType: 'json',
            success: function (response) {
                validationSuccess(response, function (query, data) {
                    setConfiguration(data)
                })
            },
            error: validationFailure
        });
    }

    /**
     * Setzt die Konfiguration in der UI und bindet die entsprechenden Handler an.
     *
     * @param data Die Konfigurationsdaten.
     */
    function setConfiguration(data) {
        userID = data.userID;
        setAuthKey(data.authKey);
        $.each(data.modules, function (idx, module) {
            modules[module.name] = new Module(module.name, module.properties, toggleModuleActivity);
        });
    }

    /**
     * Setzt den HA Authentifizierungsschluessel (auch in der UI).
     * @param key Der neue Schluessel.
     */
    function setAuthKey(key) {
        var authKey = key.substr(0, 4) + " - " + key.substr(4, 4) + " - " + key.substr(8, 4) + " - " + key.substr(12, 4) + " - " + key.substr(16, 4);
        $("#authKey").html(authKey);
    }

    /**
     * Aktiviert, bzw. Deaktiviert ein Modul.
     * @param module Das Modul.
     * @param active Ob Aktiv oder Inaktiv.
     * @param uicb Die Callbackfunktion fuer die UI.
     */
    function toggleModuleActivity(module, active, uicb) {
        $.ajax({
            url: URL_UPDATE,
            type: 'POST',
            data: {
                'action': 'setModuleProperty',
                'authKey': authKey,
                'value': {'module': module, 'properties': {'Active': active}}
            },
            dataType: 'json',
            success: function (response) {
                validationSuccess(response, function (query, data) {
                    uicb(true);
                })
            },
            error: function () {
                validationFailure(null);
                uicb(false);
            }
        });
    }

    /**
     * Verarbeitet Erfolgreiche Antworten des Servers.
     * @param response Ausgabe des Servers.
     * @param cb
     */
    function validationSuccess(response, cb) {
        if (response.result.error !== undefined) {
            validationFailure(response.result.error);
            return;
        }
        if (cb != null) {
            cb(response.result.query, response.result.data);
        }
    }

    /**
     * Verarbeitet ein Problem bei der asynchronen Serverkommunikation.
     * @param meldung Ausgabe des Servers, falls vorhanden.
     */
    function validationFailure(meldung) {
        if (typeof meldung != "string") {
            meldung = "Fehler bei der Kommunikation mit dem Webserver.";
        }
        new InfoDialog("Fehler", meldung).open();
    }

}

/**
 * Repraesentiert ein Cherriz HA Modul.
 * @param name Name des Moduls.
 * @param properties Die Eigenschaften des Moduls.
 * @param activityCB Die Callbackmethode die bei Aktivierung oder Deaktivierung durch den Nutzer aufgerufen wird.
 * @constructor
 */
function Module(name, properties, activityCB) {

    var cbx = $('#ha_' + name);

    var muted = true;

    // Initiale Werte setzen
    cbx.prop('checked', properties['active'] == 1);
    cbx.trigger('change');
    muted = false;

    $.each(properties, function (prop, val) {
        $('#ha_' + name + '_' + prop).val(val);
    });

    // Listener anbinden
    cbx.change(function () {
        if (muted) {
            return;
        }
        muted = true;
        activityCB(name, $(this).is(':checked'), function (success) {
            if (!success) {
                cbx.prop('checked', !$(this).is(':checked'));
                cbx.trigger('change');
            }
            muted = false;
        });
    });

    $.each(properties, function (prop, val) {
        if (prop == 'active') return;
        $('#ha_' + name + '_' + prop).change(function () {
            properties[prop] = $(this).val();
        });
    });

    /**
     * Liefert den Wert einer Property eines Moduls.
     * @param prop Die Eigenschaft.
     * @returns Der Wert.
     */
    this.getProperty = function (prop) {
        properties[prop] = $('#ha_' + name + '_' + prop).val();
        return properties[prop];
    }

}