//# sourceURL=asynchrounus_content_loader.js

/**
 * Triggert das Nachladen von Inhalt durch das Scrollen mit der Maus.
 * @param threshold Zahl zwischen 1 und 0 die Angibt bei welchem dargestellten Seitenanteil nachgeladen werden soll.
 * @param muteDuration Zeitintervall in ms welches angibt, wie lange Scrolling nach einem Event ignoriert wird.
 * @param contentLoader Funktion die das Nachladen von inhalten uebernimmt.
 * @param appender Funktion, welche die Elemente auf der Seite anhaengt.
 * @param notifier Funktion die nach dem ende des Ladens benachrichtigt wird.
 * @constructor
 */
function ScrollTrigger(threshold, muteDuration, contentLoader, appender, notifier) {

    this.lastLoad = 0;

    var initialHeight = 0;

    /**
     * Haengt den Listener an die Seite an.
     */
    this.addToPage = function () {
        $(document).ready(function () {
            $(window).unbind('scroll');
            $(window).scroll(processScroll);
            // Hoehe mit 10% Tolleranz um unschärfen beim Seitenaufbau aus dem Weg zu gehen
            initialHeight = $(document).height() * 1.1;
            init();
        });
    };

    /**
     * Initialisiert das Laden, falls noch Platz am Seitenende ist, wird weitergeladen.
     * @param data Ergebnis des Ladens.
     */
    function init(data) {
        if ($(document).height() > initialHeight || (data !== undefined && !data.hasMore)) {
            if (notifier != null) {
                notifier(data);
            }
            return;
        }
        contentLoader.loadContent(appender, init, LOADER_TYPE.STANDAR);
    }

    /**
     * Verarbeitet Scrollbewegungen und prueft ob diese das Nachladen von Content ausloesen.
     */
    function processScroll() {
        var wintop = $(window).scrollTop();
        var docheight = $(document).height();
        var winheight = $(window).height();

        if ((wintop / (docheight - winheight)) > threshold) {
            var curTime = new Date().getTime();
            if ((this.lastLoad + muteDuration) > curTime) {
                return;
            }
            this.lastLoad = curTime;

            contentLoader.loadContent(appender, loadingDone, LOADER_TYPE.STANDAR);
        }
    }

    /**
     * Wird aufgerufen, wenn das Laden beendet ist und beendet den Listener falls kein Inhalt mehr vorhanden ist und benachrichtigt den Notifier.
     * @param data Ergebnis des Ladens.
     */
    function loadingDone(data) {
        if (!data.hasMore) {
            $(window).unbind('scroll');
            if (notifier != null) {
                notifier(data);
            }
        }
    }

}

/**
 * Triggert das Nachladen von Elementen durch das Verarbeiten von Server Sent Events.
 * @param checkURL Die URl auf der nach neuen Inhalten geprueft wird.
 * @param contentLoader Funktion die das Nachladen von inhalten uebernimmt.
 * @param appender Funktion, welche die Elemente auf der Seite anhaengt.
 * @constructor
 */
function SSETrigger(checkURL, contentLoader, appender) {

    var scroll = false;

    /**
     * Bindet die Server Sent Events an.
     */
    this.activateServerSentEvents = function () {
        if (EventSource === undefined) {
            console.log("Error: Server Sent Events not supported.");
            return; // Not supported
        }

        var source = new EventSource(getBaseURL() + checkURL);
        source.addEventListener("post", validateEvent);
        source.onerror = handleError;
    };

    /**
     * Validiert ein Event.
     * @param event Das Event.
     */
    function validateEvent(event) {
        var data = JSON.parse(event.data);
        var curContentID = data.result.data.id;
        scroll = ($(window).scrollTop() + $(window).height()) > ($(document).height() - 155);
        if (curContentID == contentLoader.getLastLoaded()) {
            return;
        }

        contentLoader.loadContent(appender, loadingDone, LOADER_TYPE.STANDAR);
    }

    function handleError(event) {
        switch (event.target.readyState) {
            // if reconnecting
            case EventSource.CONNECTING:
                console.log("Reconnecting...");
                break;
            // if error was fatal
            case EventSource.CLOSED:
                console.log("Connection failed. Will not retry.");
                break;
        }
    }

    /**
     * Wird benachrichtigt, wenn neue Elemente geladen wurden und scrollt an das Seitenende.
     * @param data Das Ergebnis des Ladevorgangs.
     */
    function loadingDone(data) {
        if (data.hasMore) {
            contentLoader.loadContent(appender, loadingDone, LOADER_TYPE.STANDAR);
        }
        if (scroll) {
            window.scrollTo(0, document.body.scrollHeight);
        }
    }

}

/**
 * Enum fuer die verschiedenen Ladetypen.
 * @type {{STANDAR: string, BEFORE: string, AFTER: string}}
 */
var LOADER_TYPE = {

    /** Normales laden am Ende des Inhaltes. */
    STANDAR: "standard",

    /** Rueckwaertsladen innerhalb des Inhalts. */
    BEFORE: "before",

    /** Vorwaertsladen innerhalb des Inhalts. */
    AFTER: "after"

};

/**
 * Der ContentLoader dient dem asynchronen nachladen von unterschiedlichen Inhalten. Das nachladen wird durch scrollen
 * an das Seitenende ausgeloesst.
 *
 * @param blockSize Anzahl Elemente die bei einem aufruf gelesen werden.
 * @param idURL URL fuer die Abfrage der IDs
 * @param contentURL URL für die Abfrage des Contents zu einer ID
 * @param displayType Der Typ der darzustelenden Elemente.
 * @param gapConfig Konfiguration die fuer das ueberspringen von Inhalten sorgt (kann leer gelassen werden bei normalem Ladeverhalten).
 * @constructor
 * @author Frederik Kirsch
 */
function ContentLoader(blockSize, idURL, contentURL, displayType, gapConfig) {

    var infoDiv = "loaderInfo";

    var loaderText = "Loading...<br /><img src='gfx/ladebalken.gif'>";

    var timeOut = 3000;

    var retryDelay = 2000;

    var retryCount = 3;

    var itemCount = 0;

    var lastElement = 0;

    var activeLoaderCount = 0;

    if (gapConfig != null) {
        gapConfig.contentLoader = this;
        var storeID = STORE.save(gapConfig);
    }

    /**
     * Laedt Inhalt nach. Zunaechst wird auf Basis des letzten dargestellten Elements die naechsten zu ladende Inhalte
     * bestimmt. Anschliessend werden diese Inhalte erzeugt und in der Seite eingebettet.
     * @param appender Funktion, welche die Elemente auf der Seite anhaengt.
     * @param notifier Funktion die nach dem ende des Ladens benachrichtigt wird.
     * @param type Ladetyp, also wo und in welche Richtung geladen wird ({@see LOADER_TYPE}).
     */
    this.loadContent = function (appender, notifier, type) {
        if (activeLoaderCount > 0) {
            return; //Es wird bereits geladen
        }

        //Ladeposition ermitteln
        var para = calculateRequestData(type);

        $.ajax({
            url: getBaseURL() + idURL,
            data: para,
            retryLimit: retryCount,
            timeout: timeOut,
            success: function (data) {
                processData(data.result.data, appender, notifier, type);
            },
            error: function (xhr, textStatus, errorThrown) {
                this.retryLimit--;
                if ((textStatus == 'timeout' ||
                    xhr.status == 500) &&
                    this.retryLimit) {
                    console.log("There was a problem with the request. Repeating request.");
                    var curCall = this;
                    setTimeout(function () {
                        $.ajax(curCall)
                    }, retryDelay);
                } else {
                    console.log("There was an unresolvable error with the request.\n" + textStatus + "\n" + errorThrown);
                }
            },
            dataType: "json"
        });
    };

    /**
     * Berechnet den Ausschnitt der Daten die geladen werden sollen.
     *
     * @param type Ladetyp, also wo und in welche Richtung geladen wird ({@see LOADER_TYPE}).
     * @returns {{start: number, max: *}}
     */
    function calculateRequestData(type) {
        var result = {
            start: itemCount,
            max: blockSize
        };
        if (type == LOADER_TYPE.BEFORE) {
            result.start = gapConfig.preGapCount;
        } else if (type == LOADER_TYPE.AFTER) {
            result.start = gapConfig.postGapCount - (blockSize + 1);
            if (result.start < gapConfig.preGapCount) {
                result.start = gapConfig.preGapCount
            }
        }
        if (type != LOADER_TYPE.STANDAR && (result.start + blockSize) >= gapConfig.postGapCount) {
            result.max = gapConfig.postGapCount - result.start - 1;
        }

        return result;
    }

    /**
     * @returns {number} Die des aktuellsten Eintrags.
     */
    this.getLastLoaded = function () {
        return lastElement;
    };

    /**
     * Verarbeitet die Liste an IDs die nachgeladen werden sollen.
     *
     * @param data Liste an abzurufenden Elementen.
     * @param appender Funktion, welche die Elemente auf der Seite anhaengt.
     * @param notifier Funktion die nach dem ende des Ladens benachrichtigt wird.
     * @param type Ladetyp, also wo und in welche Richtung geladen wird ({@see LOADER_TYPE}).
     */
    function processData(data, appender, notifier, type) {
        var result = handlePointer(data, type);

        data.items.forEach(function (entry) {
            startLoading();
            var elementID = "asyncid" + entry.id;
            var div = appender.createAndAppend(elementID);
            $.ajax({
                url: getBaseURL() + contentURL,
                data: {
                    type: displayType.getType(entry),
                    id: entry.id
                },
                retryLimit: retryCount,
                timeout: timeOut,
                success: function (data) {
                    div.html(data);
                    if (hljs != null) {
                        div.find("pre code").each(function (i, block) {
                            hljs.highlightBlock(block);
                        });
                    } else {
                        console.warn("Hilghlighting script missing.");
                    }
                    endLoading(notifier, result);
                },
                error: function (xhr, textStatus, errorThrown) {
                    this.retryLimit--;
                    if ((textStatus == 'timeout' ||
                        xhr.status == 500) &&
                        this.retryLimit) {
                        console.log("There was a problem with the request. Repeating request.");
                        var curCall = this;
                        setTimeout(function () {
                            $.ajax(curCall)
                        }, retryDelay);
                    } else {
                        console.log("There was an unresolvable error with the request.\n" + textStatus + "\n" + errorThrown);
                    }
                }
            });
        });

        handleGap(appender, data.count);

        if (data.length == 0) {
            endLoading(notifier, result);
        }
    }

    /**
     * Aktualisiert die Pointer, welche auf die Positionen von bereits geladenem Inhalt zu noch zu ladendem Inhalt zeigen.
     * @param data Die frisch geladenen Daten
     * @param type Ladetyp, also wo und in welche Richtung geladen wird ({@see LOADER_TYPE}).
     * @returns {LoadingResult} Gibt an ob weitere Elemente verfuegbar sind.
     */
    function handlePointer(data, type) {
        var hasMore = false;

        if (type == LOADER_TYPE.STANDAR) {
            if (data.items.length > 0) {
                lastElement = data.items[data.items.length - 1].id;
            }
            itemCount += data.items.length;
            if (itemCount < data.count) {
                hasMore = true;
            }
        } else if (type == LOADER_TYPE.BEFORE) {
            gapConfig.preGapCount += data.items.length;
        } else if (type == LOADER_TYPE.AFTER) {
            gapConfig.postGapCount -= data.items.length;
        }

        if (type == LOADER_TYPE.BEFORE || type == LOADER_TYPE.AFTER) {
            if (gapConfig.preGapCount == gapConfig.postGapCount - 1) {
                $("#gap").fadeOut();
            }
        }

        return new LoadingResult(hasMore);
    }

    /**
     * Ueberspringt einige Eintreage, sofern ein Gap konfiguriert wurde.
     * @param appender Der Appender, welcher Eintraege auf der Seite anhaengt.
     * @param count Anzahl an Content Eintraegen, die insgesamt existieren.
     */
    function handleGap(appender, count) {
        if (gapConfig == null) {
            return;
        }
        if (count <= (gapConfig.preGap + gapConfig.postGap + 5)) {
            return;
        }
        if (itemCount >= count - gapConfig.postGap) {
            return;
        }
        if (itemCount < gapConfig.preGap) {
            return;
        }

        gapConfig.preGapCount = itemCount;
        itemCount = count - gapConfig.postGap;
        gapConfig.postGapCount = itemCount + 1;

        var div = appender.createAndAppend("gap");
        var text = "<h3>Einträge ausgeblendet</h3>"
        text += "Zur besseren Übersicht wurden einige Beiträge ausgeblendet.<br />";
        text += "Nachladen: <a href=\"javascript:STORE.get('" + storeID + "').preLoad();\">Vorherige</a> - <a href=\"javascript:STORE.get('" + storeID + "').postLoad();\">Folgende</a> ";
        div.html("<div class='box gap'>" + text + "</div>");
    }

    /**
     * Signalisiert den Beginn des Ladens eines Elements.
     * Zeigt die Ladeanimation an.
     */
    function startLoading() {
        if (activeLoaderCount == 0) {
            $("#" + infoDiv).html(loaderText).fadeIn();
        }
        activeLoaderCount++;
    }

    /**
     * Signalisiert das Ende des Ladens eines Elements.
     * Entfernt die Ladeanimation und benachtichtig ueber das Ladeende.
     * @param notifier Funktion die nach dem ende des Ladens benachrichtigt wird.
     * @param result Ergebnis des Ladevorgangs.
     */
    function endLoading(notifier, result) {
        activeLoaderCount--;
        if (activeLoaderCount > 0) {
            return;
        }
        $("#" + infoDiv).empty().fadeOut();
        if (notifier != null) {
            notifier(result);
        }
    }

}

/**
 * Erzeugte neue Elemente und haengt diese oben an.
 * @constructor
 */
function TopAppender() {

    var div = $("#loaderTop");

    /**
     * Erzeugt ein neuen DIV und haeng ihn oben an.
     * @param divID Die ID des neuen DIVs.
     * @returns {*|jQuery|HTMLElement}
     */
    this.createAndAppend = function (divID) {
        div.after("<div id='" + divID + "'></div>");
        return $("#" + divID);
    }

}

/**
 * Erzeugte neue Elemente und haengt diese unten an.
 * @constructor
 */
function BottomAppender() {

    var div = $("#loaderInfo");

    /**
     * Erzeugt ein neuen DIV und haeng ihn ans Ende an.
     * @param divID Die ID des neuen DIVs.
     * @returns {*|jQuery|HTMLElement}
     */
    this.createAndAppend = function (divID) {
        div.before("<div id='" + divID + "'></div>");
        return $("#" + divID);
    }

}

/**
 * Erzeugte neue Elemente und haengt diese in der Mitte ein.
 * @constructor
 */
function GapAppender(type) {

    var div = $("#gap");

    var follow = null;

    /**
     * Erzeugt ein neuen DIV und haeng ihn ans Ende an.
     * @param divID Die ID des neuen DIVs.
     * @returns {*|jQuery|HTMLElement}
     */
    this.createAndAppend = function (divID) {
        if (type == LOADER_TYPE.BEFORE) {
            div.before("<div id='" + divID + "'></div>");
        } else {
            if (follow == null) {
                follow = div.next();
            }
            follow.before("<div id='" + divID + "'></div>");
        }
        return $("#" + divID);
    }

}

/**
 * Definiert wie Elemente nachgeladene Elemente dargestellt werden sollen. Dabei ist die Darstellung jedes Elements identisch.
 * @param type Der Darstellungstyp.
 * @constructor
 */
function FixDisplayType(type) {

    this.getType = function (entry) {
        return type;
    }

}

/**
 * Das Ergebnis der Abfrage von Eintraegen
 * @param hasMore Weitere Eintraege sind verfuegbar.
 * @constructor
 */
function LoadingResult(hasMore) {
    this.hasMore = hasMore;
}

/**
 * Konfiguration fuer einen Gap (ausblenden und ueberspringen der mittleren Eintraege).
 * @param preGap Anzahl an Eintraegen, ab denen der Gap angezeigt werden soll.
 * @param postGap Anzahl an Eintraegen, die nach dem Gap nocht existieren sollen.
 * @constructor
 */
function GapConfig(preGap, postGap) {
    this.preGap = preGap;
    this.postGap = postGap;
    this.preGapCount = 0;
    this.postGapCount = 0;
    this.contentLoader = null;

    /** Laedt Elemente von vor der Luecke. */
    this.preLoad = function () {
        this.contentLoader.loadContent(new GapAppender(LOADER_TYPE.BEFORE), null, LOADER_TYPE.BEFORE);
    };

    /** Laedt Elemente von nach der Luecke. */
    this.postLoad = function () {
        this.contentLoader.loadContent(new GapAppender(LOADER_TYPE.AFTER), null, LOADER_TYPE.AFTER);
    }

}