/**
 * Lädt ein externes Stylesheet asynchron nach.
 * @param {String} style Der Pfad zum StyleSheet.
 */
function loadStyle(style) {
    var snode = document.createElement('link');
    snode.setAttribute('rel', 'stylesheet');
    snode.setAttribute('type', 'text/css');
    snode.setAttribute('href', style);
    document.getElementsByTagName('head')[0].appendChild(snode);
}

/**
 * Hinterlegt das Script zu einem Hashwert
 * @param hash
 * @param adaptionFunction
 */
function bufferExecution(hash, adaptionFunction) {
    var scripts = STORE.get("scripts_" + hash);

    //Wurden noch keine Scripts hinterlegt, Speicherplatz initialisieren
    if (scripts == null) {
        STORE.saveAssoc("scripts_" + hash, new Array({script: adaptionFunction, executed: false}));
    } else {
        scripts.push({script: adaptionFunction, executed: false});
        STORE.update("scripts_" + hash, scripts);
    }

    //Falls alle Scripts bereits geladen sind, Scripts ausführen
    var counter = STORE.get("counter_" + hash);
    if (counter == 0) {
        execute(hash);
    }
}

var asyncScriptCounter = 0;

function execute(hash) {
    //Scripts laden
    var scripts = STORE.get("scripts_" + hash);

    //Ausführen
    for (var element in scripts) {
        var script = scripts[element];
        if (!script.executed) {
            eval("//# sourceURL=tmp_" + (++asyncScriptCounter) + ".js\n" + script.script);
            script.executed = true;
        }
    }

    //Speicher aktualisieren
    STORE.update("scripts_" + hash, scripts)
}

/**
 * Lädt ein externes Javascript asynchron nach.
 * @param {String} hash Der Pfad zum Script.
 * @param {String} scripts Die zu ladenden Scripts.
 *
 * @namespace script.base
 * @namespace script.file
 */
function loadScripts(hash, scripts) {
    //Speichern auf Antwort wievieler Scripte gewartet werden muss
    STORE.saveAssoc("counter_" + hash, scripts.length);

    for (var element in scripts) {
        var script = scripts[element];
        //Referenz auf Zähler für Script speichern
        STORE.saveAssoc(script.file, hash);

        $.ajaxSetup({
            async: !script.base
        });

        $.ajax({url: script.file, dataType: "script"}).done(function () {
            var hash = STORE.get(this.url.split("?")[0]);
            var counter = STORE.get("counter_" + hash);
            counter -= 1;
            if (counter == 0) {
                execute(hash);
            } else {
                STORE.update("counter_" + hash, counter);
            }
        }).fail(function (jqxhr, settings, exception) {
	        new InfoDialog("Fehler", "Script konnte nicht geladen werden:\n" + exception).open();
        });
    }
}

var baseUrl = null;

function getBaseURL() {
    if (baseUrl == null) {
        var url = window.location.pathname;
        var pos = url.lastIndexOf('/');
        if (pos != -1) {
            url = url.substring(0, pos)
        }
        baseUrl = url;
    }
    return baseUrl;
}

/**
 * DATAStore<br />
 * Provides funktionality to save js-objects over a client-session.
 */
var STORE = {

    data: [],

    /**
     * Pushs data on the store and returns a key to retrieve the Data Back
     * @param {Object} pData The data to be saved.
     */
    save: function (pData) {
        var id = this.data.length;
        this.data[id] = pData;
        return "key_" + id;
    },

    /**
     * Saves data associative.
     *
     * @param {string} pKey The Name, also used as Key.
     * @param {Object} pData The data to be saved.
     */
    saveAssoc: function (pKey, pData) {
        this.data[pKey] = pData;
    },

    /**
     * Gets data from the store for a specific key.
     *
     * @param {Object} pKey The key to specify the data.
     */
    get: function (pKey) {
        var id = this.calculateKey(pKey);
        return this.data[id];
    },

    /**
     * Updates the data of a specific key. Should be used for generated keys.
     *
     * @param key The Key.
     * @param value The new Value.
     */
    update: function (key, value) {
        var id = this.calculateKey(key);
        this.data[id] = value;
    },

    /**
     * Returns data from the store and removes ist.
     * @param {Object} pKey The key to specify the data.
     */
    remove: function (pKey) {
        var id = this.calculateKey(pKey);
        var result = this.data[id];
        this.data[id] = null;
        return result;
    },

    calculateKey: function (pKey) {
        var key = pKey;
        if (pKey.toString().substring(0, 4) == "key_") {
            key = pKey.toString().substr(4);
        }
        return key;
    }
};