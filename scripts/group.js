//@ sourceURL=group.js

/**
 * Repraesentiert die Entitaet Gruppe.
 */
var GROUP = {

	/**
	 * Rang Mitglied.
	 */
	TYPE_MITGLIED: "Mitglied",

	/**
	 * Rang Vertreter
	 */
	TYPE_VERTRETER: "Vertreter",

	/**
	 * Rang Verwalter
	 */
	TYPE_VERWALTER: "Verwalter",

	/**
	 * URL fuer Anfragen an den Server.
	 */
	URL: "sources/api/group/group_operation.php",

	group: null,

	retFuncSuccess: null,

	retFuncFailure: null,

	/**
	 * Legt die Gruppe fest, welche durch das Objekt repraesentiert wird.
	 * @param group Die GruppenID.
	 */
	setGroup: function(group) {
		this.group = group;
	},

	/**
	 * Definiert die Rueckgabefunktionen durchgefuehrten asynchronen Aktivitaeten.
	 * @param success
	 * @param failure
	 */
	setRetFunc: function(success, failure) {
		this.retFuncSuccess = success;
		this.retFuncFailure = failure;
	},

	upgrade: function(userID) {
		$.ajax({
			url: GROUP.URL,
			type: 'POST',
			data: {
				'action': 'Upgrade',
				'group': this.group,
				'user': userID
			},
			dataType: 'json',
			success: GROUP.validationSuccess,
			error: GROUP.validationFailure
		});
	},

	downgrade: function(userID) {
		$.ajax({
			url: GROUP.URL,
			type: 'POST',
			data: {
				'action': 'Downgrade',
				'group': this.group,
				'user': userID
			},
			dataType: 'json',
			success: GROUP.validationSuccess,
			error: GROUP.validationFailure
		});
	},

	add: function(userID) {
		$.ajax({
			url: GROUP.URL,
			type: 'POST',
			data: {
				'action': 'Add',
				'group': this.group,
				'user': userID
			},
			dataType: 'json',
			success: GROUP.validationSuccess,
			error: GROUP.validationFailure
		});
	},

	remove: function(userID) {
		$.ajax({
			url: GROUP.URL,
			type: 'POST',
			data: {
				'action': 'Remove',
				'group': this.group,
				'user': userID
			},
			dataType: 'json',
			success: GROUP.validationSuccess,
			error: GROUP.validationFailure
		});
	},

	acceptRequest: function(userID) {
		$.ajax({
			url: GROUP.URL,
			type: 'POST',
			data: {
				'action': 'Accept',
				'group': this.group,
				'user': userID
			},
			dataType: 'json',
			success: GROUP.validationSuccess,
			error: GROUP.validationFailure
		});
	},

	declineRequest: function(userID) {
		$.ajax({
			url: GROUP.URL,
			type: 'POST',
			data: {
				'action': 'Decline',
				'group': this.group,
				'user': userID
			},
			dataType: 'json',
			success: GROUP.validationSuccess,
			error: GROUP.validationFailure
		});
	},

	/**
	 * Verarbeitet Erfolgreiche Antworten des Servers.
	 * @param response JSON Ausgabe des Servers.
	 */
	validationSuccess: function(response) {
		if (response.result.error !== undefined) {
			GROUP.validationFailure(response.result.query.action, response.result.error);
			return;
		}

		if (GROUP.retFuncSuccess != null) {
			//Verarbeitung
			var actionResult = new groupActionResult(response.result.query.action, TYPE_SUCCESS, null, response.result.query.user,
				response.result.data.userStatus);
			GROUP.retFuncSuccess(actionResult);
		}
	},

	/**
	 * Verarbeitet ein Problem bei der asynchronen Serverkommunikation.
	 * @param action Die ausgefuehrte Aktion.
	 * @param message JSON Ausgabe des Servers, falls vorhanden.
	 */
	validationFailure: function(action, message) {
		if (GROUP.retFuncFailure == null) {
			return;
		}

		if (typeof message != "string") {
			message = "Fehler bei der Kommunikation mit dem Webserver.";
		}

		var actionResult = new groupActionResult(action, TYPE_FAILRE, message, null, null);
		GROUP.retFuncFailure(actionResult);
	}

}

/**
 * @type {string} Erfolgreiche Anfrage.
 */
var TYPE_SUCCESS = "success";

/**
 * @type {string} Fehlerhafte Anfrage.
 */
var TYPE_FAILRE = "failure";

/**
 * Ergebnisobjekt einer Anfrage.
 * @param action Durchgefuehrte Aktion.
 * @param type Typ.
 * @param message Meldung.
 * @param userID betreffende UserID.
 * @param status Status der Anfrage.
 */
function groupActionResult(action, type, message, userID, status) {
	this.action = action;
	this.type = type;
	this.message = message;
	this.userID = userID;
	this.status = status;
}