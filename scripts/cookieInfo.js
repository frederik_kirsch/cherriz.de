var apiURL = "sources/api/user/user_operation.php";

/**
 * Schließt den CookieDialog und hinterlegt diese entweder im Nutzerprofil oder falls nicht angemeldet als Cookie.
 * @param infoDiv Der Div des Dialogs.
 * @param userID Die UserID oder null falls der Nutzer nicht angemeldet ist.
 */
function closeInfo(infoDiv, userID) {
	infoDiv.remove();

	if (userID != null) {
		$.ajax({
			url: apiURL,
			type: 'POST',
			data: {
				'action': 'changeCookieInfo',
				'user': userID,
				'value': true
			},
			dataType: 'json'
		});
	} else {
		var exdays = 30;
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		document.cookie = "CookieInfo=1; expires=" + d.toUTCString() + "; path=/";
	}
}