//@ sourceURL=genreselector_utility.js

function GenreSelector(idField, textField, selected) {

	var genres = [];

	var error = null;

	$.ajax({
		url: 'sources/api/settings/settings_operation.php',
		type: 'POST',
		data: {
			'action': 'getAllGenres'
		},
		dataType: 'json',
		success: function(response) {
			genres = response.result.data.genres;
			refreshSelected(selected)
		},
		error: function() { error = "Genres konnten nicht geladen werden."}
	});

	this.showSelectionDialog = function() {
		var content = "Wähle hier die passenden Genres und speichere deine Auswahl.<br />";

		if (error == null) {
			content += "<div id='dialog_genres' style=''>";
			$.each(genres, function(i, genre) {
				var checked = "";
				if ($.inArray(genre.id, selected) >= 0) {
					checked = "checked";
				}
				content += "<div class='field_value'><input type='checkbox' name='genres_cbx' value='" + genre.id + "' " + checked + "> " + genre.genre + "</div>"
			});
			content += "</div>";
		} else {
			content += error;
		}

		var dialog = new Dialog("Genres", content);
		dialog.setProperties({
			buttons: {
				"Speichern": function() {
					var selected = $("[name=genres_cbx]:checked").map(function () { return this.value; }).get();
					refreshSelected(selected);
					dialog.close();
				}
			}
		});
		dialog.open();
	};

	function refreshSelected(sel) {
		var ids = [];
		var text = [];
		$.each(genres, function(i, genre) {
			if ($.inArray(genre.id, sel) >= 0) {
				ids.push(genre.id);
				text.push(genre.genre);
			}
		});
		selected = ids;
		idField.val(ids.join(","));
		textField.val(text.join(", "));
	}

}