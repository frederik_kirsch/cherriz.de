<?php

/**
 * Class SubPageSettings
 * Konfigurationsobjekt fuer eine Unterseite.
 *
 * @author Frederik Kirsch
 */
class SubPageSettings {

	private $name;

	private $id;

	private $paras;

	/**
	 * SubPageSettings constructor.
	 *
	 * @param string $id ID der Seite.
	 * @param string $name anzuzeigender Name in der Navigationsleiste.
	 */
	private function __construct($id, $name) {
		$this->id = $id;
		$this->name = $name;
		$this->paras = "";
	}

	/**
	 * @param string $id   Klassenname
	 * @param string $name Anzeigename
	 *
	 * @return SubPageSettings
	 */
	public static function init($id, $name){
		return new SubPageSettings($id, $name);
	}

	/**
	 * @return string Der anzuzeigenden Name.
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return string ID der Seite.
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Übernimmt den GET-Parameter in die URL in der Seitennavigation.
	 * @param string $name Der Parameter.
	 *
	 * @return SubPageSettings
	 */
	public function addPara($name){
		return $this->addNewPara($name, $_REQUEST[$name]);
	}

	/**
	 * Setzt einen spezifischen Parameter in die URL in der Seitennavigation.
	 * @param string $name Der Parametername.
	 * @param string $value Der Parameterwert.
	 *
	 * @return $this
	 */
	public function addNewPara($name, $value){
		if($name != "" && $value != ""){
			$this->paras="&$name=$value";
		}
		return $this;
	}

	/**
	 * @return string Die Parameter als String.
	 */
	public function getParas() {
		return $this->paras;
	}

}