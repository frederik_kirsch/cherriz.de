<?php

/**
 * Class ProfileHA
 * Konfigurationsseite fuer Cherriz HA.
 * @author Frederik Kirsch
 */
class ProfileHA implements SubPage {

	private $db;

	private $login;

	private $user;

	public function __construct() {
		$this->db = DBConnect::getDBConnection();
		$this->login = Login::getInstance();

		//Anwender ermitteln
		$user = Filter::text($_GET['User']);
		if ($user == "") {
			$this->user = Login::getInstance()->getCurrentUserObject();
		} else {
			$this->user = new User($user);
		}
	}

	public function drawContent() {
		echo("<h2>Home Automation (beta): " . $this->user->get(User::PROPERTY_Nick) . "</h2>");
		echo("<p>Auf dieser Übersichtsseite kannst du die Konfiguration für dein Cherriz Home Automation System pflegen.</p>");

		$setup = $this->isSetUp();

		Toolkit::loadScripts(array("sites/profile_ha_utility.js", "ha_configuration.js"),
			$this->getChangeHandlerScript($setup));

		$visibility = "";
		if (!$setup) {
			echo("<span id='ha_setup' $visibility>");
			$this->drawSetUp();
			$visibility = "style='display: none'";
			echo("</span>");
		}

		echo("<span id='ha_config' $visibility>");
		$this->drawAuthenticationKey();
		$this->drawModules();
		$this->drawHowToUse();
		echo("</span>");
	}

	/**
	 * Zeigt den Setupdialog fuer die Konfiguration an.
	 */
	private function drawSetUp() {
		echo("<h3>Aktivieren</h3>");
		echo("<p>Für dein Profil ist Cherriz HA noch nicht aktivieren. Cherriz HA ist kostenlos befindet sich im Moment aber noch in einer frühen Betaphase. Bei interesse kannst du es einfach über die unten dargestellte Schaltfläche aktivieren.</p>");
		echo("<input type='button' value='Aktivieren' id='btnActivate'/>");
	}

	/**
	 * Zeigt den Authentifizierungsschluessel an.
	 */
	private function drawAuthenticationKey() {
		echo("<h3>Authentifizierung:</h3>");
		echo("<p>Um Cherriz Home Automation verwenden zu können benötigtst du einen eigenen Authentifizierungs- schlüssel. Mit dessen Hilfe lässt sich auf einem beliebigen Gerät dort deine privates Profil laden.</p>");
		echo("<div class='description_box'>
                <div class='description_attribute_box'>Schlüssel</div>
                <span id='authKey'></span> <input type='button' value='Neu generieren' id='btnAuthKey' />
			  </div>");
	}

	/**
	 * Zeigt die Konfigurationen fuer die Module an.
	 */
	private function drawModules() {
		echo("<h3>Module:</h3>
			  <p>Hier kannst du die verschiedenen, verfügbaren Module für Cherriz HA aktivieren oder deaktivieren sowie konfigurieren.</p>");
		echo("<h4>Timer</h4>
              <p>Der Timer bietet die Möglichkeit verschiedene Erinnerungen zu programmieren.</p>
			  <div class='description_box'>
                <div class='description_attribute_box'>Aktivieren</div>
                <input type='checkbox' id='ha_Timer' />
			  </div>");
		echo("<h4>Netatmo</h4>
              <p>Das Modul erlaubt es die eigene <a href='https://www.netatmo.com/'>Netatmo Wetterstation</a> in Cherriz HA einzubinden.</p>
			  <div class='description_box'>
                 <div class='description_attribute_box'>Aktivieren</div>
                 <input type='checkbox' id='ha_Netatmo' />
			  </div>
			  <div class='description_box netatmo_box' style='display: none'>
                 <div class='description_attribute_box'>Benutzername</div>
                 <input type='text' id='ha_Netatmo_user' />
			  </div>
			  <div class='description_box netatmo_box' style='display: none'>
                 <div class='description_attribute_box'>Passwort</div>
                 <input type='password' id='ha_Netatmo_pass' />
			  </div>
			  <div class='description_box netatmo_box' style='display: none'>
                 <div class='description_attribute_box'>&nbsp;</div>
                 <input type='button' id='ha_Netatmo_update' value='Zugangsdaten speichern' />
			  </div>");
	}

	/**
	 * Zeigt die Links zu Cherriz HA und Informationen zur Nutzung an.
	 */
	private function drawHowToUse() {
		echo("<h3>Verwendung:</h3>Cherriz HA befindet sich aktuell noch in einer sehr frühen Phase der Entwicklung. Primäres Ziel ist die Lauffähigkeit auf einem Raspberry Pi inkl. 7&quot; Touch Screen. Da die Clientseite als HTML5 JavaScript Anwendung umgesetzt wird kann sie allerdings prinzipiell in jeden Browser genutzt werden.<br />");
		echo("<div class='description_box'>
                <div class='description_attribute_box'>Browser</div>
                <a href='http://www.cherriz.de/ha'>Cherriz HA</a>
			  </div>
			  <div class='description_box'>
                <div class='description_attribute_box'>Raspberry Pi</div>
                <a href='http://www.cherriz.de/ha'>Cherriz HA</a>
			  </div>");
	}

	/**
	 * @return bool Ob Cherriz HA fuer den aktiven Benutzer bereits initialisiert ist.
	 */
	private function isSetUp() {
		LibImporter::import("api/ha/class.HomeAutomationAPI.php");
		$api = new HomeAutomationAPI($this->login->getCurrentUserObject()->get(User::PROPERTY_ID));
		$api->perform("isSetUp");
		$result = $api->getJSON();
		return $result[JSONGenerator::RESULT][JSONGenerator::DATA]["isSetUp"] == "true";
	}

	/**
	 * @return string Liefert den Authentifizierungsschluessel.
	 */
	private function getAuthKey() {
		LibImporter::import("api/ha/class.HomeAutomationAPI.php");
		$api = new HomeAutomationAPI($this->login->getCurrentUserObject()->get(User::PROPERTY_ID));
		$api->perform("getAuthKey");
		$result = $api->getJSON();
		return $result[JSONGenerator::RESULT][JSONGenerator::DATA]["authKey"];
	}

	/**
	 * Generiert das Scripts zur Kopplung der UI mit dem Backend.
	 * @param boolean $isSetup Ob Cherriz HA bereits initialisiert wurde.
	 * @return string Das Script.
	 */
	private function getChangeHandlerScript($isSetup) {
		$authKey = "null";
		if ($isSetup) {
			$authKey = "'" . $this->getAuthKey() . "'";
		}
		return "var config = new HAConfiguration($authKey);
				addVisibilityToggleToCbx($('#ha_Netatmo'), $('.netatmo_box'));
				setUpHA($('#btnActivate'), $('#ha_setup'), $('#ha_config'), STORE.get('UserID'), config);
				addRegenerateHandler($('#btnAuthKey'), config);
				saveNetatmoData($('#ha_Netatmo_update'), config);";
	}

}