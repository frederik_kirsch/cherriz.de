<?php

class ProfileOverview implements SubPage
{

    private $db;

    private $user;

    private $owner;

    public function __construct()
    {
        $this->db = DBConnect::getDBConnection();

        //Anwender ermitteln
        $user = Filter::text($_GET['User']);
        if ($user == "") {
            $this->user = Login::getInstance()->getCurrentUserObject();
        } else {
            $this->user = new User($user);
        }

        //Festlegen ob es sich um den Besitzer des Profils handelt
        $this->owner = false;
        if ($this->user->get(User::PROPERTY_ID) == Login::getInstance()->getCurrentUserObject()->get(User::PROPERTY_ID)) {
            $this->owner = true;
        }
    }

    public function drawContent()
    {
        echo("<h2>" . $this->user->get(User::PROPERTY_Nick) . "</h2>");

        $this->drawHead();
        $this->drawData();
        $this->drawGruppen();
    }

    private function drawHead()
    {
	    echo("<h3>Übersicht</h3>");
        echo("<img src='gfx/img_generator.php?type=User&size=" . UserKonstanten::AVATAR_BIG . "&id=" . $this->user->get(User::PROPERTY_ID) . "' width='200px' height='200px' alt='Avatar' id='profile_picture'/>");
        echo("<div class='description_box'>
				  <div class='description_attribute_box'>" . User::PROPERTY_Signatur . "</div>
				  <div class='description_value_box'>" . Converter::nullIfEmpty($this->user->get(User::PROPERTY_Signatur)) . "</div>
			  </div>");
    }

    private function drawData()
    {
        //Prüfen ob Daten angezeigt werden dürfen
        if (!$this->owner && $this->user->get(User::PROPERTY_Privatsphaere) == User::PRIVACY_PRIVATE) {
            return;
        }

        $data = array(array("Name", Converter::nullIfEmpty($this->user->get(User::PROPERTY_Vorname) . " " . $this->user->get(User::PROPERTY_Nachname))),
            array(User::PROPERTY_Geburtsdatum, Converter::nullIfEmpty(Converter::date($this->user->get(User::PROPERTY_Geburtsdatum)))),
            array(User::PROPERTY_Registriert, Converter::date($this->user->get(User::PROPERTY_Registriert))));

        //Daten für Limited
        if ($this->owner || $this->user->get(User::PROPERTY_Privatsphaere) == User::PRIVACY_PUBLIC) {
            $data = array_merge($data, array(array(User::PROPERTY_Email, $this->user->get(User::PROPERTY_Email)),
                array(User::PROPERTY_Twitter, $this->user->get(User::PROPERTY_Twitter) == "" ? Converter::nullIfEmpty($this->user->get(User::PROPERTY_Twitter)) : "<a href='https://twitter.com/" . $this->user->get(User::PROPERTY_Twitter) . "' target='_blank'>" . $this->user->get(User::PROPERTY_Twitter) . "</a>"),
                array(User::PROPERTY_Telefon, Converter::nullIfEmpty($this->user->get(User::PROPERTY_Telefon))),
                array(User::PROPERTY_Handy, Converter::nullIfEmpty($this->user->get(User::PROPERTY_Handy))),
                array("Adresse", $this->user->getAdressText())));
        }

        for ($i = 0; $i < count($data); $i++) {
            echo("<div class='description_box'>
					<div class='description_attribute_box'>" . $data[$i][0] . "</div>
					<div class='description_value_box'>" . $data[$i][1] . "</div>
				  </div>");
        }
    }

    private function drawGruppen()
    {
        //Prüfen ob Gruppen angezeigt werden dürfen
        if (!$this->owner && $this->user->get(User::PROPERTY_Privatsphaere) == User::PRIVACY_PRIVATE) {
            return;
        }

        echo("<h3>Gruppen</h3>");
        //Daten abrufen
        $query = "SELECT G.ID
					FROM Gruppe G
			  INNER JOIN Gruppe_Mitglied GM
			  		  ON G.ID = GM.Gruppe
			  	   WHERE GM.User = " . $this->user->get(User::PROPERTY_ID) . "
			  	ORDER BY G.Name";
        $result = $this->db->query($query);
        if ($result->num_rows == 0) {
            echo("<ul>
					<li>" . $this->user->get(User::PROPERTY_Nick) . " ist im Moment nicht Mitglied einer Gruppe.</li>
				  </ul>");
        } else {
            while ($obj = $result->fetch_object()) {
                $gruppe = new Group($obj->ID);
                $gruppe->drawGroupMedium();
            }
        }
    }

}