<?php

class ProfileSettings implements SubPage
{

    private $db;

    private $user;

    public function __construct()
    {
        $this->db = DBConnect::getDBConnection();

        //Anwender ermitteln
        $user = Filter::text($_GET['User']);
        if ($user == "") {
            $this->user = Login::getInstance()->getCurrentUserObject();
        } else {
            $this->user = new User($user);
        }
    }

    public function drawContent()
    {
        echo("<h2>Einstellungen: " . $this->user->get(User::PROPERTY_Nick) . "</h2>");

        Toolkit::loadScripts(array("jquery/jquery.form.js", "sites/profile_utility.js", "user.js"), $this->getChangeHandlerScript());
        $this->drawPrivacy();
        $this->drawNotification();
        $this->drawAdvanced();
    }

    private function drawPrivacy()
    {
        echo("<h3>Privatssphäre</h3>");
        echo("Definieren Sie hier, ihre Privatsphäreeinstellungen.");
        $privacy = $this->user->get(User::PROPERTY_Privatsphaere);
        $public = "";
        $limited = "";
        $private = "";
        if (User::PRIVACY_PUBLIC == $privacy) {
            $public = "selected";
        } else if (User::PRIVACY_LIMITED == $privacy) {
            $limited = "selected";
        } else {
            $private = "selected";
        }
        echo("<div class='description_box'>
                <div class='description_attribute_box'>Privatsphäre</div>
				<select id='" . User::PROPERTY_Privatsphaere . "'>
                    <option $public>" . User::PRIVACY_PUBLIC . "</option>
                    <option $limited>" . User::PRIVACY_LIMITED . "</option>
                    <option $private>" . User::PRIVACY_PRIVATE . "</option>
                </select>
			 </div><br />
			 <b>" . User::PRIVACY_PUBLIC . ":</b> Das Profil ist öffentlich zugänglich. Alle Informationen können von dritten betrachtet werden.<br />
             <b>" . User::PRIVACY_LIMITED . ":</b> Das Profil ist eingeschränkt zugänglich. Kontaktinformationen werden nicht angezeigt.<br />
             <b>" . User::PRIVACY_PRIVATE . ":</b> Das Profil ist als privat markiert. Nur Avatar, Nick und Signatur werden angezeigt. Auch Gruppen werden ausgeblendet.");
    }

    private function drawNotification()
    {
        echo("<h3>Benachrichtigung</h3>");
        echo("Definieren Sie hier, über welche Neuerungen Sie informiert werden möchten.<br />");
	    $newsChecked = "";
        if ($this->user->get(User::PROPERTY_Notification_News) == "1") {
            $newsChecked = "checked";
        }
        echo("<div class='description_box'>
                <div class='description_attribute_box'>News</div>
                <input type='checkbox' id='" . User::PROPERTY_Notification_News . "' " . $newsChecked . " /> Über neue News informieren.
			 </div>");
	    $commentChecked = "";
        if ($this->user->get(User::PROPERTY_Notification_Comment) == "1") {
            $commentChecked = "checked";
        }
        echo("<div class='description_box'>
                <div class='description_attribute_box'>Kommentar</div>
                <input type='checkbox' id='" . User::PROPERTY_Notification_Comment . "' " . $commentChecked . " /> Über neue Kommentare zu News informieren, die ich kommentiert habe.
			 </div>");
	    $postChecked = "";
	    if ($this->user->get(User::PROPERTY_Notification_Threadpost) == "1") {
		    $postChecked = "checked";
	    }
	    echo("<div class='description_box'>
                <div class='description_attribute_box'>Forumposts</div>
                <input type='checkbox' id='" . User::PROPERTY_Notification_Threadpost . "' " . $postChecked . " /> Über neue Beiträge in Forenthreads informieren, in denen ich aktiv war.
			 </div>");
        echo("Definiere Sie hier, auf welche Art Sie informiert werden möchten.<br />");
	    $typeMailChecked = "";
        if ($this->user->get(User::PROPERTY_Notification_Type_Email) == "1") {
            $typeMailChecked = "checked";
        }
        echo("<div class='description_box'>
                <div class='description_attribute_box'>E-Mail</div>
                <input type='checkbox' id='" . User::PROPERTY_Notification_Type_Email . "' " . $typeMailChecked . " /> Über E-Mail informieren.
			 </div>");
	    $typeTwitterChecked = "";
        if ($this->user->get(User::PROPERTY_Notification_Type_Twitter) == "1") {
            $typeTwitterChecked = "checked";
        }
        echo("<div class='description_box'>
                <div class='description_attribute_box'>Twitter</div>
                <input type='checkbox' id='" . User::PROPERTY_Notification_Type_Twitter . "' " . $typeTwitterChecked . " /> Über Twitter (Direct Message) informieren.
			 </div>");
    }

    private function drawAdvanced(){
        echo("<h3>Erweitert</h3>");
        echo("Spezifizieren Sie hier zusätzliche Funktionen.<br />");
	    $experimentalChecked = "";
        if ($this->user->get(User::PROPERTY_Use_Experimental) == "1") {
            $experimentalChecked = "checked";
        }
        echo("<div class='description_box'>
                <div class='description_attribute_box'>Beta</div>
                <input type='checkbox' id='" . User::PROPERTY_Use_Experimental . "' " . $experimentalChecked . " /> Beta Funktionen ausprobieren.
			 </div>");
    }

    private function getChangeHandlerScript()
    {
        $nComment = "false";
	    $nThreadPost = "false";
        $nNews = "false";
        $nTypeMail = "false";
        $nTypeTwitter = "false";
        $nTypeExperimental = "false";
        if ($this->user->get(User::PROPERTY_Notification_News) == "1") {
            $nNews = "true";
        }
        if ($this->user->get(User::PROPERTY_Notification_Comment) == "1") {
            $nComment = "true";
        }
	    if ($this->user->get(User::PROPERTY_Notification_Threadpost) == "1") {
		    $nThreadPost = "true";
	    }
        if ($this->user->get(User::PROPERTY_Notification_Type_Email) == "1") {
            $nTypeMail = "true";
        }
        if ($this->user->get(User::PROPERTY_Notification_Type_Twitter) == "1") {
            $nTypeTwitter = "true";
        }
        if ($this->user->get(User::PROPERTY_Use_Experimental) == "1") {
            $nTypeExperimental = "true";
        }
        return "USER.setReturnFunction(processSettingResult);
                STORE.saveAssoc('" . User::PROPERTY_Privatsphaere . "', '" . $this->user->get(User::PROPERTY_Privatsphaere) . "');
                STORE.saveAssoc('" . User::PROPERTY_Notification_News . "', '" . $nNews . "');
                STORE.saveAssoc('" . User::PROPERTY_Notification_Comment . "', '" . $nComment . "');
                STORE.saveAssoc('" . User::PROPERTY_Notification_Threadpost . "', '" . $nThreadPost . "');
                STORE.saveAssoc('" . User::PROPERTY_Notification_Type_Email . "', '" . $nTypeMail . "');
                STORE.saveAssoc('" . User::PROPERTY_Notification_Type_Twitter . "', '" . $nTypeTwitter . "');
                STORE.saveAssoc('" . User::PROPERTY_Use_Experimental . "', '" . $nTypeExperimental . "');

                $('#" . User::PROPERTY_Privatsphaere . "').change(handleSettingsChange);
                $('#" . User::PROPERTY_Notification_News . "').change(handleSettingsChange);
                $('#" . User::PROPERTY_Notification_Comment . "').change(handleSettingsChange);
                $('#" . User::PROPERTY_Notification_Threadpost . "').change(handleSettingsChange);
                $('#" . User::PROPERTY_Notification_Type_Email . "').change(handleSettingsChange);
                $('#" . User::PROPERTY_Notification_Type_Twitter . "').change(handleSettingsChange);
                $('#" . User::PROPERTY_Use_Experimental . "').change(handleSettingsChange);";
    }

}