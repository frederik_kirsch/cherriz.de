<?php

/**
 * Class ProfileData
 * Stellt die Daten des Benutzers zur Aenderung dar.
 *
 * @author Frederik Kirsch
 */
class ProfileData implements SubPage {

	private $user;

	private $message;

	/**
	 * Ermittelt den aktuellen Benutzer.
	 */
	public function __construct() {
		$user = Filter::text($_GET['User']);
		if ($user == "") {
			$this->user = Login::getInstance()->getCurrentUserObject();
		} else {
			$this->user = new User($user);
		}
	}

	public function drawContent() {
		echo("<h2>Daten: " . $this->user->get(User::PROPERTY_Nick) . "</h2>");

		$this->drawMessage($this->message);

		$this->drawProfile();
		$this->drawPersonalData();

		Toolkit::loadScripts(array("jquery/jquery.form.js", "sites/profile_utility.js", "user.js"), $this->getChangeHandlerScript());
	}

	/**
	 * Stellt die Daten des nach außen sichtbaren Profils dar.
	 */
	private function drawProfile() {
		echo("<h3>Profil</h3>
              <div class='description_box'>
                  <img src='gfx/img_generator.php?type=User&size=" . UserKonstanten::AVATAR_BIG . "&id=" . $this->user->get(User::PROPERTY_ID) . "' width='200px' height='200px' style='padding-right: 10px' alt='Avatar' id='Avatar_Big'/>
                  <img src='gfx/img_generator.php?type=User&size=" . UserKonstanten::AVATAR_MEDIUM . "&id=" . $this->user->get(User::PROPERTY_ID) . "' width='100px' height='100px' style='padding-right: 10px; padding-top: 100px' alt='Avatar' id='Avatar_Medium'/>
                  <img src='gfx/img_generator.php?type=User&size=" . UserKonstanten::AVATAR_SMALL . "&id=" . $this->user->get(User::PROPERTY_ID) . "' width='20px' height='20px' style='padding-right: 10px; padding-top: 180px' alt='Avatar' id='Avatar_Small'/>
              </div>
              <div class='description_box'>
                  <div class='description_attribute_box'>Avatar</div>
                  <form id='AvatarForm' enctype='multipart/form-data'>
                      <input type='file' class='input_passive' accept='image/png, image/jpeg' id='Avatar' name='value' />
                  </form>
              </div>
              <div class='description_box'>
                  <div class='description_attribute_box'>" . User::PROPERTY_Signatur . "</div>
                  <input type='text' class='input_passive' id='" . User::PROPERTY_Signatur . "' value='" . $this->user->get(User::PROPERTY_Signatur) . "' />
              </div>");
	}

	/**
	 * Stellt die persoenlichen Daten dar.
	 */
	private function drawPersonalData() {
		echo("<h3>Persönliche Daten</h3>");
		//Name
		$name = $this->user->get(User::PROPERTY_Vorname) . " " . $this->user->get(User::PROPERTY_Nachname);
		echo("<div class='description_box'>
                <div class='description_attribute_box'>Name</div>
				<input type='text' class='input_passive' id='Name' value='" . $name . "' />
			 </div>");
		//Geburtstag
		$geburtstag = Converter::date($this->user->get(User::PROPERTY_Geburtsdatum));
		echo("<div class='description_box'>
                <div class='description_attribute_box'>" . User::PROPERTY_Geburtsdatum . "</div>
				<input type='text' class='input_passive' id='" . User::PROPERTY_Geburtsdatum . "' value='" . $geburtstag . "' />
			 </div>");
		$minDate = date("d.m.Y", strtotime("-100 year"));
		$maxDate = date("d.m.Y", strtotime("-10 year"));
		Toolkit::printDatePickerScripts(User::PROPERTY_Geburtsdatum, false, $minDate, $maxDate);
		//Email
		$mail = $this->user->get(User::PROPERTY_Email);
		echo("<div class='description_box'>
                <div class='description_attribute_box'>" . User::PROPERTY_Email . "</div>
				<input type='text' class='input_passive' id='" . User::PROPERTY_Email . "' value='" . $mail . "' />
			 </div>");
		//Twitter
		$twitter = $this->user->get(User::PROPERTY_Twitter);
		echo("<div class='description_box'>
                <div class='description_attribute_box'>" . User::PROPERTY_Twitter . "</div>
				<input type='text' class='input_passive' id='" . User::PROPERTY_Twitter . "' value='" . $twitter . "' />
			 </div>");
		//Telefon
		$telefon = $this->user->get(User::PROPERTY_Telefon);
		echo("<div class='description_box'>
                <div class='description_attribute_box'>" . User::PROPERTY_Telefon . "</div>
				<input type='text' class='input_passive' id='" . User::PROPERTY_Telefon . "' value='" . $telefon . "' />
			 </div>");
		//Handy
		$handy = $this->user->get(User::PROPERTY_Handy);
		echo("<div class='description_box'>
                <div class='description_attribute_box'>" . User::PROPERTY_Handy . "</div>
				<input type='text' class='input_passive'  id='" . User::PROPERTY_Handy . "' value='" . $handy . "' />
			 </div>");
		//Strasse
		$strasse = $this->user->get(User::PROPERTY_Strasse);
		echo("<div class='description_box'>
                <div class='description_attribute_box'>Adresse</div>
				<input type='text' class='input_passive' id='" . User::PROPERTY_Strasse . "' value='" . $strasse . "' />
			 </div>");
		//Ort
		$ort = $this->user->get(User::PROPERTY_PLZ) . " " . $this->user->get(User::PROPERTY_Ort);
		echo("<div class='description_box'>
                <div class='description_attribute_box'>&nbsp;</div>
				<input type='text' class='input_passive' id='Ort'  value='" . $ort . "' />
			 </div>");
	}

	/**
	 * @return string Die Scripts, welche die Verarbeitung der Benutzereingaben ausloesen.
	 */
	private function getChangeHandlerScript() {
		return "USER.setReturnFunction(processResult);
                STORE.saveAssoc('" . User::PROPERTY_Signatur . "', '" . $this->user->get(User::PROPERTY_Signatur) . "');
                STORE.saveAssoc('Name', '" . $this->user->get(User::PROPERTY_Vorname) . " " . $this->user->get(User::PROPERTY_Nachname) . "');
                STORE.saveAssoc('" . User::PROPERTY_Geburtsdatum . "', '" . Converter::date($this->user->get(User::PROPERTY_Geburtsdatum)) . "');
                STORE.saveAssoc('" . User::PROPERTY_Email . "', '" . $this->user->get(User::PROPERTY_Email) . "');
                STORE.saveAssoc('" . User::PROPERTY_Twitter . "', '" . $this->user->get(User::PROPERTY_Twitter) . "');
                STORE.saveAssoc('" . User::PROPERTY_Telefon . "', '" . $this->user->get(User::PROPERTY_Telefon) . "');
                STORE.saveAssoc('" . User::PROPERTY_Handy . "', '" . $this->user->get(User::PROPERTY_Handy) . "');
                STORE.saveAssoc('" . User::PROPERTY_Strasse . "', '" . $this->user->get(User::PROPERTY_Strasse) . "');
                STORE.saveAssoc('Ort', '" . $this->user->get(User::PROPERTY_PLZ) . " " . $this->user->get(User::PROPERTY_Ort) . "');

                $('#Avatar').focus(handleChange);
                $('#" . User::PROPERTY_Signatur . "').focus(handleChange);
                $('#Name').focus(handleChange);
                $('#" . User::PROPERTY_Geburtsdatum . "').focus(handleChange);
                $('#" . User::PROPERTY_Email . "').focus(handleChange);
                $('#" . User::PROPERTY_Twitter . "').focus(handleChange);
                $('#" . User::PROPERTY_Telefon . "').focus(handleChange);
                $('#" . User::PROPERTY_Handy . "').focus(handleChange);
                $('#" . User::PROPERTY_Strasse . "').focus(handleChange);
                $('#Ort').focus(handleChange);";
	}

	public function setMessage($message) {
		$this->message = $message;
	}

	/**
	 * @param string $message Anzuzeigende Meldungen
	 */
	private function drawMessage($message) {
		if ($message == null) {
			return;
		}
		echo("<ul><li style='color: #f40000;'>" . $message . "</li></ul>");
	}

}