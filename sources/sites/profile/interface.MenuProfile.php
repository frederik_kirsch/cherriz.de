<?php

interface MenuProfile {

	const OVERVIEW = "ProfileOverview";

	const DATA     = "ProfileData";

	const SETTINGS = "ProfileSettings";

    const HA       = "ProfileHA";

}