<?php

LibImporter::import("sites/profile/interface.MenuProfile.php");

/**
 * Class Profile
 * Stellt das Profil eines Benutzers dar.
 *
 * @author Frederik Kirsch
 */
class Profile extends Page {

	private $login = null;

    private $user = null;

	public function __construct() {
        $this->login = Login::getInstance();
        $this->user = $this->login->getCurrentUserObject();
		parent::__construct();
		$this->performMailChange();
	}

	protected function getNavBarEntries() {
		$pages = array();
		array_push($pages, SubPageSettings::init(MenuProfile::OVERVIEW, "Übersicht")->addPara("user"));
		array_push($pages, SubPageSettings::init(MenuProfile::DATA, "Daten")->addPara("user"));
		array_push($pages, SubPageSettings::init(MenuProfile::SETTINGS, "Einstellungen")->addPara("user"));
        if ($this->user->get(User::PROPERTY_Use_Experimental) == "1") {
            array_push($pages, SubPageSettings::init(MenuProfile::HA, "Home Automation")->addPara("user"));
        }
		return $pages;
	}

	public function requiresLogin() {
		return true;
	}

	public function hasAccess() {
		return Login::getInstance()->isLoggedIn();
	}

	public function hasNavBar() {
		return $this->isOwner();
	}

	/**
	 * @return bool true wenn es sich um das Profil des aktiven Benutzers handelt, sonst false.
	 */
	private function isOwner() {
		$profileUser = Filter::text($_GET['user']);
		if ($profileUser == null) {
			$profileUser = $this->user->get(User::PROPERTY_ID);
		}
		if ($profileUser == $this->user->get(User::PROPERTY_ID)) {
			return true;
		}
		return false;
	}

	/**
	 * Fuehrt die Aenderung der Emailadresse durch.
	 */
	private function performMailChange() {
		//Prüfen ob Mailänderung
		$key = Filter::text($_GET['key']);
		$page = $_GET['page'];
		if ($key == "" || !$this->isOwner() || $page != MenuProfile::DATA) {
			return;
		}
		$query = "SELECT COUNT(*) AS Anzahl FROM Emailbestaetigung EB INNER JOIN Bestaetigungen B ON B.ID = EB.Bestaetigung WHERE B.Used IS NULL AND B.Key = '$key'";
		$db = DBConnect::getDBConnection();
		$dbRet = $db->query($query)->fetch_object();
		if ($dbRet->Anzahl == 0) {
			return;
		}

		//Mailänderung
		LibImporter::import("api/user/class.UserAPI.php");
		$api = new UserAPI($this->login->getCurrentUserObject()->get(User::PROPERTY_ID), $key);
		$api->perform("confirmMail");
		$result = $api->getJSON();
		$message = $result[JSONGenerator::RESULT][JSONGenerator::ERROR]["message"];
		if ($message == null) {
			//User neu Laden
			$this->login->getCurrentUserObject(true);
			$message = $result[JSONGenerator::RESULT][JSONGenerator::DATA]["message"];
		}
		$this->setSubPageProperty("message", $message);
	}

}