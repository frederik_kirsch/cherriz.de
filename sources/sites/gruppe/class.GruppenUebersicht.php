<?php
LibImporter::import("sites/gruppe/internal/class.GroupUtility.php");

/**
 * Class GruppenUebersicht
 * Stellt die Uebersicht ueber die verschiedenen Gruppen dar.
 *
 * @author Frederik Kirsch
 */
class GruppenUebersicht extends Page {

	public function __construct() {
		parent::__construct();
		$this->processGroupCreation();
	}

	public function requiresLogin() {
		return true;
	}

	public function hasAccess() {
		return Login::getInstance()->isLoggedIn();
	}

	public function hasNavBar() {
		return true;
	}

	protected function getNavBarEntries() {
		$pages = array();
		array_push($pages, SubPageSettings::init(MenuGruppen::OVERVIEW, "Gruppen Übersicht"));
		array_push($pages, SubPageSettings::init(MenuGruppen::CREATE, "Gruppe Erstellen"));
		return $pages;
	}

	/**
	 * Prueft ob eine neue Gruppe erstellt werden soll, veranlast die Validierung und Gruppenerzeugung. Stellt die Ergebnisse darüber hinaus auf der
	 * UI dar.
	 */
	private function processGroupCreation() {
		if (Filter::text($_POST['grp_submitted']) == true && $this->determineSubPage() != MenuGruppe::SETTINGS) {
			$result = $this->createGroup();
			if (!is_numeric($result)) {
				$name = Filter::text($_POST['grp_name']);
				$desc = Filter::text($_POST['grp_desc']);
				$this->setSubPageProperty("messages", $result);
				$this->setSubPageProperty("name", $name);
				$this->setSubPageProperty("description", $desc);
				$this->redirect(MenuGruppen::CREATE);
			}
		}
	}

	/**
	 * Legt eine neu Gruppe aus den übergebenen GET Parameter an.
	 * @return array|integer ID der neuen Gruppe oder Fehlermeldungen.
	 */
	private function createGroup() {
		//Globale Hilfsobjekte beziehen.
		$verify = $GLOBALS['VERIFY'];
		$user = Login::getInstance()->getCurrentUserObject();
		$db = DBConnect::getDBConnection();
		$dbBin = DBConnect::getDBConnection(DBConnect::CHARSET_UTF8_BIN);

		//Temporäre Variablen aufbauen
		$messages = array();
		$grp_id = null;

		//Formularparameter abrufen
		$name = Filter::text($_POST['grp_name']);
		$picture = Filter::file($_FILES['grp_pic']);
		$beschreibung = Filter::text($_POST['grp_desc']);

		//Validierung
		//Gruppenname
		if (strlen(trim($name)) == 0) {
			$messages[] = "Es wurde kein Name angegeben.";
		} elseif (strlen(trim($name)) < 3) {
			$messages[] = "Der Gruppenname ist zu kurz.";
		} else {
			$result = $db->query("SELECT COUNT(*) AS Anzahl FROM Gruppe WHERE Name = '$name'");
			$erg = $result->fetch_object();
			if ($erg->Anzahl > 0) {
				$messages[] = "Eine Gruppe mit diesem Namen besteht bereits.";
			}
		}
		//Logo
		$validation = $verify->checkAvatar($picture);
		if ($validation != Verify::$VALIDE) {
			$messages[] = $validation;
		}

		//Beschreibung
		if (strlen(trim($beschreibung)) == 0) {
			$messages[] = "Es wurde kein Beschreibung angegeben.";
		} elseif (strlen(trim($beschreibung)) < 50) {
			$messages[] = "Der Beschreibung ist zu kurz.";
		}

		//In DB Schreiben
		if (sizeof($messages) == 0) {
			//Gruppe
			LibImporter::import("basic/util/class.Picture.php");
			$query = "INSERT INTO Gruppe
                      (Name, Beschreibung)
                      VALUES
                      ('$name', '$beschreibung')";
			if ($db->query($query)) {
				$grp_id = $db->insert_id;
				$query = "INSERT INTO Gruppe_Mitglied
                          (User, Gruppe, Rang)
                          VALUES
                          (" . $user->get(User::PROPERTY_ID) . ", $grp_id, (SELECT ID FROM Gruppe_Rang WHERE Rang = '" . Group::RANG_VERWALTER . "'))";
				$picBig = Picture::getResizedPicture($picture, 200, 200, "image/jpg");
				$picMid = Picture::getResizedPicture($picture, 100, 100, "image/jpg");
				$picSmall = Picture::getResizedPicture($picture, 20, 20, "image/jpg");
				$queryBin = "UPDATE Gruppe SET Logo_Big = '$picBig', Logo_Medium = '$picMid', Logo_Small = '$picSmall' WHERE ID = $grp_id";
				if (!$db->query($query) || !$dbBin->query($queryBin)) {
					$db->query("DELETE FROM Gruppe WHERE ID = $grp_id");
					$messages[] = "Beim persistieren der Gruppe ist ein Problem aufgetreten.";
				}
			} else {
				$messages[] = "Beim persistieren der Gruppe ist ein Problem aufgetreten.";
			}
		}

		//Visuelle Ausgabe
		if (sizeof($messages) == 0) {
			return $grp_id;
		}
		return $messages;
	}

}