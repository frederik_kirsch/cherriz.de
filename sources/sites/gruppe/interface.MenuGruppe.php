<?php

interface MenuGruppe {

	const OVERVIEW = "GruppeOverview";

	const SETTINGS = "GruppeSettings";

	const MEMBER   = "GruppeMember";

}