<?php

/**
 * Class GroupUtility
 * Stellt von den verschiedenen Gruppenseiten gemeinsam verwendete Funktionen zur verfuegung.
 *
 * @author Frederik Kirsch
 */
abstract class GroupUtility {

    private static $gruppe = null;

    private static $userStatus = null;

    public static function setCurrentGroup($id) {
        GroupUtility::resetGroup();
        GroupUtility::$gruppe = new Group($id);
    }

	/**
	 * @return Group Die aktuell dargestellte Gruppe.
	 */
    public static function getCurrentGroup() {
        if (GroupUtility::$gruppe == null) {
            $grpID = Filter::text($_REQUEST['group']);
            if (is_numeric($grpID)) {
                GroupUtility::$gruppe = new Group($grpID);
            }
        }
        return GroupUtility::$gruppe;
    }

    public static function resetGroup() {
        GroupUtility::$gruppe = null;
        GroupUtility::$userStatus = null;
    }

    public static function getUserStatus() {
        if (GroupUtility::$userStatus == null) {
            $user = $user = Login::getInstance()->getCurrentUserObject();
            $curGroup = GroupUtility::getCurrentGroup();
            if ($curGroup != null) {
                GroupUtility::$userStatus = $curGroup->getUserStatus($user->get(User::PROPERTY_ID));
            }
        }
        return GroupUtility::$userStatus;
    }

    /**
     * Stellt Meldungen dar.
     *
     * @param $messages array Anzuzeigende Meldungen
     */
    public static function drawMessages($messages) {
        if ($messages == null) {
            return;
        }
        echo("<ul>");
        foreach ($messages as $item) {
            echo("<li style='color: #f40000;'>" . $item . "</li>");
        }
        echo("</ul>");
    }

    public static function drawGroupForm($url, $id, $name, $desc) {
        $img = "";
        if($id != null){
            $img = "<img src='gfx/img_generator.php?type=Gruppe&id=" . $id . "&size=small' style='float: left; padding-right: 5px;'/>";
        }
        echo("<form action='" . $url . "' method='post' enctype='multipart/form-data' id='grp_form'>
                  <input type='hidden' name='grp_submitted' id='grp_submitted' value='true' />
                  <div class='description_box'>
                      <div class='description_attribute_box'>Name</div>
                      <div class='description_value_box'>
                          <input type='text' name='grp_name' id='grp_name' value='" . $name . "' />
                      </div>
                  </div>
                  <div class='description_box'>
                      <div class='description_attribute_box'>Logo</div>
                      <div class='description_value_box'>
                          " . $img . "<input type='file' class='input_passive' accept='image/png, image/jpeg' name='grp_pic' id='grp_pic' />
                      </div>
                  </div>
                  <div class='description_box'>
                      <div class='description_attribute_box'>Beschreibung</div>
                      <div class='description_value_box'>
                          <textarea name='grp_desc' id='grp_desc' style='width: 450px; height: 70px' maxlength='250'>" . $desc . "</textarea>
                      </div>
                  </div>
                  <div class='description_box'>
                      <div class='description_attribute_box'>Aktion</div>
                      <div class='description_value_box'>
                          <input type='submit' value='Speichern' />
                      </div>
                  </div>
              </form>");
    }

}