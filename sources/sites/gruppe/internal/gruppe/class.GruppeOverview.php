<?php

/**
 * Class GruppeOverview
 * Stellt eine Uebersicht ueber eine Gruppe dar.
 *
 * @author Frederik Kirsch
 */
class GruppeOverview implements SubPage {

    private $gruppe = null;

    public function __construct(){
        $this->gruppe = GroupUtility::getCurrentGroup();
    }

    public function drawContent() {
        echo("<h2>Gruppe: " . $this->gruppe->getName() . "</h2>");
        echo("<h3>Beschreibung</h3>");
        echo("<div id='grp_desc'>");
        echo("<img src='gfx/img_generator.php?type=Gruppe&id=" .
             $this->gruppe->getID() .
             "&size=medium' style='float:left; padding-right: 10px;  padding-bottom: 10px;'/>");
        echo(Converter::bbToHTML($this->gruppe->getBeschreibung()));
        echo("</div>");

        //Mitgliederdaten abrufen
        $groupMember = $this->gruppe->getGroupMember();

        /*Verwalter, Vertreter und Gruppendaten*/
        echo("<h3>Verwalter</h3>");
        echo($groupMember->getVerwalter()->drawUserSmall());
        echo("<h3>Vertreter</h3>");
        if (sizeof($groupMember->getVertreter()) == 0) {
            echo("<ul><li>Dieser Gruppe sind keine Vertreter zugeordnet.</li></ul>");
        } else {
            foreach ($groupMember->getVertreter() as $usr) {
                echo($usr->drawUserSmall());
            }
        }

        //Mitglieder im Allgemeinen
        echo("<h3>Mitglieder</h3>");
        if (sizeof($groupMember->getMember()) == 0) {
            echo("<ul><li>Dieser Gruppe hat leider noch keine Mitglieder.</li></ul>");
        } else {
            foreach ($groupMember->getMember() as $usr) {
                echo($usr->drawUserSmall());
            }
        }
    }

}