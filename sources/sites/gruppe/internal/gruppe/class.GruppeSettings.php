<?php

/**
 * Class GruppeSettings
 * Stellt die Seite fuer die Gruppeneinstellungen dar.
 *
 * @author Frederik Kirsch
 */
class GruppeSettings implements SubPage {

    private $db = null;

	private $dbBin = null;

    private $gruppe = null;

    private $messages = array();

    public function __construct() {
        $this->db = DBConnect::getDBConnection();
	    $this->dbBin = DBConnect::getDBConnection(DBConnect::CHARSET_UTF8_BIN);
        $this->gruppe = GroupUtility::getCurrentGroup();

        $submitted = Filter::text($_POST["grp_submitted"]);
        if ($submitted == "true") {
            $this->messages = $this->performChange();
            GroupUtility::resetGroup();
            $this->gruppe = GroupUtility::getCurrentGroup();
        }

        Toolkit::loadScripts(array("sites/gruppe_settings_utility.js", "group.js"), "$('#grp_form').submit(createValidator(false));");
    }

    public function drawContent() {
        echo("<h2>Einstellungen: " . $this->gruppe->getName() . "</h2>");
	    GroupUtility::drawMessages($this->messages);
        echo("Hier kannst du die Gruppe " . $this->gruppe->getName() . " bearbeiten.<br />");
        $submitURL = "index.php?page=" . MenuGruppe::SETTINGS . "&group=" . $this->gruppe->getID();
        GroupUtility::drawGroupForm($submitURL, $this->gruppe->getID(), $this->gruppe->getName(), $this->gruppe->getBeschreibung());
    }

    /**
     * Speichert die Aenderungen an der Gruppe.
     * TODO: In Ajax API überführen
     */
    private function performChange() {
        $messages = array();
        $userStatus = GroupUtility::getUserStatus();
        if ($userStatus == Group::RANG_VERTRETER || $userStatus == Group::RANG_VERWALTER) {
            $verify = $GLOBALS['VERIFY'];
            //Temporäre Variablen aufbauen
            $inserts = array();
            $insertBin = array();

            //Formularparameter abrufen
            $name = null;
            if (Filter::text($_POST['grp_name']) != $this->gruppe->getName() && strlen(Filter::text($_POST['grp_name'])) >= 3) {
                $result = $this->db->query("SELECT COUNT(*) AS Anzahl FROM Gruppe WHERE Name = '" . $name."'");
                $erg = $result->fetch_object();
                if($erg->Anzahl > 0){
                    $messages[] = "Eine Gruppe mit diesem Namen besteht bereits.";
                }else{
                    $name = Filter::text($_POST["grp_name"]);
                }
            }
            $picture = null;
            if ($_FILES['grp_pic']['name'] != "") {
                $picture = Filter::file($_FILES['grp_pic']);
            }
            $beschreibung = null;
            if (Filter::text($_POST['grp_desc']) != $this->gruppe->getBeschreibung() && strlen(Filter::text($_POST['grp_desc'])) >= 50
            ) {
                $beschreibung = Filter::text($_POST['grp_desc']);
            }

            //Validierung
            //Logo
            if ($picture != null) {
                $validation = $verify->checkAvatar($picture);
                if ($validation == Verify::$VALIDE) {

                    LibImporter::import("basic/util/class.Picture.php");
	                $insertBin[] = "Logo_Big = '" . Picture::getResizedPicture($picture, 200, 200, "image/jpg") . "'";
	                $insertBin[] = "Logo_Medium = '" . Picture::getResizedPicture($picture, 100, 100, "image/jpg") . "'";
	                $insertBin[] = "Logo_Small = '" . Picture::getResizedPicture($picture, 20, 20, "image/jpg") . "'";
                } else {
                    $messages[] = $validation;
                }
            }
            //Beschreibung
            if ($beschreibung != null) {
                $inserts[] = "Beschreibung = '" . $beschreibung . "'";
            }
            //Name
            if ($name != null) {
                $inserts[] = "Name = '" . $name . "'";
            }

            //In DB Schreiben
            if (sizeof($messages) == 0) {
                //Gruppe
	            $insResult = true;
	            if(sizeof($inserts) > 0){
		            $insResult = $this->db->query("UPDATE Gruppe SET " . implode(", ", $inserts) . " WHERE ID = " . $this->gruppe->getID());
	            }
	            $insBinResult = true;
	            if(sizeof($insertBin) > 0){
		            $insBinResult = $this->dbBin->query("UPDATE Gruppe SET " . implode(", ", $insertBin) . " WHERE ID = " . $this->gruppe->getID());
	            }
                if (!$insResult || ! $insBinResult) {
                    $messages[] = "Beim Ändern der Gruppe ist ein Problem aufgetreten.";
                    $messages[] = "Fehler bei der Datenbankkommunikation.";
                }
            }

            //Darstellung aufbauen
            if (sizeof($messages) == 0) {
                $messages[] = "Aktualisierung erfolgreich.";
            }
        } else {
            echo("<h2>Fehler</h2>");
            echo("Sie haben für diese Seite nicht die notwendige Berechtigung.");
        }
        return $messages;
    }

}