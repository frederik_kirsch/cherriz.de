<?php

/**
 * Class GruppeMember
 * Stellt die Mitglieder einer Gruppe dar.
 *
 * @author Frederik Kirsch
 */
class GruppeMember implements SubPage {

	const TYPE_VERWALTER = "Verwalter";

	const TYPE_VERTRETER = "Vertreter";

	const TYPE_MITGLIED = "Mitglied";

	const ACTION_UPGRADE = "Upgrad";

	const ACTION_DOWNGRADE = "Downgrade";

	const ACTION_REMOVE = "Remove";

	const ACTION_ADD = "Add";

	private $gruppe = null;

	private $userStatus = null;

	public function __construct() {
		$this->gruppe = GroupUtility::getCurrentGroup();
		$this->userStatus = GroupUtility::getUserStatus();

		$scripts = "STORE.saveAssoc('userStatus', '" . $this->userStatus . "');";
		$scripts .= "GROUP.setGroup(" . $this->gruppe->getID() . ");";
		$scripts .= "GROUP.setRetFunc(adaptMemberChange, adaptMemberChangeFailure);";
		$scripts .= "appendUserAction();";
		Toolkit::loadScripts(array("sites/gruppe_member_utility.js", "group.js"), $scripts);
	}

	public function drawContent() {
		echo("<h2>Mitglieder: " . $this->gruppe->getName() . "</h2>");
		echo("Hier kannst du die Mitglieder der Gruppe " . $this->gruppe->getName() . " verwalten.");

		//Mitgliederdaten abrufen
		$groupMember = $this->gruppe->getGroupMember();

		//Mitglieder darstellen
		echo("<h3>Mitglieder verwalten</h3>");
		$this->drawMember(GruppeMember::TYPE_VERWALTER, array($groupMember->getVerwalter()));
		$this->drawMember(GruppeMember::TYPE_VERTRETER, $groupMember->getVertreter());
		$this->drawMember(GruppeMember::TYPE_MITGLIED, $groupMember->getMember());
		echo("<h3>Neues Mitglied</h3>");
		$this->drawAddNew();
		echo("<h3>Beitrittsanfragen</h3>");
		$this->accessRequest();
	}

	/**
	 * Stellt ein alle Mitglieder eines Ranges dar.
	 *
	 * @param string $type   Rang
	 * @param User[] $member Liste der Mitglieder.
	 */
	private function drawMember($type, $member) {
		echo("<div class='description_box' id='$type'>
                <div class='description_attribute_box'>$type</div>
                <div class='description_value_box'>");
		foreach ($member as $usr) {
			echo("<span class='usr_control' id='user_" . $usr->get(User::PROPERTY_ID) . "'>" . $usr->drawUserText() . "</span>");
		}
		if (count($member) == 0) {
			echo("Keine $type");
		}
		echo("  </div>
              </div>");
	}

	/**
	 * Schaltflaeche fuer das hinzufuegen neuer Mitglieder darstellen.
	 */
	private function drawAddNew() {
		echo("<div class='description_box'>
                  <div class='description_attribute_box'>Aktion</div>
                  <div class='description_value_box'>
                      <a style='cursor: pointer' title='Neuen Nutzer zur Gruppe hinzufügen' onclick='new UserSearchDialog(newUserSelected).open()'>[Suchen]</a>
                   </div>
              </div>
              <div style='display: none' id='temp'>
              	  <span class='usr_control' id='user_copy'><a></a></span>
                  <div class='description_box' id='user_copy'>
                    <div class='description_attribute_box'>
                        <a href='/cherriz.de/index.php?page=Profile&User=' target='_self' title='Zur Profilseite von '></a>
                    </div>
                    <div class='description_value_box'>
                    </div>
                  </div>
              </div>");
	}

	/**
	 * Stellt die offenen Beitrittsanfragen dar.
	 */
	private function accessRequest() {
		$userRequests = $this->gruppe->getActiveRequests();
		if ($userRequests->num_rows == 0) {
			echo("Aktuell gibt es keine offenen Betrittsanfragen.");
			return;
		}

		while ($request = $userRequests->fetch_object()) {
			$user = new User($request->UserID);
			$control = "<span class='anfrage_control'>";
			$control .= "<a style='cursor: pointer' onclick='acceptRequest(new User(" . $request->UserID . ", \"" . $user->get(User::PROPERTY_Nick) . "\", \"\", \"\"))'>[Annehmen]</a>&nbsp;";
			$control .= "<a style='cursor: pointer' onclick='declineRequest(" . $request->UserID . ")'>[Ablehnen]</a>";
			$control .= "</span>";
			echo("<div id='anfrage_" . $request->UserID . "' class='anfrage box'>");
			echo("<div class='anfrage_header'>Anfrage von " . $user->drawUserText() . $control . "</div>");
			echo("<div class='anfrage_content'>" . $request->Anfrage . "</div>");
			echo("</div>");
		}

	}

}