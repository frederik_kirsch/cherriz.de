<?php

/**
 * Class GruppeOverview
 * Stellt das Formular zum Erzeugen einer Gruppe dar.
 *
 * @author Frederik Kirsch
 */
class GruppeCreate implements SubPage {

    private $messages = null;

    private $name = null;

    private $desc = null;

    public function __construct(){
        Toolkit::loadScripts(array("sites/gruppe_settings_utility.js"), "$('#grp_form').submit(createValidator(true));");
    }

    public function drawContent() {
        echo("<h2>Neue Gruppe erstellen</h2>");
        echo("Hier kannst du eine neue Gruppe erstellen.");
        $submitURL = "index.php?page=" . MenuGruppen::OVERVIEW;

        GroupUtility::drawMessages($this->messages);
        GroupUtility::drawGroupForm($submitURL, null, $this->name, $this->desc);
    }

    public function setMessages($messages){
        $this->messages = $messages;
    }

    public function setName($name){
        $this->name =$name;
    }

    public function setDescription($desc){
        $this->desc =$desc;
    }

}