<?php

/**
 * Class GruppeOverview
 * Stellt die Uebersicht ueber alle Gruppen dar.
 *
 * @author Frederik Kirsch
 */
class GruppenOverview implements SubPage {

    private $db = null;

    public function __construct() {
        $this->db = DBConnect::getDBConnection();
    }

    public function drawContent() {
        echo("<h2>Gruppenübersicht</h2>");
        echo("Hier findest du eine Übersicht über alle Gruppen denen du beigetreten bist, sowie eine Übersicht aller vorhandenen Gruppen.");
        $this->showMyGroups();
        $this->showAllGrups();
    }

    /**
     * Listet alle Gruppen auf in denen der aktive Benutzer Mitglied ist.
     */
    private function showMyGroups() {
        echo("<h3>Meine Gruppen</h3>");
        $query = "SELECT ID FROM Gruppe WHERE ID IN (SELECT Gruppe FROM Gruppe_Mitglied WHERE User = " . Login::getInstance()->getCurrentUserObject()->get(User::PROPERTY_ID) . ")";
        $groups = $this->db->query($query);
        if ($groups->num_rows == 0) {
            echo("<ul>
                    <li>Du gehörst im Moment zu keiener Gruppe.<br />Du kannst nach Gruppen suchen, um eine Beitrittsanfrage abzusenden.</li>
                    <li>Du kannst eine eigene Gruppe gründen und Freunde einladen.</li>
                  </ul>");
        } else {
            while ($group = $groups->fetch_object()) {
                $gruppe = new Group($group->ID);
                $gruppe->drawGroupMedium();
            }
        }
    }

    /**
     * Listet alle verfuegbaren Gruppen auf.
     */
    private function showAllGrups() {
        //Daten abrufen
        $query = "SELECT ID FROM Gruppe ORDER BY Name";
        $result = $this->db->query($query);

        //Darstellung aufbauen
        echo("<h3>Alle Gruppen</h3>");
        if ($result->num_rows == 0) {
            echo("<ul>
                    <li>Im Moment sind keine Gruppen vorhanden.</li>
                  </ul>");
        } else {
            while ($obj = $result->fetch_object()) {
                $gruppe = new Group($obj->ID);
                $gruppe->drawGroupMedium();
            }
        }
    }

}