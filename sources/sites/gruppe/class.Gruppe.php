<?php
LibImporter::import("sites/gruppe/internal/class.GroupUtility.php");

/**
 * Class Gruppe
 * Stellt Die Seite einer einzelnen Gruppe dar.
 *
 * @author Frederik Kirsch
 */
class Gruppe extends Page {

	private $userStatus = null;

	public function __construct() {
		parent::__construct();
		Toolkit::loadStyle('group.css');
		$this->userStatus = GroupUtility::getUserStatus();
	}

	protected function getNavBarEntries() {
		$pages = array();
		array_push($pages, SubPageSettings::init(MenuGruppe::OVERVIEW, "Gruppe Übersicht")->addPara("group"));
		array_push($pages, SubPageSettings::init(MenuGruppe::SETTINGS, "Gruppe Einstellungen")->addPara("group"));
		array_push($pages, SubPageSettings::init(MenuGruppe::MEMBER, "Gruppe Mitglieder")->addPara("group"));
		return $pages;
	}

	public function requiresLogin() {
		return true;
	}

	public function hasAccess() {
		$admin = $this->userStatus == Group::RANG_VERTRETER || $this->userStatus == Group::RANG_VERWALTER;

		switch ($this->determineSubPage()) {
			case MenuGruppe::OVERVIEW:
				return true;
			case MenuGruppe::MEMBER:
				return $admin;
			case MenuGruppe::SETTINGS:
				return $admin;
		}
		return false;
	}

	public function hasNavBar() {
		if ($this->userStatus == Group::RANG_VERTRETER || $this->userStatus == Group::RANG_VERWALTER) {
			return true;
		}
		return false;
	}

}