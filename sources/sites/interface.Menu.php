<?php

interface Menu {

	const ADMIN             = "Admin";

	const NEWS              = "News";

	const FORUM             = "Forum";

	const GRUPPE            = "Gruppe";

	const GRUPPENUEBERSICHT = "GruppenUebersicht";

	const PROFILE           = "Profile";

	const REGISTRIERUNG     = "Registrierung";

	const THREAD            = "Thread";

	const WIEDERHERSTELLUNG = "Wiederherstellung";

	const FEED              = "Feed";

	const IMPRESSUM         = "Impressum";

	const DATENSCHUTZ       = "Datenschutz";

	const WARTUNG           = "Wartung";

}