<?php

/**
 * Interface SubPage
 *
 */
interface SubPage{

    public function drawContent();

}