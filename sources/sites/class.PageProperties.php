<?php

/**
 * Class PageProperties
 * Parameterobjekt, welches die Parameter fuer den Seitenaufruf kapseln.
 * @author Frederik Kirsch
 */
class PageProperties {

	private $menu;

	private $menuClass;

	private $subMenu;

	public function __construct($menu, $menuClass, $subMenu) {
		$this->menu = $menu;
		$this->menuClass = $menuClass;
		$this->subMenu = $subMenu;
	}

	/**
	 * @return String Das Menue.
	 */
	public function getMenu() {
		return $this->menu;
	}

	/**
	 * @return String Das Untermenue.
	 */
	public function getSubMenu() {
		return $this->subMenu;
	}

	/**
	 * @return String Die Klasse des Menues.
	 */
	public function getMenuClass() {
		return $this->menuClass;
	}

}