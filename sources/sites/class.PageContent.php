<?php

/**
 * Class PageContent
 * Kapselt die Inhaltsseiten eines Aufrufs.
 * @author Frederik Kirsch
 */
class PageContent {

	private $page;

	private $subPage;

	/**
	 * PageContent constructor.
	 *
	 * @param Page $page
	 * @param SubPage $subPage
	 */
	public function __construct($page, $subPage) {
		$this->page = $page;
		$this->subPage = $subPage;
	}

	/**
	 * @return Page
	 */
	public function getPage() {
		return $this->page;
	}

	/**
	 * @return SubPage
	 */
	public function getSubPage() {
		return $this->subPage;
	}

}