<?php

/**
 * Class Registrierung
 * Stellt den kompletten Registrierungsprozess dar.
 *
 * @author Frederik Kirsch
 */
class Registrierung extends Page {

	/**
	 * @var MySQLi
	 */
	private $db;

	/**
	 * @var Verify
	 */
	private $verify;

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * Korrigiert die Unterseite im Fall das die Registrierung deaktiviert ist, oder der Benutzer falsche Daten eingegeben hat.
	 */
	public function __construct() {
		parent::__construct();
		$this->setDefaultSubPage(MenuRegistrierung::DATENSCHUTZ);

		//Variablen abrufen
		$this->db = DBConnect::getDBConnection();
		$this->settings = Settings::getInstance();
		$this->verify = $GLOBALS['VERIFY'];

		//Bei deaktivierter Registrierung umleiten
		if ($this->settings->getProperty(Settings::PROPERTY_REGISTRATION_ALLOWED) != true) {
			$this->redirect(MenuRegistrierung::DECLINE);
		} else if (Filter::text($_POST["data_submitted"]) == true) {
			$this->processValidation();
		}
	}

	public function hasAccess() {
		return !Login::getInstance()->isLoggedIn();
	}

	public function drawContent() {
		echo("<h2>Registrierung</h2>");
		echo("Hallo,<br />");
		echo("es ist schön dass du ein Teil von <a href='www.cherriz.de'>cherriz.de</a> werden möchtest. Mit Abschluss dieser Registrierung wirst du alle Features dieser etwas anderen Onlinecommunity nutzen können. Vorher müssen aber noch ein paar Dinge erledigt werden.");
		Toolkit::loadStyle("registration.css");
	}

	/**
	 * Steuert die Validierung der eingegebenen Daten des Benutzers.
	 */
	private function processValidation() {
		//Validierung & Aufbereitung
		$nick = Filter::text($_POST['registration_nick']);
		$passwort1 = Filter::text($_POST['registration_passwort1']);
		$passwort2 = Filter::text($_POST['registration_passwort2']);
		$vorname = Filter::text($_POST['registration_vorname']);
		$nachname = Filter::text($_POST['registration_nachname']);
		$email = Filter::text($_POST['registration_email']);
		$geburtsdatum = Filter::text($_POST['registration_alter']);
		$plz = Filter::text($_POST['registration_plz']);
		$ort = Filter::text($_POST['registration_ort']);
		$strasse = Filter::text($_POST['registration_street']);

		$valResult = $this->verifyUserInfo($nick, $vorname, $nachname, $passwort1, $passwort2, $email, $strasse, $plz, $ort, $geburtsdatum);
		if ($valResult !== Verify::$VALIDE) {
			$this->redirect(MenuRegistrierung::DATEN);
			$this->setSubPageProperty("messages", $valResult);
		}
	}

	/**
	 * Validiert die Benutzerdaten und erzeugt Hinweise zur Korrektur.
	 *
	 * @param $nick
	 * @param $vorname
	 * @param $nachname
	 * @param $passwort1
	 * @param $passwort2
	 * @param $email
	 * @param $strasse
	 * @param $plz
	 * @param $ort
	 * @param $geburtsdatum
	 *
	 * @return array|string Fehlermeldungen oder Validitätsbestätigung.
	 */
	private function verifyUserInfo($nick, $vorname, $nachname, $passwort1, $passwort2, $email, $strasse, $plz, $ort, $geburtsdatum) {
		// Eingaben validieren
		$valResult = array();
		$validation = $this->verify->checkNick($nick);
		if ($validation != Verify::$VALIDE) {
			array_push($valResult, $validation);
		}
		$validation = $this->verify->checkName($vorname);
		if ($validation != Verify::$VALIDE && strlen(trim($vorname) != 0)) {
			array_push($valResult, $validation);
		}
		$validation = $this->verify->checkName($nachname);
		if ($validation != Verify::$VALIDE && strlen(trim($nachname) != 0)) {
			array_push($valResult, $validation);
		}
		$validation = $this->verify->checkPasswortIdentical($passwort1, $passwort2);
		if ($validation != Verify::$VALIDE) {
			array_push($valResult, $validation);
		}
		$validation = $this->verify->checkEmail($email);
		if ($validation != Verify::$VALIDE) {
			array_push($valResult, $validation);
		}
		$validation = $this->verify->checkStreet($strasse);
		if ($validation != Verify::$VALIDE && strlen(trim($strasse) != 0)) {
			array_push($valResult, $validation);
		}
		$validation = $this->verify->checkZIP($plz);
		if ($validation != Verify::$VALIDE && strlen(trim($plz) != 0)) {
			array_push($valResult, $validation);
		}
		$validation = $this->verify->checkCity($ort);
		if ($validation != Verify::$VALIDE && strlen(trim($ort) != 0)) {
			array_push($valResult, $validation);
		}
		$validation = $this->verify->checkBirthdate($geburtsdatum);
		if ($validation != Verify::$VALIDE && strlen(trim($geburtsdatum) != 0)) {
			array_push($valResult, $validation);
		}
		if (sizeof($valResult) == 0) {
			return Verify::$VALIDE;
		}
		return $valResult;
	}

}