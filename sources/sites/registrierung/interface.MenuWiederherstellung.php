<?php

interface MenuWiederherstellung {

	const DATEN    = "WiederherstellungDaten";

	const MAIL     = "WiederherstellungMail";

	const PASSWORT = "WiederherstellungPasswort";

	const ERFOLG   = "WiederherstellungErfolg";

}