<?php

/**
 * Class Wiederherstellung
 * Bietet Moeglichkeiten zur Wiederherstlelung des Accounts.
 *
 * @author Frederik Kirsch
 */
class Wiederherstellung extends Page {

	private $db = null;

	/** @var Verify */
	private $verify = null;

	/**
	 * Korrigiert die Unterseite im Fall das der Benutzer falsche Daten eingegeben hat.
	 */
	public function __construct() {
		parent::__construct();
		$this->setDefaultSubPage(MenuWiederherstellung::DATEN);

		//Variablen abrufen
		$this->db = DBConnect::getDBConnection();
		$this->verify = $GLOBALS["VERIFY"];

		//Bei deaktivierter Registrierung umleiten
		if (Filter::text($_POST["data_submitted"]) == true) {
			$this->processDataValidation();
		} else if (Filter::text($_POST["pwd_submitted"]) == true) {
			$this->processPwdValidation();
		}
	}

	public function hasAccess() {
		return !Login::getInstance()->isLoggedIn();
	}

	public function drawContent() {
		echo("<h2>Wiederherstellung</h2>");
	}

	/**
	 * Steuert die Validierung der eingegebenen Daten des Benutzers zur Accountidentifizierung.
	 */
	private function processDataValidation() {
		//Validierung & Aufbereitung
		$variante = Filter::text($_POST['variante']);
		$query = "SELECT ID FROM Users WHERE Status = (SELECT ID FROM User_Status WHERE Status = 'Aktiv') AND ";
		$message = null;
		if ($variante == "nick") {
			$nick = Filter::text($_POST['wiederherstellung_nick']);
			$query .= "Nick = '$nick'";
			$message = "Es wurde kein Benutzer zum Nick '$nick' gefunden.";
		} else {
			$mail = Filter::text($_POST['wiederherstellung_mail']);
			$query .= "Email = '$mail'";
			$message = "Es wurde kein Benutzer zum E-Mail Adresse '$mail' gefunden.";
		}

		$result = $this->db->query($query);
		if ($result->num_rows == 0) {
			$this->redirect(MenuWiederherstellung::DATEN);
			$this->setSubPageProperty("message", $message);
		} else {
			$obj = $result->fetch_object();
			$this->setSubPageProperty("userID", $obj->ID);
		}
	}

	/**
	 * Steuert die Validierung des neuen Passwortes.
	 */
	private function processPwdValidation() {
		//Validierung & Aufbereitung
		$pwd1 = Filter::text($_POST['wiederherstellung_passwort1']);
		$pwd2 = Filter::text($_POST['wiederherstellung_passwort2']);
		$key = Filter::text($_POST['key']);

		$valResult = $this->verify->checkPasswortIdentical($pwd1, $pwd2);
		if ($valResult == Verify::$VALIDE) {
			//Properties setzen
			$query = "SELECT User FROM Wiederherstellungsbestaetigungen WHERE Bestaetigung = (SELECT ID FROM Bestaetigungen WHERE `Key` = '$key' AND `Used` IS NULL)";
			$result = $this->db->query($query);
			$resultObj = $result->fetch_object();
			$this->setSubPageProperty("userID", $resultObj->User);
			$this->setSubPageProperty("password", $pwd1);
			//Schlüssel entwerten
			$update = "UPDATE Bestaetigungen SET Used = NOW() WHERE Used IS NULL AND `Key` = '$key'";
			$this->db->query($update);
		} else {
			$this->redirect(MenuWiederherstellung::PASSWORT);
			$this->setSubPageProperty("message", $valResult);
		}
	}

}