<?php

interface MenuRegistrierung {

	const DATEN       = "RegistrierungDaten";

	const DATENSCHUTZ = "RegistrierungDatenschutz";

	const MAIL        = "RegistrierungMail";

	const ERFOLG      = "RegistrierungErfolg";
	
	const DECLINE     = "RegistrierungDecline";

}