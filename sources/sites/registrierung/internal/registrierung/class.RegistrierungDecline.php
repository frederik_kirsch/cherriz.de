<?php

/**
 * Class RegistrierungDecline
 * Wird angezeigt, während die Registrierung geschlossen ist.
 */
class RegistrierungDecline implements SubPage {

	public function drawContent() {
		echo("<h3>Registrierung geschlossen</h3>");
		echo("Tut mir leid, aber die Registrierung ist im Moment leider nicht möglich.<br />");
		echo("Wenn Sie Fragen haben können Sie sich aber gerne über das <a href='index.php?page=" . Menu::IMPRESSUM . "'>Kontaktformular</a> an den Administrator wenden.<br />");
		echo("Mit freundlichen Grüßen<br />");
		echo("www.cherriz.de");
	}

}