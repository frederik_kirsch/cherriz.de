<?php

/**
 * Class RegistrierungDaten
 * Zweiter Schritt der Registrierung.
 * Eingabe oder korrektur der Benutzerdaten.
 */
class RegistrierungDaten implements SubPage {

	private $messages = null;

	public function drawContent() {
		echo("<h3>II. Persönliche Informationen</h3>");
		echo("<p>Jetzt brauche ich noch ein paar persönliche Informationen von dir.</p>");
		echo("<p>Alle mit einem * markierten Felder sind dabei zwingend auszufüllen.</p>");

		if (sizeof($this->messages) > 0) {
			echo("<h4 style='padding-top: 10px;'>Korrekturen notwendig</h4>");
			echo("<ul><li style='color: #f40000'>" . implode("</li><li style='color: #f40000'>", $this->messages) . "</li></ul>");
		}

		//Daten auslesen
		$nick = Filter::text($_POST['registration_nick']);
		$passwort1 = Filter::text($_POST['registration_passwort1']);
		$passwort2 = Filter::text($_POST['registration_passwort2']);
		$vorname = Filter::text($_POST['registration_vorname']);
		$nachname = Filter::text($_POST['registration_nachname']);
		$email = Filter::text($_POST['registration_email']);
		$geburtsdatum = Filter::text($_POST['registration_alter']);
		$plz = Filter::text($_POST['registration_plz']);
		$ort = Filter::text($_POST['registration_ort']);
		$strasse = Filter::text($_POST['registration_street']);

		echo("<form action='index.php?page=" . MenuRegistrierung::MAIL . "' method='post' id='regdata' style='margin-top: 10px;'>
				<div class='description_box'>
					<div class='description_attribute_box'>*Nick</div>
					<input type='text' name='registration_nick' id='registration_nick' value='$nick' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>*Passwort</div>
					<input type='password' name='registration_passwort1' id='registration_passwort1' value='$passwort1' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>*Wiederholen</div>
					<input type='password' name='registration_passwort2' id='registration_passwort2' value='$passwort2' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>Vorname</div>
					<input type='text' name='registration_vorname' id='registration_vorname' value='$vorname' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>Nachname</div>
					<input type='text' name='registration_nachname' id='registration_nachname' value='$nachname' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>*E-Mail</div>
					<input type='text' name='registration_email' id='registration_email' value='$email' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>Geburtsdatum</div>
					<input type='text' name='registration_alter' id='registration_alter' value='$geburtsdatum' maxlength='10' size='30' class='calendar'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>PLZ / Ort</div>
					<input type='text' name='registration_plz' id='registration_plz' value='$plz' maxlength='5' size='4'>
					<input type='text' name='registration_ort' id='registration_ort' value='$ort' maxlength='70'  size='23'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>Straße</div>
					<input type='text' name='registration_street' id='registration_street' value='$strasse' maxlength='70' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'><input type='button' value='Zurück' onClick='window.location=\"index.php?page=" . MenuRegistrierung::DATENSCHUTZ . "\";'></div>
					<input id='submit' type='submit' value='Registrierung abschließen'>
				</div>
				<input type='hidden' name='data_submitted' value='true' />
			  </form>");
		$minDate = date("d.m.Y", strtotime("-100 year"));
		$maxDate = date("d.m.Y");
		Toolkit::printDatePickerScripts("registration_alter", false, $minDate, $maxDate);
		Toolkit::loadScripts(array("sites/registrierung_utility.js"), "$('#regdata').submit(validateInput)");
	}

	/**
	 * Setzt die Fehlermeldungen falls es Probleme bei der Validierung der Daten gab.
	 *
	 * @param array $messages Korrekturhinweise.
	 */
	public function setMessages($messages) {
		$this->messages = $messages;
	}

}