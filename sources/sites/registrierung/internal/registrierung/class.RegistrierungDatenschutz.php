<?php

/**
 * Class RegistrierungDatenschutz
 * Erster Schritt der Registrierung. Bestätigung der Datenschutzerklaerung.
 *
 * @author Frederik Kirsch
 */
class RegistrierungDatenschutz implements SubPage {

	public function drawContent() {
		$settings = Settings::getInstance();
		$datenschutz = Converter::bbToHtml($settings->getProperty(Settings::PROPERTY_DATENSCHUTZ_REGISTRATION));

		echo("<h3>I. Datenschutz</h3>");
		echo("<p>Die Nutzung einer Webseite funktioniert nicht ohne Daten. Das gilt auch für cherriz.de. Bitte mache dich daher mit der Datenschutzerklärung vertraut in der Umfang der Datenerhebung und Nutzung beschrieben sind.</p>");
		echo("<div class='readText'>$datenschutz</div>");
		echo("<p>Ich habe die Datenschutzerklärung gelesen und bin mit der beschriebenen Verwendung meiner Daten einverstanden.</p>");
		echo("<input type='button' value='Ablehnen' onClick='window.location=\"index.php\";'> <input type='button' value='Akzeptieren' onClick='window.location=\"index.php?page=" . MenuRegistrierung::DATEN . "\";'>");
	}

}