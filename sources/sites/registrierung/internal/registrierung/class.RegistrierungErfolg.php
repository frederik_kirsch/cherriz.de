<?php

/**
 * Class RegistrierungErfolg
 * Vierter Schritt der Registrierung.
 * Schaltet den Benutzer frei und schließt damit die Registrierung ab.
 */
class RegistrierungErfolg implements SubPage {

	private $db = null;

	public function __construct() {
		$this->db = DBConnect::getDBConnection();
	}

	public function drawContent() {
		echo("<h3>IV. Abschluss der Registrierung</h3>");

		$activationcode = Filter::text($_GET['key']);
		$statement = "UPDATE Users AS U, Bestaetigungen AS B, Registrierungsbestaetigungen AS RB
								 SET U.Status = (SELECT ID FROM User_Status WHERE Status = 'Aktiv'),
									 B.Used = NOW()
							   WHERE B.Key = '$activationcode'
								 AND B.Used IS NULL
								 AND U.Status = (SELECT ID FROM User_Status WHERE Status = 'Registrierung')
								 AND U.ID = RB.User
								 AND RB.Bestaetigung = B.ID";

		//2 Update muss 2 Zeilen betreffen, da 2 betroffene Tabellen.
		if ($this->db->query($statement) && $this->db->affected_rows == 2) {
			echo("<p>Herzlichen Glückwunsch, dein Account wurde erfolgreich freigeschalten.<br />Du kannst dich jetzt anmelden.</p>");
		} else {
			echo("<p>Tut mir leid, aber es gab ein Problem bei der Freischaltung deines Accounts.</p>");
			echo("<p>Bitte versuchen Sie erneut den Link in Ihrer Email zu aktivieren, oder wenden Sie sich an den <a href='index.php?page=" . Menu::IMPRESSUM . "'>Administrator</a>.</p>");
		}
	}

}