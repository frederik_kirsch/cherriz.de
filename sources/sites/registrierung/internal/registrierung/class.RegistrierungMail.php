<?php

/**
 * Class RegistrierungMail
 * Dritter Schritt der Registrierung.
 * Speichert den validierten Benutzer und versendet die Bestaetigungsmail.
 */
class RegistrierungMail implements SubPage {

	private $db = null;

	public function __construct(){
		$this->db = DBConnect::getDBConnection();
	}

	public function drawContent() {
		echo("<h3>III. E-Mail validierung</h3>");
		$registrationResult = $this->registerUser();
		if ($registrationResult !== false) {
			$this->printMailForm($registrationResult);
		} else {
			echo("<p>Tut mir leid, aber leider gab es ein Problem bei der Registrierung.</p>");
			echo("<p>Bitte versuchen Sie noch einmal sich <a href='index.php?page=" . Menu::REGISTRIERUNG . "'>hier</a> zu registrieren oder wenden Sie sich an den <a href='index.php?page=" . Menu::IMPRESSUM . "'>Administrator</a>.</p>");
		}
	}

	/**
	 * Registriert den Benutzer.
	 * Die Daten werden nicht mehr validiert, da dies bereits in der Hauptseite erledigt wurde.
	 * @return bool|string Freischaltungscode oder false, falls die Erstellung des Nutzers fehlgeschlagen ist.
	 */
	private function registerUser() {
		//Daten auslesen
		$nick = Filter::text($_POST['registration_nick']);
		$passwort = Filter::text($_POST['registration_passwort1']);
		$passwort_enc = md5($passwort);
		$vorname = Filter::text($_POST['registration_vorname']);
		$nachname = Filter::text($_POST['registration_nachname']);
		$email = Filter::text($_POST['registration_email']);
		$geburtsdatum = Filter::text($_POST['registration_alter']);
		$geburtsdatum_db = Converter::dbDate($geburtsdatum);
		$plz = Filter::text($_POST['registration_plz']);
		$ort = Filter::text($_POST['registration_ort']);
		$strasse = Filter::text($_POST['registration_street']);

		//Datenbankeinträge
		//User Eintragen
		$result = false;
		$statement = "INSERT INTO Users (Nick, Vorname, Nachname, Passwort, Email, Strasse, PLZ, Ort, Signatur, Status, Geburtsdatum, Registriert) VALUES ('$nick', '$vorname', '$nachname', '$passwort_enc', '$email', '$strasse', '$plz', '$ort', 'I <3 cherriz', (SELECT ID FROM User_Status WHERE Status = 'Registrierung'), '$geburtsdatum_db', NOW())";
		if ($this->db->query($statement)) {
			//Bestätigungstoken generieren
			$userId = $this->db->insert_id;
			$regKey = Toolkit::generateKey(Toolkit::VALUE_POOL, 20);
			if ($this->db->query("INSERT INTO Bestaetigungen (`Key`) VALUES ('$regKey')")) {
				//Bestätigungstoken verknüpfen
				$bestaetigungsID = $this->db->insert_id;
				$statement = "INSERT INTO Registrierungsbestaetigungen (User, Bestaetigung) VALUES ('$userId', '$bestaetigungsID')";
				if ($this->db->query($statement) && $this->setDefaultRights($userId)) {
					$result = $regKey;
				} else {
					//Reperaturen an DB durchführen
					$this->db->query("DELETE FROM Users WHERE ID = $userId");
					$this->db->query("DELETE FROM Bestaetigungen WHERE ID = $bestaetigungsID");
				}
			} else {
				//Reperaturen an DB durchführen
				$this->db->query("DELETE FROM Users WHERE ID = $userId");
			}
		}
		return $result;
	}

	/**
	 * Stellt den Infotext zur versendeten Email dar.
	 * @param string $regKey Der Key der per Mail verschickt wird.
	 */
	private function printMailForm($regKey) {
		LibImporter::import("basic/util/internal/class.Message.php");
		LibImporter::import("basic/util/class.Mail.php");

		$url = "http://www.cherriz.de/index.php?page=" . MenuRegistrierung::ERFOLG. "&key=$regKey";
		$nick = Filter::text($_POST['registration_nick']);
		$email = Filter::text($_POST['registration_email']);

		$mail = new Mail();
		if ($mail->sendMail(Message::createInstance(Message::REGISTRIERUNGSBESTAETIGUNG, array($nick, $url)), $email)) {
			echo("<p>Um die Registrierung endültig abschließen zu können muss deine E-Mailadresse noch validiert werden.</p>");
			echo("<p>Dazu wurde dir eine E-Mail an die angegebene Adresse zugesandt. Bitte besätigen die Registrierung mit einem Klick auf den darin enthaltenen Link.</p>");
			echo("<p>Die Email sollte innerhalb weniger Sekunden bei dir eintreffen. Falls dies nicht der Fall ist wird doch bitte einen Blick in deinen Spam Ordner.</p>");
			echo("<p>Leider landen Emails von <a href='www.cherriz.de'>cherriz.de</a> häufig im Spamordner. Sollte die E-Mail auch dort nicht auffindbar sein, so wenden dich bitte an den <a href='index.php?page=" . Menu::IMPRESSUM . "'>Administrator</a>.</p>");
		} else {
			echo("<p>Tut mir Leid, aber es gab ein Problem beim Versenden der Email. Bitte wende dich an den <a href='index.php?page=" . Menu::IMPRESSUM . "'>Administrator</a>.</p>");
		}
	}

	/**
	 * Übeträgt dem Nutzer alle Standardrechte.
	 *
	 * @param int $userID Die ID des Users.
	 *
	 * @return bool Gibt an ob die Rechtevergabe erfolgreich war
	 */
	private function setDefaultRights($userID) {
		$abfrage = "SELECT ID FROM Rechte WHERE Standard = 1";
		$result = $this->db->query($abfrage);
		if ($result->num_rows > 0) {
			while ($obj = $result->fetch_object()) {
				$rechtID = $obj->ID;
				$insert = "INSERT INTO User_Rechte (User, Recht) VALUES ($userID, $rechtID)";
				if (!$this->db->query($insert)) {
					$statement = "DELETE FROM User_Rechte WHERE User = $userID";
					$this->db->query($statement);
					return false;
				}
			}
		}
		return true;
	}

}