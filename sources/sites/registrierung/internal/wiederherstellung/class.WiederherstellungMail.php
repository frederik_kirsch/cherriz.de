<?php

/**
 * Class WiederherstellungMail
 * Stellt die Maske zur Bestaetigung des Mailversands dar.
 *
 * @author Frederik Kirsch
 */
class WiederherstellungMail implements SubPage {

	private $userID = null;

	private $db = null;

	public function __construct(){
		$this->db = DBConnect::getDBConnection();
	}

	public function drawContent() {
		echo("<h3>II. Mail</h3>");
		$key = $this->createKey();
		if ($key !== false && $this->sendMail($key)) {
			echo("Eine E-Mail zur Wiederherstellung Ihres Accounts wurde erfolgreich versendet.<br />");
			echo("Sie können mit einem Klick auf den darin enthaltenen Link ihr Passwort zurücksetzen.<br />");
			echo("Die Email sollte innerhalb weniger Sekunden bei Ihnen eintreffen. Falls dies nicht der Fall ist werfen Sie bitte einen Blick in Ihren Spam Ordner.<br />");
			echo("Leider landen Emails von <a href='www.cherriz.de'>cherriz.de</a> häufig im Spamordner. Sollte Ihre Email auch dort nicht auffindbar sein, so wenden Sie sich bitte an den <a href='index.php?page=" . Menu::IMPRESSUM . "'>Administrator</a>.");
		} else {
			echo("Tut uns Leid, aber es gab ein Problem beim Versenden der Email. Bitte wenden Sie sich an den <a href='index.php?page=" . Menu::IMPRESSUM . "'>Administrator</a>.");
		}
	}

	/**
	 * Erzeugt einen Schluessel, ueber den bei der Passwortwiederherstellung der Account identifiziert werden kann.
	 * @return bool|string Der Schluessel im Erfolgsfall, sonst false.
	 */
	private function createKey() {
		//Bestätigungstoken generieren
		$regKey = TOOLKIT::generateKey(TOOLKIT::VALUE_POOL, 20);
		$statement = "INSERT INTO Bestaetigungen (`Key`) VALUES ('$regKey')";
		if ($this->db->query($statement)) {
			//Bestätigungstoken verknüpfen
			$bestaetigungsID = $this->db->insert_id;
			$statement = "INSERT INTO Wiederherstellungsbestaetigungen (User, Bestaetigung) VALUES ('$this->userID', '$bestaetigungsID')";
			if ($this->db->query($statement)) {
				return $regKey;
			} else {
				//Reperaturen an DB durchführen
				$statement = "DELETE FROM Bestaetigungen WHERE ID = " . $bestaetigungsID;
				$this->db->query($statement);
			}
		}
		return false;
	}

	/**
	 * Versendet die Verifizierungsmail.F
	 * @param string $key Der Schluessel
	 *
	 * @return bool Der Erfolgsfall.
	 */
	private function sendMail($key) {
		LibImporter::import("basic/util/internal/class.Message.php");
		LibImporter::import("basic/util/class.Mail.php");

		$userObj = new User($this->userID);
		$url = "http://www.cherriz.de/index.php?page=" . MenuWiederherstellung::PASSWORT. "&key=$key";
		$mail = new Mail();
		return $mail->sendMail(Message::createInstance(Message::WIEDERHERSTELLUNGSBESTAETIGUNG, array($userObj->get(User::PROPERTY_Nick), $url)),
		                       $userObj->get(User::PROPERTY_Email));
	}

	public function setUserID($userID) {
		$this->userID = $userID;
	}

}