<?php

/**
 * Class WiederherstellungMail
 * Stellt die Maske zur Eingabe des neuen Passworts dar.
 *
 * @author Frederik Kirsch
 */
class WiederherstellungPasswort implements SubPage {

	private $db = null;

	private $message = null;

	public function __construct() {
		$this->db = DBConnect::getDBConnection();
	}

	public function drawContent() {
		echo("<h3>III. Passwortänderung</h3>");

		$key = Filter::text($_GET["key"]);
		if ($this->checkKey($key)) {
			$this->drawPasswordChange($key);
		} else {
			echo("Achtung, der Schlüssel ist nicht mehr gültig.<br />");
			echo("Entweder wurde das Passwort bereits erfolgreich geändert, oder es gab ein Problem beim öffnen des Links.");
		}
	}

	private function drawPasswordChange($key){
		echo("Geben Sie hier Ihr neues Passwort ein. Mit der Bestätigung dieses Formulars wird anschließend Ihr Kennwort aktualisiert.");
		if ($this->message != null) {
			echo("<h4 style='padding-top: 10px;'>Korrekturen notwendig</h4>");
			echo("<ul><li style='color: #f40000'>$this->message</li></ul>");
		}
		echo("<form action='index.php?page=" . MenuWiederherstellung::ERFOLG . "' method='post' id='pwd_frm' style='margin-top: 10px;'>
				<div class='description_box'>
					<div class='description_attribute_box'>Passwort</div>
					<input type='password' name='wiederherstellung_passwort1' id='wiederherstellung_passwort1' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>Wiederholen</div>
					<input type='password' name='wiederherstellung_passwort2' id='wiederherstellung_passwort2' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>&nbsp;</div>
    				<input id='submit' type='submit' value='Passwort ändern'>
				</div>
				<input type='hidden' name='pwd_submitted' value='true' />
				<input type='hidden' name='key' value='$key' />
			  </form>");

		$script = "$('#pwd_frm').submit(validatePwd);";
		Toolkit::loadScripts(array("sites/wiederherstellung_utility.js"), $script);
	}

	/**
	 * Validiert den uebergebenen Schluessel.
	 * @param string $key Der Schluessel
	 *
	 * @return bool Ob valide.
	 */
	private function checkKey($key) {
		$query = "SELECT * FROM Bestaetigungen WHERE `Key` = '$key' AND `Used` IS NULL";
		$result = $this->db->query($query);
		if ($result->num_rows == 1) {
			return true;
		}
		return false;
	}

	public function setMessage($message) {
		$this->message = $message;
	}

}