<?php

/**
 * Class WiederherstellungDaten
 * Stellt die Maske zur Accountidentifizierung dar.
 *
 * @author Frederik Kirsch
 */
class WiederherstellungDaten implements SubPage {

	private $message = null;

	public function drawContent() {
		echo("<h3>I. Daten</h3>");
		echo("Sie können ihren Account mit Hilfe des Ihres Nicks oder Ihrere E-Mail Adresse wiederherstellen. ");
		echo("Sie erhalten anschließend eine Nachricht an die dem Account zugeordnete E-Mail Adresse mit einem neuen Passwort.");

		if (sizeof($this->message) != null) {
			echo("<h4 style='padding-top: 10px;'>Korrekturen notwendig</h4>");
			echo("<ul><li style='color: #f40000'>$this->message</li></ul>");
		}

		$variante = Filter::text($_POST['variante']);
		$nick = Filter::text($_POST['wiederherstellung_nick']);
		$mail = Filter::text($_POST['wiederherstellung_mail']);
		$nth = 0;
		if($variante=="mail"){
			$nth = 1;
		}

		echo("<form action='index.php?page=" . MenuWiederherstellung::MAIL . "' method='post' id='data_frm' style='margin-top: 10px;'>
				<div class='description_box'>
					<div class='description_attribute_box'>Variante</div>
					<input type='radio' name='variante' value='nick'> Nick
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>&nbsp;</div>
    				<input type='radio' name='variante' value='mail'> Mail
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>Nick</div>
    				<input type='text' name='wiederherstellung_nick' id='wiederherstellung_nick' value='$nick' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>E-Mail</div>
    				<input type='text' name='wiederherstellung_mail' id='wiederherstellung_mail' value='$mail' maxlength='50' size='30'>
				</div>
				<div class='description_box'>
					<div class='description_attribute_box'>&nbsp;</div>
    				<input id='submit' type='submit' value='Wiederherstellen'>
				</div>
				<input type='hidden' name='data_submitted' value='true' />
			  </form>");

		$script = "$('#data_frm').submit(validateInput);";
		$script .= "$('input[name=variante]').change(processVarianteChange);";
		$script .= "$('input[name=variante]').eq($nth).click();";
		Toolkit::loadScripts(array("sites/wiederherstellung_utility.js"), $script);
	}

	public function setMessage($message) {
		$this->message = $message;
	}

}