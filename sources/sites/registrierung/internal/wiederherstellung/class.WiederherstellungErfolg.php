<?php

/**
 * Class WiederherstellungMail
 * Stellt die Maske zur Erfolgsdarstellung dar.
 *
 * @author Frederik Kirsch
 */
class WiederherstellungErfolg implements SubPage {

	private $user = null;

	private $password = null;

	private $db = null;

	public function __construct(){
		$this->db = DBConnect::getDBConnection();
	}

	public function drawContent() {
		if ($this->changePassword()) {
			echo("<h3>IV. Erfolg</h3>");
			echo("Ihr Passwort wurde erfolgreich aktualisiert. Sie können sich ab sofort mit ihrem neuen Passwort anmelden.");
		} else {
			echo("<h3>IV. Problem</h3>");
			echo("Es ist ein uerwartetes Problem bei der Aktualisierung Ihres Accounts aufgetreten.<br />");
			echo("Bitte wenden Sie sich an den <a href='index.php?page=" . Menu::IMPRESSUM . "'>Administrator</a>.");
		}
	}

	/**
	 * Aktualisiert das Passwort des Benutzers.
	 * @return bool Gibt an ob die Aktualisierung erfolgreich war.
	 */
	private function changePassword() {
		return $this->db->query("UPDATE Users SET Passwort = '" . md5($this->password) . "' WHERE ID = " . $this->user);
	}

	public function setUserID($userID) {
		$this->user = $userID;
	}

	public function setPassword($password) {
		$this->password = $password;
	}

}