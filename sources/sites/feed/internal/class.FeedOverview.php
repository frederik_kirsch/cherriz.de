<?php

/**
 * Class FeedOverview
 * Zeigt Eintraege aus den Quellen News und Forum gesammelt an.
 *
 * @author Frederik Kirsch
 */
class FeedOverview implements SubPage {

	public function drawContent() {
		Toolkit::printAsynchrounusContentLoader("sites/feed_content_loader.js", "initFeed();");
	}

}