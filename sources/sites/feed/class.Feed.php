<?php

/**
 * Class Feed
 * Ueberseite fuer den Feed von Eintraegen aus verschiedenen Quellen.
 * @author Frederik Kirsch
 */
class Feed extends Page {

	public function __construct() {
		parent::__construct();
		Toolkit::loadStyle("feed.css");
	}

	protected function getNavBarEntries() {
		$subPages = array();
		array_push($subPages, SubPageSettings::init(MenuFeed::OVERVIEW, "Feed Übersicht"));
		array_push($subPages, SubPageSettings::init(MenuNews::OVERVIEW, "News Übersicht"));
		array_push($subPages, SubPageSettings::init(MenuNews::WRITE, "News Schreiben"));
		array_push($subPages, SubPageSettings::init(MenuNews::CONTROL, "Meine News"));
		return $subPages;
	}

}