<?php

/**
 * Class ForumCreate
 * Die Seite stellt die Oberflaeche fuer das Erzeugen eines neuen Forums zur verfuegung.
 *
 * @author Frederik Kirsch
 */
class ForumCreate implements SubPage {

	private $user = null;

	private $forum = null;

	/**
	 * Erzeugt die Seite, initialisiert variablen. Wird eine ID mit uebergeben wird das Forum zur Aenderung vorbereitet.
	 */
	public function __construct() {
		$this->user = Login::getInstance()->getCurrentUserObject();

		$forumID = Filter::text($_GET['forum']);
		$this->prepareEdit($forumID);
	}

	/**
	 * Stellt die Seite dar.
	 */
	public function drawContent() {
		$edit = $this->forum->getID() != null;

		$text = "anlegen";
		if ($edit) {
			$text = "bearbeiten";
		}
		echo("<h2>Forum " . $text . "</h2>");
		echo("Lege hier ein neues Forum an.");
		echo("<form action='index.php?page=" . MenuForum::OVERVIEW . "' method='post' id='forum_form'>");

		//Name
		echo("<div class='description_box'>
				  <div class='description_attribute_box'>Bezeichnung</div>
				  <div class='description_value_box'>
				      <input type='text' name='name' id='name' style='width: 450px' value='" . $this->forum->getName() . "'/>
				  </div>
              </div>");

		//Beschreibung
		echo("<div class='description_box' style='padding-top: 5px'>
				  <div class='description_attribute_box'>Kurzbeschreibung</div>
				  <div class='description_value_box'>
				      <textarea name='shortdesc' id='shortdesc' style='width: 450px; height: 70px' maxlength='250'>" . $this->forum->getDescription() . "</textarea>
				  </div>
              </div>");

		//Steuerung
		$text = "Erstellen";
		if ($edit) {
			$text = "Bearbeiten";
		}
		echo("<div class='description_box'>
				  <div class='description_attribute_box'>Aktionen</div>
				  <div class='description_value_box'>
				      <input type='button' value='" . $text . "' onclick='createForum()' />
				      <input type='button' value='Verwerfen' onclick='resetForum()'/>
				  </div>
              </div>");

		echo("    <input type='hidden' name='submitted' value='yes'/>
                  <input type='hidden' name='id' value='" . $this->forum->getID() . "'/>
              </form>");

		//Scripting
		Toolkit::loadScripts(array("sites/forum_utility.js"), "$('#forum_form').submit(function() { return submitForum() })");
	}

	/**
	 * Laedt anhand der uebergebenen ID das Forum aus der Datenbank oder erzeugt ein leeres Forumsobjekt.
	 *
	 * @param string $forumID ID des zu ladenden Forums.
	 */
	private function prepareEdit($forumID) {
		if ($forumID != "") {
			$this->forum = ForumItem::createFromDB($forumID);
		} else {
			$this->forum = ForumItem::createEmpty();
		}
	}

	public function setMessage($message) {
		Toolkit::showMessage("Fehler", $message);
	}

	public function setForum($forum) {
		$this->forum = $forum;
	}

}