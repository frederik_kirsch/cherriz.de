<?php

LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.ForumQuery.php");

class ForumOverview implements SubPage {

    public function drawContent() {
        echo("<h2>Forenübersicht</h2>");
        echo("Hier findest du eine Übersicht über die verschiedenen Foren.");
	    Toolkit::loadScripts(array("sites/forum_utility.js"));
	    Toolkit::printAsynchrounusContentLoader("sites/forum_content_loader.js", "init();");
    }

}