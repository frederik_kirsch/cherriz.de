<?php

LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.ThreadQuery.php");

/**
 * Class ThreadOverview
 * Uebersicht ueber alle Threads eines Forums.
 *
 * @author Frederik Kirsch
 */
class ThreadOverview implements SubPage {

    private $forum = null;

    public function __construct() {
	    Toolkit::loadStyle('forum.css');

        $forumID = Filter::text($_REQUEST['forum']);
        $this->forum = ForumItem::createFromDB($forumID);
    }

    public function drawContent() {
        echo("<h2>Forum: " . $this->forum->getName() . "</h2>");
        echo("Hier findest du eine Übersicht über die existierenden Threads.");
	    Toolkit::loadScripts(array("sites/thread_utility.js"));
	    Toolkit::printAsynchrounusContentLoader("sites/thread_content_loader.js", "initThreadOverview(".$this->forum->getID().");");
    }

}