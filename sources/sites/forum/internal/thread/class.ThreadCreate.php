<?php

/**
 * Class ThreadCreate
 * Unterseite fuer das Erstellen oder Bearbeten von Threads.
 */
class ThreadCreate implements SubPage {

    /**
     * @var User
     */
    private $user = null;

	/**
	 * @var ForumItem
	 */
    private $forum = null;

	/**
	 * @var ThreadItem
	 */
    private $thread = null;

    /**
     * Erzeugt die Seite, initialisiert variablen. Wird eine ID mit uebergeben wird das Forum zur Aenderung vorbereitet.
     */
    public function __construct() {
        $this->user = Login::getInstance()->getCurrentUserObject();

        $forumID = Filter::text($_GET['forum']);
        $this->forum = ForumItem::createFromDB($forumID);
        $threadID = Filter::text($_GET['thread']);
        $this->prepareEdit($threadID);
    }

    public function setMessage($message){
        Toolkit::showMessage("Fehler", $message);
    }

    public function setThread($thread){
        $this->thread = $thread;
    }

    public function drawContent() {
        $edit = $this->thread->getID() != null;

        $text = "anlegen";
        if ($edit) {
            $text = "bearbeiten";
        }
        echo("<h2>Thread " . $text . "</h2>");
        echo("<form action='index.php?page=" . MenuThread::SINGLE . "&forum=".$this->forum->getID()."' method='post' id='thread_form'>");

        //Name
        echo("<div class='description_box'>
				  <div class='description_attribute_box'>Titel</div>
				  <div class='description_value_box'>
				      <input type='text' name='title' id='title' value='" . $this->thread->getTitle() . "'/>
				  </div>
              </div>");

        //Berechtigung
        echo("<div class='description_box'>
				  <div class='description_attribute_box'>Berechtigung</div>
				  <div class='description_value_box'>");
        Toolkit::printGroupSelector($this->thread->getGroups());
        echo("    </div>
              </div>");

        //Beschreibung
        echo("<div class='description_box'>
				  <div class='description_attribute_box'></div>
				  <div class='description_value_box'>");
        Toolkit::printWYSIWYG("post", $this->thread->getInitialPost()->getText(), 625, 600);
        echo("	  </div>
              </div>");

        //Steuerung
        $text = "Erstellen";
        if ($edit) {
            $text = "Bearbeiten";
        }
        echo("<div class='description_box'>
				  <div class='description_attribute_box'>Aktionen</div>
				  <div class='description_value_box'>
				      <input type='button' value='" . $text . "' onclick='createThread()' />
				      <input type='button' value='Verwerfen' onclick='resetThread()'/>
				  </div>
              </div>");

        echo("    <input type='hidden' name='submitted' value='yes'/>
                  <input type='hidden' name='id' value='" . $this->thread->getID() . "'/>
              </form>");

        //Scripting
        Toolkit::loadScripts(array("sites/thread_create_utility.js"),
                             "$('#thread_form').submit(function() { return submitThread() })");
    }

    /**
     * Laedt anhand der uebergebenen ID das Forum aus der Datenbank oder erzeugt ein leeres Forumsobjekt.
     *
     * @param string $threadID ID des zu ladenden Forums.
     */
    private function prepareEdit($threadID) {
        if ($threadID != "") {
            $this->thread = ThreadItem::createFromDB($threadID);
        }else{
	        $this->thread = ThreadItem::createEmpty();
        }
    }

}