<?php

LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.ThreadPostQuery.php");

class ThreadSingle implements SubPage {

	private $login = null;

	private $forum = null;

	private $thread = null;

	public function __construct() {
		$this->login = Login::getInstance();

		Toolkit::loadStyle('post.css');

		$threadID = Filter::text($_REQUEST['thread']);
		$this->thread = ThreadItem::createFromDB($threadID);
	}

	public function setMessage($message) {
		Toolkit::showMessage("Fehler", $message);
	}

	public function setThread($thread) {
		$this->thread = $thread;
	}

	public function drawContent() {
		if($this->thread->checkAccess()){
			$this->drawThread();
		}else{
			Toolkit::printAccessDenied();
		}
	}

	public function drawThread() {
		$this->forum = ForumItem::createFromDB($this->thread->getForum());
		echo("<h2>Thread: " . $this->thread->getTitle() . "</h2>");
		//Stellt die Posts des Threads dar.
		Toolkit::printAsynchrounusContentLoader("sites/thread_content_loader.js", "initThread(".$this->thread->getID().");");
		$this->showInput(true);

		$script = "$('#post_button').click(function() {submitPost($('#Post').val()," . $this->thread->getID() . ");});";
		Toolkit::loadScripts(array("sites/thread_single_utility.js"), $script);
	}

	/**
	 * Stellt das Eingabeformular dar.
	 */
	private function showInput() {
		echo("<div id='post_box' class='post_form' style='display: none'>");
		if ($this->login->isLoggedIn()) {
			//Kommentarformular
			echo("Hinterlasse hier deine Nachricht");
			Toolkit::printWYSIWYG("Post", "", 625, 200);
			echo("<input id='post_button' type='submit' value='Veröffentlichen'>");
		} else {
			echo("Du musst eingeloggt sein um einen Kommentar zu verfassen.<br /><a href='index.php?page=" . Menu::REGISTRIERUNG . "'>Zur Registrierung</a>");
		}
		echo("</div>");
	}

}