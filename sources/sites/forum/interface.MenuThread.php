<?php

interface MenuThread {

	const OVERVIEW = "ThreadOverview";

	const CREATE   = "ThreadCreate";

	const SINGLE   = "ThreadSingle";

}