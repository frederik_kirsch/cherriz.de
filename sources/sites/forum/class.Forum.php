<?php

/**
 * Class Forum
 * Ueberseite fuer das Forum.
 *
 * @author Frederik Kirsch
 */
class Forum extends Page {

    private $user = null;

    public function __construct() {
        parent::__construct();
        $this->user = Login::getInstance()->getCurrentUserObject();

        LibImporter::import("basic/entity/class.ForumItem.php");
        Toolkit::loadStyle('forum.css');

        if (Filter::text($_REQUEST['submitted']) == "yes") {
            $id = Filter::text($_REQUEST['id']);
            if(is_numeric($id)){
                $this->processAction($id);
            }else{
                $this->processAction();
            }
        }
    }

    protected function getNavBarEntries() {
        $pages = array();
        array_push($pages, SubPageSettings::init(MenuForum::OVERVIEW, "Foren Übersicht"));
	    array_push($pages, SubPageSettings::init(MenuForum::CREATE, "Forum Erstellen"));
        return $pages;
    }

    public function hasAccess() {
        $access = false;
        switch ($this->determineSubPage()) {
            case MenuForum::CREATE:
                if (Login::getInstance()->isLoggedIn() && Login::getInstance()->getCurrentUserObject()->hasRight(Recht::FORUM_ADMIN)) {
                    $access = true;
                }
                break;
            default:
                $access = true;
                break;
        }
        return $access;
    }

    public function hasNavBar() {
        if ($this->user->hasRight(Recht::FORUM_ADMIN)) {
            return true;
        }
        return false;
    }

    /**
     * Erstellt ein neues Forum oder aktualisiert ein Bestehendes.
     * @param null|int $id ID eines Forums oder null falls neu.
     */
    private function processAction($id = null) {
        $name = Filter::text($_REQUEST['name']);
        $desc = Filter::text($_REQUEST['shortdesc']);
        $forum = null;

        if($id == null){
            // Erstellen
            $forum = ForumItem::create($name, $desc);
        }else{
            //Aktualisieren
            $forum = ForumItem::createFromDB($id);
            $forum->setName($name);
            $forum->setDescription($desc);
        }

        $result = $forum->save();
        if($result->isSuccessful()){
	        $this->redirect(MenuForum::OVERVIEW);
        }else{
	        $this->redirect(MenuForum::CREATE);
            $this->setSubPageProperty("forum", $forum);
            $this->setSubPageProperty("message", $result);
        }
    }

} 