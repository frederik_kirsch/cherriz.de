<?php

LibImporter::import("basic/entity/class.ForumItem.php");
LibImporter::import("basic/entity/class.ThreadItem.php");
LibImporter::import("basic/entity/class.ThreadPost.php");

/**
 * Class Thread
 * Ueberseite fuer Threads.
 *
 * @author Frederik Kirsch
 */
class Thread extends Page {

    private $user = null;

    private $forum = null;

    public function __construct() {
        parent::__construct();
        $this->user = Login::getInstance()->getCurrentUserObject();

        $this->forum = Filter::text($_REQUEST['forum']);

        if (Filter::text($_REQUEST['submitted']) == "yes") {
            $id = Filter::text($_REQUEST['id']);
            if(is_numeric($id)){
                $this->processAction($id);
            }else{
                $this->processAction();
            }
        }
    }

    protected function getNavBarEntries() {
        $pages = array();
	    array_push($pages, SubPageSettings::init(MenuForum::OVERVIEW, "Foren Übersicht")->addPara("forum"));
        array_push($pages, SubPageSettings::init(MenuThread::OVERVIEW, "Thread Übersicht")->addPara("forum"));
	    array_push($pages, SubPageSettings::init(MenuThread::CREATE, "Thread erstellen")->addPara("forum"));
        return $pages;
    }

    public function hasAccess() {
        $access = false;
        switch ($this->determineSubPage()) {
	        case MenuThread::CREATE:
                if (Login::getInstance()->isLoggedIn() && Login::getInstance()->getCurrentUserObject()->hasRight(Recht::OPEN_THREAD)) {
                    $access = true;
                }
                break;
            default:
                $access = true;
                break;
        }
        return $access;
    }

    public function hasNavBar() {
        if ($this->user->hasRight(Recht::OPEN_THREAD)) {
            return true;
        }
        return false;
    }

    /**
     * Erzeugt einen neuen Thread oder aktualisiert einen Bestehenden.
     * Sorgt zusaetzlich fuer die nachfolgende Darstellung abhaengig vom Ergebnis der Anlage / Aktualisierung.
     * @param null|int $id
     */
    private function processAction($id = null){
        $title = Filter::text($_REQUEST['title']);
        $post = Filter::text($_REQUEST['post']);
        $groups = Filter::text($_REQUEST['groups']);
        $forumID = Filter::text($_REQUEST['forum']);
        $grpArray = array();
        if (strlen($groups) > 0) {
            $grpArray = explode(",", $groups);
        }

        $thread = null;
        if($id == null){
            $thread = ThreadItem::create($forumID, $title, $post, $grpArray);
        }else{
            $thread = ThreadItem::createFromDB($id);
            $thread->setTitle($title);
            $thread->setPost($post);
            $thread->setGroups($grpArray);
        }

        $result = $thread->save();
        $this->setSubPageProperty("thread", $thread);
        if($result->isSuccessful()){
            $this->redirect(MenuThread::SINGLE);
        }else{
            $this->redirect(MenuThread::CREATE);
            $this->setSubPageProperty("message", $result);
        }
    }

}