<?php

/**
 * Interface Access
 * Definiert die unterschiedlichen Zugriffsvarianten für ein Element.
 *
 * @author Frederik Kirsch
 */
interface Access
{

    const TYPE_PUBLIC = "Public";

    const TYPE_MYGROUPS = "Umfeld";

    const TYPE_SPECIFIC = "Spezifisch";

    function getVisibility();

    function getGroups();

} 