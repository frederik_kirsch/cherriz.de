<?php

LibImporter::import("sites/class.SubPageSettings.php");
LibImporter::import("sites/interface.SubPage.php");

/**
 * Class Page
 * Oberklassse aller darstellbaren Seiten.
 * @author Frederik Kirsch
 */
abstract class Page {

	private $subPages;

	private $subPage;

	private $redirected = false;

	private $properties = null;

	protected $dbCon;

	/**
	 * Konstruktor kümmert sich um die initialisierung der Subseiten.
	 */
	public function __construct() {
		$this->dbCon = DBConnect::getDBConnection();
		$this->subPages = $this->getNavBarEntries();
	}

	/**
	 * Gibt an ob der User für die Darstellung dieser Seite eingeloggt sein muss.
	 * @return bool
	 */
	public function requiresLogin() {
		return false;
	}

	/**
	 * Gibt an ob der User Zugriff auf diese Seite hat.
	 *
	 * @return bool
	 */
	public function hasAccess() {
		return true;
	}

	/**
	 * Gibt an ob diese Seite eine Navigationsleiste hat.
	 *
	 * @return bool
	 */
	public function hasNavBar() {
		return count($this->subPages) > 0;
	}

	/**
	 * Stellt den Standardcontent der Seite dar.
	 */
	public function drawContent() {

	}

	/**
	 * Setzt die Standardunterseite, die gilt, falls sie nicht geandert wird.
	 * @param string $subPage Die Standardunterseite.
	 */
	public function setDefaultSubPage($subPage) {
		if (!$this->redirected && $subPage != null) {
			$this->subPage = $subPage;
		}
	}

	/**
	 * Aendert die Unterseite.
	 * @param string $sub Die neue Unterseite.
	 */
	public function redirect($sub) {
		$this->redirected = true;
		$this->subPage = $sub;
	}

	/**
	 * @return null|string Die darzustellende Unterseite.
	 */
	public function determineSubPage() {
		if ($this->subPage != null) {
			return $this->subPage;
		} else if (count($this->subPages) > 0) {
			return $this->subPages[0]->getId();
		}
		return null;
	}

	/**
	 * Hinterlegt einen Wert für eine Unterseite die bei dieser dann gesetzt werden.
	 *
	 * @param string       $prop  Eigenschaft.
	 * @param string|array $value Wert.
	 */
	protected function setSubPageProperty($prop, $value) {
		if ($this->properties == null) {
			$this->properties = array();
		}
		$this->properties[$prop] = $value;
	}

	/**
	 * @return string[]
	 */
	public function getSubPageProperties() {
		return $this->properties;
	}

	/**
	 * Zeichnet die Navigationsleiste.
	 * @return string Die Navigationsleiste.
	 */
	public final function getNavbar() {
		$result = "<h2>Navigation</h2>";
		$result .= "<ul>";
		foreach ($this->subPages as $subPageSettings) {
			$url = "index.php?page=" . $subPageSettings->getId() . $subPageSettings->getParas();
			$name = $subPageSettings->getName();
			$result .= "<li><a href='$url'>$name</a></li>";
		}
		$result .= "</ul>";
		return $result;
	}

	/**
	 * @return SubPageSettings[]
	 */
	protected function getNavBarEntries() {
		return array();
	}

}