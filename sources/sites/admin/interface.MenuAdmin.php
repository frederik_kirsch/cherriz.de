<?php

interface MenuAdmin {

	const WEBSEITE   = "AdminWebseite";

	const USER       = "AdminUser";

	const NEWS       = "AdminNews";

	const STATISTICS = "AdminStatistics";

}