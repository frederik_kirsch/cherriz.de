<?php

class Admin extends Page
{

    public function __construct()
    {
        parent::__construct();
    }

    public function requiresLogin()
    {
        return true;
    }

    protected function getNavBarEntries()
    {
	    $user = Login::getInstance()->getCurrentUserObject();
        $pages = array();
        if ($user->hasRight(Recht::CONSTRUCTION_MODE) || $user->hasRight(Recht::CHANGE_IMPRESSUM)) {
            array_push($pages, SubPageSettings::init(MenuAdmin::WEBSEITE, "Webseite"));
        }
        if ($user->hasRight(Recht::CHANGE_RIGHTS)) {
            array_push($pages, SubPageSettings::init(MenuAdmin::USER, "User"));
        }
        if ($user->hasRight(Recht::MANAGE_GENRE)) {
            array_push($pages, SubPageSettings::init(MenuAdmin::NEWS, "News"));
        }
        if ($user->hasRight(Recht::VIEW_STATISTICS)) {
            array_push($pages, SubPageSettings::init(MenuAdmin::STATISTICS, "Statistik"));
        }
        return $pages;
    }

    public function hasAccess()
    {
        $result = false;
        if (Login::getInstance()->getCurrentUserObject()->hasRight(Recht::ACCESS_ADMIN)) {
            $result = true;
        }
        return $result;
    }

    public function drawContent()
    {
        echo("<h2>Administration</h2>");
    }

}