<?php

/**
 * Class Impressum
 * Stellt das Impressum grafisch dar.
 * @author Frederik Kirsch
 */
class Impressum extends Page {

	public function drawContent() {
		$settings = Settings::getInstance();
		$text = $settings->getProperty(Settings::PROPERTY_IMPRESSUM_CONTENT);

		echo("<h2>Impressum</h2>");
		echo(Converter::bbToHtml($text));
	}

}