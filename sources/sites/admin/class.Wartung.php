<?php

/**
 * Class Wartung
 * Stellt die Wartungsseite grafisch dar.
 * @author Frederik Kirsch
 */
class Wartung extends Page {

	public function drawContent() {
		$settings = Settings::getInstance();
		$text = Converter::bbToHtml($settings->getProperty(Settings::PROPERTY_UNDER_CONSTRUCTION_CONTENT));

		echo("<h2>Wartungsarbeiten</h2>");
		echo($text);
	}

}