<?php

/**
 * Class Datenschutz
 * Stellt die Datenschutzerklaerung dar.
 * @author Frederik Kirsch
 */
class Datenschutz extends Page {

	public function drawContent() {
		$settings = Settings::getInstance();
		$text = $settings->getProperty(Settings::PROPERTY_DATENSCHUTZ_REGISTRATION);

		echo("<h2>Datenschutz</h2>");
		echo(Converter::bbToHtml($text));
	}

}