<?php

class AdminNews implements SubPage {

	const ACTION_NEWS_GENRE = "NewsGenre";

	private $db;

	private $user;

	private $settings;

	public function __construct() {
		$this->settings = Settings::getInstance();
		$this->db = DBConnect::getDBConnection();
		$this->user = Login::getInstance()->getCurrentUserObject();
	}

	public function drawContent() {
		$action = Filter::text($_REQUEST['action']);
		if ($action != "") {
			$messages = $this->processChanges($action);
			$this->drawMeldungen($messages);
		}

		$this->drawElements();
	}

	private function drawMeldungen($messages) {
		if ($messages != null && sizeof($messages) > 0) {
			echo("<ul>");
			foreach ($messages as $item) {
				echo("<li style='color: #f40000;'>" . $item . "</li>");
			}
			echo("</ul>");
		}
	}

	private function drawElements() {
		if (!$this->user->hasRight(Recht::MANAGE_GENRE)) {
			return;
		}
		echo("<h3>Genre erstellen</h3>
			  Neues Genre erstellen.
			  <form action='index.php?page=" . MenuAdmin::NEWS . "&action=" . AdminNews::ACTION_NEWS_GENRE . "' method='post'>
				  <div class='description_box'>
				      <div class='description_attribute_box'>Bezeichnung</div>
				      <div class='description_value_box'><input type='text' name='genre_new' /></div>
				  </div>
				  <div class='description_box'>
				      <div class='description_attribute_box'>Aktion</div>
				      <div class='description_value_box'><input type='submit' value='Erstellen' /></div>
				  </div>
			  </form>");

		echo("<h3>Genreübersicht & Verwendung</h3>
			  Genres können nicht entfernt sobald diese referenziert werden.");
		$genre = $this->db->query("SELECT G.Genre, (SELECT COUNT(*)
													  FROM News_Genre NG
													 WHERE NG.Genre = G.ID) AS Anz
									FROM Genre G
								ORDER BY G.Genre");

		//Formular zur Rechteverwaltung
		while ($item = $genre->fetch_object()) {
			echo("<div class='description_box'>
					  <div class='description_attribute_box'>Referenzen: " . $item->Anz . "</div>
					  <div class='description_value_box'>[" . $item->Genre . "]</div>
				  </div>");
		}
	}

	private function processChanges($action) {
		$messages = array();
		switch ($action) {
			case AdminNews::ACTION_NEWS_GENRE:
				if ($this->user->hasRight(Recht::MANAGE_GENRE)) {
					$genre_new = Filter::text($_REQUEST['genre_new']);
					if (strlen($genre_new) < 4) {
						$messages[] = "Fehler: Die Genrebezeichnung muss mindestens 4 Stellen haben.";
					} else {
						$select = "SELECT * FROM Genre WHERE Genre = '" . $genre_new . "'";
						$result = $this->db->query($select);
						if ($result->num_rows == 1) {
							$messages[] = "Fehler: Genre existiert bereits.";
						} else {
							$insert = "INSERT INTO Genre (Genre) VALUES ('" . $genre_new . "')";
							if ($this->db->query($insert)) {
								$messages[] = "Erfolg: Genre erstellt.";
							} else {
								$messages[] = "Fehler: Probleme bei der Datenbankkommunikation.";
							}
						}
					}
				} else {
					$messages[] = "Warnung: Dazu haben Sie nicht die notwendigen Rechte.";
				}
				break;
		}
		return $messages;
	}

} 