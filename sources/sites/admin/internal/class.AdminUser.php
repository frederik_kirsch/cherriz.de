<?php

/**
 * Class AdminUser
 * Administrationsseite zur Verwaltung von Benutzern.
 *
 * @author Frederik Kirsch
 */
class AdminUser implements SubPage {

	const ACTION_RECHTE = "Rechte";

	private $db;

	private $user;

	private $settings;

	public function __construct() {
		$this->settings = Settings::getInstance();
		$this->db = DBConnect::getDBConnection();
		$this->user = Login::getInstance()->getCurrentUserObject();
	}

	public function drawContent() {
		echo("<h3>Nutzer auswahl</h3>");
		echo("Wähle zuerst einen User aus um eine Berechtigung zu Vergeben, oder aufzuheben.");
		echo("<div class='description_box'>
				  <div class='description_attribute_box'>Aktion</div>
				  <div class='description_value_box'>
				  	  <input type='button' onclick='new UserSearchDialog(function(user){new AdminUserControl(user)}).open()' value='User auswählen' />
			      </div>
			  </div>");
		echo("<div id='userDiv'></div>");
		Toolkit::loadScripts(array("sites/admin_user.js"));

		echo("<h3>Benutzerübersicht</h3>");
		Toolkit::printAsynchrounusContentLoader("sites/user_content_loader.js", "initUserSearch();");
	}

} 