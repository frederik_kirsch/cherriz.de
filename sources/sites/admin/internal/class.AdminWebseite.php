<?php

/**
 * Class AdminWebseite
 * Adminbereich fuer die Verwaltung der Webseite
 * @author Frederik Kirsch
 */
class AdminWebseite implements SubPage {

	const ACTION_WARTUNG = "Wartung";

	const ACTION_WARTUNG_TEXT = "WartungText";

	const ACTION_IMPRESSUM = "Impressum";

	private $db;

	private $user;

	private $settings;

	public function __construct() {
		$this->settings = Settings::getInstance();
		$this->db = DBConnect::getDBConnection();
		$this->user = Login::getInstance()->getCurrentUserObject();
	}

	public function drawContent() {
		if ($this->user->hasRight(Recht::CONSTRUCTION_MODE)) {
			$constMode = "true";
			if ($this->settings->getProperty(Settings::PROPERTY_UNDER_CONSTRUCTION) == "true") {
				$constMode = "false";
			}

			echo("<h3>Wartungsseite</h3>");
			$this->drawWartung($constMode);
			$this->drawWartungContent();
		}
		if ($this->user->hasRight(Recht::CHANGE_IMPRESSUM)) {
			$this->drawImpressum();
		}
		if ($this->user->hasRight(Recht::CHANGE_DATENSCHUTZ)) {
			$this->drawDatenschutz();
		}
		$script = "var adminCtrl = new AdminWebsiteControl($('#wartung_btn'));
				   $('#constBtn').click(function() {adminCtrl.changeWartungText($('#constText').val())});
				   $('#impressumBtn').click(function() {adminCtrl.changeImpressumText($('#impressumText').val())});
				   $('#datenschutzBtn').click(function() {adminCtrl.changeDatenschutzText($('#datenschutzText').val())});";
		Toolkit::loadScripts(array("sites/admin_website.js"), $script);
	}

	private function drawWartung($constMode) {
		$icon = "off";
		if ($constMode == "false") {
			$icon = "on";
		}

		echo("<div class='description_box'>
				  <div class='description_attribute_box'>Wartungsseite</div>
				  <div class='description_value_box'><img id='wartung_btn' src='gfx/button/switch_$icon.png' /></div>
			  </div>");
	}

	private function drawWartungContent() {
		echo("<div class='description_box'>");
		Toolkit::printWYSIWYG("constText", $this->settings->getProperty(Settings::PROPERTY_UNDER_CONSTRUCTION_CONTENT), 600, 300);
		echo("	  <div class='description_attribute_box'>Aktion</div>
				  <div class='description_value_box'><input id='constBtn' type='button' value='Speichern' /></div>
			  </div>");
	}

	private function drawImpressum() {
		echo("<h3>Impressum</h3>");
		echo("<div class='description_box'>");
		Toolkit::printWYSIWYG("impressumText", $this->settings->getProperty(Settings::PROPERTY_IMPRESSUM_CONTENT), 600, 300);
		echo("	  <div class='description_attribute_box'>Aktion</div>
				  <div class='description_value_box'><input id='impressumBtn' type='button' value='Speichern' /></div>
			  </div>");
	}

	private function drawDatenschutz() {
		echo("<h3>Datenschutzerklärung</h3>");
		echo("<div class='description_box'>");
		Toolkit::printWYSIWYG("datenschutzText", $this->settings->getProperty(Settings::PROPERTY_DATENSCHUTZ_REGISTRATION), 600, 300);
		echo("	  <div class='description_attribute_box'>Aktion</div>
				  <div class='description_value_box'><input id='datenschutzBtn' type='button' value='Speichern' /></div>
			  </div>");
	}

} 