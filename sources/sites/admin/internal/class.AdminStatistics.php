<?php

/**
 * Class AdminStatistics Stellt Statistikdaten und generelle Informationen zur Webseite dar.
 * @author Frederik Kirsch
 */
class AdminStatistics implements SubPage {

	public function drawContent() {
		$this->drawAvailablitity();
		$this->drawPHPInfo();
	}

	private function drawAvailablitity() {
		echo("<h3>Verfügbarkeit</h3>");
		echo("<div class='description_box'>
				  <div class='description_attribute_box'>
				      Zeitraum:
				  </div>
				  <div class='description_value_box'>
				      <select>
                          <option value='day'>Tag</option>
                      </select>
				  </div>
              </div>
              <div class='description_box'>
				  <div class='description_attribute_box'>
				      Starttag:
				  </div>
				  <div class='description_value_box'>
				      <input type='text' id='date' value='" . date("d.m.Y") . "' class='calendar' />");
		Toolkit::printDatePickerScripts("date", false, date("d.m.Y", strtotime("-90 days")), date("d.m.Y"));
		echo("	  </div>
              </div>
			  <div class='description_box'>
				  <div class='description_attribute_box'>
				      Intervall:
				  </div>
				  <div class='description_value_box' id='date_text'>");
		echo(date("d.m.Y 00:00:00") . " - " . date("d.m.Y 23:59:59"));
		echo("	  </div>
			  </div>");
		echo("<img id='img_av' src='gfx/img_availability.php' />");
		echo("<img id='img_load' src='gfx/ladebalken.gif' style='display: none;'/>");
		$this->printDateSelectorScripts("date");
	}

	private function drawPHPInfo() {
		ob_start();
		phpinfo();
		$variable = htmlentities(ob_get_contents());
		ob_get_clean();
		echo("<h3>PHPInfo</h3>");
		echo("<iframe src='data:text/html;charset=utf-8,$variable' style='width: 600px; height: 400px;' ></iframe>");
	}

	private function printDateSelectorScripts($dateField) {
		$exec = "var data = {img: $('#img_av'), img_load: $('#img_load'), date_text: $('#date_text'), imgUrl: 'gfx/img_availability.php'};
                 $('#" . $dateField . "').focusout(data, handleDateChange);";
		Toolkit::loadScripts(array("sites/admin_utility.js"), $exec);
	}

}