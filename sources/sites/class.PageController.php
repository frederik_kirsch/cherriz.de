<?php

LibImporter::import("sites/admin/interface.MenuAdmin.php");
LibImporter::import("sites/news/interface.MenuNews.php");
LibImporter::import("sites/feed/interface.MenuFeed.php");
LibImporter::import("sites/forum/interface.MenuForum.php");
LibImporter::import("sites/forum/interface.MenuThread.php");
LibImporter::import("sites/gruppe/interface.MenuGruppe.php");
LibImporter::import("sites/gruppe/interface.MenuGruppen.php");
LibImporter::import("sites/profile/interface.MenuProfile.php");
LibImporter::import("sites/registrierung/interface.MenuRegistrierung.php");
LibImporter::import("sites/registrierung/interface.MenuWiederherstellung.php");

LibImporter::import("sites/class.PageProperties.php");
LibImporter::import("sites/class.PageContent.php");

/**
 * Class PageController
 *
 * @author Frederik Kirsch
 */
class PageController {

	private static $instance = null;

	private $dbCon;

	private $login;

	private $menu;

	private $subMenu;

	/**
	 * @return PageController
	 */
	public static function getInstance() {
		if (PageController::$instance == null) {
			PageController::$instance = new PageController();
		}
		return PageController::$instance;
	}

	private function __construct() {
		$this->dbCon = DBConnect::getDBConnection();;
		$this->login = Login::getInstance();
		$this->initMenu();
		$this->initSubMenu();
	}

	/**
	 * Ermittelt welche Seiten angezeigt werden sollen und erzeugt diese.
	 * @return PageContent Die erzeugten Seitenobjekte.
	 */
	public function determineAndLoadPage() {
		$properties = $this->getProperties();

		// Seite importieren und initialisieren
		LibImporter::import($properties->getMenuClass());
		$className = $properties->getMenu();
		/** @var Page $page */
		$page = new $className;
		$page->setDefaultSubPage($properties->getSubMenu());
		$subPageName = $page->determineSubPage();

		// Unterseite importieren und initialisieren
		/** @var SubPage $subPage */
		$subPage = null;
		if ($subPageName != null) {
			$props = $this->subMenu[$subPageName];
			LibImporter::import($props[1]);
			$subPage = new $subPageName;
			$properties = $page->getSubPageProperties();
			if ($properties != null) {
				foreach ($properties as $key => $value) {
					$method = "set" . ucfirst($key);
					$subPage->$method($value);
				}
			}
		}
		return new PageContent($page, $subPage);
	}

	/**
	 * Stellt erzeugte Seitenobjekte dar. Stellt dabei auch sicher, das der Anwender befugt ist die Inhalte zu betrachten.
	 *
	 * @param PageContent $pageContent
	 */
	public function showPage($pageContent) {
		$page = $pageContent->getPage();
		if (!$this->login->isLoggedIn() && $page->requiresLogin() || !$page->hasAccess()) {
			Toolkit::printAccessDenied();
		} else {
			$page->drawContent();
			$subPage = $pageContent->getSubPage();
			if ($subPage != null) {
				$subPage->drawContent();
			}
		}
	}

	/**
	 * @return PageProperties Die Eigentschaften die als Parameter beim Seitenaufruf mitgegeben wurden. Sie geben auskunft darueber, was angezeigt
	 *                        werden soll.
	 */
	public function getProperties() {
		$pageName = $this->getPageName();

		if ($this->menu[$pageName] != null) {
			return new PageProperties($pageName, $this->menu[$pageName][0], null);
		} else {
			$subMenu = $this->subMenu[$pageName];
			$menuClass = $this->menu[$subMenu[0]][0];
			return new PageProperties($subMenu[0], $menuClass, $pageName);
		}
	}

	/**
	 * @return string Der Name der anzuzeigenden Seite.
	 */
	private function getPageName() {
		$pageName = Filter::text($_GET['page']);
		$settings = Settings::getInstance();
		$loggedIn = Login::getInstance()->isLoggedIn();
		$currUsr = Login::getInstance()->getCurrentUserObject();
		if ($settings->getProperty(Settings::PROPERTY_UNDER_CONSTRUCTION) == "true" && (!$loggedIn || $loggedIn && !$currUsr->hasRight(Recht::ACCESS_ADMIN))) {
			$pageName = Menu::WARTUNG;
		} else if ($this->menu[$pageName] == null && $this->subMenu[$pageName] == null) {
			$pageName = Menu::FEED;
		}

		return $pageName;
	}

	/**
	 * Macht die Hauptseiten bekannt.
	 */
	private function initMenu() {
		$this->menu = array(Menu::ADMIN             => array("sites/admin/class.Admin.php"),
		                    Menu::NEWS              => array("sites/news/class.News.php"),
		                    Menu::FORUM             => array("sites/forum/class.Forum.php"),
		                    Menu::GRUPPE            => array("sites/gruppe/class.Gruppe.php"),
		                    Menu::GRUPPENUEBERSICHT => array("sites/gruppe/class.GruppenUebersicht.php"),
		                    Menu::PROFILE           => array("sites/profile/class.Profile.php"),
		                    Menu::REGISTRIERUNG     => array("sites/registrierung/class.Registrierung.php"),
		                    Menu::THREAD            => array("sites/forum/class.Thread.php"),
		                    Menu::WIEDERHERSTELLUNG => array("sites/registrierung/class.Wiederherstellung.php"),
		                    Menu::FEED              => array("sites/feed/class.Feed.php"),
		                    Menu::IMPRESSUM         => array("sites/admin/class.Impressum.php"),
		                    Menu::DATENSCHUTZ       => array("sites/admin/class.Datenschutz.php"),
		                    Menu::WARTUNG           => array("sites/admin/class.Wartung.php"));
	}

	/**
	 * Macht die Unterseiten bekannt.
	 */
	private function initSubMenu() {
		$this->subMenu = array(MenuAdmin::WEBSEITE             => array(Menu::ADMIN, "sites/admin/internal/class.AdminWebseite.php"),
		                       MenuAdmin::USER                 => array(Menu::ADMIN, "sites/admin/internal/class.AdminUser.php"),
		                       MenuAdmin::NEWS                 => array(Menu::ADMIN, "sites/admin/internal/class.AdminNews.php"),
		                       MenuAdmin::STATISTICS           => array(Menu::ADMIN, "sites/admin/internal/class.AdminStatistics.php"),
		                       MenuNews::OVERVIEW              => array(Menu::NEWS, "sites/news/internal/class.NewsOverview.php"),
		                       MenuNews::WRITE                 => array(Menu::NEWS, "sites/news/internal/class.NewsWrite.php"),
		                       MenuNews::CONTROL               => array(Menu::NEWS, "sites/news/internal/class.NewsControl.php"),
		                       MenuNews::SINGLE                => array(Menu::NEWS, "sites/news/internal/class.NewsSingle.php"),
		                       MenuNews::PREVIEW               => array(Menu::NEWS, "sites/news/internal/class.NewsPreview.php"),
		                       MenuForum::OVERVIEW             => array(Menu::FORUM, "sites/forum/internal/forum/class.ForumOverview.php"),
		                       MenuForum::CREATE               => array(Menu::FORUM, "sites/forum/internal/forum/class.ForumCreate.php"),
		                       MenuGruppe::OVERVIEW            => array(Menu::GRUPPE, "sites/gruppe/internal/gruppe/class.GruppeOverview.php"),
		                       MenuGruppe::SETTINGS            => array(Menu::GRUPPE, "sites/gruppe/internal/gruppe/class.GruppeSettings.php"),
		                       MenuGruppe::MEMBER              => array(Menu::GRUPPE, "sites/gruppe/internal/gruppe/class.GruppeMember.php"),
		                       MenuGruppen::OVERVIEW           => array(Menu::GRUPPENUEBERSICHT, "sites/gruppe/internal/gruppen/class.GruppenOverview.php"),
		                       MenuGruppen::CREATE             => array(Menu::GRUPPENUEBERSICHT, "sites/gruppe/internal/gruppen/class.GruppeCreate.php"),
		                       MenuProfile::OVERVIEW           => array(Menu::PROFILE, "sites/profile/internal/class.ProfileOverview.php"),
		                       MenuProfile::DATA               => array(Menu::PROFILE, "sites/profile/internal/class.ProfileData.php"),
		                       MenuProfile::SETTINGS           => array(Menu::PROFILE, "sites/profile/internal/class.ProfileSettings.php"),
                               MenuProfile::HA                 => array(Menu::PROFILE, "sites/profile/internal/class.ProfileHA.php"),
		                       MenuRegistrierung::DATENSCHUTZ  => array(Menu::REGISTRIERUNG, "sites/registrierung/internal/registrierung/class.RegistrierungDatenschutz.php"),
		                       MenuRegistrierung::DATEN        => array(Menu::REGISTRIERUNG, "sites/registrierung/internal/registrierung/class.RegistrierungDaten.php"),
		                       MenuRegistrierung::MAIL         => array(Menu::REGISTRIERUNG, "sites/registrierung/internal/registrierung/class.RegistrierungMail.php"),
		                       MenuRegistrierung::ERFOLG       => array(Menu::REGISTRIERUNG, "sites/registrierung/internal/registrierung/class.RegistrierungErfolg.php"),
		                       MenuRegistrierung::DECLINE      => array(Menu::REGISTRIERUNG, "sites/registrierung/internal/registrierung/class.RegistrierungDecline.php"),
		                       MenuThread::OVERVIEW            => array(Menu::THREAD, "sites/forum/internal/thread/class.ThreadOverview.php"),
		                       MenuThread::CREATE              => array(Menu::THREAD, "sites/forum/internal/thread/class.ThreadCreate.php"),
		                       MenuThread::SINGLE              => array(Menu::THREAD, "sites/forum/internal/thread/class.ThreadSingle.php"),
		                       MenuWiederherstellung::DATEN    => array(Menu::WIEDERHERSTELLUNG, "sites/registrierung/internal/wiederherstellung/class.WiederherstellungDaten.php"),
		                       MenuWiederherstellung::MAIL     => array(Menu::WIEDERHERSTELLUNG, "sites/registrierung/internal/wiederherstellung/class.WiederherstellungMail.php"),
		                       MenuWiederherstellung::PASSWORT => array(Menu::WIEDERHERSTELLUNG, "sites/registrierung/internal/wiederherstellung/class.WiederherstellungPasswort.php"),
		                       MenuWiederherstellung::ERFOLG   => array(Menu::WIEDERHERSTELLUNG, "sites/registrierung/internal/wiederherstellung/class.WiederherstellungErfolg.php"),
		                       MenuFeed::OVERVIEW              => array(Menu::FEED, "sites/feed/internal/class.FeedOverview.php"));
	}

}