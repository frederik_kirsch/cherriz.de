<?php

interface MenuNews {

	const MAIN     = "NewsOverview";

	const OVERVIEW = "NewsOverview";

	const SINGLE   = "NewsSingle";

	const PREVIEW  = "NewsPreview";

	const WRITE    = "NewsWrite";

	const CONTROL  = "NewsControl";

}