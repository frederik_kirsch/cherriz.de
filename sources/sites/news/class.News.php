<?php
LibImporter::import("basic/entity/class.NewsItem.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.NewsQuery.php");

/**
 * Class News
 * Uebersichtsseite fuer News.
 *
 * @author Frederik Kirsch
 */
class News extends Page {

	private $user = null;

	public function __construct() {
		parent::__construct();
		$this->user = Login::getInstance()->getCurrentUserObject();
		$this->setDefaultSubPage(MenuNews::OVERVIEW);

		Toolkit::loadStyle("feed.css");
	}

	protected function getNavBarEntries() {
		$subPages = array();
		array_push($subPages, SubPageSettings::init(MenuFeed::OVERVIEW, "Feed Übersicht"));
		array_push($subPages, SubPageSettings::init(MenuNews::OVERVIEW, "News Übersicht"));
		array_push($subPages, SubPageSettings::init(MenuNews::WRITE, "News Schreiben"));
		array_push($subPages, SubPageSettings::init(MenuNews::CONTROL, "Meine News"));
		return $subPages;
	}

	public function hasNavBar() {
		if ($this->user->hasRight(Recht::WRITE_NEWS)) {
			return true;
		}
		return false;
	}

	public function hasAccess() {
		$access = false;
		switch ($this->determineSubPage()) {
			case MenuNews::CONTROL:
			case MenuNews::PREVIEW:
			case MenuNews::WRITE:
				if (Login::getInstance()->isLoggedIn() && Login::getInstance()->getCurrentUserObject()->hasRight(Recht::WRITE_NEWS)) {
					$access = true;
				}
				break;
			default:
				$access = true;
				break;
		}
		return $access;
	}

}