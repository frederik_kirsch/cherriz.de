<?php

/**
 * Class NewsPreview
 * Die Seite stellt News als Vorschau dar und bietet Navigationsmoeglichkeiten zur weiteren Bearbeitung. Sie ist auch in der Lage erstellte Beitraege
 * zu speichern.
 *
 * @author Frederik Kirsch
 */
class NewsPreview implements SubPage {

	private $user;

	private $news;

	public function __construct() {
		$this->user = Login::getInstance()->getCurrentUserObject();
		Toolkit::loadScripts(array("sites/news_utility.js"));
		$this->news = $this->processParameters();
	}

	/**
	 * Erzeugt ein NewsItem aus den Uebgerbenen Parametern. Entweder wird eine News via ID gelesen und aus der DB geladen oder sie wird aus den
	 * Postparametern erzeugt gespeichert und dann geladen.
	 *
	 * @return NewsItem Die anzuzeigende News.
	 */
	private function processParameters() {
		if ($_GET["news"] != "") {
			return NewsItem::createNewsFromDB($_GET["news"]);
		} else {
			$news = $this->createNews();
			if ($news->isValide()) {
				$news->save();
				return NewsItem::createNewsFromDB($news->get(NewsItem::ATTRIBUTE_ID));
			} else {
				return $news;
			}
		}
	}

	public function drawContent() {
		echo("<h2>Vorschau</h2>");

		if ($this->news->isValide()) {
			$this->news->showBig(NewsItem::ACCESS_TYPE_CONTROL);
		} else {
			echo("<h2>Fehler</h2>");
			echo("Die angeforderte News kann nicht dargestellt werden. Bitte kontaktieren Sie den Webseitenadministrator.");
		}

		echo("<div class='description_box'>
                  <div class='description_attribute_box'>Aktionen</div>
                  <div class='description_value_box'>");
		$newsID = $this->news->get(NewsItem::ATTRIBUTE_ID);
		if ($this->news->get(NewsItem::ATTRIBUTE_PUBLISHED) == 1) {
			echo("<input type='button' value='Eingaben korrigieren' onclick='redirect(\"" . MenuNews::WRITE . "\", $newsID)'/>");
		} else {
			echo("<input type='button' value='Weiter bearbeiten' onclick='redirect(\"" . MenuNews::WRITE . "\", $newsID)'/>
                  <input type='button' value='Veröffentlichen' onclick='setNewsPublished($newsID, \"" . MenuNews::PREVIEW . "\")'/>
                  <input type='button' value='Entwurf speichern' onclick='redirect(\"" . MenuNews::CONTROL . "\", null)'/>");
		}
		echo("    </div>
              </div>");
	}

	/**
	 * Erzeugt ein Newsobjekt aus den uebergebene Parameter.
	 * Die News kann entweder eine geaenderte, bereits bestehende oder eine neue sin.
	 *
	 * @return NewsItem Das erzeugte Objekt.
	 */
	private function createNews() {
		//Formulardaten verarbeiten
		$ueberschrift = Filter::text($_POST['headline']);
		$text = Filter::text($_POST['news_text']);

		//Genres
		$genres = Filter::text($_POST['genres']);
		$genres_arr = array();
		if (strlen($genres) > 0) {
			$genres_arr = explode(",", $genres);
		}
		//Gruppen
		$gruppen = trim(Filter::text($_POST['groups']));
		$public = false;
		if (strlen($gruppen) == 0) {
			$public = true;
		}
		$gruppen_arr = array();
		if (strlen($gruppen) > 0) {
			$gruppen_arr = explode(",", $gruppen);
		}
		//Datum
		$delayed = null;
		if (Filter::text($_POST['publish_date_activated'])) {
			$delayed = Filter::text($_POST['publish_date']);
			$delayed = strtotime($delayed);
		}

		$published = false;
		if (Filter::text($_POST['status']) == "published") {
			$published = true;
		}

		//Prüfen ob erstellen oder Update
		$newsID = Filter::text($_POST['news']);
		$news = null;
		if ($newsID != -1) {
			$news = NewsItem::createNewsFromDB($newsID);
			$news->setTitle($ueberschrift);
			$news->setText($text);
			if ($published) {
				$news->setPublished();
			} else {
				$news->setEntwurf();
			}
			$news->setGenres($genres_arr);
			$news->setAccess($public, $gruppen_arr);
		} else {
			$news = NewsItem::createNews($ueberschrift, $this->user->get(User::PROPERTY_ID), $text, $published, $genres_arr, $gruppen_arr);
		}
		if ($delayed != null) {
			$news->setDelayed($delayed);
		}
		return $news;
	}

}