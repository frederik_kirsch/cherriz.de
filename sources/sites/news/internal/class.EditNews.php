<?php
LibImporter::import("sites/interface.Access.php");

/**
 * Class EditNews
 * Repraesentiert eine News die bearbeitet werden soll.
 *
 * @author Frederik Kirsch
 */
class EditNews implements Access
{

    private $id;

    private $title;

    private $genre;

    private $released;

    private $delayed;

    private $visibility;

    private $groups;

    private $text;

    private $author;

    public function __construct($id = -1, $title = "", $text = "", $genres = array(), $released = "", $visibility = "Public", $groups = array(), $author = -1)
    {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
        $this->genre = $genres;
	    $this->delayed = false;
        if ($released != "") {
	        if($released>time()){
		        $this->delayed = true;
	        }
            $released = date("d.m.Y H:i", $released);
        }
        $this->released = $released;
        $this->visibility = $visibility;
        $this->groups = $groups;
        if ($author == -1) {
            $author = Login::getInstance()->getCurrentUserObject();
        }
        $this->author = $author;
    }

	/**
	 * @return int Die ID der News.
	 */
    public function getID()
    {
        return $this->id;
    }

	/**
	 * @return string Der Titel der News.
	 */
    public function getTitle()
    {
        return $this->title;
    }

	/**
	 * @return string Der Inhalt der News.
	 */
    public function getText()
    {
        return $this->text;
    }

	/**
	 * @return array Die Genres der News.
	 */
    public function getGenre()
    {
        return $this->genre;
    }

	/**
	 * @return bool Ob die News bereits veroeffentlicht ist.
	 */
    public function getReleased()
    {
        return $this->released;
    }

	/**
	 * @return bool Ob die News verzoegert ist.
	 */
    public function isDelayed()
    {
        return $this->delayed;
    }

	/**
	 * @return string Typ der Sichtbarkeit.
	 */
    public function getVisibility()
    {
        return $this->visibility;
    }

	/**
	 * @return array Die Gruppen die diese News sehen duerfen.
	 */
    public function getGroups()
    {
        return $this->groups;
    }

	/**
	 * @return User Der Verfasser der News.
	 */
    public function getAuthor()
    {
        return $this->author;
    }

}