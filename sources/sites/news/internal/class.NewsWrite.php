<?php

class NewsWrite implements SubPage {

	/** @var User */
	private $user = null;

	/** @var EditNews */
	private $news = null;

	public function __construct() {
		$this->user = Login::getInstance()->getCurrentUserObject();

		LibImporter::import("sites/news/internal/class.EditNews.php");
		Toolkit::loadScripts(array("sites/news_utility.js"), "$('#news_form').submit(publishNews); handlePublishDate();");
		$this->prepareEdit($_GET['news']);
	}

	/**
	 * Bereitet ein Aenderungsobjekt fuer eine Nachricht vor.
	 *
	 * @param int $id ID der zu bearbeitenden News.
	 */
	private function prepareEdit($id) {
		if ($id != "") {
			$item = NewsItem::createNewsFromDB(Filter::text($id));

			$id = $item->get(NewsItem::ATTRIBUTE_ID);
			$title = $item->get(NewsItem::ATTRIBUTE_TITLE);
			$text = $item->get(NewsItem::ATTRIBUTE_TEXT);
			$created = $item->get(NewsItem::ATTRIBUTE_CREATED);
			$released = $item->get(NewsItem::ATTRIBUTE_DATE);
			$author = $item->get(NewsItem::ATTRIBUTE_AUTHOR);
			$date = "";
			if ($created != $released) {
				$date = $released;
			}
			$genres = array();
			/** @var Genre $genre */
			foreach ($item->get(NewsItem::ATTRIBUTE_GENRES) as $genre) {
				$genres[] = $genre->getID();
			}

			$newsAccess = Access::TYPE_PUBLIC;
			$newsGroups = array();
			if ($item->get(NewsItem::ATTRIBUTE_PUBLIC) === false) {
				$newsAccess = Access::TYPE_MYGROUPS;

				$newsGroups = $item->get(NewsItem::ATTRIBUTE_GROUPS);
				$userGroups = $this->user->getGroups();

				if (count($newsGroups) == count($userGroups)) {
					foreach ($userGroups as $grp) {
						if (!in_array($grp->getID(), $newsGroups)) {
							$newsAccess = Access::TYPE_SPECIFIC;
							break;
						}
					}
				} else {
					$newsAccess = Access::TYPE_SPECIFIC;
				}
			}

			$this->news = new EditNews($id, $title, $text, $genres, $date, $newsAccess, $newsGroups, $author);
		} else {
			$this->news = new EditNews();
		}
	}

	public function drawContent() {
		//Berechtigungsprüfung
		$user = Login::getInstance()->getCurrentUserObject();
		if ($user == null || $this->news->getAuthor()->get(User::PROPERTY_ID) != $user->get(User::PROPERTY_ID)) {
			Toolkit::printAccessDenied();
			return;
		}
		echo("<h2>News verfassen</h2>");
		echo("<form action='index.php?page=" . MenuNews::PREVIEW . "' method='post' id='news_form'>
                  <div class='description_box'>
                      <div class='description_attribute_box'>Überschrift</div>
                      <div class='description_value_box'><input type='text' name='headline' value='" . $this->news->getTitle() . "'></div>
                  </div>
                  <div class='description_box'>
                      <div class='description_attribute_box'>Genre</div>
                      <div class='description_value_box'>");
		$this->printGenreSelector($this->news->getGenre());
		$selected = "";
		$releaseDate = "";
		if ($this->news->isDelayed()) {
			$selected = "checked='checked'";
			$releaseDate = $this->news->getReleased();
		}
		echo("        </div>
                  </div>");
		echo("<div class='description_box'>
                  <div class='description_attribute_box'>Sichtbarkeit</div>
                  <div class='description_value_box'>");
		Toolkit::printGroupSelector($this->news->getGroups());
		echo("    </div>
              </div>");
		//Datum
		echo("    <div class='description_box'>
                      <div class='description_attribute_box'>Publizieren am</div>
                      <div class='description_value_box'>
                          <input type='text' name='publish_date' id='publish_date' class='calendar' value='$releaseDate' disabled/>
                          <input type='checkbox' name='publish_date_activated' id='publish_date_activated' value='true' onclick='handlePublishDate()' " . $selected . "> Verzögern");
		$minDate = date("d.m.Y H:00", strtotime("1 hour"));
		Toolkit::printDatePickerScripts("publish_date", true, $minDate);
		echo("    </div>
              </div>");
		//WYSIWYG
		echo("<div class='description_box'>");
		Toolkit::printWYSIWYG("news_text", $this->news->getText(), 625, 600);
		echo("        </div>
                  <div class='description_box'>
                      <div class='description_attribute_box'>Aktionen</div>
                      <div class='description_value_box'>
                          <input type='hidden' name='status' id='status' />
                          <input type='hidden' name='news' id='news' value='" . $this->news->getID() . "' />
                          <input type='submit' value='Direkt veröffentlichen'/>
                          <input type='button' value='Vorschau' onclick='saveNews()'/>
                          <input type='button' value='Verwerfen' onclick='redirect(\"" . MenuNews::WRITE . "\", " . $this->news->getID() . ")' />
                      </div>
                  </div>
              </form>");
	}

	private function printGenreSelector($initGenreIDs = array()) {
		$initGenres = "";
		if (count($initGenreIDs) > 0) {
			$initGenres = "'" . implode("','", $initGenreIDs) . "'";
		}
		Toolkit::loadScripts(array("genreselector_utility.js"),
		                     "genreSelector = new GenreSelector($('#genres_id'),$('#genres_text'),[$initGenres]);");

		echo("<script type='text/javascript'>var genreSelector;</script>
			  <input type='text' id='genres_text' size='53' disabled/>
              <input type='hidden' name='genres' id='genres_id' />
              <input type='button' value='Ändern' onclick='genreSelector.showSelectionDialog()' />");
	}


}