<?php

class NewsSingle implements SubPage {

	private $user = null;

	private $db = null;

	private $news = null;

	public function __construct() {
		$this->user = Login::getInstance()->getCurrentUserObject();
		$this->db = DBConnect::getDBConnection();

		Toolkit::loadStyle("post.css");

		$this->news = Filter::text($_GET['news']);
	}

	public function drawContent() {
		//Weitere notwendige Imports
		LibImporter::import("service/class.CommentQuery.php");
		LibImporter::import("basic/entity/class.Kommentar.php");

		//News
		$newsItem = NewsItem::createNewsFromDB($this->news);
		$newsItem->showBig(NewsItem::ACCESS_TYPE_AUTHOR);

		if ($newsItem->checkAccess(NewsItem::ACCESS_TYPE_AUTHOR)) {
			//Kommentarverarbeitung zu beginn um korrekte Zählweise neuer Kommentare
			$info = $this->createComment($this->news, $this->user->get(User::PROPERTY_ID));

			$comments = $newsItem->get(NewsItem::ATTRIBUTE_KOMMENTARE);
			if ($comments != 1) {
				$comments .= " Kommentare";
			} else {
				$comments .= " Kommentar";
			}

			//Kommentarsektion Header
			echo("<div class='post_header'>$comments zu \"" . $newsItem->get(NewsItem::ATTRIBUTE_TITLE) . "\"</div>");

			echo("<div class='post_form'>");
			if (Login::getInstance()->isLoggedIn()) {
				//Kommentarformular
				echo("Hinterlasse hier deine Nachricht $info <form action='index.php?page=" . MenuNews::SINGLE . "&news=" . $this->news . "' method='post'>");
				Toolkit::printWYSIWYG("Kommentar", "", 625, 200);
				echo("<input type='submit' value='Kommentieren'></form>");
			} else {
				echo("Du musst eingeloggt sein um einen Kommentar zu verfassen.<br /><a href='index.php?page=" . Menu::REGISTRIERUNG . "'>Zur Registrierung</a>");
			}
			echo("</div>");

			Toolkit::printAsynchrounusContentLoader("sites/comments_content_loader.js", "init(" . $this->news . ");");
		} else {
			Toolkit::printAccessDenied();
		}
	}

	private function createComment($newsID, $userID) {
		$result = "";
		$kommentar = Filter::text($_REQUEST['Kommentar']);
		if (strlen($kommentar) > 0) {
			$insert = "INSERT INTO Kommentar
					   (News, Author, Text, Created) VALUES
					   ($newsID, $userID, '$kommentar', NOW())";
			if ($this->db->query($insert)) {
				$result = "<br />Kommentar erfolgreich!";
				//Benachrichtigung auslösen
				$this->createScheduleTask($this->db->insert_id);
			} else {
				$result = "<br />Fehler beim erstellen des Kommentars. Bitte versuchen Sie es erneut!";
			}
		}
		return $result;
	}

	private function createScheduleTask($commentID) {
		LibImporter::import("cron/class.JobScheduler.php");
		$scheduler = JobScheduler::getInstance();
		$job = CommentJob::createInitial($commentID);
		$scheduler->addJob($job);
	}

}