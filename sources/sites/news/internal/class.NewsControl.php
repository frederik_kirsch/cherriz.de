<?php

class NewsControl implements SubPage
{

    private $user;

    private $login;

    public function __construct()
    {
        $this->login = Login::getInstance();
	    $this->user =$this->login->getCurrentUserObject();
    }

    public function drawContent()
    {
        if($this->login->isLoggedIn() && $this->user->hasRight(Recht::WRITE_NEWS)){
	        echo("<h2>Meine News</h2>");
	        echo("Hier findest du eine übersicht über deine bisher erstellten News und kannst sie zur erneuten Bearbeitung aufrufen.");
	        echo("<br /><b>Anzeigen:</b> ");
	        echo("<input type='radio' name='type' value='all' checked> Alle ");
	        echo("<input type='radio' name='type' value='entwurf'> Entwurf ");
	        echo("<input type='radio' name='type' value='open'> Ausstehend");
	        Toolkit::printAsynchrounusContentLoader("sites/news_content_loader.js", "initControl(".$this->user->get(User::PROPERTY_ID).");");
        }else{
	        echo("<h2>Fehler</h2>");
	        echo("Sie sind nicht eingeloggt oder haben fehlende Berechtigungen.");
        }
    }

}