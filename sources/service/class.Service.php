<?php

LibImporter::import("service/internal/class.Query.php");

/**
 * Class Service
 * Fuehrt Querys gegen die Datenbank aus, und liefert die Ergebnisse zurueck.
 * @author Frederik Kirsch
 */
class Service {

	//Variablen
	private $db = null;

	public function __construct() {
		$this->db = DBConnect::getDBConnection();
	}

	/**
	 * Fuehrt die Query aus und liefert die ermittelten Eintraege zurueck.
	 *
	 * @param Query $query Die Abfrage
	 *
	 * @return array Gefundenen Eintraege.
	 */
	public function requestItems($query) {
		$result = $this->db->query($query);
		$items = array();
		if ($result->num_rows > 0) {
			while ($obj = $result->fetch_object()) {
				array_push($items, $obj);
			}
		}

		return $items;
	}

	/**
	 * Liefert die Anzahl der zur Query insgesamt auffindbaren Eintraege zurueck.
	 *
	 * @param Query $query Die Abfrage.
	 *
	 * @return int Die Anzahl der Eintraege-
	 */
	public function countItems($query) {
		$result = $this->db->query($query->getCountQuery());
		$resultObj = $result->fetch_object();
		return $resultObj->Anz;
	}

}