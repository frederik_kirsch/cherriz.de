<?php

/**
 * Class CommentQuery
 * Abfragekonstrukt zum lesen von Kommentaren.
 *
 * @author Frederik Kirsch
 */
class CommentQuery extends Query {

    private static $SELECT = "SELECT C.ID, C.Created AS Date FROM Kommentar C ";

    private static $ORDER = " ORDER BY C.Created";

    public function __construct($news){
	    $this->setSelect(CommentQuery::$SELECT);
	    $this->setOrder(CommentQuery::$ORDER);
        $this->addCondition("C.News = " . $news);
        $this->setSortOrder(Query::SORT_ORDER_DESC);
    }

}