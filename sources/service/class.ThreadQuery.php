<?php

/**
 * Class ThreadQuery
 * Anfrageobjekt fuer das Lesen von Threads.
 *
 * @author Frederik Kirsch
 */
class ThreadQuery extends Query {

    const SELECT = "SELECT T.ID AS ID, T.Title AS Title FROM Thread T ";

    const ORDER = " ORDER BY T.Relevance DESC, T.Title";

    /**
     * @param integer $forum Das Forum fuer welches die Threads geladen werden sollen.
     */
    public function __construct($forum) {
	    $this->setSelect(ThreadQuery::SELECT);
	    $this->setOrder(ThreadQuery::ORDER);
        $this->addCondition("T.Forum = ".$forum);
    }

    /**
     * @param boolean $deleted <code>true</code> ob geloeschte Threads mitgeladen werden sollen, sonst <code>false</code>.
     */
    public function setDeleted($deleted) {
        $dbDeleted = 1;
        if($deleted === false){
            $dbDeleted = 0;
        }
        $this->addCondition("T.Deleted = ".$dbDeleted);
    }

	/**
	 * @param boolean $public
	 */
    public function setPublic($public) {
        $dbPub = 1;
        if($public === false){
            $dbPub = 0;
        }
        $this->addCondition("T.Public = ".$dbPub);
    }

	/**
	 * @param User $user Der Benutzer der die Threads sehen moechte. Es werden nur Ergebnisse geliefert, zu denen der Benutzer auch berechtigt ist.
	 */
    public function setUser($user) {
        //Gruppen des Users auslesen
        $usr_grps = $user->getGroups();
        $all_grps = array();
        foreach ($usr_grps as $grp) {
            $all_grps[] = $grp->getID();
        }

        $query = "(T.Public = 1 OR T.Creator = " . $user->get(User::PROPERTY_ID) . " ";
        if (count($all_grps) > 0) {
            $query .= "OR (SELECT COUNT(*) FROM Thread_Allowed_Groups A
                          WHERE A.Thread = T.ID
                          AND A.Group IN (" . implode(",", $all_grps) . ")) > 0";
        }
        $query .= " )";
        $this->addCondition($query);
    }

}