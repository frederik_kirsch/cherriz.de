<?php

/**
 * Class ThreadPostQuery
 * Abfragekonstrukt fuer das Lesen von Posts im Forum.
 */
class ThreadPostQuery extends Query {

	const SELECT = "SELECT TP.ID AS ID, TP.Created AS Date FROM ThreadPost TP INNER JOIN Thread T ON T.ID = TP.Thread ";

	const ORDER = " ORDER BY TP.ID";

	/**
	 * @param integer $thread Der Thread zu dem die Posts gelesen werden sollen.
	 */
	public function __construct($thread = null) {
		$this->setSelect(ThreadPostQuery::SELECT);
		$this->setOrder(ThreadPostQuery::ORDER);
		if($thread != null){
			$this->addCondition("TP.Thread = " . $thread);
		}
	}

	public function onlyNewestOfThread(){
		$this->addCondition("TP.ID IN (SELECT TP3.ID FROM (SELECT MAX(TP2.ID) AS ID, TP2.Thread FROM ThreadPost TP2 GROUP BY TP2.Thread) as TP3)");
	}

	/**
	 * @param boolean $public Ob die Ergebnisse oeffentlich einsehbar sein muessen.
	 */
	public function setPublic($public) {
		$dbPub = 1;
		if ($public === false) {
			$dbPub = 0;
		}
		$this->addCondition("T.Public = " . $dbPub);
	}

	/**
	 * @param User $user Der Benutzer der die Posts lesen moechte. Es werden nur Ergebnisse geliefert, zu denen der Benutzer auch berechtigt ist.
	 */
	public function setUser($user) {
		//Gruppen des Users auslesen
		$usr_grps = $user->getGroups();
		$all_grps = array();
		foreach ($usr_grps as $grp) {
			$all_grps[] = $grp->getID();
		}

		$query = "(T.Public = 1 OR T.Creator = " . $user->get(User::PROPERTY_ID) . " ";
		if (count($all_grps) > 0) {
			$query .= "OR (SELECT COUNT(*) FROM Thread_Allowed_Groups A
                          WHERE A.Thread = T.ID
                          AND A.Group IN (" . implode(",", $all_grps) . ")) > 0";
		}
		$query .= " )";
		$this->addCondition($query);
	}

}