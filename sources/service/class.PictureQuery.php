<?php

/**
 * Class PictureQuery
 * Abfragekonstrukt zum lesen von Bildern.
 *
 * @author Frederik Kirsch
 */
class PictureQuery extends Query {

	const SELECT = "SELECT P.ID,
					       P.User AS User_ID,
					       (Select U.Nick
					          FROM Users U
					         WHERE U.ID = P.User) AS User_Nick,
					       P.Tags,
					       P.Date,
					       MATCH (P.Tags) AGAINST ('::find::') AS Relevance
			  		  FROM Picture P ";

	const ORDER_RELEVANCE = " ORDER BY Relevance";

	const ORDER_DATE = " ORDER BY P.Date";

	/** Konstruktor */
	public function __construct() {
		$select = str_replace("::find::", "*", PictureQuery::SELECT);
		$this->setSelect($select);
	}

	/**
	 * @param array $needle Liste mit Woertern anhand derer nach passenden Grafiken gesucht werden soll.
	 */
	public function setNeedle($needle){
		$keywords = "+" . implode("* +", $needle) . "*";
		$select = str_replace("::find::", $keywords, PictureQuery::SELECT);

		$this->setSelect($select);
		$this->addCondition("MATCH (P.Tags) AGAINST ('$keywords' IN BOOLEAN MODE)");
	}

	/**
	 * @param string $status Nur Grafiken mit dem definierten Status duerfen geliefert werden.
	 */
	public function setStatus($status) {
		$this->addCondition("P.Status = (SELECT PS.ID
			  					FROM Picture_Status PS
			  					WHERE PS.Status = '" . $status . "')");
	}

	/**
	 * @param int $author Es werden nur Bilder des festgelegten Authors angezeigt.
	 */
	public function setAuthor($author) {
		$this->addCondition("P.User = $author");
	}

	/**
	 * Sortiert das Ergebnis nach Relevanz.
	 * @param string $sortOrder Sortierrichtung.
	 */
	public function orderByRelevance($sortOrder = Query::SORT_ORDER_DESC){
		$this->setOrder(PictureQuery::ORDER_RELEVANCE, $sortOrder);
	}

	/**
	 * Sortiert das Ergebnis nach Datum.
	 * @param string $sortOrder Sortierrichtung.
	 */
	public function orderByDate($sortOrder = Query::SORT_ORDER_DESC){
		$this->setOrder(PictureQuery::ORDER_DATE, $sortOrder);
	}

}