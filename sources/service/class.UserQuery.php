<?php

/**
 * Class UserQuery
 * Abfragekonstrukt zum lesen von Usern.
 *
 * @author Frederik Kirsch
 */
class UserQuery extends Query {

	const SELECT = "SELECT U.ID, U.Nick, U.Vorname, U.Nachname FROM Users U INNER JOIN User_Status US ON US.ID = U.Status ";

	const ORDER = " ORDER BY U.Nick";

	public function __construct() {
		$this->setSelect(UserQuery::SELECT);
		$this->setOrder(UserQuery::ORDER);
	}

	public function setStatus($status){
		$this->addCondition("US.Status = '$status'");
	}

	public function setSearch($search){
		$this->addCondition("U.Nick LIKE '%$search%'");
	}

}