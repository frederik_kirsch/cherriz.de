<?php

/**
 * Class FeedQuery
 * Abfragekonstrukt zum lesen von verschiedenen Inhalten.
 *
 * @author Frederik Kirsch
 */
class FeedQuery extends Query {

	private $newsQuery = null;

	private $postQuery = null;

	public function __construct() {
		$this->newsQuery = new NewsQuery();
		$this->newsQuery->setPublished(true);
		$this->newsQuery->setReleased();

		$this->postQuery = new ThreadPostQuery();
		$this->postQuery->onlyNewestOfThread();
	}

	/**
	 * @param boolean $public Ob nur oeffentliche Eintraege gelesen werden sollen.
	 */
	public function setPublic($public) {
		$this->newsQuery->setPublic($public);
		$this->postQuery->setPublic($public);
	}

	/**
	 * @param User $user Liefert nur die Eintraege zurueck, die der gesetzte User sehen darf.
	 */
	public function setUser($user) {
		$this->newsQuery->setUser($user);
		$this->postQuery->setUser($user);
	}

	/**
	 * Wandelt die Abfrage so um, das die Gesamtanzahl aller Eintraege unter Beruecksichtung der Bedingungen mit Ausname der Range ermittelt wird.
	 * @return string
	 */
	public function getCountQuery() {
		$query = $this->__toString();
		$queryLength = strlen($query);
		$end = strripos($query, "LIMIT");
		if ($end === false) {
			$end = 0;
		} else {
			$end = $queryLength - $end;
		}
		return "SELECT COUNT(*) AS Anz FROM (" . substr($query, 0, $queryLength - $end) . ") AS TMP";
	}

	public function __toString() {
		$query = "SELECT *, \"news\" as Type FROM (".$this->newsQuery->__toString().") News UNION ALL SELECT *, \"threadpoststandalone\" FROM (".$this->postQuery->__toString(). ") Post ORDER BY Date DESC ";
		if($this->limit != null){
			$query .= $this->limit;
		}
		return $query;
	}

}