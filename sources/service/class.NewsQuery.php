<?php

/**
 * Class NewsQuery
 * Abfragekonstrukt zum lesen von News.
 *
 * @author Frederik Kirsch
 */
class NewsQuery extends Query {

	const SELECT_RELEASED = "SELECT N.ID, N.Released AS Date FROM News N ";

	const SELECT_ID = "SELECT N.ID, N.ID AS Date FROM News N ";

	const ORDER_RELEASED = " ORDER BY N.Released";

	const ORDER_ID = " ORDER BY N.ID";

	const TYPE_ID = "ID";

	const TYPE_RELEASED = "Released";

	public function __construct($type = NewsQuery::TYPE_RELEASED) {
		if($type == NewsQuery::TYPE_RELEASED){
			$this->setSelect(NewsQuery::SELECT_RELEASED);
			$this->setOrder(NewsQuery::ORDER_RELEASED, Query::SORT_ORDER_DESC);
		}else if($type == NewsQuery::TYPE_ID){
			$this->setSelect(NewsQuery::SELECT_ID);
			$this->setOrder(NewsQuery::ORDER_ID, Query::SORT_ORDER_DESC);
		}
	}

	/**
	 * @param boolean $public Ob nur oeffentliche News gelesen werden sollen.
	 */
	public function setPublic($public) {
		$dbPub = 1;
		if ($public === false) {
			$dbPub = 0;
		}
		$this->addCondition("N.Public = " . $dbPub);
	}

	/**
	 * @param string $genre Es wird nur nach News mit dem festgelegten Genre gesucht.
	 */
	public function setGenre($genre){
		$dbGen = Filter::text($genre);
		$this->addCondition("N.ID IN (SELECT NG.News FROM News_Genre NG WHERE NG.Genre IN (SELECT G.ID FROM Genre G WHERE G.Genre LIKE '$dbGen'))");
	}

	/**
	 * @param boolean $published Ob nur publizierte News gelesen werden sollen.
	 */
	public function setPublished($published) {
		$dbPub = 1;
		if ($published === false) {
			$dbPub = 0;
		}
		$this->addCondition("N.Published = " . $dbPub);
	}

	/**
	 * Legt fest das nur News zurueckgeliefert werden die zu diesem Zeitpunkt bereits released wurden.
	 */
	public function setReleased() {
		$this->addCondition("N.Released <= NOW()");
	}

	/**
	 * Legt fest das nur News zurueckgeliefert werden die zu diesem Zeitpunkt noch nicht released wurden.
	 */
	public function setNotReleasedYet() {
		$this->addCondition("N.Released > NOW()");
	}

	/**
	 * @param integer $author Liefert nur News eines spezifischen Authors zurueck.
	 */
	public function setAuthor($author) {
		$this->addCondition("N.Author = " . $author);
	}

	/**
	 * @param User $user Liefert nur die News zurueck, die der gesetzte User sehen darf.
	 */
	public function setUser($user) {
		//Gruppen des Users auslesen
		$usr_grps = $user->getGroups();
		$all_grps = array();
		foreach ($usr_grps as $grp) {
			$all_grps[] = $grp->getID();
		}

		$query = "(N.Public = 1 OR N.Author = " . $user->get(User::PROPERTY_ID) . " ";
		if (count($all_grps) > 0) {
			$query .= "OR (SELECT COUNT(*) FROM News_Allowed_Groups A
                          WHERE A.News = N.ID
                          AND A.Group IN (" . implode(",", $all_grps) . ")) > 0";
		}
		$query .= " )";
		$this->addCondition($query);
	}

}