<?php

/**
 * Class ForumQuery
 * Abfragekonstrukt zum lesen von Foren.
 *
 * @author Frederik Kirsch
 */
class ForumQuery extends Query {

	const SELECT = "SELECT F.ID AS ID, F.Name AS Name FROM Forum F ";

	const ORDER = " ORDER BY F.Name";

	public function __construct() {
		$this->setSelect(ForumQuery::SELECT);
		$this->setOrder(ForumQuery::ORDER);
	}

	/**
	 * @param boolean $deleted Definiert ob auch geloeschte Foren als Ergebnisse zurueckgeliefert werden sollen oder nicht.
	 */
	public function setDeleted($deleted) {
		$dbDeleted = 1;
		if ($deleted === false) {
			$dbDeleted = 0;
		}
		$this->addCondition("F.Deleted = " . $dbDeleted);
	}

}