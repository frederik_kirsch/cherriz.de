<?php

/**
 * Class Query
 * Dient als Grundgeruest fuer das Abfragen von sequentiell zusammengehoerigen Eintraegen.
 * @author Frederik Kirsch
 */
abstract class Query {

	/**
	 * Aufsteigende Sortierung der Eintraege.
	 */
	const SORT_ORDER_ASC = "ASC";

	/**
	 * Absteigende Sortierung der Eintraege.
	 */
	const SORT_ORDER_DESC = "DESC";

	protected $select = null;

	protected $order = null;

	protected $sortOrder = null;

	private $cond = array();

	protected $limit = "";

	/**
	 * Dies ist eine Pflichtangabe und muss zur ordnungsgemäßen Funktion gesetzt sein.
	 *
	 * @param string $select Select Teil eines SQL Statements.
	 */
	protected function setSelect($select) {
		$this->select = $select;
	}

	/**
	 * Dies ist eine Pflichtangabe und muss zur ordnungsgemäßen Funktion gesetzt sein.
	 *
	 * @param string $order Sortierspalten
	 * @param string $dir   Sortierrichtung.
	 */
	protected function setOrder($order, $dir = Query::SORT_ORDER_ASC) {
		$this->order = $order;
		$this->setSortOrder($dir);
	}

	/**
	 * Definiert den Ausschnitt der Abfrage.
	 *
	 * @param int $start Index ab dem die Eintraege gelesen werden.
	 * @param int $max   Maximale Anzahl Eintraege.
	 */
	public function setRange($start, $max) {
		$this->limit = " LIMIT $start, $max";
	}

	/**
	 * @param string $sortOrder Die Sortierrichtung.
	 */
	public function setSortOrder($sortOrder) {
		$this->sortOrder = " " . $sortOrder;
	}

	/**
	 * @param string $condition Fuegt eine Bedingung an die Query an.
	 */
	protected function addCondition($condition) {
		$this->cond[] = $condition;
	}

	/**
	 * Wandelt die Abfrage so um, das die Gesamtanzahl aller Eintraege unter Berücksichtung der Bedingungen mit Ausname der Range ermittelt wird.
	 * @return string
	 */
	public function getCountQuery() {
		$query = $this->__toString();
		$queryLength = strlen($query);
		$start = strpos($query, "FROM");
		$end = strpos($query, "LIMIT");
		if ($end === false) {
			$end = 0;
		} else {
			$end = $queryLength - $end;
		}
		return "SELECT COUNT(*) AS Anz " . substr($query, $start, $queryLength - $start - $end);
	}

	/**
	 * @return string Die Basisabfrage ohne Limit uns Sortierung.
	 */
	public function getBaseQuery(){
		$query = $this->select;
		if (count($this->cond) > 0) {
			$query .= "WHERE " . implode(" AND ", $this->cond);
		}
		return $query;
	}

	/**
	 * @return string Die SQL Query als String.
	 */
	public function __toString() {
		$query = $this->getBaseQuery();
		$query .= $this->order;
		$query .= $this->sortOrder;
		$query .= $this->limit;
		return $query;
	}

}