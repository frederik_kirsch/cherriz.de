<?php

/**
 * Interface Job beschreibt die Schnittstelle für verschiedene JobTypen.
 *
 */
abstract class Job
{

    private $type = null;

    const ITEM_SEPERATOR = ",";

    const VALUE_SEPERATOR = "=";

    const STATUS_INIT = "Init";

    const STATUS_ACTIVE = "Active";

    const STATUS_FINISHED = "Finished";

    const STATUS_CHANGED = "Changed";

    protected function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * Prüft ob der Job noch valide ist und damit ausgeführt werden darf.
     *
     * @return bool Validität
     */
    abstract function verify();

    /**
     * Führt den Job aus.
     *
     */
    abstract function perform();

    /**
     * Persistiert den aktuellen Job in der Datenbank.
     *
     */
    abstract function persist();

    /**
     * Liefert den Typ des Jobs zurück.
     *
     * @return string Der Jobtyp.
     */
    function getType()
    {
        return $this->type;
    }

    /**
     * Serialisiert einen assiziativen Array.
     *
     * @param array $input Zu serialisierender, assoziativer Array.
     * @return string Der serialisierte Array als String.
     */
    protected function serialize(array $input)
    {
        $result = array();
        foreach ($input as $key => $value) {
            $result[] = $key . Job::VALUE_SEPERATOR . $value;
        }
        return (String)implode(Job::ITEM_SEPERATOR, $result);
    }

    /**
     * Deserialisiert einen String.
     *
     * @param String $input Zu deserialisierender String.
     * @return array Der deserialisierte String als assoziativer Array.
     */
    protected  function deserialize($input){
        $result = array();
        $temp = explode(Job::ITEM_SEPERATOR, $input);
        foreach ($temp as $value) {
            $item = explode(Job::VALUE_SEPERATOR, $value);
            $result[$item[0]] = $item[1];
        }
        return $result;
    }

}