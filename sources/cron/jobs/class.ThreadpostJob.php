<?php

LibImporter::import("basic/entity/class.ThreadItem.php");
LibImporter::import("basic/entity/class.ThreadPost.php");
LibImporter::import("basic/util/class.Twitter.php");
LibImporter::import("basic/util/class.Mail.php");
LibImporter::import("basic/util/internal/class.Message.php");

/**
 * Class ThreadpostJob
 *
 * Enthält die Logik für das Benachrichtigen über neue Forenbeiträge.
 *
 * @Author Frederik Kirsch
 */
final class ThreadpostJob extends Job {

	// Processing
	/** @var Twitter */
	private $twitterAPI = null;

	/** @var Mail */
	private $mailAPI = null;

	private $db = null;

	private $devMode = false;

	//Job Information
	private $user = null;

	private $id = null;

	private $data = null;

	private $validationData = null;

	private $status = null;

	private $threadpost = null;

	private $thread = null;

	/**
	 * Erzeugt erstmalig eine Instanz des Jobs zu einem bestimmten Posts.
	 *
	 * @param int $postID Die ID des Posts
	 *
	 * @return CommentJob Die Jobinstanz.
	 */
	public static function createInitial($postID) {
		$data = "ID" . Job::VALUE_SEPERATOR . $postID;
		return new ThreadpostJob(null, $data, $data, Job::STATUS_INIT);
	}

	/**
	 * Erzeugt alle relevaten Konstanten für die Verarbeitung.
	 * Achtung: Konstruktor ist privat. Instanzerzeugung nur über statische Factorymethoden.
	 *
	 * @param int    $id             Die JobID oder <code>null</code>, falls Job noch nie persistiert wurde.
	 * @param string $data           Alle für die Ausführung relevanten Jobdaten.
	 * @param string $validationData Alle für die Validierung relevanten Jobdaten.
	 * @param string $status         Status des Jobs.
	 */
	public function __construct($id, $data, $validationData, $status) {
		parent::__construct("ThreadpostJob");
		$this->db = DBConnect::getDBConnection();
		$settings = Settings::getInstance();
		$this->devMode = $settings->getProperty(Settings::PROPERTY_PAGE_MODE) == Settings::PROPERTY_PAGE_MODE_DEVELOPMENT;

		$this->id = $id;
		$this->data = $this->deserialize($data);
		$this->validationData = $this->deserialize($validationData);
		$this->status = $status;

		$this->threadpost = ThreadPost::createFromDB($this->data["ID"]);
		$this->thread = ThreadItem::createFromDB($this->threadpost->getThread());
	}

	/**
	 * Prüft ob der Job noch valide ist und damit ausgeführt werden darf.
	 *
	 * @return bool Validität
	 */
	function verify() {
		$valide = true;

		//Pruefen ob letzter Post
		$query = "SELECT ID FROM Threadpost WHERE Thread = " . $this->threadpost->getThread() . " AND ID > " . $this->threadpost->getID();
		if ($this->db->query($query)->num_rows) {
			$this->status = Job::STATUS_CHANGED;
			$valide = false;
		}

		//Pruefen ob geloescht
		if ($this->thread->isDeleted()) {
			$valide = false;
		}

		return (boolean) $valide;
	}

	/**
	 * Führt den Job aus.
	 *
	 */
	function perform() {
		$this->init();

		if ($this->devMode) {
			echo("<div>ThreadpostJob(" . $this->id . ")<br />");
			echo("Data: " . implode(", ", $this->data) . "<br />");
			echo("Validation Data: " . implode(", ", $this->validationData) . "<br />");
			echo("Status: " . $this->status . "<br />");
			echo("User: ");
			foreach ($this->user as $usr) {
				echo($usr->Nick . " ");
			}
			echo("</div>");
		} else {
			$this->performDirectMessages();
			$this->performMails();
		}

		$this->status = Job::STATUS_FINISHED;
	}

	/**
	 * Persistiert den aktuellen Job in der Datenbank.
	 *
	 */
	function persist() {
		if ($this->id == null) {
			//Erstmalige speicherung
			$data = $this->serialize($this->data);
			$validationData = $this->serialize($this->validationData);
			$released = date("Y.m.d H:i:s");
			$insert = "INSERT INTO Job (Type, ExecutionTime, Data, ValidationData, Status) VALUES ('" . $this->getType() . "', '" . $released . "', '" . $data . "', '" . $validationData . "', '" . $this->status . "')";
			$this->db->query($insert);
			$this->id = $this->db->insert_id;
		} else {
			//Aktualisierung
			$update = "UPDATE Job SET Status='" . $this->status . "' WHERE ID = " . $this->id;
			$this->db->query($update);
		}
	}

	/**
	 * Initialisiert die Hilfsklassen und ermittelt die zur Verarbeitung notwendigen Variablen
	 */
	private function init() {
		//APIs initialisieren
		$this->twitterAPI = new Twitter();
		$this->mailAPI = new Mail();

		$this->user = $this->determineUser();
	}

	/**
	 * Twitter Direktnachrichten an ermittelten User versenden.
	 */
	private function performDirectMessages() {
		foreach ($this->user as $usr) {
			if ($usr->NotificationTypeTwitter == 1 && !empty($usr->Twitter)) {
				$this->twitterAPI->directMessage($this->getMessage($usr->Nick), $usr->Twitter);
			}
		}
	}

	/**
	 * Emails an die ermittelten User versenden.
	 */
	private function performMails() {
		foreach ($this->user as $usr) {
			if ($usr->NotificationTypeEmail == 1 && !empty($usr->Email)) {
				$this->mailAPI->sendMail($this->getMessage($usr->Nick), $usr->Email);
			}
		}
	}

	/**
	 * Liefert alle User zurück, welche Berechtigt sind diesen Threadpost zu lesen.
	 * @return array Liste der User.
	 */
	private function determineUser() {
		$query = "SELECT U.Nick,
						 U.Email,
						 U.Twitter,
						 U.NotificationThreadpost,
						 U.NotificationTypeEmail,
						 U.NotificationTypeTwitter
					FROM Users U
			  INNER JOIN User_Status US
			  		  ON U . Status = US . ID ";
		if ($this->thread->getVisibility() == Access::TYPE_PUBLIC) {
			$query .= "WHERE US.Status = 'Aktiv'
						 AND U.NotificationThreadpost = 1
						 AND U.ID != (SELECT P.Author
						                FROM ThreadPost P
									   WHERE P.Thread = " . $this->thread->getID() . "
									ORDER BY P.ID DESC
				                       LIMIT 1)
						 AND (U.ID IN (SELECT P.Author
						                 FROM ThreadPost P
										WHERE P.Thread = " . $this->thread->getID() . ")
							  OR U.ID = (SELECT T.Creator
							               FROM Thread T
										  WHERE T.ID = " . $this->thread->getID() . "))";
		} else {
			$query .= "INNER JOIN Gruppe_Mitglied GM
							   ON U.ID = GM.User
							WHERE US.Status = 'Aktiv'
							  AND GM.Gruppe IN (SELECT TAG.Group
							                      FROM Thread_Allowed_Groups TAG
												 WHERE Thread = " . $this->thread->getID() . ")
							  AND U.NotificationThreadpost = 1
							  AND U.ID != (SELECT P.Author
							                 FROM ThreadPost P
											WHERE P.Thread = " . $this->thread->getID() . "
										 ORDER BY P.ID DESC
										    LIMIT 1)
							  AND (U.ID IN (SELECT P.Author
							                  FROM ThreadPost P
											 WHERE P.Thread = " . $this->thread->getID() . ")
								   OR U.ID = (SELECT T.Creator
								                FROM Thread T
											   WHERE T.ID = " . $this->thread->getID() . "))
						 GROUP BY U.Nick";
		}

		$result = $this->db->query($query);
		$users = array();
		while ($user = $result->fetch_object()) {
			$users[] = $user;
		}
		return $users;
	}

	/**
	 * @param String $nick Nich des Users.
	 *
	 * @return Message Die zu versendende Meldung.
	 */
	private function getMessage($nick = null) {
		// 1: Nick
		// 2: Überschrift
		// 3: URL
		$paras = array($nick,
		               $this->thread->getTitle(),
		               "http://www.cherriz.de/index.php?page=" . MenuThread::SINGLE . "&forum=" . $this->thread->getForum() . "&thread=" . $this->thread->getID());
		return Message::createInstance(Message::BENACHRICHTIGUNGTHREADPOST, $paras);
	}

}