<?php

/**
 * Class StatisticJob
 *
 * Enthält die Logik für das Speichern eines Requests durch einen externen Cronjob.
 * Damit kann erkannt werden, wann die Webseite nicht verfügbar war..
 *
 * @Author Frederik Kirsch
 */
final class StatisticJob extends Job {

	private $settings;

	private $db;

	private $id;

	/**
	 * Erzeugt alle relevaten Variablen für die Verarbeitung.
	 *
	 * @param int    $id Die JobID oder <code>null</code>, falls Job noch nie persistiert wurde.
	 * @param string $data
	 * @param string $validationData
	 * @param string $status
	 */
	public function __construct($id, $data, $validationData, $status) {
		parent::__construct("StatisticJob");
		$this->id = $id;
		$this->db = DBConnect::getDBConnection();
		$this->settings = Settings::getInstance();
	}

	/**
	 * Prüft ob der Job noch valide ist und damit ausgeführt werden darf.
	 *
	 * @return bool Validität
	 */
	function verify() {
		return true;
	}

	/**
	 * Führt den Job aus.
	 *
	 */
	function perform() {
		$insert = "INSERT INTO Request (`Time`) VALUES (SYSDATE())";
		$this->db->query($insert);
		$delete = "DELETE FROM Request WHERE `Time` < (SYSDATE() - INTERVAL " . $this->settings->getProperty(Settings::PROPERTY_HISTORY_DAYS) . " DAY)";
		$this->db->query($delete);
	}

	/**
	 * Persistiert den aktuellen Job in der Datenbank.
	 *
	 */
	function persist() {
		//Aktualisierung
		$update = "UPDATE Job SET Status='" . Job::STATUS_ACTIVE . "' WHERE ID = " . $this->id;
		$this->db->query($update);
	}

}