<?php

LibImporter::import("basic/entity/class.NewsItem.php");
LibImporter::import("basic/util/class.Twitter.php");
LibImporter::import("basic/util/class.Mail.php");
LibImporter::import("basic/util/internal/class.Message.php");

/**
 * Class NewsJob
 *
 * Enthält die Logik für das Publizieren darüber das eine News released wurde.
 * Wurde als Job umgesetzt da News auch Zeitverzögert publiziert werden können.
 *
 * @Author Frederik Kirsch
 */
final class NewsJob extends Job {

	const TYPE = "NewsJob";

	// Processing
	/** @var Twitter */
	private $twitterAPI = null;

	/** @var Mail */
	private $mailAPI = null;

	private $db;

	private $settings;

	//Job Information
	private $user = null;

	private $id = null;

	private $public = false;

	private $news = null;

	private $data = null;

	private $validationData = null;

	private $status = null;

	/**
	 * Erzeugt erstmalige eine Instanz des Jobs zu einer bestimmten News.
	 *
	 * @param int $newsID Die ID der News
	 *
	 * @return NewsJob Die Jobinstanz.
	 */
	public static function createInitial($newsID) {
		$news = NewsItem::createNewsFromDB($newsID);

		$data = NewsItem::ATTRIBUTE_ID . "=" . $news->get(NewsItem::ATTRIBUTE_ID);
		$validationData = NewsItem::ATTRIBUTE_UPDATED . Job::VALUE_SEPERATOR . $news->get(NewsItem::ATTRIBUTE_UPDATED);

		return new NewsJob(null, $data, $validationData, Job::STATUS_INIT);
	}

	/**
	 * Erzeugt alle relevaten Konstanten für die Verarbeitung.
	 * Achtung: Konstruktor ist privat. Instanzerzeugung nur über statische Factorymethoden.
	 *
	 * @param int    $id             Die JobID oder <code>null</code>, falls Job noch nie persistiert wurde.
	 * @param string $data           Alle für die Ausführung relevanten Jobdaten.
	 * @param string $validationData Alle für die Validierung relevanten Jobdaten.
	 * @param string $status         Status des Jobs
	 */
	public function __construct($id, $data, $validationData, $status) {
		parent::__construct(NewsJob::TYPE);
		$this->db = DBConnect::getDBConnection();
		$this->settings = Settings::getInstance();

		$this->id = $id;
		$this->data = $this->deserialize($data);
		$this->validationData = $this->deserialize($validationData);
		$this->status = $status;

		$this->news = NewsItem::createNewsFromDB($this->data[NewsItem::ATTRIBUTE_ID]);
	}

	function verify() {
		$valide = true;

		//Prüfe unverändert
		if ($this->validationData[NewsItem::ATTRIBUTE_UPDATED] != $this->news->get(NewsItem::ATTRIBUTE_UPDATED)) {
			$this->status = Job::STATUS_CHANGED;
			$valide = false;
		}

		//Prüfe ob Veröffentlicht
		if ($this->news->get(NewsItem::ATTRIBUTE_PUBLISHED) !== true) {
			$valide = false;
		}

		//Prüfe Datum
		if ($this->news->checkReleased() !== true) {
			$valide = false;
		}

		//Benachrichtigung bereits durchgefuehrt
		$query = "SELECT * FROM Job WHERE Type = '" . NewsJob::TYPE . "' AND Status = '" . JOB::STATUS_FINISHED . "' AND Data = 'id=" . $this->news->get(NewsItem::ATTRIBUTE_ID) . "'";
		$result = $this->db->query($query);
		if($result && $result->num_rows > 0){
			$this->status = Job::STATUS_CHANGED;
			$valide = false;
		}

		return $valide;
	}

	function persist() {
		if ($this->id == null) {
			//Erstmalige speicherung
			$data = $this->serialize($this->data);
			$validationData = $this->serialize($this->validationData);
			$released = date("Y.m.d H:i:s", $this->news->get(NewsItem::ATTRIBUTE_RELEASED));
			$insert = "INSERT INTO Job (Type, ExecutionTime, Data, ValidationData, Status) VALUES ('" . $this->getType() . "', '" . $released . "', '" . $data . "', '" . $validationData . "', '" . $this->status . "')";
			$this->db->query($insert);
			$this->id = $this->db->insert_id;
		} else {
			//Aktualisierung
			$update = "UPDATE Job SET Status='" . $this->status . "' WHERE ID = " . $this->id;
			$this->db->query($update);
		}
	}

	public function perform() {
		$this->init();

		if ($this->settings->getProperty(Settings::PROPERTY_PAGE_MODE) == Settings::PROPERTY_PAGE_MODE_PRODUCTION) {
			$this->performPublicTweet();
			$this->performDirectMessages();
			$this->performMails();
		} else {
			echo("<div>NewsJOB(" . $this->id . ")<br />");
			echo("Date: " . implode(", ", $this->data) . "<br />");
			echo("Validation Data: " . implode(", ", $this->validationData) . "<br />");
			echo("Status: " . $this->status . "<br />");
			echo("</div>");
		}

		$this->status = Job::STATUS_FINISHED;
	}

	/**
	 * Initialisiert die Hilfsklassen und ermittelt die zur Verarbeitung notwendigen variablen
	 */
	private function init() {
		//APIs initialisieren
		$this->twitterAPI = new Twitter();
		$this->mailAPI = new Mail();

		$this->public = false;
		if ($this->news->get(NewsItem::ATTRIBUTE_PUBLIC)) {
			$this->public = true;
		}

		$this->user = $this->determineUser();
	}

	/**
	 * Verbreitet die Nachricht als öffentlichen Tweet
	 */
	private function performPublicTweet() {
		if ($this->public === true) {
			$this->twitterAPI->tweet($this->getMessage());
		}
	}

	/**
	 * Liefert alle User zurück, welche Berechtigt sind diese News zu lesen.
	 * @return array Liste der User.
	 */
	private function determineUser() {
		$query = "SELECT U.Nick, U.Email, U.Twitter, U.NotificationNews, U.NotificationTypeEmail, U.NotificationTypeTwitter FROM Users U INNER JOIN User_Status US ON U.Status = US.ID ";
		if ($this->news->get(NewsItem::ATTRIBUTE_PUBLIC) === true) {
			$query .= "WHERE US.Status = 'Aktiv' AND U.NotificationNews = 1";
		} else {
			$newsId = $this->news->get(NewsItem::ATTRIBUTE_ID);
			$query .= "INNER JOIN Gruppe_Mitglied GM ON U.ID = GM.User WHERE US.Status = 'Aktiv' AND GM.Gruppe IN (SELECT NAG.Group FROM News_Allowed_Groups NAG WHERE News = $newsId) AND U.NotificationNews = 1 GROUP BY U.Nick";
		}

		$result = $this->db->query($query);
		$users = array();
		while ($user = $result->fetch_object()) {
			$users[] = $user;
		}

		return $users;
	}

	private function performDirectMessages() {
		foreach ($this->user as $usr) {
			if ($usr->NotificationTypeTwitter == 1 && !empty($usr->Twitter)) {
				$this->twitterAPI->directMessage($this->getMessage($usr->Nick), $usr->Twitter);
			}
		}
	}

	private function performMails() {
		foreach ($this->user as $usr) {
			if ($usr->NotificationTypeEmail == 1 && !empty($usr->Email)) {
				$this->mailAPI->sendMail($this->getMessage($usr->Nick), $usr->Email);
			}
		}
	}

	private function getMessage($nick = null) {
		// 1: Nick
		// 2: Überschrift
		// 3: URL
		$paras = array($nick,
		               $this->news->get(NewsItem::ATTRIBUTE_TITLE),
		               "http://www.cherriz.de/index.php?page=" . MenuNews::SINGLE . "&news=" . $this->news->get(NewsItem::ATTRIBUTE_ID));
		return Message::createInstance(Message::BENACHRICHTIGUNGNEWS, $paras);
	}

}