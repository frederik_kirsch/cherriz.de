<?php

LibImporter::import("basic/entity/class.ThreadItem.php");
LibImporter::import("basic/entity/class.ThreadPost.php");

class ThreadRelevanceCalculationJob  extends Job {

	private $db = null;

	private $id = null;

	/**
	 * Erzeugt alle relevaten Variablen für die Verarbeitung.
	 *
	 * @param integer $id Die JobID oder <code>null</code>, falls Job noch nie persistiert wurde.
	 * @param String $data
	 * @param String $validationData
	 * @param String $status
	 */
	public function __construct($id, $data, $validationData, $status)
	{
		parent::__construct("ThreadRelevanceCalculationJob");
		$this->db = DBConnect::getDBConnection();
		$this->id = $id;
	}

	/**
	 * Prüft ob der Job noch valide ist und damit ausgeführt werden darf.
	 *
	 * @return bool Validität
	 */
	function verify() {
		return true;
	}

	/**
	 * Führt den Job aus.
	 */
	function perform() {
		//Alle Foren auslesen
		$result = $this->db->query("SELECT ID FROM Thread WHERE Deleted = 0");
		while ($thread = $result->fetch_object()) {
			$threadObj = ThreadItem::createFromDB($thread->ID);
			$threadObj->calculateRelevance();
			$threadObj->save();
		}
	}

	/**
	 * Persistiert den aktuellen Job in der Datenbank.
	 */
	function persist() {
		//Aktualisierung
		$update = "UPDATE Job SET Status='" . Job::STATUS_ACTIVE . "' WHERE ID = " . $this->id;
		$this->db->query($update);
	}

}