<?php

LibImporter::import("basic/entity/class.NewsItem.php");
LibImporter::import("basic/entity/class.Kommentar.php");
LibImporter::import("basic/util/class.Twitter.php");
LibImporter::import("basic/util/class.Mail.php");
LibImporter::import("basic/util/internal/class.Message.php");

/**
 * Class NewsJob
 *
 * Enthält die Logik für das Publizieren darüber das eine News released wurde.
 * Wurde als Job umgesetzt da News auch Zeitverzögert publiziert werden können.
 *
 * @Author Frederik Kirsch
 */
final class CommentJob extends Job {

	// Processing
	private $twitterAPI = null;

	private $mailAPI = null;

	private $db;

	private $settings;

	//Job Information
	private $user = null;

	private $id = null;

	private $data = null;

	private $validationData = null;

	private $status = null;

	private $kommentar = null;

	private $news = null;

	/**
	 * Erzeugt erstmalige eine Instanz des Jobs zu einem bestimmten Kommentar.
	 *
	 * @param int $kommentarID Die ID der Kommentars
	 *
	 * @return CommentJob Die Jobinstanz.
	 */
	public static function createInitial($kommentarID) {
		$data = Kommentar::ATTRIBUTE_ID . Job::VALUE_SEPERATOR . $kommentarID;
		$validationData = $data;

		return new CommentJob(null, $data, $validationData, Job::STATUS_INIT);
	}

	/**
	 * Erzeugt alle relevaten Konstanten für die Verarbeitung.
	 * Achtung: Konstruktor ist privat. Instanzerzeugung nur über statische Factorymethoden.
	 *
	 * @param int    $id             Die JobID oder <code>null</code>, falls Job noch nie persistiert wurde.
	 * @param string $data           Alle für die Ausführung relevanten Jobdaten.
	 * @param string $validationData Alle für die Validierung relevanten Jobdaten.
	 * @param string $status
	 */
	public function __construct($id, $data, $validationData, $status) {
		parent::__construct("CommentJob");
		$this->db = DBConnect::getDBConnection();
		$this->settings = Settings::getInstance();

		$this->id = $id;
		$this->data = $this->deserialize($data);
		$this->validationData = $this->deserialize($validationData);
		$this->status = $status;

		$this->kommentar = new Kommentar($this->data[Kommentar::ATTRIBUTE_ID]);
		$this->news = NewsItem::createNewsFromDB($this->kommentar->get(Kommentar::ATTRIBUTE_NEWS));
	}

	/**
	 * Prüft ob der Job noch valide ist und damit ausgeführt werden darf.
	 *
	 * @return bool Validität
	 */
	function verify() {
		$valide = true;

		//Prüfe ob letzter Kommentar
		$query = "SELECT ID FROM Kommentar WHERE News = " . $this->kommentar->get(Kommentar::ATTRIBUTE_NEWS) . " AND ID > " . $this->kommentar->get(Kommentar::ATTRIBUTE_ID);
		if ($this->db->query($query)->num_rows) {
			$this->status = Job::STATUS_CHANGED;
			$valide = false;
		}

		//Prüfe ob Veröffentlicht
		if ($this->news->get(NewsItem::ATTRIBUTE_PUBLISHED) !== true) {
			$valide = false;
		}

		//Prüfe Datum
		if ($this->news->checkReleased() !== true) {
			$valide = false;
		}

		return (boolean) $valide;
	}

	/**
	 * Führt den Job aus.
	 *
	 */
	function perform() {
		$this->init();

		if ($this->settings->getProperty(Settings::PROPERTY_PAGE_MODE) == Settings::PROPERTY_PAGE_MODE_PRODUCTION) {
			$this->performDirectMessages();
			$this->performMails();
		} else {
			echo("<div>CommentJob(" . $this->id . ")<br />");
			echo("Data: " . implode(", ", $this->data) . "<br />");
			echo("Validation Data: " . implode(", ", $this->validationData) . "<br />");
			echo("Status: " . $this->status . "<br />");
			echo("</div>");
		}

		$this->status = Job::STATUS_FINISHED;
	}

	/**
	 * Persistiert den aktuellen Job in der Datenbank.
	 *
	 */
	function persist() {
		if ($this->id == null) {
			//Erstmalige speicherung
			$data = $this->serialize($this->data);
			$validationData = $this->serialize($this->validationData);
			$released = date("Y.m.d H:i:s");
			$insert = "INSERT INTO Job (Type, ExecutionTime, Data, ValidationData, Status) VALUES ('" . $this->getType() . "', '" . $released . "', '" . $data . "', '" . $validationData . "', '" . $this->status . "')";
			$this->db->query($insert);
			$this->id = $this->db->insert_id;
		} else {
			//Aktualisierung
			$update = "UPDATE Job SET Status='" . $this->status . "' WHERE ID = " . $this->id;
			$this->db->query($update);
		}
	}

	/**
	 * Initialisiert die Hilfsklassen und ermittelt die zur Verarbeitung notwendigen variablen
	 */
	private function init() {
		//APIs initialisieren
		$this->twitterAPI = new Twitter();
		$this->mailAPI = new Mail();

		$this->user = $this->determineUser();
	}

	private function performDirectMessages() {
		foreach ($this->user as $usr) {
			if ($usr->NotificationTypeTwitter == 1 && !empty($usr->Twitter)) {
				$this->twitterAPI->directMessage($this->getMessage($usr->Nick), $usr->Twitter);
			}
		}
	}

	private function performMails() {
		foreach ($this->user as $usr) {
			if ($usr->NotificationTypeEmail == 1 && !empty($usr->Email)) {
				$this->mailAPI->sendMail($this->getMessage($usr->Nick), $usr->Email);
			}
		}
	}

	/**
	 * Liefert alle User zurück, welche Berechtigt sind diese News zu lesen.
	 * @return array Liste der User.
	 */
	private function determineUser() {
		$query = "SELECT " . "U.Nick, " . "U.Email, " . "U.Twitter, " . "U.NotificationComment, " . "U.NotificationTypeEmail, " . "U.NotificationTypeTwitter " . "FROM Users U " . "INNER JOIN User_Status US ON U . Status = US . ID ";
		if ($this->news->get(NewsItem::ATTRIBUTE_PUBLIC) === true) {
			$query .= "WHERE US . Status = 'Aktiv' AND U.NotificationComment = 1 " . "AND U.ID != (SELECT K.Author FROM Kommentar K WHERE K.News = " . $this->news->get(NewsItem::ATTRIBUTE_ID) . " ORDER BY K.ID DESC LIMIT 1) " . "AND (U.ID IN (SELECT K.Author FROM Kommentar K WHERE K.News = " . $this->news->get(NewsItem::ATTRIBUTE_ID) . ") OR U.ID = (SELECT N.Author FROM News N WHERE N.ID = " . $this->news->get(NewsItem::ATTRIBUTE_ID) . "))";
		} else {
			$query .= "INNER JOIN Gruppe_Mitglied GM ON U.ID = GM.User " . "WHERE US.Status = 'Aktiv' " . "AND GM.Gruppe IN (SELECT " . "NAG.Group " . "FROM News_Allowed_Groups NAG " . "WHERE News = " . $this->news->get(NewsItem::ATTRIBUTE_ID) . ") " . "AND U.NotificationComment = 1 " . "AND U.ID != (SELECT K.Author FROM Kommentar K WHERE K.News = " . $this->news->get(NewsItem::ATTRIBUTE_ID) . " ORDER BY K.ID DESC LIMIT 1) " . "AND (U.ID IN (SELECT K.Author FROM Kommentar K WHERE K.News = " . $this->news->get(NewsItem::ATTRIBUTE_ID) . ") OR U.ID = (SELECT N.Author FROM News N WHERE N.ID = " . $this->news->get(NewsItem::ATTRIBUTE_ID) . "))" . "GROUP BY U.Nick";
		}

		$result = $this->db->query($query);
		$users = array();
		while ($user = $result->fetch_object()) {
			$users[] = $user;
		}
		return $users;
	}

	private function getMessage($nick = null) {
		// 1: Nick
		// 2: Überschrift
		// 3: URL
		$paras = array($nick,
		               $this->news->get(NewsItem::ATTRIBUTE_TITLE),
		               "http://www.cherriz.de/index.php?page=" . MenuNews::SINGLE . "&news=" . $this->news->get(NewsItem::ATTRIBUTE_ID));
		return Message::createInstance(Message::BENACHRICHTIGUNGKOMMENTAR, $paras);
	}

}