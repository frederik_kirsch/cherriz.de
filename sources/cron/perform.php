<?php

set_time_limit(0);
ignore_user_abort(true);
require '../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../");
LibImporter::import("basic/globals.php");
LibImporter::import("cron/class.JobScheduler.php");
LibImporter::import("basic/util/class.Settings.php");

$scheduler = JobScheduler::getInstance();
$scheduler->perform();