<?php

LibImporter::import("cron/class.Job.php");
LibImporter::import("cron/jobs/class.NewsJob.php");
LibImporter::import("cron/jobs/class.CommentJob.php");
LibImporter::import("cron/jobs/class.ThreadpostJob.php");
LibImporter::import("cron/jobs/class.StatisticJob.php");
LibImporter::import("cron/jobs/class.ThreadRelevanceCalculationJob.php");

/**
 * User: Frederik Kirsch
 */
class JobScheduler {

	private $db;

	private $settings;

	private static $instance = null;

	/**
	 * @return JobScheduler Eine Instanz des JobScheduler.
	 */
	public static function getInstance() {
		if (JobScheduler::$instance == null) {
			JobScheduler::$instance = new JobScheduler();
		}
		return JobScheduler::$instance;
	}

	private function __construct() {
		$this->db = DBConnect::getDBConnection();
		$this->settings = Settings::getInstance();
	}

	public function perform() {
		$ids = $this->getOpenJobIDs();

		foreach ($ids as $id) {
			$query = "SELECT J.ID, J.Data, J.ValidationData, J.Status, J.Type
                  FROM Job J
                  WHERE J.ID = " . $id;
			$job = $this->db->query($query)->fetch_object();
			$instance = new $job->Type($job->ID, $job->Data, $job->ValidationData, $job->Status);
			if ($instance->verify()) {
				$result = $instance->perform();
				$this->writeHistory($job->ID, $result);
			}
			$instance->persist();
		}
	}

	public function addJob(Job $job) {
		$job->persist();
	}

	private function writeHistory($jobId, $info) {
		$insert = "INSERT INTO Job_History (Job, Execution, Info) VALUES (" . $jobId . ", NOW(), '" . $info . "')";
		$this->db->query($insert);
		$delete = "DELETE FROM Job_History WHERE Execution < (SYSDATE() - INTERVAL " . $this->settings->getProperty(Settings::PROPERTY_HISTORY_DAYS) . " DAY)";
		$this->db->query($delete);
	}

	private function getOpenJobIDs() {
		//Anstehende Jobs ermitteln
		$query = "SELECT J.ID
                  FROM Job J
                  WHERE J.Status != '" . Job::STATUS_FINISHED . "'
                    AND J.Status != '" . Job::STATUS_CHANGED . "'
                    AND J.ExecutionTime < NOW()";
		$resultObj = $this->db->query($query);

		$result = array();
		while ($row = $resultObj->fetch_assoc()) {
			$result[] = $row["ID"];
		}
		return $result;
	}

}