<?php

/**
 * Class UserBox Stellt das Userprofil oder den Logindialog dar.
 * @author Frederik Kirsch
 */
class UserBox extends SidebarBox{

	protected function getContent() {
		$login = Login::getInstance();

		if ($login->isLoggedIn()) {
			return $login->getUserOverview();
		} else {
			return $login->getLoginScreen();
		}
	}

}