<?php

/**
 * Class FeedBox Stellt die Webseite dar in der der RSS Feed und Twitter zum Abonement angeboten werden.
 * @author Frederik Kirsch
 */
class FeedBox extends SidebarBox {

	protected function getContent() {
		$result = "<a href='index.php?page=" . Menu::IMPRESSUM . "' title='Zum Impressum' style='text-decoration: none;' class='icon icon_sidebar'>Impressum</a><br />
				   <a href='index.php?page=" . Menu::DATENSCHUTZ . "' title='Zur Datenschutzaerklaerung' style='text-decoration: none;' class='icon icon_sidebar'>Datenschutz</a><br />
				   <span class='icon icon_sidebar iconFeed'/>
				   <div id='sidebar_box_smedia'>
				   	   <a href='http://www.cherriz.de/sources/api/rss.php?type=News' target='_blank'>News RSS</a>
				   	   <a href='https://twitter.com/cherriz_de' class='twitter-follow-button' data-show-count='false' data-show-screen-name='false'>Follow @cherriz_de</a>
				   </div>
				   <script type='application/javascript'>
					   !function (d, s, id) {
						   var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
						   if (!d.getElementById(id)) {
							   js = d.createElement(s);
							   js.id = id;
							   js.src = p + '://platform.twitter.com/widgets.js';
							   fjs.parentNode.insertBefore(js, fjs);
						   }
					   }(document, 'script', 'twitter-wjs');
				   </script>";
		return $result;
	}

}