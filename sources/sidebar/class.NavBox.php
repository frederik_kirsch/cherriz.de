<?php

/**
 * Class NavBox stellt die Navigation einer Seite dar. Diese muss im Konstruktur als Parameter uebergeben werden.
 * @author Frederik Kirsch
 */
class NavBox extends SidebarBox{

	protected function getContent() {
		/** @var PageContent $page */
		$page = $this->para;
		if ($page->getPage()->hasNavBar()) {
			return $page->getPage()->getNavbar();
		}else{
			return false;
		}
	}

}