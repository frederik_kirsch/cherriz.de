<?php

/**
 * Class SidebarBox beschreibt die Oberklasse fuer Seitenleisten.
 * @author Frederik Kirsch
 */
abstract class SidebarBox {

	protected $para;

	/**
	 * @param null $para Optionales Konstruktorelement. Wird Implementierungen zugeanglich gemacht.
	 */
	public function __construct($para = null) {
		$this->para = $para;
	}

	/**
	 * Stellt die Seitenleiste dar, sofern die Implementierung einen Inhalt liefert.
	 */
	public function draw() {
		$content = $this->getContent();

		if ($content) {
			echo("<div class='sidebar_box box'>");
			echo($content);
			echo("</div>");
		}
	}

	/**
	 * Inhalt der Seitenleiste falls vorhanden.
	 * @return string|boolean Inhalt der Seitenleiste oder false.
	 */
	protected abstract function getContent();

}