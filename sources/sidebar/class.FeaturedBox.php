<?php

/**
 * Class FeaturedBox Stellt die Sidebox dar welche Unterseiten von Cherriz bewirbt.
 * @author Frederik Kirsch
 */
class FeaturedBox extends SidebarBox {

	protected function getContent() {
		return "<h2>Featured</h2>
				<a href='http://training.cherriz.de' title='Weiterbildungsunterlagen zum Thema Softwareentwicklung' target='_blank' style='text-decoration: none;' class='icon icon_sidebar iconBildung'>Cherriz Training</a><br />
				<a href='http://www.cherriz.de/master/' title='Masterthesis mit dem Thema \"Konzeption und prototypische Umsetzung der Integration fachlich getriebener Prozessmodellierung in die Softwareentwicklung unter Verwendung der Business Process Model and Notation 2.0\" (inkl. aller Unterlagen)' target='_blank' style='text-decoration: none' class='icon icon_sidebar iconZahnraeder'>Cherriz Master</a><br />";
	}

}