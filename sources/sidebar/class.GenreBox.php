<?php

/**
 * Class GenreBox Stellt die Top 5 Genres in der Sidebar dar.
 * @author Frederik Kirsch
 */
class GenreBox extends SidebarBox {

	protected function getContent() {
		$page = $_GET['page'];
		if ($page != "" && $page != Menu::NEWS) {
			return false;
		}
		//Genres ermitteln
		$query = "SELECT G.Genre, COUNT(*) AS Anz FROM Genre G
				  INNER JOIN News_Genre NG
				  ON NG.Genre = G.ID
				  INNER JOIN News N
				  ON N.ID = NG.News
				  WHERE N.Published = 1
				  AND N.Released < NOW()
				  GROUP BY G.Genre
				  ORDER BY Anz DESC
				  LIMIT 5";
		$dbResult = DBConnect::getDBConnection()->query($query);

		//Darstellen
		$result = "<h2>Top 5 Genres</h2>";
		$result .= "<ul>";
		while ($obj = $dbResult->fetch_object()) {
			$result .= "<li><a class='icon icon_sidebar icon$obj->Genre' href='index.php?page=" . Menu::NEWS . "&genre=$obj->Genre'>$obj->Genre <span>($obj->Anz)</span></a></li>";
		}
		$result .= "</ul>";
		return $result;
	}

}