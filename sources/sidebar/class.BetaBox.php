<?php

/**
 * Class BetaBox Stellt die Sidebox dar welche ueber das Betastadium der Webseite informiert.
 * @author Frederik Kirsch
 */
class BetaBox extends SidebarBox {

	protected function getContent() {
		$result = "<h2>Beta</h2>";
		$result .= "<p>Willkommen auf www.cherriz.de</p>";
		$result .= "<p>Aktuell befindet sich die Seite in einer Betaphase, weswegen noch Fehler enthalten sein können.</p>";
		$result .= "<p>Fehler kannst du <a href='https://bitbucket.org/frederik_kirsch/cherriz.de/issues?status=new&status=open' class='icon icon_sidebar iconBitbucket' target='_blank' title='Ticketübersicht auf Butbucket'>hier</a> melden.</p>";
		$result .= "<p>Desweiteren wird es immer wieder größere Änderungen geben.</p>";
		return $result;
	}

}