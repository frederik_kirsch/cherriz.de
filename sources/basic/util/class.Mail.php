<?php

/**
 * Class Mail
 * Beinhaltet alle Funktionen rund um das Versenden von E-Mails.
 *
 * @author Frederik Kirsch
 */
class Mail {

    private $settings;

    public function __construct() {
        $this->settings = Settings::getInstance();
    }

    /**
     * Versendet eine übergebene Message per Mail.
     *
     * @param Message $message    Die Message
     * @param string  $empfaenger Der Empfänger
     *
     * @return bool Gibt an ob das Versenden der Email erfolgreich war.
     */
    public function sendMail(Message $message, $empfaenger) {
        $defaultMail = $this->settings->getProperty(Settings::PROPERTY_DEFAULT_EMAIL);
        $header =
            "From: " .
            $defaultMail .
            "\n" .
            "Reply-To: " .
            $defaultMail .
            "\n" .
            "Content-type: text/plain; charset=utf-8\n";

        if (mail($empfaenger, $message->getBetreff(), $message->getText(), $header)) {
            return true;
        }
        return false;
    }

}