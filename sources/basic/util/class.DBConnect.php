<?php

LibImporter::import("/basic/util/internal/class.DBConf.php");

/**
 * Die Klasse verwaltet die Verbindung zur Datenbank.
 * @author Frederik Kirsch
 */
class DBConnect {

	const CHARSET_DEFAULT = "noCharset";

	const CHARSET_UTF8 = "utf8";

	const CHARSET_UTF8_BIN = "utf8_bin";

	private static $db = null;

	private static $dbBin = null;

	/**
	 * @param string $charset Der Standardzeichensatz fuer die Datenbankverbindung falls spezifischer gewuenscht.
	 *
	 * @return MySQLi Liefert eine Verbindung zur Datenbank.
	 */
	public static function getDBConnection($charset = DBConnect::CHARSET_UTF8) {
		switch ($charset) {
			case DBConnect::CHARSET_UTF8:
				if (DBConnect::$db == null) {
					DBConnect::$db = DBConnect::createConnection(DBConnect::CHARSET_UTF8);
				}
				return DBConnect::$db;
			case DBConnect::CHARSET_UTF8_BIN:
				if (DBConnect::$dbBin == null) {
					DBConnect::$dbBin = DBConnect::createConnection(DBConnect::CHARSET_UTF8_BIN);
				}
				return DBConnect::$dbBin;
		}
		return null;
	}

	/**
	 * Erzeugt eine Verbindung zur Datenbank.
	 *
	 * @param string $charset Der gewuenschte Zeichensatz.
	 *
	 * @return MySQLi Verbindung zur Datenbank
	 */
	private static function createConnection($charset) {
		$con = new MySQLi(DBConf::getHost(), DBConf::getUser(), DBConf::getPass(), DBConf::getDB());
		if (!$con) {
			die("Keine Verbindung zur Datenbank möglich.");
		} else {
			if ($charset != DBConnect::CHARSET_DEFAULT) {
				$con->query("SET NAMES '$charset'");
				$con->query("SET CHARACTER SET '$charset'");
			}
		}
		return $con;
	}

}