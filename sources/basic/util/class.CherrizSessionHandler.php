<?php

class CherrizSessionHandler implements SessionHandlerInterface {

	/** Property der ID der aktuellen Sitzung */
	const NAME_SESSIONID = "SessionID";

	/** Property des aktuell angemeldeten Benutzers */
	const NAME_USER = "User";

	/** Property des Sitzungsbeginns */
	const NAME_LOGGEDINSINCE = "LoggedInSince";

	/** Property ob die Anmeldung gespeichert werden soll */
	const NAME_REMEMBER = "Remember";

	/** Sitzung soll lange gespeichert werden */
	const REMEMBER_TRUE = 1;

	/** Sitzung soll nicht lange gespeichert werden */
	const REMEMBER_FALSE = 0;

	private $sessionDuration;

	private $sessionLife;

	private $rememberDuration;

	private $rememberLife;

	private $db;

	private $settings;

	public static function init(){
		$handler = new CherrizSessionHandler();
		session_set_save_handler($handler, true);
		session_start();
	}

	private function __construct() {
		$this->db = DBConnect::getDBConnection();
		$this->settings = Settings::getInstance();
		$this->sessionDuration = $this->settings->getProperty(Settings::PROPERTY_SESSION_DURATION);
		$this->rememberDuration = $this->settings->getProperty(Settings::PROPERTY_SESSION_REMEMBER_DURATION);
		$this->sessionLife = time() - $this->sessionDuration;
		$this->rememberLife = time() - $this->rememberDuration;
	}

	/**
	 * Initialize session
	 * @link  http://php.net/manual/en/sessionhandlerinterface.open.php
	 *
	 * @param string $save_path  The path where to store/retrieve the session.
	 * @param string $session_id The session id.
	 *
	 * @return bool <p>
	 * The return value (usually TRUE on success, FALSE on failure).
	 * Note this value is returned internally to PHP for processing.
	 * </p>
	 * @since 5.4.0
	 */
	public function open($save_path, $session_id) {
		return true;
	}

	/**
	 * Read session data
	 * @link  http://php.net/manual/en/sessionhandlerinterface.read.php
	 *
	 * @param string $sessionID The session id to read data for.
	 *
	 * @return string <p>
	 * Returns an encoded string of the read data.
	 * If nothing was read, it must return an empty string.
	 * Note this value is returned internally to PHP for processing.
	 * </p>
	 * @since 5.4.0
	 */
	public function read($sessionID) {
		$withCookie = false;
		if (isset($_COOKIE[CherrizSessionHandler::NAME_SESSIONID]) && $_COOKIE[CherrizSessionHandler::NAME_SESSIONID] != $sessionID) {
			//Session in Cookie gesetzt, daraus übernehmen
			$sessionID = $_COOKIE[CherrizSessionHandler::NAME_SESSIONID];
			session_id($sessionID);
			$withCookie = true;
		} elseif (isset($_GET[CherrizSessionHandler::NAME_SESSIONID]) && $_GET[CherrizSessionHandler::NAME_SESSIONID] != $sessionID) {
			//Session in GET-Parametern gesetzt, daraus übernehmen
			$sessionID = Filter::text($_GET[CherrizSessionHandler::NAME_SESSIONID]);
			session_id($sessionID);
		}

		//Daten zur Session laden
		$sessionData = $this->getSessionData($sessionID);

		//Cookie, welches die Session ID speichert aktualisieren
		if ($withCookie) {
			if ($this->getPropertyFromSessionData($sessionData, CherrizSessionHandler::NAME_REMEMBER) == CherrizSessionHandler::REMEMBER_TRUE) {
				setcookie(CherrizSessionHandler::NAME_SESSIONID, $sessionID, time() + $this->rememberDuration, "/");
			} else {
				setcookie(CherrizSessionHandler::NAME_SESSIONID, $sessionID, time() + $this->sessionDuration, "/");
			}
		}

		return $sessionData;
	}

	/**
	 * Write session data
	 * @link  http://php.net/manual/en/sessionhandlerinterface.write.php
	 *
	 * @param string $sessionID    The session id.
	 * @param string $data         <p>
	 *                             The encoded session data. This data is the
	 *                             result of the PHP internally encoding
	 *                             the $_SESSION superglobal to a serialized
	 *                             string and passing it as this parameter.
	 *                             Please note sessions use an alternative serialization method.
	 *                             </p>
	 *
	 * @return bool <p>
	 * The return value (usually TRUE on success, FALSE on failure).
	 * Note this value is returned internally to PHP for processing.
	 * </p>
	 * @since 5.4.0
	 */
	public function write($sessionID, $data) {
		$success = false;
		if ($data != null) {
			// Statement, um eine bestehende Sitzung zu aktualisieren.
			$sessionStatement = "UPDATE Sessions
								    SET LastUpdated = '" . time() . "',
								        Value = '" . $this->db->real_escape_string($data) . "'
								  WHERE ID = '$sessionID'
								    AND (LastUpdated > $this->sessionLife
								        OR Start > $this->rememberLife
								        AND Remember = " . CherrizSessionHandler::REMEMBER_TRUE . ")";
			$this->db->query($sessionStatement);
			//Ergebnis prüfen
			if ($this->db->affected_rows == 1) {
				//Bestehende Session wurde aktualisiert
				$success = true;
			} else {
				// Behandlung von Longtimelogins
				$remember = CherrizSessionHandler::REMEMBER_FALSE;
				if ($this->getPropertyFromSessionData($data, CherrizSessionHandler::NAME_REMEMBER) == CherrizSessionHandler::REMEMBER_TRUE) {
					$remember = CherrizSessionHandler::REMEMBER_TRUE;
				}
				//Andernfalls muss eine neue Session erstellt werden
				//Workaround alte Session loeschen
				$this->db->query("DELETE FROM Sessions WHERE ID = '$sessionID'");
				$sessionStatement = "INSERT INTO Sessions (ID, LastUpdated, Start, Remember, Value)
										  VALUES ('$sessionID', '" . time() . "', '" . time() . "', $remember, '" . $this->db->real_escape_string($data) . "')";
				if ($this->db->query($sessionStatement)) {
					$success = true;
				}
			}
		} else {
			$success = true;
		}

		return $success;
	}

	/**
	 * Close the session
	 * @link  http://php.net/manual/en/sessionhandlerinterface.close.php
	 * @return bool <p>
	 * The return value (usually TRUE on success, FALSE on failure).
	 * Note this value is returned internally to PHP for processing.
	 * </p>
	 * @since 5.4.0
	 */
	public function close() {
		$this->gc(0);
		return true;
	}

	/**
	 * Destroy a session
	 * @link  http://php.net/manual/en/sessionhandlerinterface.destroy.php
	 *
	 * @param string $sessionID The session ID being destroyed.
	 *
	 * @return bool <p>
	 * The return value (usually TRUE on success, FALSE on failure).
	 * Note this value is returned internally to PHP for processing.
	 * </p>
	 * @since 5.4.0
	 */
	public function destroy($sessionID) {
		$result = $this->db->query("DELETE FROM Sessions WHERE ID = '$sessionID'");
		if ($result === true) {
			$_SESSION = array();
		}
		return $result;
	}

	/**
	 * Cleanup old sessions
	 * @link  http://php.net/manual/en/sessionhandlerinterface.gc.php
	 *
	 * @param int $maxlifetime <p>
	 *                         Sessions that have not updated for
	 *                         the last maxlifetime seconds will be removed.
	 *                         </p>
	 *
	 * @return bool <p>
	 * The return value (usually TRUE on success, FALSE on failure).
	 * Note this value is returned internally to PHP for processing.
	 * </p>
	 * @since 5.4.0
	 */
	public function gc($maxlifetime) {
		//Zeitraum, nachdem die Session als abgelaufen gilt.
		$sessionStatement = "DELETE FROM Sessions
								   WHERE LastUpdated < $this->sessionLife
								     AND (Remember = " . CherrizSessionHandler::REMEMBER_FALSE . "
								          OR Start < $this->rememberLife)";
		return $this->db->query($sessionStatement);
	}

	/**
	 * Liefert die serialisierten Daten der Sitzung.
	 *
	 * @param string $sessionID Die ID der Sitzung.
	 *
	 * @return string Die Daten der Sitzung.
	 */
	private function getSessionData($sessionID) {
		$sessionStatement = "SELECT *
							   FROM Sessions
							  WHERE ID = '$sessionID'
							    AND (LastUpdated > $this->sessionLife
							    	OR Start > $this->rememberLife
								   AND Remember = " . CherrizSessionHandler::REMEMBER_TRUE . ")";
		$result = $this->db->query($sessionStatement);

		if ($result != null && $result->num_rows == 1) {
			$resultObj = $result->fetch_object();
			return $resultObj->Value;
		}

		return "";
	}

	/**
	 * Liest einen Wert aus den serialisierten Daten der Sitzung.
	 *
	 * @param string $sessionData Die Daten der Sitzung.
	 * @param string $property    Die auszulesende Eigenschaft.
	 *
	 * @return string Der Wert der Eigenschaft
	 */
	private function getPropertyFromSessionData($sessionData, $property) {
		$data = explode(";", $sessionData);
		for ($i = 0; $i < count($data); $i++) {
			if (strpos($data[$i], $property) === 0) {
				$tmp = explode(":", $data[$i]);
				return trim($tmp[count($tmp) - 1], "\"");
			}
		}
		return "";
	}

}