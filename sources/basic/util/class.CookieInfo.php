<?php

/**
 * Class CookieInfo
 * Verwaltet die Anzeige der Datenschutzinformation.
 * Kuemmert sich darum das die Information nicht mehr angezeigt werden wenn der Nutzer diese bestaetigt hat.
 */
class CookieInfo {

	const COOKIE_INFO = "CookieInfo";

	private static $usrID = "null";

	/**
	 * Stellt den Informationsdialog dar, wenn dieser angezeigt werden muss.
	 */
	public static function showCookieBannerIfRequired() {
		if (CookieInfo::isInfoRequired()) {
			CookieInfo::showInfo();
		}
	}

	/**
	 * @return bool Gibt an ob der Dialog angezeigt werden muss. Prueft dazu entweder auf ein gesetztes Cookie oder die Profileinstellungen.
	 */
	private static function isInfoRequired() {
		$login = Login::getInstance();
		if ($login->isLoggedIn()) {
			$usr = Login::getInstance()->getCurrentUserObject();
			CookieInfo::$usrID = $usr->get(User::PROPERTY_ID);
			if ($usr->get(User::PROPERTY_Read_Cookie_Info) == 0) {
				return true;
			}
		} else {
			if ($_COOKIE[CookieInfo::COOKIE_INFO] != 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Zeigt den CookieInfo Dialog an.
	 */
	private static function showInfo() {
		echo("<div class='infoBanner'>
				<div class='infoBannerText'>Cherriz.de verwendet Cookies um die Nutzung der Webseite zu vereinfachen.<br>
				Durch die Nutzung der Webseite stimmen Sie dem Speichern von Cookies auf Ihrem Endgerät zu.<span class='infoBannerControl' id='btnCloseBanner'>Fortfahren</span></div>
				
			  </div>");
		$script = "$('#btnCloseBanner').click(function(){closeInfo($('#btnCloseBanner').parent().parent()," . CookieInfo::$usrID . ")});";
		Toolkit::loadScripts(array("cookieInfo.js"), $script);
		Toolkit::loadStyle("infoBanner.css");
	}

}