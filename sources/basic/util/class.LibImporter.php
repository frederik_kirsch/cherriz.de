<?php

/**
 *
 */
class LibImporter
{

    private static $execPath = "sources/";

    private static $files = array();

    public static function setExecutionPath($execPath)
    {
        LibImporter::$execPath = $execPath;
    }

    /**
     * Lädt die Datei zum übergeben Pfad in den Classpath.
     * Der Pfad muss relativ zum Verzeichnis sources/ definiert werden.
     *
     * @param $file
     */
    public static function import($file)
    {
        if (!in_array($file, LibImporter::$files)) {
            array_push(LibImporter::$files, $file);
            require LibImporter::$execPath . $file;
        }
    }
}