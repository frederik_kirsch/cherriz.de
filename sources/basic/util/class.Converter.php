<?php

/**
 * Class Converter
 * Dient der Convertierung verschiedener Daten.
 *
 * @author Frederik Kirsch
 */
class Converter {

	/** Definiert Null fuer die UI */
	const NULL = "N/A";

	/** Platzhalter fuer Quellcode */
	const REPLACE_PATTERN_CODE = "__code__";

	/** Platzhalter fuer Bild ID */
	const REPLACE_PATTERN_GALLERY = "__gallery__";

	private static $galCounter = 0;

	private static $bbElements = array(/** @lang RegExp */ "#\[br\]#",
	                                   /** @lang RegExp */ "#\[b\](.*?)\[/b\]#",
	                                   /** @lang RegExp */ "#\[i\](.*?)\[/i\]#",
	                                   /** @lang RegExp */ "#\[u\](.*?)\[/u\]#",
	                                   /** @lang RegExp */ "#\[s\](.*?)\[/s\]#",
	                                   /** @lang RegExp */ "#\[url=(mailto:|http://|https://|.{0})(.*?)\|(.*?)\](.*?)\[/url\]#",
	                                   /** @lang RegExp */ "#\[img\](\d*?)\[/img\]#",
	                                   /** @lang RegExp */ "#\[img\](.*?)\[/img\]#",
	                                   /** @lang RegExp */ "#\[video=youtube\](.*?)\[/video\]#",
	                                   /** @lang RegExp */ "#\[list=0\](.*?)\[/list\]#",
	                                   /** @lang RegExp */ "#\[list=1\](.*?)\[/list\]#",
	                                   /** @lang RegExp */ "#\[\*\](.*?)\[/\*\]#",
	                                   /** @lang RegExp */ "#\[head\](.*?)\[/head\]#",
	                                   /** @lang RegExp */ "#\[right\](.*?)\[/right\]#",
	                                   /** @lang RegExp */ "#\[center\](.*?)\[/center\]#",
	                                   /** @lang RegExp */ "#\[left\](.*?)\[/left\]#",
	                                   /** @lang RegExp */ "#\[code=(.*?)\]([\S\s.]*?)\[/code\]#");

	private static $bbElementsOpen = array("[b]",
	                                       "[i]",
	                                       "[u]",
	                                       "[s]",
	                                       "[url=",
	                                       "[img]",
	                                       "[video=",
	                                       "[list",
	                                       "[*]",
	                                       "[head]",
	                                       "[right]",
	                                       "[center]",
	                                       "[left]",
	                                       "[code=");

	private static $bbElementsClose = array("[/b]",
	                                        "[/i]",
	                                        "[/u]",
	                                        "[/s]",
	                                        "[/url]",
	                                        "[/img]",
	                                        "[/video]",
	                                        "[/list]",
	                                        "[/*]",
	                                        "[/head]",
	                                        "[/right]",
	                                        "[/center]",
	                                        "[/left]",
	                                        "[/code]");

	private static $htmlElements = array("<br />",
	                                     "<b>$1</b>",
	                                     "<i>$1</i>",
	                                     "<u>$1</u>",
	                                     "<strike>$1</strike>",
	                                     "<a href=\"$1$2\" target=\"$3\">$4</a>",
	                                     "<a href=\"gfx/img_generator.php?type=Picture&amp;id=$1&amp;size=Big\" data-lightbox=\"__gallery__\"><img src=\"gfx/img_generator.php?type=Picture&amp;id=$1&amp;size=Standard\"></a>",
	                                     "<a href=\"$1\" data-lightbox=\"__gallery__\"><img src=\"$1\"></a>",
	                                     "<iframe width=\"600\" height=\"340\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",
	                                     "<ul>$1</ul>",
	                                     "<ol>$1</ol>",
	                                     "<li>$1</li>",
	                                     "<h2>$1</h2>",
	                                     "<div style=\"text-align: right;\">$1</div>",
	                                     "<div style=\"text-align: center;\">$1</div>",
	                                     "<div style=\"text-align: left;\">$1</div>",
	                                     "<pre><code class=\"$1\">__code__</code></pre>");

	private static $xmlElements = array("",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "",
	                                    "");

	/**
	 * Konvertiert einen beliebigen Datumsstring in ein Standardformat.
	 *
	 * @param string $date Eingabedatum.
	 *
	 * @return string Datum im Standardformat.
	 */
	public static function date($date) {
		if (Converter::nullIfEmpty($date) == Converter::NULL) {
			return "";
		} else {
			return date_format(date_create($date), 'd.m.Y');
		}
	}

	/**
	 * Konvertiert einen beliebigen DatetimeString in ein Standardformat.
	 *
	 * @param string $date Eingabedatum.
	 *
	 * @return string Datetime im Standardformat.
	 */
	public static function dateTime($date) {
		return date_format(date_create($date), 'G:i \U\h\r d.m.Y');
	}

	/**
	 * Konvertiert Datumsstring in das Format der Datenbank.
	 *
	 * @param string $date Eingabedatum.
	 *
	 * @return string Datum im DBFormat.
	 */
	public static function dbDate($date) {
		return date_format(date_create($date), 'Y-m-d');
	}

	/**
	 * Liefert null falls der uebergebene Wert leer oder null ist. Funktioniert auch mit Datum und Zahlen.
	 *
	 * @param string $value Der Eingabewert.
	 *
	 * @return string Der aufbereitete Wert.
	 */
	public static function nullIfEmpty($value) {
		$result = $value;
		if (strlen(trim($value)) == 0 || $value == "0000-00-00" || $value == "00.00.0000" || $value === 0) {
			$result = Converter::NULL;
		}
		return $result;
	}

	/**
	 * Konvertiert BBCode zu HTML.
	 *
	 * @param string $text Der Eingabetext in BBFormat.
	 *
	 * @return string Der Text als HTML.
	 */
	public static function bbToHtml($text) {
		// Codefragmente extrahieren
		$hits = preg_match_all(/** @lang RegExp */ "#\[code=.+?\]([\S\s.]+?)\[/code\]#", $text, $ausgabe, PREG_SET_ORDER);

		// Standardtransformation
		$conv = preg_replace(Converter::$bbElements, Converter::$htmlElements, $text);

		// Vorschau fuer Bilder
		if (strpos($conv, Converter::REPLACE_PATTERN_GALLERY) !== false) {
			$conv = str_replace(Converter::REPLACE_PATTERN_GALLERY, "gallery" . ++Converter::$galCounter, $conv);
		}

		//HTML Entities fuer Code ersetzen
		for ($i = 0; $i < $hits; $i++) {
			$conv = preg_replace("#".Converter::REPLACE_PATTERN_CODE."#", htmlentities($ausgabe[$i][1]), $conv, 1);
		}

		return $conv;
	}

	/**
	 * Konvertiert BBCode zu XML.
	 *
	 * @param string $text Der Eingabetext in BBFormat.
	 *
	 * @return string Der Text als XML.
	 */
	public static function bbToXml($text) {
		$text = preg_replace(Converter::$bbElements, Converter::$xmlElements, $text);
		$text = str_replace("&nbsp;", "&#160;", $text);

		return $text;
	}

	/**
	 * Schneidet den ersten Teil eines Textes bis zur definierten Laenge ohne dabei die integritaet des BBCodes zu zerstoeren.
	 *
	 * @param string  $text   Der Eingabetext im BBFormat.
	 * @param integer $length Die Ziellaenge.
	 *
	 * @return string Der gekuerzte Text im BBFormat.
	 */
	public static function cutBB($text, $length) {
		$closeTags = array();
		$closeTagsPos = array();
		$splitpos = round($length * 0.95);

		if ($splitpos > strlen($text)) {
			$result = $text;
		} else {
			$splitpos = strpos($text, " ", $splitpos);
			if ($splitpos === false) {
				$result = $text;
			} else {
				$starText = substr($text, 0, $splitpos);
				//Nach offenen Tags suchen
				for ($i = 0; $i < count(Converter::$bbElementsOpen); $i++) {
					$occ = strrpos($starText, Converter::$bbElementsOpen[$i]);
					//Schließentag fehlt
					if ($occ !== false && strpos($starText, Converter::$bbElementsClose[$i], $occ) === false) {
						$closeTags[] = Converter::$bbElementsClose[$i];
						$closeTagsPos[] = $occ;
					}
				}

				//Result zusammenbauen
				array_multisort($closeTagsPos, $closeTags);
				$result = $starText . implode("", $closeTags) . " ...";
			}

		}

		return $result;
	}

}