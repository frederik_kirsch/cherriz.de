<?php

/**
 * Class Settings
 * Verwaltet Konfigurationswerte der Webseite.
 *
 * @author Frederik Kirsch
 */
class Settings {

	//Properties

	const PROPERTY_UNDER_CONSTRUCTION = "Under Construction";

	const PROPERTY_UNDER_CONSTRUCTION_CONTENT = "Under Construction Content";

	const PROPERTY_REGISTRATION_ALLOWED = "Registration Allowed";

	const PROPERTY_DATENSCHUTZ_REGISTRATION = "Datenschutz Registration";

	const PROPERTY_DEFAULT_EMAIL = "Default Email";

	const PROPERTY_ALLOWED_IMAGE_TYPES = "Allowed Image Types";

	const PROPERTY_MAX_IMAGE_SIZE = "Max Image Size";

	const PROPERTY_IMPRESSUM_CONTENT = "Impressum Content";

	const PROPERTY_NEWS_SPLIT_SIZE = "News Split Size";

	const PROPERTY_FORUM_SPLIT_SIZE = "Forum Split Size";

	const PROPERTY_THREADPOST_SPLIT_SIZE = "Threadpost Split Size";

	const PROPERTY_PAGE_MODE = "Page Mode";

	const PROPERTY_PAGE_MODE_PRODUCTION = "Production";

	const PROPERTY_PAGE_MODE_DEVELOPMENT = "Development";

	const PROPERTY_HISTORY_DAYS = "History Days";

	const OAUTH_ACCESS_TOKEN = "oauth_access_token";

	const OAUTH_ACCESS_TOKEN_SECRET = "oauth_access_token_secret";

	const CONSUMER_KEY = "consumer_key";

	const CONSUMER_SECRET = "consumer_secret";

	const PROPERTY_SESSION_DURATION = "Session Duration";

	const PROPERTY_SESSION_REMEMBER_DURATION = "Session Remember Duration";

	const PROPERTY_NETATMO_CLIENTID = "Netatmo ClientID";

	const PROPERTY_NETATMO_CLIENTSECRET = "Netatmo ClientSecret";

	// Class Constants

	const SPLIT_CHAR = "|";

	private $db;

	private static $instance = null;

	/**
	 * @return Settings Instanz der Klasse.
	 */
	public static function getInstance() {
		if (Settings::$instance == null) {
			Settings::$instance = new Settings();
		}
		return Settings::$instance;
	}

	private function __construct() {
		$this->db = DBConnect::getDBConnection();
	}

	/**
	 * Liest den Wert einer Property aus der Datenbank aus.
	 *
	 * @param string $property Der Name der Property.
	 *
	 * @return string|integer Der Wert der Property.
	 */
	public function getProperty($property) {
		$result = $this->db->query("SELECT Value FROM Settings WHERE Property = '$property'");
		$property = null;
		if ($result->num_rows == 1) {
			$hit = $result->fetch_object();
			$property = $hit->Value;
		}
		return $property;
	}

	/**
	 * Liest den Wert einer Property aus der Datenbank aus und liefert ihn als Array zurück.
	 *
	 * @param string $property Der Name der Property.
	 *
	 * @return array Der Wert der Property.
	 */
	public function getPropertyAsArray($property) {
		$result = $this->getProperty($property);
		return explode(Settings::SPLIT_CHAR, $result);
	}

	/**
	 * Setzt eine Property.
	 *
	 * @param string       $property Die Property die gespeichert werden soll.
	 * @param string|array $value    Der Wert der Property.
	 *
	 * @return bool Gibt den Erfolg der Aktion an.
	 */
	public function setProperty($property, $value) {
		if (is_array($value)) {
			$value = implode(Settings::SPLIT_CHAR, $value);
		}
		$result = false;
		if (Login::getInstance()->getCurrentUserObject()->hasRight(Recht::CHANGE_PROPERTIES)) {
			$result = $this->db->query("UPDATE Settings SET Value = '$value' WHERE Property = '$property'");
		} else {
			echo("WARNUNG: Sie haben keine Berechtigung zur Änderung der Webseiteneinstellungen.");
		}
		return $result;
	}

}