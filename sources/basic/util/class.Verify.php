<?php

/**
 * Class Verify
 *
 * Definiert Validierungsmethoden für verschiedene Attribute.
 *
 * @author Frederik Kirsch
 */
class Verify {

	//TODO FK: In Konstante überführen
	//const VALIDE = "OK";
	public static $VALIDE = "OK";

	private $db;

	private $filter;

	private $settings;

	public function __construct() {
		$this->filter = new Filter();
		$this->db = DBConnect::getDBConnection();
		$this->settings = Settings::getInstance();
	}

	/**
	 * Diese Funktion validiert einen Loginnamen
	 *
	 * @param string $nick
	 *
	 * @return string
	 */
	public function checkNick($nick) {
		//Länge prüfen
		if (strlen($nick) >= 3 && strlen($nick) <= 15) {
			//Regex prüfen
			if (preg_match('/^[A-Za-z]+[A-Za-z0-9_]*$/', $nick)) {
				//Prüfen ob bereits existent
				$abfrage = "SELECT ID FROM Users WHERE Nick = '$nick'";
				$result = $this->db->query($abfrage);
				if ($result->num_rows == 0) {
					$valResult = Verify::$VALIDE;
				} else {
					$valResult = "Der Nick ist leider schon vergeben.";
				}
			} else {
				$valResult = "Der Nick muss mit Buchstaben beginnen und darf mit Zahlen enden.";
			}
		} else {
			$valResult = "Der Nick muss mindestens 3 und darf maximal 15 Stellen haben.";
		}
		return $valResult;
	}

	/**
	 * Diese Funktion validiert eine Emailadresse
	 *
	 * @param string $email
	 *
	 * @return string
	 */
	public function checkEmail($email) {
		if (preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i', $email)) {
			$abfrage = "SELECT ID FROM Users WHERE Email = '$email'";
			$result = $this->db->query($abfrage);
			if ($result->num_rows == 0) {
				$valResult = Verify::$VALIDE;
			} else {
				$valResult = "Es gibt bereits einen Nutzer mit dieser E-Mail Adresse.";
			}
		} else {
			$valResult = "Dies ist keine gültige E-Mail Adresse.";
		}
		return $valResult;
	}

	/**
	 * Diese Funktion validiert einen Twitteraccount.
	 *
	 * @param string $twitter Den Namen des Twitteraccounts.
	 *
	 * @return string Das Ergebnis der Validierung.
	 */
	public function checkTwitter($twitter) {
		if (preg_match('/^@\w{1,15}$/i', $twitter)) {
			LibImporter::import("basic/util/class.Twitter.php");
			$twitterAPI = new Twitter();
			if ($twitterAPI->checkUserExists($twitter) === true) {
				$valResult = Verify::$VALIDE;
			} else {
				$valResult = "Es existiert kein Account mit diesem Namen.";
			}
		} else {
			$valResult = "Dies ist kein gültiger Accountname.";
		}
		return $valResult;
	}

	/**
	 * DIese Funktion validiert eine Telefon- oder Handynummer.
	 *
	 * @param integer $number
	 *
	 * @return string
	 */
	public function checkPhoneNumber($number) {
		if (preg_match('/^(\d+\s*)*$/i', $number)) {
			$valResult = Verify::$VALIDE;
		} else {
			$valResult = "Dies ist keine gültige Telefonnummer.";
		}
		return $valResult;
	}

	/**
	 * Diese Funktion validiert zwei Passwörter
	 *
	 * @param string $pwd1
	 * @param string $pwd2
	 *
	 * @return string
	 */
	public function checkPasswortIdentical($pwd1, $pwd2) {
		if ($pwd1 == $pwd2) {
			$valResult = $this->checkPasswort($pwd1);
		} else {
			$valResult = "Die Passwörter stimmen nicht überein.";
		}
		return $valResult;
	}

	/**
	 * Diese Funktion validiert ein Passwort
	 *
	 * @param string $pwd
	 *
	 * @return string
	 */
	public function checkPasswort($pwd) {
		if (strlen($pwd) >= 8 && strlen($pwd) <= 25) {
			$valResult = Verify::$VALIDE;
		} else {
			$valResult = "Das Passwort muss mindestens 8 und darf maximal 25 Stellen haben.";
		}
		return $valResult;
	}

	/**
	 * Diese Funktion validiert einen Namen
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	public function checkName($name) {
		if (strlen($name) >= 3 && strlen($name) <= 25) {
			if (preg_match('/^([A-Za-z]+\s*)+$/', $name)) {
				$valResult = Verify::$VALIDE;
			} else {
				$valResult = "Für Namen sind nur Buchstaben erlaubt.";
			}
		} else {
			$valResult = "Ein Name muss mindestens 3 und darf maximal 25 Stellen haben.";
		}
		return $valResult;
	}

	/**
	 * Validiert den Wert fuer ein Toggle.
	 * @param string $toggle Der Wert.
	 * @return string Das Ergebnis der Validierung.
	 */
	public function checkToggle($toggle) {
		if (preg_match('/^[01]{1}$/', $toggle)) {
			return Verify::$VALIDE;
		}
		if (strtolower($toggle) === "true" || strtolower($toggle) === "false") {
			return Verify::$VALIDE;
		}
		return "Der Wert ist als Toggle nicht zulaessig.";
	}

	/**
	 * Diese Funktion validiert ein Geburtsdatum nach dem Schema DD.MM.YYYY
	 *
	 * @param string $date
	 *
	 * @return string
	 */
	public function checkBirthdate($date) {
		if (preg_match('/^[0-9]{2}.[0-9]{2}.[0-9]{4}$/', $date)) {
			$date = explode(".", $date);
			if (checkdate($date[1], $date[0], $date[2])) {
				if ($date[2] >= date("Y") - 100 && $date[2] <= date("Y")) {
					$valResult = Verify::$VALIDE;
				} else {
					$valResult = "Das Geburtsdatum ist ungültig.";
				}
			} else {
				$valResult = "Das Geburtsdatum ist kein existierendes Datum.";
			}
		} else {
			$valResult = "Das Geburtsdatum entspricht nicht dem Format DD.MM.JJJJ.";
		}
		return $valResult;
	}

	/**
	 * Diese Funktion validiert eine Straße
	 *
	 * @param string $street
	 *
	 * @return string
	 */
	public function checkStreet($street) {
		if (strlen($street) >= 5 && preg_match('/^([a-zA-ZÜÄÖüäö\s\.\-\ß]+)\s(.*[0-9]+.*)$/', $street)) {
			$valResult = Verify::$VALIDE;
		} else {
			$valResult = "Die Straße entspricht keinem gültigen Format.";
		}
		return $valResult;
	}

	/**
	 * Validiert eine Postleitzahl syntaktisch.
	 *
	 * @param integer $plz
	 *
	 * @return string
	 */
	public function checkZIP($plz) {
		if (strlen($plz) == 5) {
			if (is_numeric($plz)) {
				$valResult = Verify::$VALIDE;
			} else {
				$valResult = "Die PLZ muss eine Zahl sein.";
			}
		} else {
			$valResult = "Die PLZ muss genau 5 Stellen haben.";
		}
		return $valResult;
	}

	/**
	 * Diese Funktion Validiert einen Ortsnamen
	 *
	 * @param string $city
	 *
	 * @return string
	 */
	public function checkCity($city) {
		if (strlen($city) >= 3) {
			if (preg_match('/^([a-zA-ZÜÄÖüäö\s\-\ß]+)$/', $city)) {
				$valResult = Verify::$VALIDE;
			} else {
				$valResult = "Der Ortsnamen entspricht keinem gültigen Format.";
			}
		} else {
			$valResult = "Der Ortsnamen muss mindestens 3 Stellen haben.";
		}
		return $valResult;
	}

	/**
	 * Diese Funktion validiert eine hochgeladen Grafik auf Avatartauglichkeit.
	 *
	 * @param resource $avatar
	 *
	 * @return string
	 */
	public function checkAvatar($avatar) {
		return $this->checkImage($avatar);
	}

	/**
	 * Validiert eine hochgeladene Grafik inklusive Tags ob diese in der Datenbank gespeichert werden kann.
	 *
	 * @param resource $picture
	 * @param string   $tags
	 *
	 * @return string
	 */
	public function checkPicture($picture, $tags) {
		$valResult = $this->checkImage($picture);
		if ($valResult == Verify::$VALIDE) {
			if (strlen(trim($tags)) == 0) {
				$valResult = "Es muss mindestens ein Tag angegeben sein.";
			}
		}
		return $valResult;
	}

	/**
	 * Validiert eine hochgeladene Grafik ob diese in der Datenbank gespeichert werden kann.
	 *
	 * @param resource $image
	 *
	 * @return string
	 */
	private function checkImage($image) {
		//	Prüfung ob Upload funktioniert hat.
		if ($image['tmp_name'] != "" && $image['size'] > 0) {
			//	Prüfung ob erlaubter Dateityp.
			$supportedTypes = explode(",", $this->settings->getProperty(Settings::PROPERTY_ALLOWED_IMAGE_TYPES));
			if (in_array($image['type'], $supportedTypes)) {
				//	Prüfung ob Dateigröße in Ordnung.
				if ($image['size'] < $this->settings->getProperty(Settings::PROPERTY_MAX_IMAGE_SIZE)) {
					$valResult = Verify::$VALIDE;
				} else {
					$valResult = "Datei ist zu groß.";
				}
			} else {
				$valResult = "Dieser Dateityp wird nicht untersützt. Nur " . implode(", ",
						$supportedTypes) . " erlaubt.";
			}
		} else {
			$valResult = "Bild nicht vorhanden.";
		}
		return $valResult;
	}

	/**
	 * Validiert eine UserSignatur
	 *
	 * @param string $signatur
	 *
	 * @return string
	 */
	public function checkSignatur($signatur) {
		if (preg_match('/^[\w\d\s:._-]+$/', $signatur)) {
			$valResult = Verify::$VALIDE;
		} else {
			$valResult = "Die Signatur entspricht keinem gültigen Format. Erlaubt sind: A-Z, 0-9, :._-";
		}
		return $valResult;
	}

	/**
	 * Validiert gegen die möglichen Privatsphärestatus.
	 *
	 * @param string $privatsphaere
	 *
	 * @return string
	 */
	public function checkPrivatsphaere($privatsphaere) {
		$valResult = "Dies ist kein gültiger Status für die Privatsphäre.";
		switch ($privatsphaere) {
			case User::PRIVACY_PUBLIC :
			case User::PRIVACY_LIMITED :
			case User::PRIVACY_PRIVATE :
				$valResult = Verify::$VALIDE;
				break;
		}
		return $valResult;
	}

	/**
	 * Bietet eine Grundvalidierung fuer Authentifizierungsdaten die an dritte Systeme uebertragen werden sollen
	 * @param string $phrase Die zu validierende Phrase.
	 * @return string Ergebnis der Validierung
	 */
	public function checkExternalAuthData($phrase) {
		if (strlen($phrase) < 3 || strlen($phrase) > 50) {
			return "Der Wert muss mindestens 3 und darf maximal 50 Stellen haben.";
		}
		return Verify::$VALIDE;;
	}

}