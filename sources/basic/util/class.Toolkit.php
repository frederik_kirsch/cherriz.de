<?php

class Toolkit {

	const VALUE_POOL = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz0123456789";

    const VALUE_POOL_NUMERIC = "0123456789";

	private static $asyncScriptsLoaded = false;

	public static function showMessage($title, $message) {
		echo("<script type='text/javascript'>
                new InfoDialog('$title', '$message').open();
              </script>");
	}

	public static function printDatePickerScripts($dateField, $time = false, $min = null, $max = null) {
		//Mit Zeit Verarbeiten
		$inputStr = "'DD.MM.YYYY HH:mm'";
		$timeStr = "";
		if (!$time) {
			$inputStr = "'DD.MM.YYYY'";
			$timeStr = ", time: false";
		}

		//Min und Maximaldatum
		$minStr = "";
		if ($min != null) {
			$minStr = ", min: '" . $min . "'";
		}
		$maxStr = "";
		if ($max != null) {
			$maxStr = ", max: '" . $max . "'";
		}

		$code = "rome($('#" . $dateField . "')[0], {inputFormat: " . $inputStr . $timeStr . $minStr . $maxStr . "});";

		Toolkit::loadStyle("rome/rome.min.css");
		Toolkit::loadScripts(array("rome/rome.min.js"), $code);
	}

	public static function generateKey($pValuePool, $pLength) {
		$key = "";
		srand((double) microtime() * 1000000);
		for ($i = 0; $i < $pLength; $i++) {
			$key .= substr($pValuePool, (rand() % (strlen($pValuePool))), 1);
		}
		return $key;
	}

	public static function printGroupAccessRequest($groupID) {
		Toolkit::loadScripts(array("sites/gruppen_utility.js"));
		$user = Login::getInstance()->getCurrentUserObject();
		$uID = $user->get(User::PROPERTY_ID);
		$uNick = $user->get(User::PROPERTY_Nick);
		$gruppe = new Group($groupID);
		echo("<span id='gar$groupID' data-tooltip='Beitrittsanfrage erstellen' title='Beitrittsanfrage erstellen' class='icon icon_sidebar iconQuestion' onclick='openAccessRequestDialog($uID, \"$uNick\", " . $gruppe->getID() . ", \"" . $gruppe->getName() . "\")'> </span>");
	}

	public static function printGroupSelector($initGroupIDs = array()) {
		$initGroups = "";
		if (count($initGroupIDs) > 0) {
			$initGroups = "'" . implode("','", $initGroupIDs) . "'";
		}

		echo("<span title='Jeder darf diesen Beitrag sehen.'><input type='radio' name='visibility' value='Public' checked/> Öffentlich</span>
			  <span title='Der Beitrag ist für meine Gruppen sichtbar.'><input type='radio' name='visibility' value='Umfeld'/> Mein Umfeld</span>
			  <span title='Der Beitrag ist für spezifische Gruppen sichtbar. Mehrfachauswahl über Strg + Click.'><input type='radio' name='visibility' value='Spezifisch'/> Benutzerdefiniert</span><br />
			  <ol id='grp_lst' class='dialog_group' style='display: none; width: 370px;'></ol>
			  <input id='groups' name='groups' type='text' style='display: none' />
			  <script type='text/javascript'>var groupSelector;</script>");

		Toolkit::loadScripts(array("groupselector_utility.js"),
		                     "groupSelector = new GroupSelector($('[name=visibility]'), $('#grp_lst'), $('#groups'), [$initGroups])");
	}

	public static function printWYSIWYG($pName, $pInitialData, $pWidth, $pHeight) {
		//Betaabfrage, solange Code in Beta ist
		$beta = "false";
		$user = Login::getInstance()->getCurrentUserObject();
		if ($user->get(User::PROPERTY_Use_Experimental) == "1") {
			$beta = "true";
		}

		Toolkit::loadStyle("cleditor/jquery.cleditor.css");
		Toolkit::loadScripts(array("cleditor/jquery.cleditor.xhtml.js",
		                           "cleditor/jquery.cleditor.bbcode.js",
		                           "cleditor/jquery.cleditor.cimage.js",
		                           "cleditor/jquery.cleditor.cimagegal.js",
		                           "cleditor/jquery.cleditor.clink.js",
		                           "cleditor/jquery.cleditor.cvideo.js",
		                           "cleditor/jquery.cleditor.cheader.js",
		                           "cleditor/jquery.cleditor.ccode.js"), "var editor = createEditor('$pName', $pWidth, $pHeight, $beta);",
		                     array("jquery/jquery.form.js", "cleditor/jquery.cleditor.min.js", "wysiwyg_utility.js"));
		echo("<textarea id='$pName' name='$pName'>$pInitialData</textarea>");
	}

	/**
	 * Initialisiert den Contentloader mit der uebergebenen Konfiguration. Dient dem nachladen von Inhalten beim
	 * scrollen an das Seitenende.
	 *
	 * @param string $file     Konfigurationsdatei mit der der Loader initialisiert wird.
	 * @param string $bindings Script zum Start der Konfigurationsdatei.
	 */
	public static function printAsynchrounusContentLoader($file, $bindings) {
		if (self::$asyncScriptsLoaded) {
			return;
		}
		self::$asyncScriptsLoaded = true;

		echo("<div id='loaderTop' style='display: none'></div>");
		echo("<div id='loaderInfo' style='display: none'></div>");
		Toolkit::loadScripts(array("asynchrounus_content_loader.js", $file), $bindings);
	}

	/**
	 * Zeigt die Standardmeldung dafuer, dass der Nutzer auf den gewuenschten Content keinen Zugriff hat.
	 */
	public static function printAccessDenied() {
		echo("<h2>Sorry...</h2>");
		if (Login::getInstance()->isLoggedIn()) {
			echo("<p>...du hast keine Berechtigung um diesen Inhalt betrachten zu können.</p>");
		} else {
			echo("<p>...du musst eingeloggt sein um diesen Inhalt betrachten zu können.</p>");
			echo("<p>Nutze die Loginbox auf der rechten Seite, um dich einzuloggen, oder um einen neuen Account zu erstellen.</p>");
		}
	}

	private static $scriptsLoaded = array();

	/**
	 * Laed scripts asynchron nach und bietet die moeglichkeit nach deren Laden Code zur initialisierung auszufuehren.
	 *
	 * @param string[] $files           Array mit Pfaden zu den nachzuladenden Scripts.
	 * @param null     $executions      Javascriptcode, welcher erst ausgefuehrt werden darf, wenn alle Scripts geladen sind.
	 * @param null     $baseFiles       Basisdateien die geladen sein muessen, bevor die eigentlichen Scripts geladen werden
	 *                                  duerfen.
	 */
	public static function loadScripts($files, $executions = null, $baseFiles = null) {
		//Scripts für Übergabe in JS Funktion aufbereiten
		$scripts = array();
		if ($baseFiles != null) {
			foreach ($baseFiles as $file) {
				$scripts[] = "{file: 'scripts/" . $file . "', base: true}";
			}
		}
		foreach ($files as $file) {
			$scripts[] = "{file: 'scripts/" . $file . "', base: false}";
		}
		$scriptStr = implode(",", $scripts);

		//Hashwert für Scriptvergleich erzeugen
		$hash = md5($scriptStr);

		//JS erzeugen
		echo("<script type='text/javascript'>");
		if ($executions != null) {
			//Adaptionsfunktion immer ausführen
			echo("bufferExecution('" . $hash . "', \"" . Filter::javaScript($executions) . "\");");
		}
		//Scripts nur beim ersten mal laden
		if (!in_array($hash, Toolkit::$scriptsLoaded)) {
			echo("loadScripts('" . $hash . "', new Array(" . $scriptStr . "));");
		}
		echo("</script>");


		//Geladene Scripts merken
		Toolkit::$scriptsLoaded[] = $hash;
	}

	private static $stylesLoaded = array();

	/**
	 * Laedt das uebergebene Stylesheet.
	 *
	 * @param $style String Pfad zum Stylesheet.
	 */
	public static function loadStyle($style) {
		//Nur einmalig laden
		if (in_array($style, Toolkit::$stylesLoaded)) {
			return;
		}

		//Style laden
		echo("<script type='text/javascript'>
                  loadStyle('styles/" . $style . "');
              </script>");

		//Merken
		Toolkit::$stylesLoaded[] = $style;
	}

}