<?php

/**
 * Class Filter
 *
 */
class Filter {

	public static function text($text) {
		$db = DBConnect::getDBConnection();
		$text = $db->real_escape_string($text);
		$text = trim($text);
		return $text;
	}

	public static function picture($pic) {
		$db = DBConnect::getDBConnection();
		return $pic = $db->real_escape_string($pic);
	}

	public static function file($file) {
		$db = DBConnect::getDBConnection();
		$file['name'] = $db->real_escape_string($file['name']);
		$file['type'] = $db->real_escape_string($file['type']);
		$file['tmp_name'] = $db->real_escape_string($file['tmp_name']);
		$file['error'] = $db->real_escape_string($file['error']);
		$file['size'] = $db->real_escape_string($file['size']);
		return $file;
	}

	public static function javaScript($script) {
		$script = str_replace(array("\r\n", "\r", "\n"), "", $script);
		$script = preg_replace('/[ ]+/', ' ', $script);
		return $script;
	}

	public static function message($text) {
		return preg_replace('#(?<!\r)\n#si', "\n", $text);
	}

	public static function mailText($text) {
		$text = Filter::message($text);
		$text = wordwrap($text, 70);
		return $text;
	}

	public static function tags($tags) {
		$result = Filter::text($tags);
		$result = str_replace("  ", " ", $result);
		$result = str_replace(",,", ",", $result);
		$result = preg_replace('/,$/', "", $result);
		$result = preg_replace('/^,/', "", $result);
		$result = trim($result);
		return $result;
	}

	/**
	 * @param string $searchQuery Suchstring aus GET Parameter.
	 *
	 * @return array Aufbereitete Suchanfrage.
	 */
	public static function searchQuery($searchQuery) {
		$result = Filter::text($searchQuery);
		$result = str_replace(",", " ", $result);
		$result = str_replace("  ", " ", $result);
		$result = trim($result);
		if($result == ""){
			return array();
		}
		return explode(" ", $result);
	}

}