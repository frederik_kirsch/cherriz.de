<?php

/**
 * Class Message definiert eine Nachricht, welche über verschiedene Kanäle versendet werden kann.
 */
class Message {

	const REGISTRIERUNGSBESTAETIGUNG = "Registrierungsbestaetigung";

	const WIEDERHERSTELLUNGSBESTAETIGUNG = "Wiederherstellungsbestaetigung";

	const EMAILBESTAETIGUNG = "Emailbestaetigung";

	const BEITRITTSANFRAGEGRUPPE = "BeitrittsanfrageGruppe";

	const BEITRITTSANFRAGEGRUPPEANGENOMMEN = "BeitrittsanfrageGruppeAngenomm";

	const BEITRITTSANFRAGEGRUPPEABGELEHNT = "BeitrittsanfrageGruppeAbgelehn";

	const BENACHRICHTIGUNGNEWS = "BenachrichtigungNews";

	const BENACHRICHTIGUNGKOMMENTAR = "BenachrichtigungKommentar";

	const BENACHRICHTIGUNGTHREADPOST = "BenachrichtigungThreadpost";

	private static $messageCache = array();

	/**
	 * Erzeugt eine Nachricht.
	 *
	 * @param string      $type Typ
	 * @param null| array $para Parameter
	 *
	 * @return Message
	 */
	public static function createInstance($type, $para = null) {
		//Message laden
		$result = Message::obtainMessage($type);

		//Objekt erzeugen
		$message = new Message($result->Betreff, $result->Text, $result->Text_Short);

		//Variablen ersetzen
		$message->prepareText($para);

		return $message;
	}

	private static function obtainMessage($type) {
		if (!array_key_exists($type, Message::$messageCache)) {
			$db = DBConnect::getDBConnection();
			$result = $db->query("SELECT Betreff, Text, Text_Short FROM Message WHERE Type = '" . $type . "'")->fetch_object();
			Message::$messageCache[$type] = $result;
		}
		return Message::$messageCache[$type];
	}

	private $betreff;

	private $text;

	private $shortText;

	private function __construct($betreff, $text, $shortText) {
		$this->betreff = $betreff;
		$this->text = $text;
		$this->shortText = $shortText;
	}

	public function getBetreff() {
		return $this->betreff;
	}

	public function getText() {
		return $this->text;
	}

	public function getShortText() {
		return $this->shortText;
	}

	private function prepareText($vars) {
		//Namearray aufbauen
		if ($vars != null) {
			$names = array();
			for ($i = 1; $i <= count($vars); $i++) {
				$names[] = "%v" . $i . "%";
			}

			//Daten ersetzen
			$this->shortText = str_replace($names, $vars, $this->getShortText());
			$this->text = str_replace($names, $vars, $this->getText());
		}

		//Daten filtern
		$this->betreff = Filter::message($this->getBetreff());
		$this->shortText = Filter::message($this->getShortText());
		$this->text = Filter::mailText($this->getText());
	}

}