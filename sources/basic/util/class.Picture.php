<?php

/**
 * Class Picture Dient zur Anzeige von Bilder aus der Datenbank, Nutzer- und Gruppenavataren.
 * 
 * @author Frederik Kirsch
 */
class Picture {

	const USER = "User";

	const GRUPPE = "Gruppe";

	const PICTURE = "Picture";

	const UPLOAD = "Upload";

	const PICTURE_SIZE_SMALL = "Small";

	const PICTURE_SIZE_STANDARD = "Standard";

	const PICTURE_SIZE_BIG = "Big";

	const STATUS_STANDARD = "Standard";

	const STATUS_GELOESCHT = "Gelöscht";

	const STATUS_GESPERRT = "Gesperrt";

	private $db;

	private $dbBin;

	private $filter;

	public function __construct() {
		$this->db = DBConnect::getDBConnection();
		$this->dbBin = DBConnect::getDBConnection(DBConnect::CHARSET_UTF8_BIN);
		$this->filter = new Filter();
	}

	/**
	 * Zeigt den Avatar eines Benutzers.
	 *
	 * @param int    $id   Die ID des Nutzers.
	 * @param string $size Die Groesse des Avatars.
	 */
	public function showUser($id, $size) {
		$id = $this->filter->text($id);
		$size = $this->filter->text($size);
		$name = "Avatar_$size";
		$result = $this->db->query("SELECT $name FROM Users WHERE ID = $id");
		$resultObj = $result->fetch_object();
		header('Content-type: image/jpg');
		$pic = $resultObj->$name;
		if ($pic != "") {
			echo($pic);
		} else {
			readfile("avatar/" . $name . ".png");
		}
	}

	/**
	 * Zeigt das Logo einer Gruppe.
	 *
	 * @param int    $id   ID der Gruppe.
	 * @param string $size Groesse des Bildes.
	 */
	public function showGruppe($id, $size) {
		$id = $this->filter->text($id);
		$size = $this->filter->text($size);
		$col = "Logo_$size";
		$result = $this->dbBin->query("SELECT $col FROM Gruppe WHERE ID = $id");
		$resultObj = $result->fetch_object();
		header('Content-type: image/jpg');
		echo($resultObj->$col);
	}

	/**
	 * Zeigt ein Bild.
	 *
	 * @param int    $id   Die ID des Bildes.
	 * @param string $size Die Groesse des Bildes.
	 */
	public function showPicture($id, $size) {
		$id = $this->filter->text($id);
		$size = $this->filter->text($size);
		$result = $this->dbBin->query("SELECT P.Data_$size AS Data, P.Public,
									    (SELECT PDT.Data_Type
										    FROM Picture_Data_Type PDT
											WHERE P.Data_Type = PDT.ID) AS Data_Type,
										(SELECT PS.Status
										    FROM Picture_Status PS
										    WHERE P.Status = PS.ID) AS Status
										FROM Picture P
										WHERE P.ID = $id");
		$resultObj = $result->fetch_object();

		$access = $this->hasAccess($id, $resultObj->Status, $resultObj->Public);

		if ($access === true) {
			header("Content-type: " . $resultObj->Data_Type);
			echo($resultObj->Data);
		} else {
			header("Content-type: image/png");
			$imageData = getimagesizefromstring($resultObj->Data);
			$errorImage = $this->createImageWithText($imageData[0], $imageData[1], $access);
			echo($errorImage);
		}
	}

	/**
	 * Prueft ob der angemeldete Benutzer ein bestimmtes Bild sehen darf. Wenn nein wird der Grund zurueckgegeben.
	 *
	 * @param int    $id     Die ID des Bildes.
	 * @param string $status Der Status des Bildes.
	 * @param int    $public Ob das Bild oeffentlich sichtbar ist.
	 *
	 * @return bool|string true wenn Zugriff, sonst false.
	 */
	private function hasAccess($id, $status, $public) {
		$access = true;
		if (Picture::STATUS_STANDARD == $status && $public == 0) {
			if (isset($_SESSION[CherrizSessionHandler::NAME_USER])) {
				$userID = $_SESSION[CherrizSessionHandler::NAME_USER];
				$grpResult = $this->db->query("SELECT COUNT(*) AS Allowed
											 FROM Picture_Allowed_Groups
			                                WHERE Picture = $id AND `Group` IN (SELECT Gruppe FROM Gruppe_Mitglied WHERE User = $userID)");
				$grpResultObj = $grpResult->fetch_object();
				if ($grpResultObj->Allowed == 0) {
					$access = "Sie haben leider keine Berechtigung das Bild zu sehen.";
				}
			} else {
				$access = "Sie müssen angemeldet sein um das Bild zu sehen.";
			}
		} elseif (Picture::STATUS_GESPERRT == $status) {
			$access = "Das Bild ist leider nicht für die Anzeige verfügbar.";
		} elseif (Picture::STATUS_GELOESCHT == $status) {
			$access = "Das Bild ist leider nichtmehr verfügbar. Es wurde gelöscht.";
		}
		return $access;
	}

	/**
	 * Erzeugt ein leeres Bild mit dem definierten Text in der Mitte.
	 *
	 * @param int    $width  Die Breite.
	 * @param int    $height Die Höhe.
	 * @param string $text   Der Text.
	 *
	 * @return resource Das Bild als png;
	 */
	private function createImageWithText($width, $height, $text) {
		$font = 3;
		$bild = imagecreatetruecolor($width, $height);
		$white = imagecolorallocate($bild, 255, 255, 255);
		$black = imagecolorallocate($bild, 0, 0, 0);
		imagefill($bild, 0, 0, $white);

		$textWidth = imagefontwidth($font) * strlen($text);
		$xMax = $width - 10;
		$xMin = 10;
		$xLoc = ($xMax - $xMin - $textWidth) / 2 + $xMin + $font;
		$y = ($height / 2) - 10;
		imagestring($bild, $font, $xLoc, $y, $text, $black);

		return imagepng($bild);
	}

	/**
	 * Komprimiert ein Bild und behaelt dabei Dateiformat und Aufloessung bei.
	 *
	 * @param string[] $picture Das Quellbild.
	 *
	 * @return string Das erzeugte Bild.
	 */
	public static function getCompressedPicture($picture) {
		list($imgWidth, $imgHeight, ,) = getimagesize($picture['tmp_name']);
		return Picture::getBasicResizedPicture($picture, $imgWidth, $imgHeight, $picture['type'], false);
	}

	/**
	 * Aendert die Bildgroesse.
	 *
	 * @param string[] $picture  Das Bild.
	 * @param int      $width    Die Breite des neuen Bilds.
	 * @param int      $height   Die Hoehe des neuen Bilds.
	 * @param string   $destType Das Zieldateiformat.
	 *
	 * @return string Das umgerechnete Bild.
	 */
	public static function getResizedPicture($picture, $width, $height, $destType) {
		return Picture::getBasicResizedPicture($picture, $width, $height, $destType, false);
	}

	/**
	 * Aendert die Bildgroesse und erzeugt ein quadratisches Bild.
	 *
	 * @param string[] $picture    Das Bild.
	 * @param int      $sideLength Kantenlaenge des neuen Bilds.
	 * @param string   $destType   Zieldateiformat.
	 *
	 * @return string Das umgerechnete Bild.
	 */
	public static function getResizedPictureAsSquare($picture, $sideLength, $destType) {
		return Picture::getBasicResizedPicture($picture, $sideLength, $sideLength, $destType, true);
	}

	/**
	 * Rechnet das Bild auf eine andere Groesse um, ohne dabei das Seitenverhaltnis zu verlieren.
	 *
	 * @param string[] $picture   Das Bild.
	 * @param int      $maxWidth  Maximalbreite des Zielbildes.
	 * @param int      $maxHeight Maximalhoehe des Zielbildes.
	 * @param string   $destType  Zieldateiformat.
	 *
	 * @return string Das umgerechnete Bild.
	 */
	public static function getResizedPictureKeepingAspect($picture, $maxWidth, $maxHeight, $destType) {
		list($imgWidth, $imgHeight, ,) = getimagesize($picture['tmp_name']);
		$widthRatio = $imgWidth / $maxWidth;
		$heightRatio = $imgHeight / $maxHeight;
		$destWidht = $imgWidth;
		$destHeight = $imgHeight;

		if ($widthRatio > 1 && $widthRatio >= $heightRatio) {
			$destWidht = $imgWidth / $widthRatio;
			$destHeight = $imgHeight / $widthRatio;
		} else if ($heightRatio > 1) {
			$destWidht = $imgWidth / $heightRatio;
			$destHeight = $imgHeight / $heightRatio;
		}
		return Picture::getBasicResizedPicture($picture, $destWidht, $destHeight, $destType, false);
	}

	/**
	 * Rechnet das Bild auf eine neue Auflösung um. Das Bild wird in den uebergebenen Dateityp umgewandelt.
	 *
	 * @param string[] $picture     Das zu verarbeitende Bild.
	 * @param int      $pWidth      Die Zielbreite.
	 * @param int      $pHeight     Die Zielhöhe.
	 * @param string   $destType    Zieldateiformat.
	 * @param boolean  $cutAsSquare Gibt an ob das Bild quadratisch sein soll, ohne zu verziehen. Schneidet die Seiten ab.
	 *
	 * @return string Das Bild als String.
	 */
	private static function getBasicResizedPicture($picture, $pWidth, $pHeight, $destType, $cutAsSquare) {
		list($width, $height, ,) = getimagesize($picture['tmp_name']);
		switch ($picture['type']) {
			case "image/png":
				$imageOriginal = imagecreatefrompng($picture['tmp_name']);
				break;
			case "image/gif":
				$imageOriginal = imagecreatefromgif($picture['tmp_name']);
				break;
			case "image/jpg":
			case "image/jpeg":
				$imageOriginal = imagecreatefromjpeg($picture['tmp_name']);
				break;
			default:
				$picture['type'] = "image/jpg";
				$imageOriginal = imagecreatefromjpeg($picture['tmp_name']);
				break;
		}

		$image_dest = imagecreatetruecolor($pWidth, $pHeight);
		//Transparenz sicherstellen
		if ($picture['type'] == "image/png" or $picture['type'] == "image/gif") {
			imagecolortransparent($image_dest, imagecolorallocatealpha($image_dest, 0, 0, 0, 127));
			imagealphablending($image_dest, false);
			imagesavealpha($image_dest, true);
		}

		$src_x = 0;
		$src_y = 0;
		if ($cutAsSquare === true) {
			if ($width > $height) {
				$diff = ($width - $height) / 2;
				$src_x = $diff;
				$width -= $diff * 2;
			} else {
				$diff = ($height - $width) / 2;
				$src_y = $diff;
				$height -= $diff * 2;
			}
		}
		imagecopyresampled($image_dest, $imageOriginal, 0, 0, $src_x, $src_y, $pWidth, $pHeight, $width, $height);

		$tmpPath = $picture['tmp_name'] . rand(1, 9999);
		switch ($destType) {
			case "image/jpg":
			case "image/jpeg":
				imagejpeg($image_dest, $tmpPath, 100);
				break;
			case "image/png":
				imagepng($image_dest, $tmpPath, 8);
				break;
			case "image/gif":
				imagegif($image_dest, $tmpPath);
				break;
		}
		$stream = fopen($tmpPath, "rb");
		$image_dest = fread($stream, filesize($tmpPath));
		fclose($stream);
		$image_dest = Filter::picture($image_dest);
		return $image_dest;
	}

}