<?php
/**
 * Beinhaltet alle zentralen Klassen und Variablen als Grundlage einer selbstständig aufrufbaren Seite.
 *
 * Import auf diese Weise
 * require '../basic/class.LibImporter.php';
 * LibImporter::setExecutionPath("../");
 * LibImporter::import("basic/globals.php");
 */

//Fehleranzeige
error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", 1);
//seit PHP 5.3 sollte die Zeitzone gesetzt werden
date_default_timezone_set('Europe/Berlin');

//Basic Includes
LibImporter::import("sites/interface.Menu.php");
LibImporter::import("basic/util/class.DBConnect.php");
LibImporter::import("basic/util/class.Filter.php");
LibImporter::import("basic/util/class.Converter.php");
LibImporter::import("basic/util/class.Settings.php");
LibImporter::import("basic/util/class.CherrizSessionHandler.php");
LibImporter::import("basic/interaction/class.Login.php");
LibImporter::import("basic/entity/interface.Recht.php");
LibImporter::import("basic/entity/class.User.php");

//Basis Hilfsklassen initialisieren
CherrizSessionHandler::init();