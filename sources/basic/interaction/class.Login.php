<?php

/**
 * Class Login
 * Kapselt die Logik fuer die Authentifizierung von Benutzern.
 *
 * @author Frederik Kirsch
 */
class Login {

	const LOGIN_NAME = "login_name";

	const LOGIN_PASS = "login_pass";

	const LOGIN_REMEMBER = "login_remember";

	private $db = null;

	private $curUser = null;

	private $message = "";

	private static $instance = null;

	/**
	 * @return Login
	 */
	public static function getInstance() {
		if (Login::$instance == null) {
			Login::$instance = new Login();
		}
		return Login::$instance;
	}

	public function __construct() {
		$this->db = DBConnect::getDBConnection();

		if (isset($_POST['login_submitted']) && $_POST['login_submitted'] == true) {
			//Alle Daten des Loginformulars abrufen
			$user = $_POST[Login::LOGIN_NAME];
			$pass = $_POST[Login::LOGIN_PASS];

			$remember = false;
			if ($_POST[Login::LOGIN_REMEMBER] == "true") {
				$remember = true;
			}
			$this->message = $this->processLogin($user, $pass, $remember);
		} elseif (isset($_REQUEST['logout']) && $_REQUEST['logout'] == true) {
			$this->processLogout();
		}
	}

	/**
	 * Fuehrt den Login durch.
	 *
	 * @param string $user
	 * @param string $password
	 * @param bool   $remember
	 *
	 * @return boolean|string Fehlermeldung beim Login oder nichts bei Erfolg.
	 */
	public function processLogin($user, $password, $remember = false) {
		//Prüfen ob Alle Felder ausgefüllt sind
		if ($user == "" || $password == "") {
			return "Nick oder Passwort nicht angegeben.";
		}
		$abfrage = "SELECT U.ID, US.Status
                      FROM Users U, User_Status US
                     WHERE U.Status = US.ID
                       AND U.Nick = '" . Filter::text($user) . "'
                       AND U.Passwort = '" . md5($password) . "'";
		$result = $this->db->query($abfrage);
		if ($result->num_rows != 1) {
			return "Nick oder Passwort falsch.";
		}
		$userObj = $result->fetch_object();
		if ($userObj->Status != "Aktiv") {
			return "Sie konnten nicht angemeldet werden. Ihr Accountstatus ist " . $userObj->Status . ".";
		}

		//Erfolgreich eingeloggt
		session_regenerate_id(true);
		$_SESSION[CherrizSessionHandler::NAME_USER] = $userObj->ID;
		$_SESSION[CherrizSessionHandler::NAME_LOGGEDINSINCE] = date("d.m.Y H:i", time());
		if ($remember) {
			$_SESSION[CherrizSessionHandler::NAME_REMEMBER] = CherrizSessionHandler::REMEMBER_TRUE;
		} else {
			$_SESSION[CherrizSessionHandler::NAME_REMEMBER] = CherrizSessionHandler::REMEMBER_FALSE;
		}
		return true;
	}

	/**
	 * Meldet den aktiven Benutzer ab.
	 */
	private function processLogout() {
		session_destroy();
	}

	/**
	 * @return bool true wenn ein Benutzer authentifiziert ist, sonst false.
	 */
	public function isLoggedIn() {
		if (isset($_SESSION[CherrizSessionHandler::NAME_USER])) {
			return true;
		}
		return false;
	}

	/**
	 * @return string Die ID der aktuellen Sitzung. Leerstring wenn kein Benutzer angemeldet ist.
	 */
	public function getSessionID() {
		if ($this->isLoggedIn()) {
			return session_id();
		}
		return "";
	}

	/**
	 * Liefert den Loginscreen zurueck.
	 * @return string Der Loginscreen.
	 */
	public function getLoginScreen() {
		$paras = $_SERVER['QUERY_STRING'];
		if (strpos($paras, "page=Registrierung") !== false) {
			$paras = "";
		}
		$result = "<h2>Login</h2>
              <form action='" . $_SERVER['PHP_SELF'] . "?" . $paras . "' method='post'>
                  <input type='text' name='login_name' class='login_element'><br />
                  <input type='password' name='login_pass' class='login_element'><br />
                  <input type='submit' value='Login' class='login_element'> merken <input type='checkbox' name='login_remember' checked='true' value='true' class='login_element'><br />
                  <input type='hidden' name='login_submitted'  value='true'>
              </form>";
		if ($this->message != "") {
			$result .= "<span class='sidebar_box_content_info'>" . $this->message . "</message>";
		}
		$result .= "<ul>";
		$result .= "<li><a href='index.php?page=" . Menu::REGISTRIERUNG . "'>Registrieren</a></li>";
		$result .= "<li><a href='index.php?page=" . Menu::WIEDERHERSTELLUNG . "'>Passwort vergessen</a></li>";
		$result .= "</ul>";
		$this->message = "";
		return $result;
	}

	/**
	 * Liefert den den Profilblock zurueck.
	 * @return string der Profilblock.
	 */
	public function getUserOverview() {
		$user = $this->getCurrentUserObject();
		$result = "<img src='gfx/img_generator.php?type=User&size=" . UserKonstanten::AVATAR_MEDIUM . "&id=" . $user->get(User::PROPERTY_ID) . "' width='100px' height='100px' alt='Avatar' id='sidebar_box_avatar'/>";
		$result .= "<h2>" . $user->get(User::PROPERTY_Nick) . "</h2>";
		$name = $user->get(User::PROPERTY_Vorname);
		if (strlen($name) > 0 && $user->get(User::PROPERTY_Nachname) != "") {
			$name .= " ";
		}
		$result .= $name . $user->get(User::PROPERTY_Nachname);
		$result .= "<ul>";
		$result .= "<li><a href='index.php?page=" . Menu::PROFILE . "&user=" . $user->get(User::PROPERTY_ID) . "'>Zum Profil</a></li>";
		if ($user->hasRight(Recht::ACCESS_ADMIN)) {
			$result .= "<li><a href='index.php?page=" . Menu::ADMIN . "'>Administration</a></li>";
		}
		$result .= "<li><a href='index.php?logout=true'>Ausloggen</a></li>";
		$result .= "</ul>";
		return $result;
	}

	/**
	 * Liefert den aktiven benutzer zurück.
	 *
	 * @param bool $reload Gibt an ob der User neu geladen soll.
	 *
	 * @return User Der aktive Benutzer.
	 */
	public function getCurrentUserObject($reload = false) {
		if ($this->curUser == null || $reload) {
			$this->curUser = new User($_SESSION[CherrizSessionHandler::NAME_USER]);
		}
		return $this->curUser;
	}

}