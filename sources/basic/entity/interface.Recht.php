<?php

/**
 * Interface Rechte definiert alle existierenden Rechte, sowie die Schnittstelle zum auslesen und Aendern fuer Entitaeten.
 *
 * @author Frederik Kirsch
 */
interface Recht {

	/** Administrationsseite aufrufen. */
	const ACCESS_ADMIN = "Access Admin";

	/** Thread erstellen. */
	const OPEN_THREAD = "Open Thread";

	/** Neues Forum erstellen. */
	const FORUM_ADMIN = "Forum Admin";

	/** News schreiben. */
	const WRITE_NEWS = "Write News";

	/** Genres aendern. */
	const MANAGE_GENRE = "Manage Genre";

	/** Wartungsmodus fuer Webseite aktivieren / deaktivieren. */
	const CONSTRUCTION_MODE = "Construction Mode";

	/** Berechtigungen fuer Nutzer veraendern. */
	const CHANGE_RIGHTS = "Change Rights";

	/** Eigenschaften der Webseite aendern. */
	const CHANGE_PROPERTIES = "Change Properties";

	/** Impressum der Webseite aendern. */
	const CHANGE_IMPRESSUM = "Change Impressum";

	/** Datenschutzaerklaerung aendern. */
	const CHANGE_DATENSCHUTZ = "Change Datenschutz";

	/** Statistikdaten der Webseite betrachten. */
	const VIEW_STATISTICS = "View Statistics";

	/**
	 * Prueft ob die Entitaent das uebergebene Recht besitzt.
	 *
	 * @param string $recht Das zu ueberpruefende Recht.
	 *
	 * @return boolean <code>true</code> wenn die Entitaet das Recht besitzt, sonst <code>false</code>.
	 */
	public function hasRight($recht);

	/**
	 * Liefert alle Berechtigungen der Entitaet zurueck.
	 *
	 * @return string[] Die Liste aller Rechte.
	 */
	public function getRights();

	/**
	 * Fuegt der Entitaet ein Recht hinzu.
	 *
	 * @param string $recht Das zusaetzliche Recht.
	 *
	 * @return boolean <code>true</code> wenn das Recht erfolgreich vergeben wurde, sonst <code>false</code>.
	 */
	public function addRight($recht);

	/**
	 * Entzieht der Entitaet das uebergebene Recht.
	 *
	 * @param string $recht Das zu entziehende Recht.
	 *
	 * @return boolean <code>true</code> wenn das Recht erfolgreich entzogen wurde, sonst <code>false</code>.
	 */
	public function removeRight($recht);

}