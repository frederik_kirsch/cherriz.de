<?php

/**
 * Class Group
 * Repraesentiert die Entitaet einer Gruppe. Stellt Funktionen rund um diese zur Verfuegung.
 *
 * @author Frederik Kirsch
 */
class Group {

	const RANG_VERWALTER = "Verwalter";

	const RANG_VERTRETER = "Vertreter";

	const RANG_MITGLIED = "Mitglied";

	private $db = null;

	private $gruppe = null;

	public function __construct($id) {
		$this->db = DBConnect::getDBConnection();
		$result = $this->db->query("SELECT ID, Name, Beschreibung, Logo_Medium FROM Gruppe WHERE ID = $id");
		$this->gruppe = $result->fetch_object();
	}

	public function getID() {
		return $this->gruppe->ID;
	}

	public function getName() {
		return $this->gruppe->Name;
	}

	public function getBeschreibung() {
		return $this->gruppe->Beschreibung;
	}

	public function requestAccess($userID, $anfrage) {
		LibImporter::import("sites/gruppe/interface.MenuGruppe.php");
		$result = false;
		$user = new User($userID);

		if ($user->isActive() && $this->getID() != null) {
			// Anfrage eintragen
			$insert = "INSERT INTO Gruppe_AccessRequest
								   (GruppeID, UserID, Anfrage)
							VALUES (" . $this->getID() . ", $userID, '$anfrage')";

			// Wenn erfolgreich Mails versenden
			if ($this->db->query($insert) === true) {
				$result = true;
				$member = $this->getGroupMember($this->getID());
				$empfaenger = array($member->getVerwalter());
				if ($member->getVertreter() != null) {
					$empfaenger = array_merge($empfaenger, $member->getVertreter());
				}
				$mail = new Mail();

				foreach ($empfaenger as $empf) {
					$mailVars = array($empf->get(User::PROPERTY_Nick),
					                  $this->getName(),
					                  "http://www.cherriz.de/index.php?page=" . MenuGruppe::MEMBER. "&group=" . $this->getID(),
					                  $anfrage);
					$mail->sendMail(Message::createInstance(Message::BEITRITTSANFRAGEGRUPPE, $mailVars), $empf->get(User::PROPERTY_Email));
				}
			}
		}

		return $result;
	}

	public function acceptRequest($pUserID, $pAdminID) {
		$result = false;
		if ($this->addGroupMember($pUserID, $pAdminID)) {
			$delete = "DELETE FROM Gruppe_AccessRequest
			 		    WHERE UserID = $pUserID AND GruppeID = " . $this->getID();
			$this->db->query($delete);

			$newUser = new User($pUserID);
			$mail = new Mail();
			$mailVars = array($newUser->get(User::PROPERTY_Nick), $this->getName());
			$mail->sendMail(Message::createInstance(Message::BEITRITTSANFRAGEGRUPPEANGENOMMEN, $mailVars), $newUser->get(User::PROPERTY_Email));
			$result = true;
		}
		return $result;
	}

	public function declineRequest($pUserID, $pAdminID) {
		$result = false;
		if ($this->isStatuHigher($this->getUserStatus($pAdminID), Group::RANG_MITGLIED)) {
			$delete = "DELETE FROM Gruppe_AccessRequest
			 		   	WHERE UserID = $pUserID AND GruppeID = " . $this->getID();
			$this->db->query($delete);
			if ($this->db->affected_rows == 1) {
				$newUser = new User($pUserID);
				$mail = new Mail();
				$mailVars = array($newUser->get(User::PROPERTY_Nick), $this->getName());
				$mail->sendMail(Message::createInstance(Message::BEITRITTSANFRAGEGRUPPEABGELEHNT, $mailVars), $newUser->get(User::PROPERTY_Email));
				$result = true;
			}
		}
		return $result;
	}

	public function drawGroupMedium() {
		$user = Login::getInstance()->getCurrentUserObject();
		echo("<div class='profile box'>
				<a href='index.php?page=" . MenuGruppe::OVERVIEW. "&group=" . $this->gruppe->ID . "' target='_self' title='Zur Seite der Gruppe " . $this->gruppe->Name . "'>
				  <img src='gfx/img_generator.php?type=Gruppe&id=" . $this->gruppe->ID . "&size=Medium' />" . $this->gruppe->Name . "</a>");
		if ($this->getUserStatus($user->get(User::PROPERTY_ID)) == null && !$this->hasActiveAccessRequest($user->get(User::PROPERTY_ID))
		) {
			Toolkit::printGroupAccessRequest($this->gruppe->ID);
		}
		echo("</div>");
	}

	public function hasActiveAccessRequest($pUserID) {
		$query = "SELECT *
					FROM Gruppe_AccessRequest
				   WHERE UserID = $pUserID
					 AND GruppeID = " . $this->gruppe->ID;
		$dbResult = $this->db->query($query);
		if ($dbResult->num_rows == 1) {
			return true;
		}
		return false;
	}

	public function getActiveRequests() {
		$query = "SELECT *
				    FROM Gruppe_AccessRequest
				   WHERE GruppeID = " . $this->gruppe->ID;
		return $this->db->query($query);
	}

	public function drawGroupSmall($leading = null) {
		echo("<div class='selection_object'>");
		if ($leading != null) {
			echo($leading);
		}
		$url = "index.php?page=" . Menu::GRUPPE . "&group=" . $this->gruppe->ID;
		echo("<a href='$url' target='_self' title='Zur Seite der Gruppe " . $this->gruppe->Name . "'>
                  <img src='gfx/img_generator.php?type=Gruppe&id=" . $this->gruppe->ID . "&size=Small' /> " . $this->gruppe->Name . "
              </a>
         </div>");
	}

	public function getUserStatus($id) {
		$result = null;
		$query = "SELECT GR.Rang
					FROM Gruppe_Rang GR
			  INNER JOIN Gruppe_Mitglied GM
			  		  ON GR.ID = GM.Rang
			  	   WHERE GM.Gruppe = " . $this->gruppe->ID . "
				     AND GM.User = $id";
		$dbResult = $this->db->query($query);
		if ($dbResult != null) {
			$result = $dbResult->fetch_object()->Rang;
		}
		return $result;
	}

	/**
	 * @return GroupMembers Die Mitglieder der Gruppe.
	 */
	public function getGroupMember() {
		$query = "SELECT U.ID, GR.Rang
					FROM Users U
			  INNER JOIN User_Status US
					  ON U.Status = US.ID
			  INNER JOIN Gruppe_Mitglied GM
			  		  ON GM.User = U.ID
			  INNER JOIN Gruppe_Rang GR
			  		  ON GR.ID = GM.Rang
			  	   WHERE US.Status = 'Aktiv'
			  	     AND GM.Gruppe = " . $this->getID() . "
				ORDER BY GR.ID, U.Nick";
		$result = $this->db->query($query);

		$verwalter = null;
		$vertreter = array();
		$mitglieder = array();
		while ($obj = $result->fetch_object()) {
			switch ($obj->Rang) {
				case Group::RANG_VERWALTER:
					$verwalter = new User($obj->ID);
					break;
				case Group::RANG_VERTRETER:
					$vertreter[] = new User($obj->ID);
					break;
				case Group::RANG_MITGLIED:
					$mitglieder[] = new User($obj->ID);
					break;
			}
		}
		return new GroupMembers($verwalter, $vertreter, $mitglieder);
	}

	public function addGroupMember($uID, $adminUID) {
		$result = false;
		$userStatus = Group::RANG_MITGLIED;
		$adminStatus = $this->getUserStatus($adminUID);
		$query = "SELECT *
					FROM Gruppe_Mitglied
				   WHERE Gruppe = " . $this->getID() . "
				     AND User = $uID";
		$dbResult = $this->db->query($query);
		if ($dbResult->num_rows == 0 && $this->isStatuHigher($adminStatus, $userStatus)) {
			$insert = "INSERT INTO Gruppe_Mitglied (Gruppe, User, Rang, Date)
							VALUES (" . $this->getID() . ", $uID, (SELECT ID FROM Gruppe_Rang WHERE Rang = '" . Group::RANG_MITGLIED . "'), NOW())";
			$this->db->query($insert);
			if ($this->db->affected_rows == 1) {
				$result = true;
			}
		}
		return $result;
	}

	public function removeGroupMember($uID, $adminUID) {
		$result = false;
		$userStatus = $this->getUserStatus($uID);
		$adminStatus = $this->getUserStatus($adminUID);
		if ($this->isStatuHigher($adminStatus, $userStatus)) {
			$delete = "DELETE FROM Gruppe_Mitglied
						WHERE User = $uID
						  AND Gruppe = " . $this->getID();
			if ($this->db->query($delete) && $this->db->affected_rows == 1) {
				$result = true;
			}
		}
		return $result;
	}

	public function upgrade($uID, $adminUID) {
		$result = false;
		$userStatus = $this->getUserStatus($uID);
		$adminStatus = $this->getUserStatus($adminUID);
		if ($this->isStatuHigher($adminStatus, $userStatus)) {
			switch ($userStatus) {
				case Group::RANG_MITGLIED:
					$update = "UPDATE Gruppe_Mitglied GM
								  SET GM.Rang = (SELECT GR.ID
								  				   FROM Gruppe_Rang GR
								  				  WHERE GR.Rang = '" . Group::RANG_VERTRETER . "')
								WHERE GM.User = $uID
								  AND GM.Gruppe = " . $this->getID();
					$this->db->query($update);
					if ($this->db->affected_rows == 1) {
						$result = true;
					}
					break;
				case Group::RANG_VERTRETER:
					$update_downgrade = "UPDATE Gruppe_Mitglied GM
									            SET GM.Rang = (SELECT GR.ID
									  			     	         FROM Gruppe_Rang GR
									  				            WHERE GR.Rang = '" . Group::RANG_VERTRETER . "')
								    	      WHERE GM.User = $adminUID
									  		    AND GM.Gruppe = " . $this->getID();
					$update_upgrade = "UPDATE Gruppe_Mitglied GM
									          SET GM.Rang = (SELECT GR.ID
									  			     	       FROM Gruppe_Rang GR
									  				          WHERE GR.Rang = '" . Group::RANG_VERWALTER . "')
								    	    WHERE GM.User = $uID
									  		  AND GM.Gruppe = " . $this->getID();
					$update_rollback = "UPDATE Gruppe_Mitglied GM
									           SET GM.Rang = (SELECT GR.ID
									  			     	        FROM Gruppe_Rang GR
									  				           WHERE GR.Rang = '" . Group::RANG_VERWALTER . "')
								    	     WHERE GM.User = $adminUID
									  		   AND GM.Gruppe = " . $this->getID();
					if ($this->db->query($update_downgrade) && $this->db->affected_rows == 1) {
						if ($this->db->query($update_upgrade) && $this->db->affected_rows == 1) {
							$result = true;
						} else {
							$this->db->query($update_rollback);
						}
					}
					break;
			}
		}
		return $result;
	}

	public function downgrade($uID, $adminUID) {
		$result = false;
		$userStatus = $this->getUserStatus($uID);
		$adminStatus = $this->getUserStatus($adminUID);
		if ($userStatus == Group::RANG_VERTRETER && $this->isStatuHigher($adminStatus, $userStatus)) {
			$update = "UPDATE Gruppe_Mitglied GM
							  SET GM.Rang = (SELECT GR.ID
								  			   FROM Gruppe_Rang GR
								  			  WHERE GR.Rang = '" . Group::RANG_MITGLIED . "')
							WHERE GM.User = $uID
							  AND GM.Gruppe = " . $this->getID();
			if ($this->db->query($update) && $this->db->affected_rows) {
				$result = true;
			}
		}
		return $result;
	}

	private function isStatuHigher($pStatusA, $pStatusB) {
		$result = false;
		if ($pStatusA == Group::RANG_VERWALTER && ($pStatusB == Group::RANG_MITGLIED || $pStatusB == Group::RANG_VERTRETER)) {
			$result = true;
		}
		if ($pStatusA == Group::RANG_VERTRETER && $pStatusB == Group::RANG_MITGLIED) {
			$result = true;
		}
		return $result;
	}

}

/**
 * Class GroupMembers
 * POJO zur Kapselung der Mitglieder einer Gruppe.
 *
 * @author Frederik Kirsch
 */
class GroupMembers {

	private $verwalter = null;

	private $vertreter = null;

	private $member = null;

	/**
	 * @param User   $verwalter Der Verwalter der Gruppe.
	 * @param User[] $vertreter Die Vertreter der Gruppe.
	 * @param User[] $member    Die restlichen Mitglieder der Gruppe.
	 */
	public function __construct($verwalter, $vertreter, $member) {
		$this->verwalter = $verwalter;
		$this->vertreter = $vertreter;
		$this->member = $member;
	}

	/**
	 * @return User
	 */
	public function getVerwalter() {
		return $this->verwalter;
	}

	/**
	 * @return User[] Die Vertreter der Gruppe.
	 */
	public function getVertreter() {
		return $this->vertreter;
	}

	/**
	 * @return User[] Die restlichen Mitglieder der Gruppe.
	 */
	public function getMember() {
		return $this->member;
	}

}