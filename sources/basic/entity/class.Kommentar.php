<?php

/**
 * Class Kommentar
 * Repraesentiert einen Kommentar in einer News.
 *
 * @author Frederik Kirsch
 */
class Kommentar {

	/**
	 * ID des Kommentars.
	 */
	const ATTRIBUTE_ID   = "id";

	/**
	 * News zum Kommentar.
	 */
	const ATTRIBUTE_NEWS = "news";

	/**
	 * Verfasser
	 */
	const ATTRIBUTE_USER = "user";

	/**
	 * Inhalt
	 */
	const ATTRIBUTE_TEXT = "text";

	/**
	 * Erstellzeitpunkt
	 */
	const ATTRIBUTE_DATE = "date";

	private $id = null;

	private $news = null;

	private $user = null;

	private $text = null;

	private $date = null;

	public function __construct($id) {
		$db = DBConnect::getDBConnection();
		$query = "SELECT * FROM Kommentar WHERE ID = " . $id;
		$result = $db->query($query);
		$kommentar = $result->fetch_object();

		$this->id = $kommentar->ID;
		$this->news = $kommentar->News;
		$this->user = $kommentar->Author;
		$this->text = Converter::bbToHtml($kommentar->Text);
		$this->date = strtotime($kommentar->Created);
	}

	public function get($attribute) {
		return $this->$attribute;
	}

	/**
	 * @return string Stellt den Kommentar als HTML dar.
	 */
	public function __toString() {
		//Parameter aufbereiten
		$user = new User($this->get(Kommentar::ATTRIBUTE_USER));
		$date = date("d.m.Y H:i", $this->date);
		if (date("d.m.Y", $this->date) == date("d.m.Y")) {
			$date = date("H:i", $this->date);
		}

		$result = "<div class='post_avatar'>" . $user->drawUserSmall() . "</div>";
		$result .= "<div class='post_content'>";
		$result .= $this->get(Kommentar::ATTRIBUTE_TEXT);
		$result .= "<div class='post_footer'>$date Uhr - <span style='font-style: italic;'>\"" . $user->get(User::PROPERTY_Signatur) . "\"</span></div>";
		$result .= "</div>";
		return "<div class='post'>$result</div>";
	}

}