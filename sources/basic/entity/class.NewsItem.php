<?php

LibImporter::import("basic/entity/class.Genre.php");
LibImporter::import("sites/news/interface.MenuNews.php");

class NewsItem {

	const ATTRIBUTE_ID         = "id";
	const ATTRIBUTE_TITLE      = "title";
	const ATTRIBUTE_TEXT       = "text";
	const ATTRIBUTE_GENRES     = "genres";
	const ATTRIBUTE_CREATED    = "created";
	const ATTRIBUTE_RELEASED   = "released";
	const ATTRIBUTE_DATE       = "released";
	const ATTRIBUTE_UPDATED    = "updated";
	const ATTRIBUTE_KOMMENTARE = "kommentare";
	const ATTRIBUTE_PUBLIC     = "public";
	const ATTRIBUTE_GROUPS     = "groupIDs";
	const ATTRIBUTE_PUBLISHED  = "published";
	const ATTRIBUTE_AUTHOR     = "author";

	const ACCESS_TYPE_STANDARD = "standard";
	const ACCESS_TYPE_CONTROL  = "control";
	const ACCESS_TYPE_AUTHOR   = "author";

	private $db = null;

	//DB Variablen
	private $id = null;

	private $title = null;

	private $author = null;

	private $text = null;

	private $public = null;

	private $published = null;

	private $created = null;

	private $released = null;

	private $updated = null;

	/**
	 * @var Genre[]
	 */
	private $genres = null;

	private $groupIDs = null;

	private $kommentare = 0;

	// Sonstige
	private $delayed = null;

	/**
	 * Liest eine News von der Datenbank.
	 *
	 * @param int $id die ID der News.
	 *
	 * @return NewsItem Das erzeugte Objekt.
	 */
	public static function createNewsFromDB($id) {
		$db = DBConnect::getDBConnection();
		$result = $db->query("SELECT * FROM News WHERE ID = $id");
		$newsObj = $result->fetch_object();

		//Genres aufbereiten
		$result = $db->query("SELECT Genre FROM News_Genre WHERE News = $id");
		$genreIDs = array();
		if ($result->num_rows > 0) {
			while ($genre = $result->fetch_object()) {
				array_push($genreIDs, $genre->Genre);
			}
		}

		// Zugreifbare Gruppen aufbereiten
		$result = $db->query("SELECT * FROM News_Allowed_Groups WHERE News = $newsObj->ID");
		$groupIDs = array();
		if ($result->num_rows > 0) {
			while ($group = $result->fetch_object()) {
				array_push($groupIDs, $group->Group);
			}
		}

		$public = false;
		if ($newsObj->Public == "1") {
			$public = true;
		}
		$published = false;
		if ($newsObj->Published == "1") {
			$published = true;
		}

		$created = null;
		if ($newsObj->Created != null) {
			$created = strtotime($newsObj->Created);
		}
		$released = null;
		if ($newsObj->Released != null) {
			$released = strtotime($newsObj->Released);
		}
		$updated = null;
		if ($newsObj->Updated != null) {
			$updated = strtotime($newsObj->Updated);
		}

		return new NewsItem($newsObj->ID, $newsObj->Title, $newsObj->Author, $newsObj->Text, $public, $published, $created, $released, $updated,
		                    $genreIDs, $groupIDs);
	}

	public static function createNews($title, $authorID, $text, $published, $genreIDs, $groupIDs) {
		// Zugreifbare Gruppen aufbereiten
		$public = true;
		if (count($groupIDs) > 0) {
			$public = false;
		}

		return new NewsItem(null, $title, $authorID, $text, $public, $published, null, null, null, $genreIDs, $groupIDs);
	}

	/**
	 * @param int     $id        ID in Database
	 * @param string  $title     Newstitle
	 * @param int     $authorID  Die UserID des Authors
	 * @param string  $text      Newstext
	 * @param boolean $public    Definiert ob die News öffneltich ist oder nur für bestimmte Gruppen
	 * @param boolean $published Definiert ob die News für die Veröffentlichung freigegeben ist, oder sich noch im Entwurfsstatus befindet
	 * @param int     $created   Datum an dem die News geschrieben wurde
	 * @param int     $released  Datum an dem die News öffentlich zugänglich wurde
	 * @param int     $updated   Datum an dem die News aktualisiert wurde, oder null
	 * @param int[]   $genreIDs  Ein Array mit den IDs der Genres der News
	 * @param int[]   $groupIDs  Array der IDs der Gruppen die diese News lesen dürfen, oder null falls öffentlich
	 */
	private function __construct($id, $title, $authorID, $text, $public, $published, $created, $released, $updated, $genreIDs, $groupIDs) {
		$this->db = DBConnect::getDBConnection();

		$this->id = $id;
		$this->setTitle($title);
		$this->author = new User($authorID);
		$this->setText($text);
		$this->setAccess($public, $groupIDs);
		if ($published === true) {
			$this->setPublished();
		} else {
			$this->setEntwurf();
		}
		$this->created = $created;
		$this->released = $released;
		$this->updated = $updated;
		$this->setGenres($genreIDs);
	}

	/**
	 * Prüft ob der angemeldete Benutzer die News sehen darf. Es werden zwei Abfragetypen unterstützt.
	 *
	 * @param string $type
	 *
	 * @return boolean
	 */
	public function checkAccess($type = NewsItem::ACCESS_TYPE_STANDARD) {
		if ($this->published === true && $this->public === true) {
			return true;
		}
		$user = Login::getInstance()->getCurrentUserObject();
		if ($this->published === true && $user != null) {
			$groups = $user->getGroups();
			foreach ($groups as $group) {
				if (in_array($group->getID(), $this->groupIDs)) {
					return true;
				}
			}
		}
		if ($this->published === true && $type == NewsItem::ACCESS_TYPE_AUTHOR && $this->author->get(User::PROPERTY_ID) == $user->get(User::PROPERTY_ID)) {
			return true;
		}
		if ($type == NewsItem::ACCESS_TYPE_CONTROL && $this->author->get(User::PROPERTY_ID) == $user->get(User::PROPERTY_ID)) {
			return true;
		}
		return false;
	}

	/**
	 * @return bool Gibt an ob die News bereits released ist.
	 */
	public function checkReleased() {
		if ($this->published !== true) {
			return false;
		} else if ($this->released - time() > 0) {
			return false;
		}
		return true;
	}

	/**
	 * Stellt die News komplett dar.
	 *
	 * @param string $type Definiert die Art des Zugriffs. Abhaengig davon wird entschieden ob die News dargestellt werden darf.
	 */
	public function showBig($type = NewsItem::ACCESS_TYPE_STANDARD) {
		if ($this->checkAccess($type)) {
			echo("<div class='feed_item'>");
			//Header
			echo("<div class='feed_item_header'>" . $this->title . "</div>");
			//Subheader
			echo("<div class='feed_item_subheader'>News vom " . $this->newsDateAsString() . " - von <a href='index.php?page=" . Menu::PROFILE . "&User=" . $this->author->get(User::PROPERTY_ID) . "' target='_self' title='Zur Profilseite von " . $this->author->get(User::PROPERTY_Nick) . "'>" . $this->author->get(User::PROPERTY_Nick) . "</a> " . $this->getGenreString() . "</div>");
			//Content
			echo("<div class='feed_item_content'>" . Converter::bbToHtml($this->text) . "</div>");
			echo("</div>");
			echo("<script type='text/javascript'>$('.feed_item').find(\"pre code\").each(function(i, block) {hljs.highlightBlock(block)});</script>");
		}
	}

	/**
	 * Stellt die News als kurzen Ausschnitt dar.
	 *
	 * @param string $type Definiert die Art des Zugriffs. Abhaengig davon wird entschieden ob die News dargestellt werden darf.
	 */
	public function showSmall($type = NewsItem::ACCESS_TYPE_STANDARD) {
		if (!$this->checkAccess($type)) {
			return;
		}
		$genStr = $this->getGenreString();
		$link = "index.php?page=" . MenuNews::SINGLE . "&news=$this->id";

		$settings = Settings::getInstance();
		$text = Converter::cutBB($this->text, $settings->getProperty(Settings::PROPERTY_NEWS_SPLIT_SIZE));

		echo("<div class='feed_item'>");
		//Header
		echo("<h2 class='feed_item_header'>");
		echo("<a href='$link'>$this->title</a>");
		echo("</h2>");
		//Subheader
		echo("<div class='feed_item_subheader'>");
		echo("News vom " . $this->newsDateAsString() . " - von <a href='index.php?page=" . Menu::PROFILE . "&User=" . $this->author->get(User::PROPERTY_ID) . "' target='_self' title='Zur Profilseite von " . $this->author->get(User::PROPERTY_Nick) . "'>" . $this->author->get(User::PROPERTY_Nick) . "</a> $genStr");
		echo("</div>");
		//Content
		echo("<div class='feed_item_content'>");
		echo(Converter::bbToHtml($text));
		echo("</div>");
		//Footer
		echo("<div class='feed_item_footer'>
				  <a href='$link'>Weiterlesen</a> >> Möchtest du einen <a href='$link'>Kommentar</a> (" . $this->get(NewsItem::ATTRIBUTE_KOMMENTARE) . ") schreiben?
			  </div>");
		echo("</div>");
	}

	/**
	 * Zeigt die News als Verwaltungsobjekt dar.
	 */
	public function showControl() {
		if (!$this->checkAccess(NewsItem::ACCESS_TYPE_CONTROL)) {
			return;
		}
		LibImporter::import("basic/entity/class.Group.php");
		//Daten aufbereiten
		$link = "index.php?page=" . MenuNews::WRITE . "&news=$this->id";
		$updated = "";
		if ($this->updated != null) {
			$updated = date("d.m.Y", $this->updated);
		}
		$access = "Public";
		if ($this->public === false) {
			$grp_array = array();
			foreach ($this->groupIDs as $id) {
				$grp = new Group($id);
				$grp_array[] = $grp->getName();
			}
			$access = implode(", ", $grp_array);
		}
		$published = "Entwurf";
		if ($this->published === true) {
			$published = "Veröffentlicht";
		}

		$data = array();
		$data["Erstellt"] = date("d.m.Y H:i", $this->created);
		$released = "nein";
		if ($this->released != null) {
			$released = date("d.m.Y H:i", $this->released);
		}
		$data["Released"] = $released;
		if ($updated == "") {
			$updated = "nein";
		}
		$data["Updated"] = $updated;
		$data["Kommentare"] = $this->get(NewsItem::ATTRIBUTE_KOMMENTARE);
		$data["Genres"] = implode(", ", $this->genres);
		$data["Status"] = $published;
		$data["Access"] = $access;

		echo("<div class='news_control_item box'>");
		//Header
		echo("<h3><a title='News bearbeiten' href='$link'>#$this->id $this->title</a></h3>");
		//Infos
		foreach ($data as $key => $value) {
			echo("<div class='news_control_item_info'>
			        <span class='news_control_item_title'>$key:</span> $value
			      </div>");
		}
		echo("</div>");
	}

	/**
	 * @return string Die Genres, kommaseperiert und als Link.
	 */
	private function getGenreString() {
		$genStr = "in";
		foreach ($this->genres as $gen) {
			$genStr .= " <a class='icon icon_sidebar icon$gen' href='index.php?page=" . Menu::NEWS . "&genre=$gen' title='Alle News des Genre $gen'>$gen</a>, ";
		}
		return substr($genStr, 0, strlen($genStr) - 2);
	}

	/**
	 * @param string $title Der Titel.
	 */
	public function setTitle($title) {
		if ($title != "") {
			$this->title = $title;
		}
	}

	/**
	 * @param string $text Der Textinhalt.
	 */
	public function setText($text) {
		if ($text != "") {
			$this->text = $text;
		}
	}

	/**
	 * Markiert die News als im Entwurf befindlich.
	 */
	public function setEntwurf() {
		$this->published = false;
	}

	/**
	 * Markiert die News als veroeffentlicht.
	 */
	public function setPublished() {
		$this->published = true;
	}

	/**
	 * @param int[] $genreIDs Die Genres der News.
	 */
	public function setGenres($genreIDs) {
		//Genres aufbereiten
		$this->genres = array();
		$result = $this->db->query("SELECT * FROM Genre WHERE ID IN (" . implode(",", $genreIDs) . ")");
		if ($result->num_rows > 0) {
			while ($genre = $result->fetch_object()) {
				array_push($this->genres, new Genre($genre->ID, $genre->Genre));
			}
		}
	}

	/**
	 * @param string $public   Gibt an ob die News oeffentlich ist.
	 * @param int[]  $groupIDs Die IDs der Gruppen die diese News sehen duerfen.
	 */
	public function setAccess($public, $groupIDs) {
		if ($public === true) {
			$this->public = true;
			$this->groupIDs = array();
		} else {
			$this->public = false;
			$this->groupIDs = $groupIDs;
		}
	}

	/**
	 * @return bool Ob die News in sich konsistent ist und gespeichert werden darf.
	 */
	public function isValide() {
		if (strlen($this->title) == 0) {
			return false;
		} else if (strlen($this->text) == 0) {
			return false;
		} else if (count($this->genres) == 0) {
			return false;
		} else if (!$this->public && count($this->groupIDs) == 0) {
			return false;
		} else if ($this->created != null && preg_match('/\d\d.\d\d.\d\d\d\d/', $this->created) != 1) {
			return false;
		} else if ($this->released != null && preg_match('/\d\d.\d\d.\d\d\d\d/', $this->released) != 1) {
			return false;
		} else if ($this->updated != null && preg_match('/\d\d.\d\d.\d\d\d\d/', $this->updated) != 1) {
			return false;
		}
		return true;
	}

	/**
	 * Liefert ein beliebiges Attribut der News zurueck.
	 *
	 * @param string $attribute Das Attribut.
	 *
	 * @return string Der Wert.
	 */
	public function get($attribute) {
		if ($attribute == NewsItem::ATTRIBUTE_KOMMENTARE) {
			$this->lacygetCommentCount();
		}
		return $this->$attribute;
	}

	/**
	 * @return string Bereitet das Veroeffentlichungsdatum der News zur Anzeige als Text vor.
	 */
	private function newsDateAsString() {
		$string = date("d.m.Y", $this->released);
		if ($this->updated != null) {
			$string .= " (aktualisiert am " . date("d.m.Y", $this->updated) . ")";
		}
		return $string;
	}

	/**
	 * Laedt die Kommentaranzahl lacy.
	 */
	private function lacygetCommentCount() {
		if ($this->kommentare == null) {
			// Anzahl Kommentare ermitteln
			$result = $this->db->query("SELECT COUNT(*) AS Anzahl FROM Kommentar WHERE News = $this->id");
			$erg = $result->fetch_object();
			$this->kommentare = $erg->Anzahl;
		}
	}

	/**
	 * Speichert die News in der DB oder aktuelisiert sie.
	 */
	public function save() {
		//Datumer berechnen
		$this->saveDates();

		//Bool Flags fuer DB aufebreiten
		$numPublic = 0;
		if ($this->public === true) {
			$numPublic = 1;
		}
		$numPublished = 0;
		if ($this->published === true) {
			$numPublished = 1;
		}

		//Datuemmer fuer DB aufbereiten
		$created = "null";
		if ($this->created != null) {
			$created = "'" . date("Y.m.d H:i:s", $this->created) . "'";
		}
		$released = "null";
		if ($this->released != null) {
			$released = "'" . date("Y.m.d H:i:s", $this->released) . "'";
		}
		$updated = "null";
		if ($this->updated != null) {
			$updated = "'" . date("Y.m.d H:i:s", $this->updated) . "'";
		}

		//News ist neu und wurde noch nicht gespeichert.
		if ($this->id == null) {
			$insert = "INSERT INTO News
				       		  (Title, Author, Text, Public, Published, Created, Released)
					   VALUES ('$this->title', " . $this->author->get(User::PROPERTY_ID) . ", '$this->text', $numPublic, $numPublished, $created, $released)";
			if ($this->db->query($insert)) {
				//ID der News setzen
				$this->id = $this->db->insert_id;
				// Gruppen speichern
				$this->saveGroups();
				// Genre speichern
				$this->saveGenres();
				// News Notification
				if ($this->get(NewsItem::ATTRIBUTE_PUBLISHED) == 1) {
					$this->createScheduleTask();
				}
			}
		} else { //News existiert bereits und muss aktualisiert werden
			//Newseintrag aktualisieren
			$update = "UPDATE News SET 	Title = '$this->title', Text = '$this->text', Public = $numPublic, Published = $numPublished, Released = $released, Updated = $updated WHERE ID = $this->id";
			//Wenn erfolgreich
			if ($this->db->query($update)) {
				//Gruppen aktualisieren
				$this->db->query("DELETE FROM News_Allowed_Groups WHERE News = $this->id");
				$this->saveGroups();
				//Genres aktualisieren
				$this->db->query("DELETE FROM News_Genre WHERE News = $this->id");
				$this->saveGenres();
				// News Notification
				if ($this->get(NewsItem::ATTRIBUTE_PUBLISHED) == 1) {
					$this->createScheduleTask();
				}
			}
		}
	}

	/**
	 * Speichert die Gruppen einer News.
	 */
	private function saveGroups() {
		if ($this->public === false) {
			foreach ($this->groupIDs as $grpID) {
				$this->db->query("INSERT INTO News_Allowed_Groups (News, `Group`) VALUES ($this->id, $grpID)");
			}
		}
	}

	/**
	 * Speichert die Genres einer News
	 */
	private function saveGenres() {
		foreach ($this->genres as $genre) {
			$this->db->query("INSERT INTO News_Genre (News, Genre) VALUES ($this->id," . $genre->getID() . ")");
		}
	}

	/**
	 * Berechnet die Datuemmer: Erstellung, Veroeffentlichung und Update.
	 * <b>ACHTUNG:</b> Muss vor dem Speichern der News aufgerufen werden.
	 */
	private function saveDates() {
		$now = time();
		// Beim ersten Speichern Created setzen
		if ($this->id == null) {
			$this->created = $now;
		}

		//Anderen Daten muessen nur berechnet werden, wenn die News auch veroeffentlicht wird
		if ($this->published) {
			// News bis jetzt noch nie veroeffentlicht?
			if ($this->released == null) {
				// Soll News verzoegert veroeffentlicht werden?
				if ($this->delayed != null) {
					$this->released = $this->delayed;
				} else {
					// Sonst direkt veroeffentlichen
					$this->released = $now;
				}
			} else {
				// News war schonmal oeffentlich
				// Soll News verzoegert veroeffentlicht werden?
				if ($this->delayed != null) {
					$this->released = $this->delayed;
					// Verzögerung muss in der Zukunft liegen daher muss updated auf jeden Fall null sein
					$this->updated = null;
				} else {
					//News wird nicht nochmal verzoegert und war schon released
					if ($this->released < $now) {
						// also updated setzen
						$this->updated = $now;
					} else {
						$this->released = $now;
					}
				}
			}
		} else if ($this->delayed != null && $this->delayed > $now) {
			$this->released = $this->delayed;
		} else if ($this->delayed == null) {
			$this->released = $now;
		}
	}

	/**
	 * @param int $date Vermerkt bei der News das diese spaeter veroeffentlicht werden soll.
	 */
	public function setDelayed($date) {
		$this->delayed = $date;
	}

	/**
	 * Legt einen Job an.
	 */
	private function createScheduleTask() {
		LibImporter::import("cron/class.JobScheduler.php");
		$scheduler = JobScheduler::getInstance();
		$job = NewsJob::createInitial($this->id);
		$scheduler->addJob($job);
	}

}