<?php

LibImporter::import("basic/entity/basic/class.PersistResult.php");
LibImporter::import("basic/entity/class.ThreadItem.php");
LibImporter::import("basic/entity/class.ThreadPost.php");
LibImporter::import("service/internal/class.Query.php");
LibImporter::import("service/class.ThreadQuery.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("sites/forum/interface.MenuForum.php");

/**
 * Class ForumItem
 * Die Klasse repraesentiert ein Forum.
 *
 * @author Frederik Kirsch
 */
class ForumItem {

	//ATTRIBUTE
	private $id = null;

	private $creator = null;

	private $created = null;

	private $name = null;

	private $description = null;

	private $deleted = null;

	//Variablen
	private $db = null;

	private $user = null;

	/**
	 * Laedt das Forum aus der Datenbank.
	 *
	 * @param int $id ID des Forums.
	 *
	 * @return ForumItem Das geladene Forum.
	 */
	public static function createFromDB($id) {
		return new ForumItem($id);
	}

	/**
	 * Erzeugt ein leeres Objekt.
	 *
	 * @return ForumItem Das leere Forum.
	 */
	public static function createEmpty() {
		return new ForumItem(null, "", "", "", "", 0);
	}

	/**
	 * Erzeugt auf Basis der uebergebenen Daten ein neues Forum.
	 *
	 * @param string $name        Name des Forums.
	 * @param string $description Kurzbeschreibung des Forums.
	 *
	 * @return ForumItem Das erzeugte Forum.
	 */
	public static function create($name, $description) {
		return new ForumItem(null, Login::getInstance()->getCurrentUserObject()->get(User::PROPERTY_ID), date("Y.m.d H:i:s"), $name, $description, 0);
	}

	/**
	 * Erzeugt ein vollstaendiges Objekt.
	 *
	 * @param int    $id ID des Forums
	 * @param int    $creator
	 * @param string $created
	 * @param string $name
	 * @param string $description
	 * @param int    $deleted
	 */
	private function __construct($id, $creator = null, $created = null, $name = null, $description = null, $deleted = null) {
		//Benötigte Variablen instanziieren
		$this->db = DBConnect::getDBConnection();
		$this->user = Login::getInstance()->getCurrentUserObject();

		//Variablen aufbereiten
		if ($id != null) {
			//Existierendes Forumobjekt
			$query = "SELECT F.ID, F.Creator, F.Created, F.Name, F.Description, F.Deleted FROM Forum F WHERE ID = " . $id;
			$forumObj = $this->db->query($query)->fetch_object();

			$id = $forumObj->ID;
			$creator = $forumObj->Creator;
			$created = $forumObj->Created;
			$name = $forumObj->Name;
			$description = $forumObj->Description;
			$deleted = $forumObj->Deleted;
		}

		//Objektvariablen setzen
		$this->id = $id;
		$this->creator = new User($creator);
		$this->created = $created;
		$this->name = $name;
		$this->description = $description;
		$this->deleted = $deleted;
	}

	/**
	 * Stellt das Objekt grafisch dar.
	 */
	public function showSmall() {
		$link = "index.php?page=" . MenuThread::OVERVIEW . "&forum=" . $this->id;
		$control = "";
		if ($this->user->hasRight(Recht::FORUM_ADMIN)) {
			$edit = "index.php?page=" . MenuForum::CREATE. "&forum=" . $this->id;
			$control = "<span class='forum_control'>
							<a onclick='new ConfirmationDialog(\"Bestätigung\", \"Sind Sie sicher das Sie das Forum löschen wollen?\", function(){deleteForum(\"".$this->id."\")}).open()'>[löschen]</a>
							<a href='$edit'>[bearbeiten]</a>
						</span>";
		}

		echo("<div id='forum_" . $this->id . "' class='forum box'>");
		echo("<div class='forum_header'><a href='$link'>" . $this->name . "</a>$control</div>");
		echo("<div class='forum_content'>\"" . $this->description . "\"</div>");
		echo("<div>" . $this->getForumDesc() . "</div>");
		echo("</div>");
	}

	/**
	 * @return string Detailinformationen zum Forum.
	 */
	private function getForumDesc() {
		//Quera aufbauen
		$query = new ThreadQuery($this->id);
		$query->setDeleted(false);
		$uID = -1;
		if (Login::getInstance()->isLoggedIn()) {
			$query->setUser($this->user);
			$this->user->get(User::PROPERTY_ID);
		} else {
			$query->setPublic(true);
		}
		//Daten abrufen
		$service = new Service();
		$threadCount = $service->countItems($query);

		$desc = $threadCount . " Threads";
		if ($threadCount > 0) {
			$usr_grps = $this->user->getGroups();
			$all_grps = array(-1);
			foreach ($usr_grps as $grp) {
				$all_grps[] = $grp->getID();
			}

			$query = "SELECT T.ID AS Thread, TP.Created, TP.Author
				  FROM Thread T INNER JOIN ThreadPost TP
				  ON TP.Thread = T.ID
				  WHERE T.Forum = $this->id
				  AND T.Deleted = 0
				  AND (T.Public = 1
				  	OR T.Creator = $uID
				  	OR (SELECT COUNT(*) FROM Thread_Allowed_Groups A
                          WHERE A.Thread = T.ID
                          AND A.Group IN (" . implode(",", $all_grps) . ")) > 0)
				  ORDER BY TP.ID DESC LIMIT 1";
			$dbResult = $this->db->query($query);
			$resultObj = $dbResult->fetch_object();
			$date = date("d.m.Y", strtotime($resultObj->Created));
			if ($date == date("d.m.Y")) {
				$date = date("H:i", strtotime($resultObj->Created)) . " Uhr";
			}
			$userObj = new User($resultObj->Author);
			$threadObj = ThreadItem::createFromDB($resultObj->Thread);
			$desc .= ", letzte Aktion \"" . $threadObj->asText(true) . "\" ($date) von " . $userObj->drawUserText();
		}
		return $desc;
	}

	/**
	 * Persistiert den aktuellen Zustand des Objekts in der Datenbank.
	 *
	 * @return PersistResult Ergebnis des Speichervorgangs.
	 */
	public function save() {
		//Speichern...
		if ($this->id == null) {
			//Prüfen ob Forum bereits existiert
			$query = "SELECT * FROM Forum WHERE Name = '" . $this->name . "'";
			$dbResult = $this->db->query($query);
			if ($dbResult->num_rows == 1) {
				return new PersistResult(PersistResult::FAILURE, "Ein Forum mit diesem Namen existiert bereits.");
			}

			$creatorID = $this->creator->get(User::PROPERTY_ID);
			$insert = "INSERT INTO Forum (Creator, Created, Name, Description, Deleted) VALUES ($creatorID, '$this->created', '$this->name', '$this->description', $this->deleted)";
			if ($this->db->query($insert)) {
				//ID der News setzen
				$this->id = $this->db->insert_id;
			}
		} else {
			//... oder nur aktualisieren
			$update = "UPDATE Forum SET Name = '$this->name', Description = '$this->description', Deleted = $this->deleted WHERE ID = $this->id";
			$this->db->query($update);
		}

		//Alles gut wenn bis hier hin kein Fehler aufgetreten ist
		return new PersistResult(PersistResult::SUCCESS);
	}

	/**
	 * @return int|null ID des Forums.
	 */
	public function getID() {
		return $this->id;
	}

	/**
	 * @return null|string Name des Forums.
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return null|string Beschreibung des Forums.
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Markiert das forum als geloescht.
	 */
	public function setDeleted() {
		$this->deleted = 1;
	}

	/**
	 * @param string $desc Setzt die Beschreibung am Objekt.
	 */
	public function setDescription($desc) {
		$this->description = $desc;
	}

	/**
	 * @param string $name Setzt den Namen am Objekt.
	 */
	public function setName($name) {
		$this->name = $name;
	}

}