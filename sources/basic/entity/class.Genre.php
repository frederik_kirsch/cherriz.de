<?php

class Genre {

	private $id = null;

	private $genre = null;

	public function __construct($id, $genre) {
		$this -> id = $id;
		$this -> genre = $genre;
	}
	
	public function getID(){
		return $this -> id;
	}

	public function __toString() {
		return (string) $this -> genre;
	}

}
?>