<?php

LibImporter::import("sites/interface.Access.php");
LibImporter::import("sites/forum/interface.MenuThread.php");
LibImporter::import("basic/entity/basic/class.PersistResult.php");

/**
 * Class ThreadItem
 * Die Klasse repraesentiert ein Thread.
 *
 * @author Frederik Kirsch
 */
class ThreadItem implements Access {

	//ATTRIBUTE
	private $id = null;

	private $forum = null;

	private $creator = null;

	private $created = null;

	private $title = null;

	private $post = null;

	private $relevance = null;

	private $public = null;

	private $groups = null;

	private $deleted = null;

	//Nicht persistierte Attribute
	private $postCount = 0;

	//Variablen
	private $settings = null;

	private $user = null;

	private $db = null;

	/**
	 * Laedt den Thread aus der Datenbank.
	 *
	 * @param int $id ID des Thread.
	 *
	 * @return ThreadItem Der geladene Thread.
	 */
	public static function createFromDB($id) {
		return new ThreadItem($id);
	}

	/**
	 * Erzeugt ein leeres Objekt.
	 *
	 * @return ThreadItem Der leere Thread.
	 */
	public static function createEmpty() {
		return new ThreadItem(null, "", null, "", null, array());
	}

	/**
	 * Erzeugt auf Basis der uebergebenen Daten ein neues Forum.
	 *
	 * @param int    $forum  ID des zugehörigen Forums.
	 * @param string $title  Name des Threads.
	 * @param string $text   Inhalt des ersten Posts.
	 * @param array  $groups Die Berechtigten Gruppen oder null wenn öffentlich.
	 *
	 * @return ThreadItem Das erzeugte Forum.
	 */
	public static function create($forum, $title, $text, $groups) {
		return new ThreadItem(null, $forum, Login::getInstance()->getCurrentUserObject(), $title, $text, $groups);
	}

	/**
	 * Erzeugt ein vollstaendiges Objekt.
	 *
	 * @param int    $id      ID des Threads.
	 * @param int    $forum   ID des darüberliegenden Forums.
	 * @param User   $creator Ersteller.
	 * @param string $title   Titel des Threads.
	 * @param string $text    Text des führenden Threadposts.
	 * @param array  $groups  Die Berechtigten Gruppen oder null wenn öffentlich.
	 */
	private function __construct($id, $forum = null, $creator = null, $title = null, $text = null, $groups = null) {
		//Benötigte Variablen instanziieren
		$this->settings = Settings::getInstance();
		$this->user = Login::getInstance()->getCurrentUserObject();
		$this->db = DBConnect::getDBConnection();
		$post = null;
		$relevance = null;
		$deleted = false;
		$created = null;
		$public = true;
		//Variablen aufbereiten
		if ($id != null) {
			//Existierendes Forumobjekt
			$result = $this->db->query("SELECT T.Forum, T.Creator, T.Created, T.Title, T.Relevance, T.Public, T.Deleted, (SELECT COUNT(*) FROM ThreadPost Where Thread = T.ID) AS PostCount FROM Thread T WHERE ID = $id");
			$thread = $result->fetch_object();
			$forum = $thread->Forum;
			$creator = new User($thread->Creator);
			$created = $thread->Created;
			$title = $thread->Title;
			$relevance = $thread->Relevance;
			$this->postCount = $thread->PostCount;
			$public = false;
			if ($thread->Public == 1) {
				$public = true;
			}
			$deleted = false;
			if ($thread->Deleted == 1) {
				$deleted = true;
			}

			$result = $this->db->query("SELECT ID FROM ThreadPost WHERE Thread = $id ORDER BY ID LIMIT 1");
			$resultObj = $result->fetch_object();
			$post = $resultObj->ID;

			// Zugreifbare Gruppen aufbereiten
			$groups = array();
			if ($public === false) {
				$result = $this->db->query("SELECT `Group` FROM Thread_Allowed_Groups WHERE Thread = $id");
				while ($obj = $result->fetch_object()) {
					array_push($groups, $obj->Group);
				}
			}

			$post = ThreadPost::createFromDB($post);
		}

		//Objektvariablen setzen
		$this->id = $id;
		$this->forum = $forum;
		$this->creator = $creator;
		$this->title = $title;
		$this->deleted = $deleted;
		if ($post == null) {
			if ($text == null) {
				$post = ThreadPost::createEmpty();
			} else {
				$post = ThreadPost::create($creator->get(User::PROPERTY_ID), $text);
			}
		}
		$this->post = $post;
		if ($created == null) {
			$created = date("Y.m.d H:i:s");
		}
		$this->created = $created;
		$this->groups = $groups;
		$this->public = $public;
		if ($this->groups != null) {
			$this->public = false;
		}
		//Relevanz muss immer als letztes Gesetzt werden. Andere Werte zur Berechnung benötigt.
		if ($relevance == null) {
			$this->calculateRelevance();
		} else {
			$this->relevance = $relevance;
		}
	}

	public function getID() {
		return $this->id;
	}

	public function getTitle() {
		return $this->title;
	}

	public function isDeleted() {
		return $this->deleted;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function setPost($post) {
		$this->post->setText($post);
	}

	public function setGroups($grpArray) {
		if ($grpArray == null || count($grpArray) == 0) {
			$this->public = true;
			$this->groups = array();
		} else {
			$this->public = false;
			$this->groups = $grpArray;
		}
	}

	public function getForum() {
		return $this->forum;
	}

	public function getCreator() {
		return $this->creator;
	}

	public function getInitialPost() {
		return $this->post;
	}

	/**
	 * Stellt das Objekt grafisch dar.
	 */
	public function showSmall() {
		$control = "";
		if ($this->user->get(User::PROPERTY_ID) == $this->creator->get(User::PROPERTY_ID)) {
			$control = "<span class='forum_control'>
							<a onclick='new ConfirmationDialog(\"Bestätigung\", \"Sind Sie sicher das Sie diesen Thread löschen wollen?\", function(){deleteThread(\"" . $this->id . "\")}).open()'>[löschen]</a>
							<a href='index.php?page=" . MenuThread::CREATE . "&forum=$this->forum&thread=$this->id'>[bearbeiten]</a>
						</span>";
		}

		echo("<div id='thread_" . $this->id . "' class='forum box'>");
		echo("<div class='forum_header'>" . $this->asText() . $control . "</div>");
		$text = Converter::cutBB($this->post->getText(), $this->settings->getProperty(Settings::PROPERTY_FORUM_SPLIT_SIZE));
		echo("<div class='forum_content'>\"" . Converter::bbToHtml($text) . "\"</div>");
		echo("<div>" . $this->getThreadDesc() . "</div>");
		echo("</div>");
	}

	/**
	 * @return string Detailinformationen zum Thread.
	 */
	private function getThreadDesc() {
		$query = "SELECT Created, Author FROM ThreadPost WHERE Thread = $this->id ORDER BY ID DESC LIMIT 1";
		$dbResult = $this->db->query($query);
		$resultObj = $dbResult->fetch_object();
		$date = date("d.m.Y", strtotime($resultObj->Created));
		if ($date == date("d.m.Y")) {
			$date = date("H:i", strtotime($resultObj->Created)) . " Uhr";
		}
		$userObj = new User($resultObj->Author);
		return $this->postCount . " Posts, letzte Aktion ($date) von " . $userObj->drawUserText();
	}

	/**
	 * @return string Darstellung des Forums als Text.
	 *
	 * @param bool $withTooltip Ob ein Tooltip angezeigt werden soll oder nicht.
	 */
	public function asText($withTooltip = false) {
		$tooltip = "";
		if ($withTooltip) {
			$tooltip = "Zum Thread $this->title";
		}

		$link = "index.php?page=" . MenuThread::SINGLE . "&forum=$this->forum&thread=$this->id";
		return "<a title='$tooltip' href='$link'>$this->title</a>";
	}

	/**
	 * Persistiert den aktuellen Zustand des Objekts in der Datenbank.
	 *
	 * @return PersistResult Ergebnis des Speichervorgangs.
	 */
	public function save() {
		$deleted = 0;
		if ($this->deleted === true) {
			$deleted = 1;
		}
		$public = 0;
		if ($this->public === true) {
			$public = 1;
		}

		//Speichern...
		if ($this->id == null) {
			//Prüfen ob Forum bereits existiert
			$result = $this->db->query("SELECT COUNT(*) AS Anzahl FROM Thread WHERE Title = '$this->title' AND Forum = '$this->forum'");
			$reultObj = $result->fetch_object();
			if ($reultObj->Anzahl > 0) {
				return new PersistResult(PersistResult::FAILURE, "Ein Thread mit diesem Namen existiert bereits.");
			}

			$creator = $this->creator->get(User::PROPERTY_ID);
			$result = $this->db->query("INSERT INTO Thread (Forum, Creator, Created, Title, Relevance, Deleted, Public) VALUES ($this->forum, $creator, '$this->created', '$this->title', $this->relevance, $deleted, $public)");
			//ID der News setzen
			$this->id = $this->db->insert_id;
			$this->post->setThread($this->id);
			if ($result && $this->post->save()->isSuccessful()) {
				$this->saveGroups();
				return new PersistResult(PersistResult::SUCCESS);
			} elseif (is_numeric($this->id)) {
				//Thread wieder entfernen
				$this->db->query("DELETE FROM Thread WHERE ID = $this->id");
				$this->id = null;
			}
		} else {
			//... oder nur aktualisieren
			$result = $this->db->query("UPDATE Thread SET Title = '$this->title', Relevance = $this->relevance, Deleted = $deleted, `Public` = $public WHERE ID = $this->id");
			if ($result && $this->post->save()) {
				$this->saveGroups();
				return new PersistResult(PersistResult::SUCCESS);
			}
		}
		return new PersistResult(PersistResult::FAILURE, "Fehler beim Speichern des Threads.");
	}

	/**
	 * Speichert die Gruppen des Threads.
	 */
	private function saveGroups() {
		$delete = "DELETE FROM Thread_Allowed_Groups WHERE Thread = " . $this->id;
		$this->db->query($delete);
		if ($this->public == false) {
			foreach ($this->groups as $grpID) {
				$this->db->query("INSERT INTO Thread_Allowed_Groups (`Thread`, `Group`) VALUES (" . $this->id . "," . $grpID . ")");
			}
		}
	}

	/**
	 * Berechnet die aktuelle Relevanz
	 */
	public function calculateRelevance() {
		if ($this->id == null) {
			$this->relevance = 50;
		} else {
			$query = "SELECT (SELECT COUNT(*) FROM ThreadPost WHERE Thread = t.ID) AS Posts,
				         (SELECT MAX(tpma.Anz) FROM (SELECT COUNT(*) AS Anz FROM ThreadPost GROUP BY Thread ) tpma) AS MaxPosts,
				         (SELECT MIN(UNIX_TIMESTAMP(Created)) FROM ThreadPost) AS MaxAge,
				         (SELECT SUM(UNIX_TIMESTAMP(Created)) DIV Posts FROM ThreadPost WHERE Thread = t.ID) AS AvgAgeThread,
				         UNIX_TIMESTAMP(NOW()) AS Now,
				         (SELECT COUNT(*) FROM ThreadPost WHERE Thread = t.ID AND Created > DATE_ADD(NOW(),INTERVAL -7 DAY)) AS PostsThreadWeek,
				         (SELECT COUNT(*) FROM ThreadPost WHERE Thread IN (SELECT ID FROM Thread WHERE Forum = t.Forum) AND Created > DATE_ADD(NOW(),INTERVAL -7 DAY)) AS PostsForumWeek,
				         (SELECT COUNT(*) FROM ThreadPost WHERE Thread = t.ID AND Created > DATE_ADD(NOW(),INTERVAL -1 DAY)) AS PostsThreadDay,
				         (SELECT COUNT(*) FROM ThreadPost WHERE Thread IN (SELECT ID FROM Thread WHERE Forum = t.Forum) AND Created > DATE_ADD(NOW(),INTERVAL -1 DAY)) AS PostsForumDay
				  FROM Thread t
				  WHERE ID = $this->id";
			$result = $this->db->query($query);
			$resultObj = $result->fetch_object();

			//Gewichtung
			$fPosts = 0.25;
			$fAge = 0.30;
			$fWeek = 0.30;
			$fDay = 0.15;

			//Werte
			$vPosts = $resultObj->Posts / $resultObj->MaxPosts;
			$vAge = ($resultObj->AvgAgeThread - $resultObj->MaxAge + 1) / ($resultObj->Now - $resultObj->MaxAge + 1);
			$vWeek = ($resultObj->PostsThreadWeek + 1) / ($resultObj->PostsForumWeek + 1);
			$vDay = ($resultObj->PostsThreadDay + 1) / ($resultObj->PostsForumDay + 1);

			$this->relevance = round(($vPosts * $fPosts + $vAge * $fAge + $vWeek * $fWeek + $vDay * $fDay) * 100);
		}
	}

	/**
	 * Liefert die Art der Sichtbarkeit (Oeffentlich, Spezifisch, Eingeschraenkt).
	 *
	 * @return string Die Sichtbarkeit.
	 */
	public function getVisibility() {
		$visibility = Access::TYPE_PUBLIC;
		if ($this->public === false) {
			$visibility = Access::TYPE_MYGROUPS;
			$threadGroups = $this->getGroups();
			$userGroups = $this->creator->getGroups();

			foreach ($userGroups as $grp) {
				if (!in_array($grp->getID(), $threadGroups)) {
					$visibility = Access::TYPE_SPECIFIC;
					break;
				}
			}
		}
		return $visibility;
	}

	/**
	 * Prueft ob ein Benutzer diesen Thread sehen darf. Wird keine UserID uebergeben wird der aktuell angemeldete
	 * Benutzer geprueft.
	 *
	 * @param null|User $user Zu pruefende Nutzer oder null fuer den aktuellen.
	 *
	 * @return bool Ob der Nutzer den Thread sehen darf.
	 */
	public function checkAccess($user = null) {
		if ($user == null) {
			$user = Login::getInstance()->getCurrentUserObject();
		}
		if ($this->public === true) {
			return true;
		}
		foreach ($user->getGroups() as $group) {
			if (in_array($group->getID(), $this->getGroups())) {
				return true;
			}
		}
		if ($this->creator->get(User::PROPERTY_ID) == $user->get(User::PROPERTY_ID)) {
			return true;
		}
		return false;
	}

	function getGroups() {
		return $this->groups;
	}

}