<?php

/**
 * Class PersistResult
 * TODO: Comment
 *
 * @author Frederik Kirsch
 */
class PersistResult {

    const SUCCESS = TRUE;

    const FAILURE = FALSE;

    private $success;

    private $message;

    public function __construct($success, $message = null) {
        $this->success = $success;
        $this->message = $message;
    }

    public function isSuccessful() {
        return $this->success;
    }

    public function __toString() {
        $string = null;
        if ($this->success) {
            $string = "Erfolg";
        } else {
            $string = "Fehler";
        }

        if ($this->message != null) {
            $string = $this->message;
        }

        return $string;
    }

}