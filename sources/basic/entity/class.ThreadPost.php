<?php

LibImporter::import("basic/entity/basic/class.PersistResult.php");

class ThreadPost {

	//Attribute
	private $id = null;

	private $thread = null;

	private $author = null;

	private $created = null;

	private $text = null;

	//Variablen
	private $db = null;

	/**
	 * Laedt den ThreadPost aus der Datenbank.
	 *
	 * @param int $id ID des ThreadPosts.
	 *
	 * @return ThreadPost Der geladene Thread.
	 */
	public static function createFromDB($id) {
		return new ThreadPost($id);
	}

	/**
	 * Erzeugt auf Basis der uebergebenen Daten einen neuen ThreadPost.
	 *
	 * @param string $author UserID des Erstellers.
	 * @param string $text   Text des Posts.
	 *
	 * @return ThreadPost Der erzeugte ThreadPost.
	 */
	public static function create($author, $text) {
		return new ThreadPost(null, null, $author, time(), $text);
	}

	public static function createEmpty() {
		return new ThreadPost(null, null, null, time(), "");
	}

	private function __construct($id, $thread = null, $author = null, $created = null, $text = null) {
		//Benötigte Variablen instanziieren
		$this->db = DBConnect::getDBConnection();

		//Variablen aufbereiten
		if ($id != null) {
			//Existierendes Forumobjekt
			$result = $this->db->query("SELECT Thread, Author, Created, Text FROM ThreadPost WHERE ID = $id");
			$post = $result->fetch_object();
			$thread = $post->Thread;
			$author = $post->Author;
			$created = strtotime($post->Created);
			$text = $post->Text;
		}

		//Objektvariablen setzen
		$this->id = $id;
		$this->thread = $thread;
		$this->author = new User($author);
		$this->created = $created;
		$this->text = $text;
	}

	public function getID() {
		return $this->id;
	}

	public function getText() {
		return $this->text;
	}

	public function setText($text) {
		$this->text = $text;
	}

	public function setThread($thread) {
		$this->thread = $thread;
	}

	public function getThread() {
		return $this->thread;
	}

	/**
	 * Stellt das Objekt grafisch dar.
	 */
	public function toHTML() {
		// Parameter aufbereiten
		$date = $this->getDateString($this->created);

		// Ausgabe
		$result = "<div class='post_avatar'>" . $this->author->drawUserSmall() . "</div>";
		$result .= "<div class='post_content'>";
		$result .= Converter::bbToHtml($this->text);
		$result .= "<div class='post_footer'>$date Uhr - <span style='font-style: italic;'>\"" . $this->author->get(User::PROPERTY_Signatur) . "\"</span></div>";
		$result .= "</div>";
		return "<div id='threadpost_$this->id' class='post'>$result</div>";
	}

	public function toHTMLStandalone() {
		LibImporter::import("sites/forum/interface.MenuThread.php");

		// Parameter aufbereiten
		$date = $this->getDateString($this->created);
		$settings = Settings::getInstance();
		$splitSize = $settings->getProperty(Settings::PROPERTY_THREADPOST_SPLIT_SIZE);
		$threadInfo = $this->getThreadInfo();
		$linkForum = "index.php?page=" . MenuThread::OVERVIEW . "&forum=$threadInfo->forumID";
		$linkThread = "index.php?page=" . MenuThread::SINGLE . "&forum=$threadInfo->forumID&thread=$threadInfo->threadID";
		$mainAuthor = $this->author;
		if ($this->author . $this->getID() != $threadInfo->firstTPAuthor) {
			$mainAuthor = new User($threadInfo->firstTPAuthor);
		}

		$result = "<div class='feed_item'>";
		//Titel
		$result .= "<h2 class='feed_item_header'><a href='$linkThread'>$threadInfo->threadTitle</a></h2>";
		$result .= "<div class='feed_item_subheader'>Thread - gepostet von " . $mainAuthor->drawUserText() . " in <a href='$linkForum'>$threadInfo->forumName</a></div>";

		if($threadInfo->postAnz>1){
			//Startpost
			$firstDate = $this->getDateString($threadInfo->firstTPCreated);
			$result .= "<div class='feed_item_content'>" . Converter::bbToHtml(Converter::cutBB($threadInfo->firstTPText, $splitSize)) . "</div>";
			$result .= "<div class='feed_item_subheader'>" . $mainAuthor->drawUserText() . " - $firstDate - <span style='font-style: italic;'>\"" . $mainAuthor->get(User::PROPERTY_Signatur) . "\"</span></div>";
			$result .= "<div class='feed_item_splitter'><a href='$linkThread'>Neuste Antwort</a><hr /></div>";
		}

		// Endpost
		$result .= "<div class='feed_item_content'>" . Converter::bbToHtml(Converter::cutBB($this->text, $splitSize)) . "</div>";
		$result .= "<div class='feed_item_subheader'>" . $this->author->drawUserText() . " - $date - <span style='font-style: italic;'>\"" . $this->author->get(User::PROPERTY_Signatur) . "\"</span></div>";

		//Footer
		$result .= "<div class='feed_item_footer'>
				  <a href='$linkThread'>Weiterlesen</a> >> Zum Thread <a href='$linkThread'>$threadInfo->threadTitle</a> ($threadInfo->postAnz Beiträge)
			  </div>";

		$result .= "</div>";
		return $result;
	}

	/**
	 * @return ThreadInfo
	 */
	private function getThreadInfo() {
		$result = $this->db->query("SELECT T.Forum AS ForumID, F.Name AS ForumName, T.ID AS ThreadID, T.Title AS ThreadTitle, (SELECT COUNT(*) FROM ThreadPost WHERE Thread = $this->thread) AS PostAnz, TP.ID AS FirstTPID, TP.Text AS FirstTPText, TP.Created AS FirstTPCreated, TP.Author AS FirstTPAuthor
									FROM Thread T
									INNER JOIN ThreadPost TP
									  ON TP.Thread = T.ID
									INNER JOIN Forum F
									  ON F.ID = T.Forum
									WHERE T.ID = $this->thread
									AND TP.ID = (SELECT MIN(ID) FROM ThreadPost WHERE Thread = $this->thread)");
		$threadInfo = $result->fetch_object();
		return new ThreadInfo($threadInfo->ForumID, $threadInfo->ForumName, $threadInfo->ThreadID, $threadInfo->ThreadTitle, $threadInfo->PostAnz, $threadInfo->FirstTPID, $threadInfo->FirstTPText, $threadInfo->FirstTPCreated,$threadInfo->FirstTPAuthor);
	}

	private function getDateString($dateStr) {
		$date = date("d.m.Y H:i", $dateStr);
		if (date("d.m.Y", $dateStr) == date("d.m.Y")) {
			$date = date("H:i", $dateStr);
		}
		return "$date Uhr";
	}

	/**
	 * Persistiert den aktuellen Zustand des Objekts in der Datenbank.
	 *
	 * @return PersistResult Ergebnis des Speichervorgangs.
	 */
	public function save() {
		//Speichern...
		if ($this->id == null) {
			$author = $this->author->get(User::PROPERTY_ID);
			$created = date("Y.m.d H:i:s", $this->created);
			$result = $this->db->query("INSERT INTO ThreadPost (Thread, Author, Created, Text) VALUES ($this->thread, $author, '$created', '$this->text')");
			if ($result) {
				//ID des Posts setzen
				$this->id = $this->db->insert_id;
				$this->createScheduleTask($this->id);
				return new PersistResult(PersistResult::SUCCESS);
			}
		} else {
			//... oder nur aktualisieren
			$result = $this->db->query("UPDATE ThreadPost SET Text = '$this->text' WHERE ID = $this->id");
			if ($result) {
				return new PersistResult(PersistResult::SUCCESS);
			}
		}
		return new PersistResult(PersistResult::FAILURE, "Fehler beim Speichern des Posts.");
	}

	private function createScheduleTask($postID) {
		LibImporter::import("cron/class.JobScheduler.php");
		$scheduler = JobScheduler::getInstance();
		$job = ThreadpostJob::createInitial($postID);
		$scheduler->addJob($job);
	}

}

class ThreadInfo {

	public $forumID;

	public $forumName;

	public $threadID;

	public $threadTitle;

	public $postAnz;

	public $firstTPID;

	public $firstTPText;

	public $firstTPCreated;

	public $firstTPAuthor;

	public function __construct($forumID, $forumName, $threadID, $threadTitle, $postAnz, $firstTPID, $firstTPText, $firstTPCreated, $firstTPAuthor) {
		$this->forumID = $forumID;
		$this->forumName = $forumName;
		$this->threadID = $threadID;
		$this->threadTitle = $threadTitle;
		$this->postAnz = $postAnz;
		$this->firstTPID = $firstTPID;
		$this->firstTPText = $firstTPText;
		$this->firstTPCreated = $firstTPCreated;
		$this->firstTPAuthor = $firstTPAuthor;
	}

}