<?php

/**
 * Class User
 * Repraesentiert die Entitaet eines Benutzers. Stellt Funktionen Rund um diese Entitaet zur Verfuegung.
 *
 * @author Frederik Kirsch
 */
class User implements Recht {

	//Attribute
	const PROPERTY_ID = "ID";

	const PROPERTY_Nick = "Nick";

	const PROPERTY_Vorname = "Vorname";

	const PROPERTY_Nachname = "Nachname";

	const PROPERTY_Geburtsdatum = "Geburtsdatum";

	const PROPERTY_Email = "Email";

	const PROPERTY_Twitter = "Twitter";

	const PROPERTY_Telefon = "Telefon";

	const PROPERTY_Handy = "Handy";

	const PROPERTY_Strasse = "Strasse";

	const PROPERTY_PLZ = "PLZ";

	const PROPERTY_Ort = "Ort";

	const PROPERTY_Signatur = "Signatur";

	const PROPERTY_Registriert = "Registriert";

	const PROPERTY_Privatsphaere = "Privatsphaere";

	const PROPERTY_Use_Experimental = "UseExperimental";

	const PROPERTY_Notification_Type_Email = "NotificationTypeEmail";

	const PROPERTY_Notification_Type_Twitter = "NotificationTypeTwitter";

	const PROPERTY_Notification_News = "NotificationNews";

	const PROPERTY_Notification_Comment = "NotificationComment";

	const PROPERTY_Notification_Threadpost = "NotificationThreadpost";

	const PROPERTY_Read_Cookie_Info = "ReadCookieInfo";

	//Konstanten
	const PRIVACY_PUBLIC = "Öffentlich";

	const PRIVACY_LIMITED = "Eingeschränkt";

	const PRIVACY_PRIVATE = "Privat";

	//Instanzvariablen
	private $db = null;

	private $userObj = null;

	private $rechte = null;

	private $groups = null;

	public function __construct($id) {
		$this->db = DBConnect::getDBConnection();
		$this->reloadUserObj($id);
	}

	public function get($attribute) {
		$result = $this->userObj->$attribute;
		if ($attribute == User::PROPERTY_PLZ) {
			$result = "";
		}
		return $result;
	}

	/**
	 * @return Group[] Die Gruppen zu denen der User gehoert.
	 */
	public function getGroups() {
		LibImporter::import("basic/entity/class.Group.php");
		if ($this->groups == null) {
			$this->groups = array();
			$query = "SELECT Gruppe FROM Gruppe_Mitglied WHERE User = " . $this->get(User::PROPERTY_ID);
			$result = $this->db->query($query);
			if ($result->num_rows > 0) {
				while ($obj = $result->fetch_object()) {
					$this->groups[] = new Group($obj->Gruppe);
				}
			}
		}
		return $this->groups;
	}

	public function hasRight($pRecht) {
		return in_array($pRecht, $this->getRights());
	}

	public function isActive() {
		$query = "SELECT COUNT(*)
                  FROM Users U
                  INNER JOIN User_Status US ON US.ID = U.Status
                  WHERE U.ID = " . $this->get(User::PROPERTY_ID) . " AND US.Status = 'Aktiv'";
		$result = $this->db->query($query);
		if ($result->num_rows > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function getRights() {
		if ($this->rechte == null) {
			$this->rechte = array();

			$query = "SELECT R.Recht
                      FROM User_Rechte UR
				 	  INNER JOIN Rechte R ON UR.Recht = R.ID
				 	  WHERE UR.User = " . $this->get(User::PROPERTY_ID) ."
				 	  ORDER BY R.Recht";
			$result = $this->db->query($query);
			if ($result->num_rows > 0) {
				while ($obj = $result->fetch_object()) {
					$this->rechte[] = $obj->Recht;
				}
			}
		}
		return $this->rechte;
	}

	public function addRight($recht) {
		$insert = "INSERT INTO User_Rechte
                               (User, Recht)
                        VALUES (" . $this->get(User::PROPERTY_ID) . ", (SELECT ID FROM Rechte WHERE Recht = '$recht'))";
		$this->db->query($insert);
		$this->rechte = null;
		return $this->db->affected_rows == 1;
	}

	public function removeRight($recht) {
		$remove = "DELETE FROM User_Rechte
                   WHERE User = " . $this->get(User::PROPERTY_ID) . " AND Recht = (SELECT ID FROM Rechte WHERE Recht = '$recht')";
		$this->db->query($remove);
		$this->rechte = null;
		return $this->db->affected_rows == 1;
	}

	private function reloadUserObj($id) {
		if ($id == null) {
			$id = $this->get(User::PROPERTY_ID);
		}
		$query = "SELECT " . User::PROPERTY_ID . ", " .
							 User::PROPERTY_Nick . ", " .
							 User::PROPERTY_Vorname . ", " .
							 User::PROPERTY_Nachname . ", " .
							 User::PROPERTY_Geburtsdatum . ", " .
							 User::PROPERTY_Email . ", " .
							 User::PROPERTY_Twitter . ", " .
							 User::PROPERTY_Telefon . ", " .
							 User::PROPERTY_Handy . ", " .
							 User::PROPERTY_Strasse . ", " .
							 User::PROPERTY_PLZ . ", " .
							 User::PROPERTY_Ort . ", " .
							 User::PROPERTY_Signatur . ", " .
							 User::PROPERTY_Registriert . ", " .
							 User::PROPERTY_Privatsphaere . ", " .
							 User::PROPERTY_Use_Experimental . ", " .
							 User::PROPERTY_Notification_Type_Email . ", " .
							 User::PROPERTY_Notification_Type_Twitter . ", " .
							 User::PROPERTY_Notification_News . ", " .
							 User::PROPERTY_Notification_Comment . ", " .
							 User::PROPERTY_Notification_Threadpost . ", " .
							 User::PROPERTY_Read_Cookie_Info .
							 " FROM Users WHERE ID = $id";
		$result = $this->db->query($query);

		if ($result != null && $result->num_rows == 1) {
			$this->userObj = $result->fetch_object();
		}
	}

	public function getAdressText() {
		//Adressdaten ermitteln
		$strasse = $this->get(User::PROPERTY_Strasse);
		$plz = $this->get(User::PROPERTY_PLZ);
		$ort = $this->get(User::PROPERTY_Ort);

		//URL aufbauen
		$url = array();
		$url[] = $strasse;
		$url[] = $ort;

		//Adresse aufbauen
		$adresse = Converter::NULL;
		if ($strasse != "") {
			$adresse = $strasse;
		}
		if ($strasse != "" && ($plz != "" || $ort != "")) {
			$adresse .= "<br />";
		}
		if ($plz != "") {
			$adresse .= $plz;
		}
		if ($ort != "") {
			if ($plz != "") {
				$adresse .= " ";
			}
			$adresse .= $ort;
		}

		//Link erzeugen, wenn genügend Daten für eine Ortung bestehen
		if ($plz != 0 || $ort != "") {
			$adresse = "<a href='http://maps.google.com/maps?q=" . implode(".+", $url) . "' target='_blank'>$adresse</a>";
		}
		return $adresse;
	}

	/**
	 * @return string Stellt den User als Text dar.
	 */
	public function drawUserText() {
		$profileURL = "index.php?page=" . Menu::PROFILE . "&User=" . $this->get(User::PROPERTY_ID);
		return "<a href='$profileURL' target='_self' title='Zur Profilseite von " . $this->get(User::PROPERTY_Nick) . "'>" . $this->get(User::PROPERTY_Nick) . "</a>";
	}

	public function drawUserSmall() {
		$url = "index.php?page=" . Menu::PROFILE. "&User=" . $this->get(User::PROPERTY_ID);
		return "<div class='profile box'>
                    <a href='" . $url . "' target='_self' title='Zur Profilseite von " . $this->get(User::PROPERTY_Nick) . "'>
                    <img src='gfx/img_generator.php?type=User&id=" . $this->get(User::PROPERTY_ID) . "&size=Medium' />" . $this->get(User::PROPERTY_Nick) . "</a>
                </div>";
	}

	public function __toString() {
		return $this->drawUserText();
	}

}