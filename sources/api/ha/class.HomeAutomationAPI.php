<?php

LibImporter::import("api/basic/class.AbstractPropertyAPI.php");
LibImporter::import("basic/util/class.Verify.php");
LibImporter::import("basic/util/class.Toolkit.php");
LibImporter::import("api/ha/netatmo/Netatmo/autoload.php");

/**
 * Class HomeAutomationAPI
 * API zur Aenderung der Home Automation Konfiguration eines Benutzers.
 */
class HomeAutomationAPI extends AbstractPropertyAPI {

	// Konstanten
	private $properties = array("Timer"   => array("Active" => array("col" => "TimerActive", "validate" => "checkToggle")),
	                            "Netatmo" => array("Active" => array("col" => "NetatmoActive", "validate" => "checkToggle"),
	                                               "User"   => array("col" => "NetatmoUser", "validate" => "checkExternalAuthData"),
	                                               "Pass"   => array("col" => "NetatmoPass", "validate" => "checkExternalAuthData")));

	//Variablen
	private $userID;

	private $key;

	private $value;

	//Util Klassen
	private $verify;

	private $db;

	/**
	 * HomeAutomationAPI constructor.
	 * @param int    $user  Die UserID.
	 * @param string $key   Der Authentifizierungsschlussel des Benutzers.
	 * @param string $value Die uebergebenen Daten.
	 */
	public function __construct($user, $key = null, $value = null) {
		$this->userID = $user;
		$this->key = $key;
		$this->value = $value;

		if (is_array($value)) {
			$value = json_encode($value);
		}
		if (strlen($user) > 0) {
			$this->addQueryItem("user", $user);
		}
		if (strlen($value) > 0) {
			$this->addQueryItem("value", $value);
		}
		if (strlen($key) > 0) {
			$this->addQueryItem("authKey", $key);
		}

		$this->verify = new Verify();
		$this->db = DBConnect::getDBConnection();
	}

	/** @return bool|string Ob der Nutzer alle Erfragen darf ob Cherriz HA fuer den angegebenen Benutzer initialisiert ist. */
	protected function verifyIsSetUp() {
		return $this->isCurrentUser();
	}

	/**
	 * Ob der Setup fuer den Benutzer bereits ausgeführt wurde.
	 * @return bool Ob die Anfrage erfolgreich war.
	 */
	protected function performIsSetUp() {
		$result = $this->db->query("SELECT COUNT(*) AS Anz FROM HAConfiguration WHERE UserID = $this->userID");
		$resultObj = $result->fetch_object();
		$this->addResultItem("isSetUp", $resultObj->Anz == 1);
		return true;
	}

	/** @return bool|string Ob der Nutzer den Setup ausfuehren darf. */
	protected function verifySetUp() {
		return $this->isCurrentUser();
	}

	/**
	 * Fuehrt den Setup der Kofniguration fuer Cherriz HA fuer einen Nutzer aus.
	 * @return bool|string Erfolg oder Fehler.
	 */
	protected function performSetUp() {
		$key = Toolkit::generateKey(Toolkit::VALUE_POOL_NUMERIC, 20);
		if ($this->db->query("INSERT INTO HAConfiguration (UserID, AuthKey, TimerActive, NetatmoActive) VALUES ($this->userID, '$key', 1, 0)")) {
			$this->key = $key;
			return $this->performGetHAData();
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	/** @return bool|string Ob der Nutzer den Auth Key abfragen darf. */
	protected function verifyGetAuthKey() {
		return $this->isCurrentUser();
	}

	/**
	 * Liest den Authentifizierungsschluesse aus.
	 * @return bool Ob die Anfrage erfoglreich war.
	 **/
	protected function performGetAuthKey() {
		$result = $this->db->query("SELECT AuthKey FROM HAConfiguration WHERE UserID = $this->userID");
		$resultObj = $result->fetch_object();
		$this->addResultItem("authKey", $resultObj->AuthKey);
		return true;
	}

	/** @return bool|string Ob der Nutzer die Konfigurationsdaten abfragen darf. */
	protected function verifyGetHAData() {
		return $this->validateKey();
	}

	/**
	 * Liest die Konfiguration aus.
	 * @return bool Ob die Anfrage erfolgreich war.
	 */
	protected function performGetHAData() {
		$result = $this->db->query("SELECT UserID, AuthKey, TimerActive, NetatmoActive, NetatmoUser, NetatmoPass FROM HAConfiguration WHERE AuthKey LIKE '$this->key'");
		$resultObj = $result->fetch_object();

		$this->addResultItem("userID", $resultObj->UserID);
		$this->addResultItem("authKey", $resultObj->AuthKey);
		$this->addResultItem("modules",
			array(array('name'       => 'Timer',
			            'properties' => array('active' => $resultObj->TimerActive)),
				array('name'       => 'Netatmo',
				      'properties' => array('active' => $resultObj->NetatmoActive,
				                            'user'   => $resultObj->NetatmoUser,
				                            'pass'   => $resultObj->NetatmoPass))));
		return true;
	}

	/** @return bool|string Ob der Nutzer Konfigurationswerte aendern darf. */
	protected function verifySetModuleProperty() {
		$valide = $this->validateKey();
		if ($valide !== true) {
			return $valide;
		}
		foreach ($this->value["properties"] as $key => $val) {
			$prop = $this->identifyProperty($this->value["module"], $key);
			if ($prop == null) {
				return "Ungueltige Property.";
			}
			$valResult = $this->verify->$prop["validate"]($val);
			if (Verify::$VALIDE !== $valResult) {
				return $valResult;
			}
		}
		return true;
	}

	/**
	 * Fuehrt eine Konfigurationsaendeurng durch.
	 * @return bool|string Ob die Aenderung erfolgreich war.
	 */
	protected function performSetModuleProperty() {
		$colArray = array();
		foreach ($this->value["properties"] as $key => $newVal) {
			$col = $this->identifyProperty($this->value["module"], $key)["col"];
			if ($newVal === "true" && $key == "Active") {
				$newVal = "1";
			} else if ($newVal === "false" && $key == "Active") {
				$newVal = "0";
			}
			$colArray[] = "$col = '$newVal'";
		}
		$colStr = implode(", ", $colArray);

		if ($this->db->query("UPDATE HAConfiguration SET $colStr WHERE AuthKey LIKE '$this->key'")) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	/**
	 * Ermittelt eine Property fuer ein Modul.
	 * @param string $module Der Modulname.
	 * @param string $property Der Propertyname.
	 * @return array Die Property.
	 */
	private function identifyProperty($module, $property) {
		return $this->properties[$module][$property];
	}

	/** @return bool|string Ob der Nutzer Konfigurationswerte aendern darf. */
	protected function verifyRegenerateAuthKey() {
		return $this->validateKey();
	}

	/**
	 * Regeneriert den Authentifizierungsschluesse.
	 * @return bool|string Ob die Aenderung erfolgreich war.
	 */
	protected function performRegenerateAuthKey() {
		$key = Toolkit::generateKey(Toolkit::VALUE_POOL_NUMERIC, 20);
		if ($this->db->query("UPDATE HAConfiguration SET AuthKey = '$key'")) {
			$this->key = $key;
			$this->addResultItem("authKey", $key);
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyValidateAuthKey(){
		$valResult = $this->validateKey();
		if($valResult !== true){
			return $valResult;
		}
		return true;
	}

	protected function performValidateAuthKey(){
		$result = $this->db->query("SELECT COUNT(*) AS Anz FROM HAConfiguration WHERE AuthKey LIKE '$this->key'");
		$resultObj = $result->fetch_object();
		$valide = false;
		if ($resultObj->Anz == 1) {
			$valide = true;
		}
		$this->addResultItem("valide", $valide);
		return true;
	}

	/**
	 * Prueft ob der Authentifizierte Benutzer dem definierten Benutzer entspricht.
	 *
	 * @return bool|string <code>true</code> wenn Benutzer uebereinstimmen, sonst Fehlermeldung.
	 */
	private function isCurrentUser() {
		if (Login::getInstance()->getCurrentUserObject()->get(User::PROPERTY_ID) != $this->userID) {
			return "Der angemeldete Benutzer entspricht nicht dem Benutzer der Abfrage.";
		}
		return true;
	}

	/**
	 * @return bool|string Ob der Authenzifizierungsschluessel valide ist.
	 */
	private function validateKey() {
		$result = $this->db->query("SELECT COUNT(*) AS Anz FROM HAConfiguration WHERE AuthKey LIKE '$this->key'");
		$resultObj = $result->fetch_object();
		if ($resultObj->Anz != 1) {
			return "Der angegebene Schluessel ist ungueltig.";
		}
		return true;
	}

}