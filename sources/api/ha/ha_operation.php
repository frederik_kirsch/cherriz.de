<?php
/** Schnittstelle fuer Aktionen bezogen auf die Konfiguration von Cherriz Home Automation. */

header("Access-Control-Allow-Origin: *");

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/ha/class.HomeAutomationAPI.php");

$action = Filter::text($_REQUEST['action']);
$user = Filter::text($_REQUEST['user']);
$key = Filter::text($_REQUEST['authKey']);
$value = null;
if(is_array($_POST['value'])){
	$value = array();
	foreach ($_POST['value'] as $arrKey => $arrVal){
		if(is_array($arrVal)){
			$subValue = array();
			foreach ($arrVal as $subArrKey => $subArrVal){
				$subValue[Filter::text($subArrKey)] = Filter::text($subArrVal);
			}
			$value[Filter::text($arrKey)] = $subValue;
		}else{
			$value[Filter::text($arrKey)] = Filter::text($arrVal);
		}
	}
}else{
	$value = Filter::text($_POST['value']);
}

$api = new HomeAutomationAPI($user, $key, $value);
$api->perform($action);
$api->createOutput();