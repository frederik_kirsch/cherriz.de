<?php

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.NewsQuery.php");
LibImporter::import("basic/entity/class.NewsItem.php");
LibImporter::import("basic/entity/interface.Recht.php");
LibImporter::import("basic/entity/class.User.php");
LibImporter::import("basic/entity/class.Group.php");
LibImporter::import("service/class.ThreadPostQuery.php");
LibImporter::import("basic/entity/class.ThreadPost.php");
LibImporter::import("service/class.FeedQuery.php");

//Parameter abrufen
$start = Filter::text($_REQUEST['start']);
$max = Filter::text($_REQUEST['max']);

//Query aufbauen
$query = new FeedQuery();
if (Login::getInstance()->isLoggedIn()) {
	$query->setUser(Login::getInstance()->getCurrentUserObject());
} else {
	$query->setPublic(true);
}
$query->setRange($start, $max);

//Daten abrufen
$service = new Service();
$feedItems = $service->requestItems($query);
$anz = $service->countItems($query);

//JSON erzeugen
$generator = new JSONGenerator();
$generator->addQueryData("start", $start);
$generator->addQueryData("max", $max);

$items = array();
foreach ($feedItems as $item) {
	array_push($items, array("id" => $item->ID, "marker" => strtotime($item->Date), "type" => $item->Type));
}
$generator->addResultData("items", $items);
$generator->addResultData("count", $anz);

//XML ausgeben
$generator->generateJSONOutput();