<?php

/**
 * Class JSONGenerator
 * Dient der Generierung vom JSON Dokumenten.
 *
 * @author Frederik Kirsch
 */
class JSONGenerator {

	/**
	 * Root Tag.
	 */
	const RESULT = "result";

	/**
	 * Tag für Anfrageeigenschaften
	 */
	const QUERY = "query";

	/**
	 * Ergebnisdaten.
	 */
	const DATA = "data";

	/**
	 * Fehler.
	 */
	const ERROR = "error";

	private $query = null;

	private $data = null;

	private $error = null;

	public function __construct() {
		$this->query = array();
		$this->data = array();
	}

	/**
	 * Abfrageattribut hinzufuegen-.
	 *
	 * @param string $prop  Name
	 * @param string $value Wert
	 */
	public function addQueryData($prop, $value) {
		$this->query[$prop] = $value;
	}

	/**
	 * @param           string $name  Name der Property.
	 * @param string|int|array $value Wert der Property.
	 */
	public function addResultData($name, $value) {
		$this->data[$name] = $value;
	}

	/**
	 * @param string $error Fehler falls aufgetreten.
	 */
	public function setError($error) {
		$this->error = $error;
	}

	/**
	 * @return array Das JSON Dokument als PHP Array.
	 */
	public function getJSONObject() {
		$result = array(JSONGenerator::QUERY => $this->query);
		if ($this->error == null) {
			$result[JSONGenerator::DATA] = $this->data;
		} else {
			$result[JSONGenerator::ERROR] = $this->error;
		}
		return array(JSONGenerator::RESULT => $result);
	}

	/**
	 * @return string Das JSON Objekt als String.
	 */
	public function getJSONString() {
		return json_encode($this->getJSONObject());
	}

	/**
	 * Erzeugt ein Ergebnisdokument und gibt es aus.
	 */
	public function generateJSONOutput() {
		header("Content-Type: application/json;charset=UTF-8");
		echo($this->getJSONString());
	}

}