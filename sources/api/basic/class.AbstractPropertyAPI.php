<?php

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");

/**
 * Class AbstractPropertyAPI
 * Basisklasse für Entitaets APIs.
 *
 * @author Frederik Kirsch
 */
abstract class AbstractPropertyAPI {

	/**
	 * Erfolgreiche Anfrage.
	 */
	const RESULT_SUCCESS = "success";

	/**
	 * Fehlgeschlagene Anfrage.
	 */
	const RESULT_FAILURE = "failure";

	private $result = null;

	/**
	 * Fuehrt die gewaehlte Aktion durch.
	 *
	 * @param string $action Die Aktion.
	 */
	public function perform($action) {
		$this->addQueryItem("action", $action);

		$method = "verify" . ucfirst($action);
		$verResult = $this->$method();
		if ($verResult === true) {
			$method = "perform" . ucfirst($action);
			$perfResult = $this->$method();
			if ($perfResult === true) {
				$this->setSuccess();
			} else {
				$this->setFailure($perfResult);
			}
		} else {
			$this->setFailure($verResult);
		}
	}

	/**
	 * Hinterlegt ein Attribut aus der Abfrage.
	 * @param string $prop Attribut das zurueckgegeben werden soll.
	 * @param string $val  Wert des Attributs.
	 */
	protected function addQueryItem($prop, $val) {
		if ($this->result == null) {
			$this->result = new JSONGenerator();
		}
		$this->result->addQueryData($prop, $val);
	}

	/**
	 * Hinterlegt ein Ergebnisattribut.
	 * @param string $prop Attribut das zurueckgegeben werden soll.
	 * @param string $val  Wert des Attributs.
	 */
	protected function addResultItem($prop, $val) {
		$this->result->addResultData($prop, $val);
	}

	/**
	 * Definiert die Erfolgreiche durchfuehrung der Schnittstellenaktion.
	 */
	private function setSuccess() {
		$this->addResultItem("status", AbstractPropertyAPI::RESULT_SUCCESS);
		$this->addResultItem("message", "Änderung erfolgreich.");
	}

	/**
	 * Aktion ist fehlgeschlagen.
	 *
	 * @param string $message Begruendung.
	 */
	private function setFailure($message) {
		$this->result->setError($message);
	}

	/**
	 * AusgabeXML erzeugen.
	 */
	public function createOutput() {
		$this->result->generateJSONOutput();
	}

	/**
	 * Ergebnispbjekt zurueckgeben.
	 * @return array Ergebnisobjekt.
	 */
	public function getJSON() {
		return $this->result->getJSONObject();
	}

} 