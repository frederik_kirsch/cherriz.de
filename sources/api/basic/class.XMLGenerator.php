<?php

/**
 * Class XMLGenerator
 * Dient der Generierung vom XML Dokumenten.
 *
 * @author Frederik Kirsch
 */
class XMLGenerator {

    public function generateXMLOutput($data) {
        $this->printHeader();
        echo($data);
    }

    public function generateRSSOutput($title, $link, $description, $itemArray) {
        $this->printHeader();
        echo("<rss version='2.0'>
				    <channel>
				      <title>" . $title . "</title> 
      				  <link>" . $link . "</link> 
      				  <description>" . $description . "</description> 
      				  <pubDate>" . date(DATE_RSS) . "</pubDate>");
        if (is_array($itemArray)) {
            foreach ($itemArray as $item) {
                echo($item);
            }
        } else {
            echo("<exception>Items need to be passed as array.</exception>");
        }
        echo("  </channel>
			      </rss>");
    }

    private function printHeader() {
        header("Content-Type: text/xml; charset=utf-8");
    }

}

/**
 * Class XMLItem
 * Repraesentiert einen einzelnen Tag.
 * Kann ueber Untertags verfuegen.
 *
 * @author Frederik Kirsch
 */
class XMLItem {

    private $name = null;

    private $data = null;

    public function __construct($name, $data) {
        $this->name = $name;

        if (is_array($data)) {
            $this->data = $data;
        } else {
            $this->data = array();
            $this->add($data);
        }
    }

    public function add($item) {
        array_push($this->data, $item);
    }

    public function get($name) {
        $result = null;
        if ($this->name == $name) {
            $result = $this->data;
            if (count($this->data) == 1) {
                $result = $this->data[0];
            }
        } else {
            if (count($this->data) > 0) {
                foreach ($this->data as $item) {
                    if ($item instanceof XMLItem) {
                        $result = $item->get($name);
                        if ($result != null) {
                            break;
                        }
                    }
                }
            }
        }
        return $result;
    }

    public function __toString() {
        $result = "<" . $this->name . ">";
        foreach ($this->data as $item) {
            $result .= $item;
        }
        $result .= "</" . $this->name . ">";
        return $result;
    }

}

/**
 * Class RSSItem
 * Roottag für ein RSS Dokument.
 *
 * @author Frederik Kirsch
 */
class RSSItem extends XMLItem {
    public function __construct($title, $link, $description, $pubDate) {
        parent::__construct("item", array());
        $this->add(new XMLItem("title", $title));
        $this->add(new XMLItem("link", $link));
        $this->add(new XMLItem("description", $description));
        $this->add(new XMLItem("pubDate", date(DATE_RSS, $pubDate)));
    }

}