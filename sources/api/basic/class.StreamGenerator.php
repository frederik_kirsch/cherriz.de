<?php

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");

/**
 * Erzeugt einen Stream zur Verwendung als Server Sent Event.
 * User: Frederik Kirsch
 */
class StreamGenerator
{

    /** Data Tag */
    const DATA = "data: ";

    /**  Event Typ */
    const EVENT = "event: ";

    /** Retry Tag */
    const RETRY = "retry: ";

    /** Default Retry Intervall in ms. */
    const RETRY_DEFAULT = 2;

    private $retry = null;

    private $event = null;

    private $properties = null;

    private $jsonGen = null;

    /**
     * Erzeugt den Generator.
     *
     * @param string $event Der Eventtyp.
     * @param int|null $retry Das Retryintervall.
     */
    public function __construct($event, $retry = StreamGenerator::RETRY_DEFAULT * 1000)
    {
        $this->event = $event;
        $this->retry = $retry;
        $this->properties = array();
        $this->jsonGen = new JSONGenerator();
    }

    /**
     * Ein Attribut das ausgegeben werden soll.
     *
     * @param string $name Attribut
     * @param string $value Wert.
     */
    public function addProperty($name, $value)
    {
        $this->jsonGen->addResultData($name, $value);
    }

    /**
     * Erzeugt den Stream.
     */
    public function generateStreamOutput()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        //header('Connection: keep-alive');
        ob_end_flush();

        while (true) {
            echo(StreamGenerator::EVENT . $this->event . "\r\n");
            echo(StreamGenerator::RETRY . $this->retry . "\r\n");
            echo(StreamGenerator::DATA . $this->jsonGen->getJSONString() . "\r\n\r\n");

            flush();
            sleep(StreamGenerator::RETRY_DEFAULT);
        }
    }

}