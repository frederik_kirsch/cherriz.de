<?php

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("basic/util/class.Settings.php");
LibImporter::import("basic/util/class.Verify.php");
LibImporter::import("basic/util/class.Picture.php");
LibImporter::import("service/internal/class.Query.php");
LibImporter::import("service/class.UserQuery.php");
LibImporter::import("api/basic/class.JSONGenerator.php");
LibImporter::import("service/class.Service.php");

// Hilfsklassen initialisieren
$VERIFY = new Verify();
$gen = new JSONGenerator();

//Anwender authentifizieren
if (Login::getInstance()->isLoggedIn()) {
	$search = Filter::text($_GET['search']);
	$max = Filter::text($_GET['max']);
	$start = Filter::text($_GET['start']);

	//Abrage aufbauen
	$query = new UserQuery();
	if ($search != "") {
		$query->setSearch($search);
	}
	if ($max == "") {
		$max = 5;
	}
	if ($start == "") {
		$start = 0;
	}
	$query->setRange($start, $max);

    //Daten abrufen
    $service = new Service();
    $result = $service->requestItems($query);
    $anz = $service->countItems($query);

	$items = array();
    foreach ($result as $hit) {
		array_push($items, array("id"       => $hit->ID,
		                         "nick"     => $hit->Nick,
		                         "vorname"  => $hit->Vorname,
		                         "nachname" => $hit->Nachname,
		                         "marker"   => $hit->Nick));
	}

    // JSON erzeugen
    if ($search != "") {
        $gen->addQueryData("search", $search);
    }
    $gen->addQueryData("start", $start);
    $gen->addQueryData("max", $max);
	$gen->addResultData("items", $items);
    $gen->addResultData("count", $anz);
} else {
	$gen->setError("Benutzer muss authentifiziert sein.");
}
//Ausgabe erzeugen.
$gen->generateJSONOutput();