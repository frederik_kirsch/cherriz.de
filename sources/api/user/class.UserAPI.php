<?php

LibImporter::import("api/basic/class.AbstractPropertyAPI.php");

/**
 * Class UserAPI
 * Verarbeitungslogik der Schnittstelle fuer eine Gruppe.
 *
 * @author Frederik Kirsch
 */
class UserAPI extends AbstractPropertyAPI {

	//Variablen
	private $user = null;

	private $value = null;

	//Util Klassen
	private $verify = null;

	private $db = null;

	public function __construct($user, $value) {
		$this->user = $user;
		$this->value = $value;

		$this->addQueryItem("user", $user);
		$this->addQueryItem("value", $value);

		$this->verify = new Verify();
		$this->db = DBConnect::getDBConnection();
	}

	/**
	 * Prueft ob der Authentifizierte Benutzer dem definierten Benutzer entspricht.
	 *
	 * @return bool|string <code>true</code> wenn Benutzer uebereinstimmen, sonst Fehlermeldung.
	 */
	private function isCurrentUser() {
		if (Login::getInstance()->getCurrentUserObject()->get(User::PROPERTY_ID) != $this->user) {
			return "Der angemeldete Benutzer entspricht nicht dem Benutzer der Abfrage.";
		}
		return true;
	}

	protected function verifyChangeAvatar() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		if ($this->value['tmp_name'] == "") {
			return "Die Dateigröße des gewählten Bildes ist zu groß.";
		}
		$validation = $this->verify->checkAvatar($this->value);
		if ($validation != Verify::$VALIDE) {
			return $validation;
		}
		return true;
	}

	protected function performChangeAvatar() {
		LibImporter::import("basic/util/class.Picture.php");
		$big = Picture::getResizedPictureAsSquare($this->value, 200, "image/jpg");
		$med = Picture::getResizedPictureAsSquare($this->value, 100, "image/jpg");
		$sma = Picture::getResizedPictureAsSquare($this->value, 20, "image/jpg");

		$dbBin = DBConnect::getDBConnection(DBConnect::CHARSET_UTF8_BIN);
		if ($dbBin->query("UPDATE Users SET Avatar_Big = '$big', Avatar_Medium = '$med', Avatar_Small = '$sma' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeSignatur() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$valResult = $this->verify->checkSignatur($this->value);
		if ($valResult != Verify::$VALIDE) {
			return $valResult;
		}
		return true;
	}

	protected function performChangeSignatur() {
		$val = $this->value;
		if (strlen($this->value) == 0) {
			$val = "N/A";
		}
		if ($this->db->query("UPDATE Users SET Signatur = '" . $val . "' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeName() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$names = $this->getNames($this->value);

		$validationf = $this->verify->checkName($names["Vorname"]);
		$validationl = $this->verify->checkName($names["Nachname"]);

		if ($validationf != Verify::$VALIDE && strlen($names["Vorname"]) != 0) {
			return $validationf;
		}
		if ($validationl != Verify::$VALIDE && strlen($names["Nachname"]) != 0) {
			return $validationl;
		}
		return true;
	}

	protected function performChangeName() {
		$names = $this->getNames($this->value);
		if ($this->db->query("UPDATE Users SET Vorname = '" . $names["Vorname"] . "', Nachname = '" . $names["Nachname"] . "' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	private function getNames($name) {
		$splitPos = strlen($name) - strpos(strrev(trim($name)), " ");
		if ($splitPos == -1) {
			$splitPos = strlen($name) - 1;
		}
		$result = array();
		$result["Vorname"] = trim(substr($name, 0, $splitPos));
		$result["Nachname"] = substr($name, $splitPos);
		return $result;
	}

	protected function verifyChangeGeburtsdatum() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$validation = $this->verify->checkBirthdate($this->value);
		if ($validation != Verify::$VALIDE && strlen(trim($this->value) != 0)) {
			return $validation;
		}
		return true;
	}

	protected function performChangeGeburtsdatum() {
		$bd = "0000-00-00";
		if (strlen(trim($this->value)) > 0) {
			$bd = Converter::dbDate($this->value);
		}

		if ($this->db->query("UPDATE Users SET Geburtsdatum = '$bd' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeEmail() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$validation = $this->verify->checkEmail($this->value);
		if ($validation != Verify::$VALIDE) {
			return $validation;
		}
		return true;
	}

	protected function performChangeEmail() {
		LibImporter::import("basic/util/internal/class.Message.php");
		LibImporter::import("basic/util/class.Mail.php");
		LibImporter::import("basic/util/class.Toolkit.php");
		LibImporter::import("sites/profile/interface.MenuProfile.php");

		$key = TOOLKIT::generateKey(TOOLKIT::VALUE_POOL, 20);
		$statement = "INSERT INTO Bestaetigungen (`Key`) VALUES ('$key')";
		if ($this->db->query($statement) === false) {
			return "Emailadresse konnte nicht geändert werden. Fehler bei der Datenbankkommunikation.";
		}

		//Bestätigungstoken verknüpfen
		$bestaetigungsID = $this->db->insert_id;
		$statement = "INSERT INTO Emailbestaetigung (User, Bestaetigung, Email) VALUES ('" . $this->user . "', '$bestaetigungsID', '" . $this->value . "')";
		if ($this->db->query($statement) === false) {
			//Reperaturen an DB durchführen
			$statement = "DELETE FROM Bestaetigungen WHERE ID = $bestaetigungsID";
			$this->db->query($statement);
			return "Emailadresse konnte nicht geändert werden. Fehler bei der Datenbankkommunikation. Rollback wurde durchgeführt.";
		}

		$mail = new Mail();
		$userObj = new User($this->user);
		$url = "http://www.cherriz.de/index.php?page=" . MenuProfile::DATA . "&key=$key";
		if ($mail->sendMail(Message::createInstance(Message::EMAILBESTAETIGUNG, array($userObj->get(User::PROPERTY_Nick), $url)), $this->value) === false) {
			$statement = "DELETE FROM Bestaetigungen WHERE ID = $bestaetigungsID";
			$this->db->query($statement);
			$statement = "DELETE FROM Emailbestaetigung WHERE User = $bestaetigungsID AND Bestaetigung = $bestaetigungsID";
			$this->db->query($statement);
			return "Emailadresse konnte nicht geändert werden. Fehler beim versenden der Bestätigungsemail. Rollback wurde durchgeführt.";
		}

		$this->addResultItem("info",
		                     "Bestätigungsemail wurde versand. Bitte bestaetige deine neue Emailadresse um sie für www.cherriz.de zu aktivieren.");
		return true;
	}

	protected function verifyConfirmMail() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$query = "SELECT COUNT(*) AS Anzahl FROM Emailbestaetigung EB INNER JOIN Bestaetigungen B ON EB.Bestaetigung = B.ID WHERE B.Key = '" . $this->value . "' AND B.Used IS NULL";
		$bestaetigung = $this->db->query($query)->fetch_object();

		if ($bestaetigung->Anzahl == 0) {
			return "Fehler: Die Emailadresse wurde bereits geändert.";
		}
		return true;
	}

	protected function performConfirmMail() {
		$query = "UPDATE Users U
                   INNER JOIN Emailbestaetigung EB
                      ON U.ID = EB.User
                   INNER JOIN Bestaetigungen B
                      ON B.ID = EB.Bestaetigung
                     SET U.Email = EB.Email,
                         B.Used = NOW()
                   WHERE B.Used IS NULL AND B.Key = '" . $this->value . "'";

		if ($this->db->query($query)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeTwitter() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$validation = $this->verify->checkTwitter($this->value);
		if ($validation != Verify::$VALIDE && strlen($this->value) != 0) {
			return $validation;
		}
		return true;
	}

	protected function performChangeTwitter() {
		if ($this->db->query("UPDATE Users SET Twitter = '" . $this->value . "' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeTelefon() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$validation = $this->verify->checkPhoneNumber($this->value);
		if ($validation != Verify::$VALIDE && strlen($this->value) != 0) {
			return $validation;
		}
		return true;
	}

	protected function performChangeTelefon() {
		if ($this->db->query("UPDATE Users SET Telefon = '" . $this->value . "' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeHandy() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$validation = $this->verify->checkPhoneNumber($this->value);
		if ($validation != Verify::$VALIDE && strlen($this->value) != 0) {
			return $validation;
		}
		return true;
	}

	protected function performChangeHandy() {
		if ($this->db->query("UPDATE Users SET Handy = '" . $this->value . "' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeStrasse() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$validation = $this->verify->checkStreet($this->value);
		if ($validation != Verify::$VALIDE && strlen($this->value) != 0) {
			return $validation;
		}
		return true;
	}

	protected function performChangeStrasse() {
		if ($this->db->query("UPDATE Users SET Strasse = '" . $this->value . "' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeOrt() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$ort = $this->getOrt($this->value);
		$validationPLZ = $this->verify->checkZIP($ort["PLZ"]);
		$validationOrt = $this->verify->checkCity($ort["City"]);

		if ($validationPLZ != Verify::$VALIDE && strlen($ort["PLZ"]) != 0) {
			return $validationPLZ;
		} elseif ($validationOrt != Verify::$VALIDE && strlen($ort["City"]) != 0) {
			return $validationOrt;
		}
		return true;
	}

	protected function performChangeOrt() {
		$ort = $this->getOrt($this->value);
		if ($this->db->query("UPDATE Users SET PLZ = '" . $ort["PLZ"] . "', Ort = '" . $ort["City"] . "' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	private function getOrt($ort) {
		$result = array();
		$result["PLZ"] = "";
		$result["City"] = "";
		$value = trim($ort);
		if (preg_match(' /^\d{5}\s * [\w\säüößÄÜÖ] * $/', $value)) {
			$result["PLZ"] = substr($value, 0, 5);
			$result["City"] = trim(substr($value, 5));
		} elseif (strlen($value) > 0) {
			$result["City"] = $value;
		}
		return $result;
	}

	protected function verifyChangeUseExperimental() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		if ($this->boolToInt($this->value) == null) {
			return "Kein gültiger Wert.";
		}
		return true;
	}

	protected function performChangeUseExperimental() {
		if ($this->db->query("UPDATE Users SET UseExperimental = " . $this->boolToInt($this->value) . " WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	private function boolToInt($bool) {
		if ($bool == "true") {
			return "1";
		} elseif ($bool == "false") {
			return "0";
		}
		return null;
	}

	protected function verifyChangeNotificationTypeEmail() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		if ($this->boolToInt($this->value) == null) {
			return "Kein gültiger Wert.";
		}
		return true;
	}

	protected function performChangeNotificationTypeEmail() {
		if ($this->db->query("UPDATE Users SET NotificationTypeEmail = " . $this->boolToInt($this->value) . " WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeNotificationTypeTwitter() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		if ($this->boolToInt($this->value) == null) {
			return "Kein gültiger Wert.";
		}
		return true;
	}

	protected function performChangeNotificationTypeTwitter() {
		if ($this->db->query("UPDATE Users SET NotificationTypeTwitter = " . $this->boolToInt($this->value) . " WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeNotificationNews() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		if ($this->boolToInt($this->value) == null) {
			return "Kein gültiger Wert.";
		}
		return true;
	}

	protected function performChangeNotificationNews() {
		if ($this->db->query("UPDATE Users SET NotificationNews = " . $this->boolToInt($this->value) . " WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeNotificationComment() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		if ($this->boolToInt($this->value) == null) {
			return "Kein gültiger Wert.";
		}
		return true;
	}

	protected function performChangeNotificationComment() {
		if ($this->db->query("UPDATE Users SET NotificationComment = " . $this->boolToInt($this->value) . " WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangeNotificationThreadpost() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		if ($this->boolToInt($this->value) == null) {
			return "Kein gültiger Wert.";
		}
		return true;
	}

	protected function performChangeNotificationThreadpost() {
		if ($this->db->query("UPDATE Users SET NotificationThreadpost = " . $this->boolToInt($this->value) . " WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangePrivatsphaere() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$validation = $this->verify->checkPrivatsphaere($this->value);
		if ($validation != Verify::$VALIDE && strlen($this->value) != 0) {
			return $validation;
		}
		return true;
	}

	protected function performChangePrivatsphaere() {
		if ($this->db->query("UPDATE Users SET Privatsphaere = '" . $this->value . "' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyChangePassword() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		$validation = $this->verify->checkPasswort($this->value);
		if ($validation != Verify::$VALIDE) {
			return $validation;
		}
		return true;
	}

	protected function performChangePassword() {
		if ($this->db->query("UPDATE Users SET Passwort = '" . md5($this->value) . "' WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyGetGroups() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		return true;
	}

	protected function performGetGroups() {
		$userObj = new User($this->user);
		$groups = $userObj->getGroups();
		$groupArray = array();
		foreach ($groups as $group) {
			array_push($groupArray, array("id" => $group->getID(), "name" => $group->getName()));
		}
		$this->addResultItem("groups", $groupArray);
		return true;
	}

	protected function verifyGetRights() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser === true) {
			return true;
		} else if (Login::getInstance()->getCurrentUserObject()->hasRight(Recht::CHANGE_RIGHTS)) {
			return true;
		}
		return $isCurUser;
	}

	protected function performGetRights() {
		$userObj = new User($this->user);
		$this->addResultItem("rechte", $userObj->getRights());
		return true;
	}

	protected function verifyRemoveRight() {
		if (Login::getInstance()->getCurrentUserObject()->hasRight(Recht::CHANGE_RIGHTS)) {
			return true;
		}
		return "Erforderlichen Rechte sind nicht vorhanden.";
	}

	protected function performRemoveRight() {
		$userObj = new User($this->user);
		return $userObj->removeRight($this->value);
	}

	protected function verifyAddRight() {
		if (Login::getInstance()->getCurrentUserObject()->hasRight(Recht::CHANGE_RIGHTS)) {
			return true;
		}
		return "Erforderlichen Rechte sind nicht vorhanden.";
	}

	protected function performAddRight() {
		$userObj = new User($this->user);
		return $userObj->addRight($this->value);
	}

	protected function verifyChangeCookieInfo() {
		$isCurUser = $this->isCurrentUser();
		if ($isCurUser !== true) {
			return $isCurUser;
		}
		if ($this->boolToInt($this->value) == null) {
			return "Kein gültiger Wert.";
		}
		return true;
	}

	protected function performChangeCookieInfo() {
		if ($this->db->query("UPDATE Users SET ReadCookieInfo = " . $this->boolToInt($this->value) . " WHERE ID = " . $this->user)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

}