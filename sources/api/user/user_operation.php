<?php
/**
 * Schnittstelle fuer Aktionen bezogen auf einen Benutzer.
 */
//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/user/class.UserAPI.php");
LibImporter::import("basic/entity/class.User.php");
LibImporter::import("basic/util/class.Verify.php");
LibImporter::import("basic/util/class.Settings.php");

$action = Filter::text($_REQUEST['action']);
$user = Filter::text($_REQUEST['user']);
$value = Filter::text($_REQUEST['value']);
if($action == "changeAvatar"){
    $value = Filter::file($_FILES['value']);
}

$userAPI = new UserAPI($user, $value);
$userAPI->perform($action);
$userAPI->createOutput();