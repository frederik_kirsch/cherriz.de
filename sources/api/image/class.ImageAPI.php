<?php

LibImporter::import("api/basic/class.AbstractPropertyAPI.php");
LibImporter::import("sites/interface.Access.php");
LibImporter::import("basic/util/class.Settings.php");
LibImporter::import("basic/util/class.Verify.php");
LibImporter::import("basic/util/class.Picture.php");

/**
 * Class ImageAPI
 * Verarbeitungslogik für Bilder.
 *
 * @author Frederik Kirsch
 */
class ImageAPI extends AbstractPropertyAPI {

	const PARA_PICTURE = "picture";

	const PARA_TAGS = "tags";

	const PARA_VISIBILITY = "visibility";

	const PARA_GROUPS = "groups";

	const MAX_HEIGHT = 5000;

	private $para;

	private $db;

	private $dbBin;

	private $user;

	private $verify;

	public function __construct($parameterData) {
		$this->para = $parameterData;
		$this->db = DBConnect::getDBConnection();
		$this->dbBin = DBConnect::getDBConnection(DBConnect::CHARSET_UTF8_BIN);
		$this->user = Login::getInstance()->getCurrentUserObject();
		$this->verify = new Verify();

		foreach ($this->para as $key => $value) {
			$this->addQueryItem($key, $value);
		}
	}

	/**
	 * Prueft ob der aktuelle Nutzer ein Bild hochladen darf.
	 *
	 * @return bool|string <tt>true</tt> bei Erfolg, sonst Fehlertext.
	 */
	protected function verifyUpload() {
		if (!Login::getInstance()->isLoggedIn()) {
			return "Zum Upload eines Bildes müssen Sie angemeldet sein.";
		}

		// Parameter inhaltlich validieren
		$tags = Filter::tags($this->para[ImageAPI::PARA_TAGS]);
		$validation = $this->verify->checkPicture($this->para[ImageAPI::PARA_PICTURE], $tags);
		if ($validation != Verify::$VALIDE) {
			return $validation;
		}

		// Zugriff validieren
		if ($this->para[ImageAPI::PARA_VISIBILITY] == Access::TYPE_SPECIFIC) {
			if (strlen(trim($this->para[ImageAPI::PARA_GROUPS])) == 0) {
				return "Es muss mindestens eine Gruppe ausgewählt werden.";
			}
		}

		return true;
	}

	/**
	 * Fuehert den Upload eines Bildes durch.
	 *
	 * @return bool|string <tt>true</tt> bei Erfolg, sonst Fehlertext.
	 */
	protected function performUpload() {
		$tags = Filter::tags($this->para[ImageAPI::PARA_TAGS] . ", " . $this->user->get(User::PROPERTY_Nick));
		$picture = $this->para[ImageAPI::PARA_PICTURE];
		$pictureStatus = "Standard";
		$publicDBVal = 0;
		if ($this->para[ImageAPI::PARA_VISIBILITY] == Access::TYPE_PUBLIC) {
			$publicDBVal = 1;
		}

		//Daten speichern
		$resPictureSmall = Picture::getResizedPictureKeepingAspect($picture, 100, 100, $picture['type']);
		$resPictureStandard = Picture::getResizedPictureKeepingAspect($picture, 600, ImageAPI::MAX_HEIGHT, $picture['type']);
		$resPictureBig = Picture::getCompressedPicture($picture);
		$query = "INSERT INTO Picture
							  (User, Status, Tags, Date, Data_Type, Public)
					   VALUES (" . $this->user->get(User::PROPERTY_ID) . ",
							  (SELECT ID FROM Picture_Status WHERE Status = '$pictureStatus'),
							  '$tags',
							  NOW(),
							  (SELECT ID FROM Picture_Data_Type WHERE Data_Type = '" . $picture['type'] . "'),
							  $publicDBVal)";
		$result = $this->db->query($query);
		$imageID = $this->db->insert_id;

		$query = "UPDATE Picture SET Data_Small = '$resPictureSmall', Data_Standard = '$resPictureStandard', Data_Big = '$resPictureBig' WHERE ID = $imageID";
		$this->dbBin->query($query);

		$groupArray = array();
		if ($this->para[ImageAPI::PARA_VISIBILITY] != Access::TYPE_PUBLIC) {
			if ($this->para[ImageAPI::PARA_VISIBILITY] == Access::TYPE_MYGROUPS) {
				foreach ($this->user->getGroups() as $group) {
					array_push($groupArray, $group->getID());
				}
			} else {
				$groupArray = explode(",", $this->para[ImageAPI::PARA_GROUPS]);
			}
			foreach ($groupArray as $grpID) {
				$this->db->query("INSERT INTO Picture_Allowed_Groups (`Picture`, `Group`) VALUES ($imageID, $grpID)");
			}
		}

		if ($result) {
			$this->addResultItem("success", "Grafik erfolgreich gespeichert.");
			$this->addResultItem("insert_id", $imageID);
			$this->addResultItem("user_id", $this->user->get(User::PROPERTY_ID));
			$this->addResultItem("picture_status", $pictureStatus);
			$this->addResultItem("tags", $tags);
			$this->addResultItem("date", time());
			$this->addResultItem("visibility", $this->para[ImageAPI::PARA_VISIBILITY]);
			if ($this->para[ImageAPI::PARA_VISIBILITY] != Access::TYPE_PUBLIC) {
				$this->addResultItem("groups", $groupArray);
			}
			$this->addResultItem("picture_type", $picture['type']);
			return true;
		} else {
			return "Fehler bei der Datenbankkommunikation.";
		}
	}

}