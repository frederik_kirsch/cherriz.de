<?php

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.PictureQuery.php");
LibImporter::import("basic/util/class.Settings.php");
LibImporter::import("basic/util/class.Verify.php");
LibImporter::import("basic/util/class.Picture.php");

//Konstanten
define("ORDER_BY_RELEVANCE", "relevance");
define("ORDER_BY_DATE", "date");
define("SORT_ORDER_DESC", "desc");
define("SORT_ORDER_ASC", "asc");

// Hilfsklassen initialisieren
$VERIFY = new Verify();
$gen = new JSONGenerator();

// Parameter abrufen
$search = FILTER::searchQuery($_GET['search']);
$start = Filter::text($_GET['start']);
$max = Filter::text($_GET['max']);
$author = Filter::text($_GET['author']);

$orderBy = Filter::text($_GET['orderBy']);
$sortOrder = Filter::text($_GET['sortOrder']);

// Parameter aufbereiten
if ($start == "") {
	$start = 0;
}
if ($max == "") {
	$max = 5;
}

//Anwender authentifizieren
if (Login::getInstance()->isLoggedIn()) {
	//Abrage aufbauen
	$picQuery = new PictureQuery();
	$picQuery->setStatus("Standard");

	if (count($search) > 0) {
		$picQuery->setNeedle($search);
		$gen->addQueryData("search", implode(", ", $search));
	}

	$picQuery->setRange($start, $max);
	$gen->addQueryData("start", $start);
	$gen->addQueryData("max", $max);

	if ($author != "") {
		$picQuery->setAuthor($author);
		$gen->addQueryData("author", $author);
	}

	if ($orderBy == ORDER_BY_RELEVANCE) {
		$gen->addQueryData("orderBy", ORDER_BY_RELEVANCE);
		if ($sortOrder == SORT_ORDER_ASC) {
			$gen->addQueryData("sortOrder", SORT_ORDER_ASC);
			$picQuery->orderByRelevance(Query::SORT_ORDER_ASC);
		} else {
			$gen->addQueryData("sortOrder", SORT_ORDER_DESC);
			$picQuery->orderByRelevance(Query::SORT_ORDER_DESC);
		}
	} elseif ($orderBy == ORDER_BY_DATE) {
		$gen->addQueryData("orderBy", ORDER_BY_DATE);
		if ($sortOrder == SORT_ORDER_ASC) {
			$gen->addQueryData("sortOrder", SORT_ORDER_ASC);
			$picQuery->orderByDate(Query::SORT_ORDER_ASC);
		} else {
			$gen->addQueryData("sortOrder", SORT_ORDER_DESC);
			$picQuery->orderByDate(Query::SORT_ORDER_DESC);
		}
	}

	//Abfrage ausfuehren
	$service = new Service();
	$images = $service->requestItems($picQuery);

	//JSON erzeugen
	$items = array();
	$maxRel = null;
	foreach ($images as $item) {
		if ($maxRel == null) {
			$maxRel = $item->Relevance + 1;
		}
		array_push($items, array("id"       => $item->ID,
		                         "userid"   => $item->User_ID,
		                         "usernick" => $item->User_Nick,
		                         "tags"     => $item->Tags,
		                         "date"     => $item->Date,
		                         "quality"  => ($item->Relevance + 1 / $maxRel) * 100));
	}
	$gen->addResultData("items", $items);
} else {
	$gen->setError("Sie müssen angemeldet sein, um nach bildern zu suchen.");
}

$gen->generateJSONOutput();