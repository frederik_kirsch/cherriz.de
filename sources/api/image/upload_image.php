<?php
/**
 * Schnittstelle fuer Aktionen zu Bildern.
 */
//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/image/class.ImageAPI.php");

//Daten abrufen
$param = array();
$action = $_REQUEST['action'];
$param["picture"] = Filter::file($_FILES['picture']);
$param["tags"] = $_POST['tags'];
$param["visibility"] = $_POST['visibility'];
$param["groups"] = $_POST['groups'];

$imageAPI = new ImageAPI($param);
$imageAPI->perform($action);
$imageAPI->createOutput();