<?php

//Standard API Initialisieren
require '../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../");
LibImporter::import("basic/globals.php");
LibImporter::import("basic/entity/interface.Recht.php");
LibImporter::import("basic/util/class.Settings.php");
LibImporter::import("basic/entity/class.User.php");

//Parameter abrufen
$type = Filter::text($_REQUEST['type']);
$id = Filter::text($_REQUEST['id']);

switch ($type) {
	case "news":
        LibImporter::import("basic/entity/class.NewsItem.php");
		$news = NewsItem::createNewsFromDB($id);	
		$news->showSmall();	
		break;
	case "newsControl":
        LibImporter::import("basic/entity/class.NewsItem.php");
		$news = NewsItem::createNewsFromDB($id);	
		$news->showControl();	
		break;
	case "comment":
        LibImporter::import("basic/entity/class.NewsItem.php");
		LibImporter::import("basic/entity/class.Kommentar.php");
		$kommentar = new Kommentar($id);
		
		//Berechtigung prüfen
		$hasAccess = false;
		$login = Login::getInstance();
		$news = NewsItem::createNewsFromDB($kommentar->get(Kommentar::ATTRIBUTE_NEWS));
		if($news->get(NewsItem::ATTRIBUTE_PUBLIC)===true && $news->get(NewsItem::ATTRIBUTE_PUBLISHED)===true){
			$hasAccess = true;
		}else if($login->isLoggedIn() && $news->get(NewsItem::ATTRIBUTE_PUBLISHED)===true){
			$user = Login::getInstance()->getCurrentUserObject();
			$newsGroups = $news->get(NewsItem::ATTRIBUTE_GROUPS);
			$userGroups = $user->getGroups();
			foreach ($userGroups as $grp) {
				if(in_array($grp->getID(), $newsGroups)){
					$hasAccess = true;
					break;
				}
			}
		}
		if($hasAccess === true){
			echo ($kommentar);
		}else{
			echo ("Fehler: Sie besitzen nicht die notwendigen Berechtigungen für diese Abfrage.");
		}
		break;
    case "forum":
        LibImporter::import("basic/entity/class.ForumItem.php");
        $forum = ForumItem::createFromDB($id);
        $forum->showSmall();
        break;
    case "thread":
        LibImporter::import("basic/entity/class.ThreadItem.php");
        LibImporter::import("basic/entity/class.ThreadPost.php");
        $thread = ThreadItem::createFromDB($id);
        $thread->showSmall();
        break;
	case "threadpost":
		LibImporter::import("basic/entity/class.ThreadPost.php");
		$post = ThreadPost::createFromDB($id);
		echo($post->toHTML());
		break;
	case "threadpoststandalone":
        LibImporter::import("basic/entity/class.ThreadPost.php");
        $post = ThreadPost::createFromDB($id);
        echo($post->toHTMLStandalone());
        break;
	case "userSmall":
		$user = new User($id);
		echo($user->drawUserSmall());
		break;
}