<?php
/**
 * Erzeugt ein XML Dokument der letzten News.
 */

//Standard API Initialisieren
require '../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.XMLGenerator.php");

//Konstanten
define("PARA_TYPE", "type");
define("TYPE_NEWS", "News");

//Parameter aufbereiten
$type = FILTER::text($_REQUEST[PARA_TYPE]);
$title = "Cherriz.de - ";
$link = "http://cherriz.de/index.php?page=";
$description = "cherriz.de - the unique community blog";
$itemArray = array();

switch ($type) {
	case TYPE_NEWS:
		LibImporter::import("service/class.Service.php");
		LibImporter::import("service/class.NewsQuery.php");
		LibImporter::import("basic/entity/class.NewsItem.php");
		LibImporter::import("basic/entity/interface.Recht.php");
		LibImporter::import("basic/entity/class.User.php");
		$title .= "News";
		$link .= Menu::NEWS;

		$query = new NewsQuery();
		$query->setPublic(true);
		$query->setPublished(true);
		$query->setReleased();
		$query->setRange(0, 10);
		$service = new Service();
		$news = $service->requestItems($query);

		foreach ($news as $item) {
			$item = NewsItem::createNewsFromDB($item->ID);
			$nTitle = $item->get(NewsItem::ATTRIBUTE_TITLE);
			$nLink = "http://cherriz.de/index.php?page=" . MenuNews::SINGLE . "&amp;news=" . $item->get(NewsItem::ATTRIBUTE_ID);
			$nText = Converter::bbToXml($item->get(NewsItem::ATTRIBUTE_TEXT));
			$nDate = $item->get(NewsItem::ATTRIBUTE_DATE);
			array_push($itemArray, new RSSItem($nTitle, $nLink, $nText, $nDate));
		}
		break;
}
$generator = new XMLGenerator();
$generator->generateRSSOutput($title, $link, $description, $itemArray);