<?php

LibImporter::import("api/basic/class.AbstractPropertyAPI.php");
//Zusätzlich Imports
LibImporter::import("basic/entity/class.User.php");
LibImporter::import("basic/entity/class.NewsItem.php");
LibImporter::import("basic/util/class.Verify.php");
LibImporter::import("basic/util/class.Settings.php");

/**
 * Class NewsAPI
 * Verarbeitungslogik der Schnittstelle fuer eine News.
 *
 * @author Frederik Kirsch
 */
class NewsAPI extends AbstractPropertyAPI {

	private $news = null;

	/** @var NewsItem */
	private $newsObj = null;

	private $db = null;

	public function __construct($news) {
		$this->db = DBConnect::getDBConnection();
		$this->news = $news;
		$this->addQueryItem("news", $news);
	}

	/**
	 * Prueft ob die News veroeffentlicht werden darf. Dies erfordert die entsprechenden Rechte und es duefen nur eigene News veroeffentlicht werden.
	 *
	 * @return bool|string <code>true</code> oder eine entsprechende Fehlermeldung.
	 */
	protected function verifyMakePublic() {
		$user = Login::getInstance()->getCurrentUserObject();
		if (!Login::getInstance()->isLoggedIn() || !$user->hasRight(Recht::WRITE_NEWS)) {
			return "Fehlende Berechtigung.";
		}
		$this->newsObj = NewsItem::createNewsFromDB($this->news);
		if ($this->newsObj->get(NewsItem::ATTRIBUTE_AUTHOR)->get(User::PROPERTY_ID) != Login::getInstance()->getCurrentUserObject()->get(User::PROPERTY_ID)) {
			return "Es duerfen nur eigene News bearbeitet werden";
		}
		return true;
	}

	/**
	 * Veroeffentlicht eine News.
	 * @return bool <code>true</code>
	 */
	protected function performMakePublic() {
		$this->newsObj->setPublished();
		$this->newsObj->save();
		return true;
	}

}