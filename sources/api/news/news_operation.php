<?php
/**
 * Schnittstelle fuer Aktionen bezogen auf eine News.
 */

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/news/class.NewsAPI.php");

$action = Filter::text($_REQUEST['action']);
$news = Filter::text($_REQUEST['news']);

$newsAPI = new NewsAPI($news);
$newsAPI->perform($action);
$newsAPI->createOutput();