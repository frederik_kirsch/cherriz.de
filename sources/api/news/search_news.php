<?php

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.NewsQuery.php");
LibImporter::import("basic/entity/class.NewsItem.php");
LibImporter::import("basic/entity/interface.Recht.php");
LibImporter::import("basic/entity/class.User.php");
LibImporter::import("basic/entity/class.Group.php");

//Parameter abrufen
$start = Filter::text($_GET['start']);
$max = Filter::text($_GET['max']);
$author = Filter::text($_GET['author']);
$genre = $_GET['genre'];
$released = $_GET['released'];
$entwurf = $_GET['entwurf'];

//Query aufbauen
$query = null;
if ($author != "") {
	$query = new NewsQuery(NewsQuery::TYPE_ID);
	$query->setAuthor($author);
} else {
	$query = new NewsQuery();
	$query->setPublished(true);
	$query->setReleased();
}

if($entwurf == "yes"){
	$query->setPublished(false);
}

if($released == "notYet"){
	$query->setNotReleasedYet();
}

if (Login::getInstance()->isLoggedIn()) {
	$query->setUser(Login::getInstance()->getCurrentUserObject());
} else {
	$query->setReleased();
	$query->setPublic(true);
}
$query->setRange($start, $max);
if($genre != ""){
	$query->setGenre($genre);
}

//Daten abrufen
$service = new Service();
$news = $service->requestItems($query);
$anz = $service->countItems($query);

//JSON erzeugen
$generator = new JSONGenerator();
$generator->addQueryData("start", $start);
$generator->addQueryData("max", $max);
$generator->addQueryData("author", $author);

$items = array();
foreach ($news as $item) {
	array_push($items, array("id" => $item->ID, "marker" => strtotime($item->Date)));
}
$generator->addResultData("items", $items);
$generator->addResultData("count", $anz);

//XML ausgeben
$generator->generateJSONOutput();