<?php

LibImporter::import("api/basic/class.AbstractPropertyAPI.php");

/**
 * Class SettingsAPI
 * API zur Aenderung von Webseiteneinstellungen.
 */
class SettingsAPI extends AbstractPropertyAPI {

	private $db;

	private $para;

	public function __construct($para) {
		$this->db = DBConnect::getDBConnection();
		$this->para = $para;
	}

	/**
	 * @return bool Ob der Nutzer alle NewsGenres erfragen darf.
	 */
	protected function verifyGetAllGenres() {
		return true;
	}

	/**
	 * Liest alle Genres aus.
	 * @return bool Ob die Anfrage erfoglreich war.
	 */
	protected function performGetAllGenres() {
		$result = $this->db->query("SELECT ID, Genre FROM Genre");
		$genres = array();
		while ($obj = $result->fetch_object()) {
			array_push($genres, array("id" => $obj->ID, "genre" => $obj->Genre));
		}
		$this->addResultItem("genres", $genres);
		return true;
	}

	/**
	 * @return bool Ob der Nutzer die existierende Rechte abfragen darf.
	 */
	protected function verifyGetAllRights() {
		$userObj = Login::getInstance()->getCurrentUserObject();
		if ($userObj->hasRight(Recht::CHANGE_RIGHTS)) {
			return true;
		}
		return "Fehlende Berechtigung fuer diese Abfrage.";
	}

	/**
	 * Liest alle bestehenden Berechtigungen aus.
	 * @return bool Ob die Anfrage erfoglreich war.
	 */
	protected function performGetAllRights() {
		$result = $this->db->query("SELECT ID, Recht FROM Rechte ORDER BY Recht");
		$rechte = array();
		while ($obj = $result->fetch_object()) {
			array_push($rechte, array("id" => $obj->ID, "recht" => $obj->Recht));
		}
		$this->addResultItem("rechte", $rechte);
		return true;
	}

	/**
	 * @return bool|string Ob der Nutzer die Wartungsseite aktivieren darf.
	 */
	protected function verifyChangeWartung() {
		$userObj = Login::getInstance()->getCurrentUserObject();
		if (!$userObj->hasRight(Recht::ACCESS_ADMIN) || !$userObj->hasRight(Recht::CONSTRUCTION_MODE)) {
			return "Fehlende Berechtigung fuer diese Aktion.";
		}
		$maintenance = $this->para["wartung"];
		if ($maintenance != "true" && $maintenance != "false") {
			return "Ungueltiger Wert \"" . $maintenance . "\" fuer Parameter \"maintenance\".";
		}
		return true;
	}

	/**
	 * Aktiviert die Wartungsseite
	 * @return bool|string
	 */
	protected function performChangeWartung() {
		return $this->performChangeAdminText("wartung", Settings::PROPERTY_UNDER_CONSTRUCTION);
	}

	/**
	 * @return bool|string Ob der Nutzer den Text der Wartungsseite aendern darf.
	 */
	protected function verifyChangeWartungText() {
		return $this->validateAdminText("text", Recht::CONSTRUCTION_MODE);
	}

	/**
	 * Aendert den Text der Wartungsseite.
	 * @return bool|string
	 */
	protected function performChangeWartungText() {
		return $this->performChangeAdminText("text", Settings::PROPERTY_UNDER_CONSTRUCTION_CONTENT);
	}

	/**
	 * @return bool|string Ob der Nutzer den Text des Impressumsaendern darf.
	 */
	protected function verifyChangeImpressum() {
		return $this->validateAdminText("text", Recht::CHANGE_IMPRESSUM);
	}

	/**
	 * Aendert den Text des Impressums.
	 * @return bool|string
	 */
	protected function performChangeImpressum() {
		return $this->performChangeAdminText("text", Settings::PROPERTY_IMPRESSUM_CONTENT);
	}

	/**
	 * @return bool|string Ob der Nutzer den Text der Datenschutzaerklaerung aendern darf.
	 */
	protected function verifyChangeDatenschutz() {
		return $this->validateAdminText("text", Recht::CHANGE_DATENSCHUTZ);
	}

	/**
	 * Aendert den Text der Datenschutzaerklaerung.
	 * @return bool|string
	 */
	protected function performChangeDatenschutz() {
		return $this->performChangeAdminText("text", Settings::PROPERTY_DATENSCHUTZ_REGISTRATION);
	}

	/**
	 * @param string $para  Die Property.
	 * @param string $right Das notwendige Recht.
	 *
	 * @return bool|string Ob der Nutzer die Property aendern darf.
	 */
	private function validateAdminText($para, $right) {
		$userObj = Login::getInstance()->getCurrentUserObject();
		if (!$userObj->hasRight(Recht::ACCESS_ADMIN) || !$userObj->hasRight($right)) {
			return "Fehlende Berechtigung fuer diese Aktion.";
		}
		$text = $this->para[$para];
		if ($text == "") {
			return "Ungueltiger Wert \"" . $text . "\" fuer Parameter \"$para\".";
		}
		return true;
	}

	/**
	 * Aendert die definierte Property.
	 *
	 * @param string $para     Gibt an welcher Parameter den neuen Wert enthaelt.
	 * @param string $property Die zu aendernde Property.
	 *
	 * @return bool|string
	 */
	private function performChangeAdminText($para, $property) {
		$settings = Settings::getInstance();
		$text = Filter::text($this->para[$para]);
		if ($settings->setProperty($property, $text)) {
			return true;
		}
		return "Fehler beim Schreiben der Property.";
	}

}