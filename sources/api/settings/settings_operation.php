<?php
/** Schnittstelle fuer Aktionen bezogen auf die Konfiguration der Webseite. */

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/settings/class.SettingsAPI.php");

$action = $_REQUEST['action'];
$para = $_REQUEST;

$api = new SettingsAPI($para);
$api->perform($action);
$api->createOutput();