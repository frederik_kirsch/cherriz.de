<?php
/**
 * Schnittstelle fuer Aktionen bezogen auf einen Thread.
 */

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/thread/class.ThreadAPI.php");
LibImporter::import("basic/util/class.Settings.php");

$action = FILTER::text($_REQUEST['action']);
$thread = FILTER::text($_REQUEST['thread']);
$text = FILTER::text($_REQUEST['text']);

$threadAPI = new ThreadAPI($thread, $text);
$threadAPI->perform($action);
$threadAPI->createOutput();