<?php

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.ThreadQuery.php");
LibImporter::import("basic/entity/class.ThreadItem.php");
LibImporter::import("basic/entity/class.User.php");

//Parameter abrufen
$start = Filter::text($_REQUEST['start']);
$max = Filter::text($_REQUEST['max']);
$forum = Filter::text($_REQUEST['forum']);

//Quera aufbauen
$query = new ThreadQuery($forum);
$query->setDeleted(false);
if (Login::getInstance()->isLoggedIn()) {
	$query->setUser(Login::getInstance()->getCurrentUserObject());
} else {
	$query->setPublic(true);
}
$query->setRange($start, $max);

//Daten abrufen
$service = new Service();
$news = $service->requestItems($query);
$anz = $service->countItems($query);
//XML erzeugen
$gen = new JSONGenerator();
$gen->addQueryData("start", $start);
$gen->addQueryData("max", $max);
$gen->addQueryData("forum", $forum);

$items = array();
foreach ($news as $item) {
	array_push($items, array("id" => $item->ID, "marker" => $item->Title));
}
$gen->addResultData("items", $items);
$gen->addResultData("count", $anz);

$gen->generateJSONOutput();