<?php

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.ThreadPostQuery.php");
LibImporter::import("basic/entity/class.ThreadPost.php");
LibImporter::import("basic/entity/class.User.php");

//Parameter abrufen
$start = FILTER::text($_REQUEST['start']);
$max = FILTER::text($_REQUEST['max']);
$thread = FILTER::text($_REQUEST['thread']);

//Quary aufbauen
$service = new Service();
$query = new ThreadPostQuery($thread);
if (Login::getInstance()->isLoggedIn()) {
	$query->setUser(Login::getInstance()->getCurrentUserObject());
} else {
	$query->setPublic(true);
}
$query->setRange($start, $max);

//Abfragen ausführen
$anz = $service->countItems($query);
$posts = $service->requestItems($query);

//Ergebnis aufbereiten und ausgeben
$items = array();
foreach ($posts as $item) {
	array_push($items, array("id" => $item->ID, "marker" => $item->ID));
}
$gen = new JSONGenerator();
$gen->addQueryData("start", $start);
$gen->addQueryData("max", $max);
$gen->addQueryData("thread", $thread);
$gen->addResultData("count", $anz);
$gen->addResultData("items", $items);

$gen->generateJSONOutput();