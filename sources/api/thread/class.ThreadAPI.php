<?php

LibImporter::import("api/basic/class.AbstractPropertyAPI.php");
//Zusätzlich Imports
LibImporter::import("basic/entity/class.User.php");
LibImporter::import("basic/entity/class.ThreadItem.php");
LibImporter::import("basic/entity/class.ThreadPost.php");
LibImporter::import("basic/util/class.Verify.php");
LibImporter::import("basic/util/class.Settings.php");

/**
 * Class ThreadAPI
 * Verarbeitungslogik der Schnittstelle fuer einen Thread.
 *
 * @author Frederik Kirsch
 */
class ThreadAPI extends AbstractPropertyAPI {

	private $thread = null;

	private $text = null;

	private $db = null;

	private $login = null;

	public function __construct($thread, $text) {
		$this->db = DBConnect::getDBConnection();
		$this->login = Login::getInstance();
		$this->thread = $thread;
		$this->text = $text;

		$this->addQueryItem("thread", $thread);
		if ($text != null && sizeof($text) > 0) ;
		{
			$this->addQueryItem("text", $text);
		}
	}

	protected function verifyDelete() {
		$user = Login::getInstance()->getCurrentUserObject();
		if ($this->login->isLoggedIn() && $user->hasRight(Recht::FORUM_ADMIN)) {
			return true;
		}
		$threadObj = ThreadItem::createFromDB($this->thread);
		if ($this->login->isLoggedIn() && $user->get(User::PROPERTY_ID) == $threadObj->getCreator()) {
			return true;
		}
		return "Fehlende Berechtigung.";
	}

	protected function performDelete() {
		if ($this->db->query("UPDATE Thread SET Deleted = 1 WHERE ID = " . $this->thread)) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

	protected function verifyPost() {
		$user = Login::getInstance()->getCurrentUserObject();
		$threadObj = ThreadItem::createFromDB($this->thread);
		if ($this->login->isLoggedIn() && $threadObj->checkAccess($user)) {
			return true;
		}
		return "Fehlende Berechtigung.";
	}

	public function performPost() {
		$user = Login::getInstance()->getCurrentUserObject();
		$post = ThreadPost::create($user->get(User::PROPERTY_ID), $this->text);
		$post->setThread($this->thread);
		if ($post->save()) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

}