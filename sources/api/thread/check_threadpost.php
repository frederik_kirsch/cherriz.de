<?php

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.StreamGenerator.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.ThreadPostQuery.php");
LibImporter::import("basic/entity/class.ThreadPost.php");
LibImporter::import("basic/entity/class.User.php");

//Parameter abrufen
$thread = Filter::text($_REQUEST['thread']);

//Quera aufbauen
$query = new ThreadPostQuery($thread);
$query->setRange(0, 1);
$query->setSortOrder(Query::SORT_ORDER_DESC);
if (Login::getInstance()->isLoggedIn()) {
    $query->setUser(Login::getInstance()->getCurrentUserObject());
} else {
    $query->setPublic(true);
}

//Daten abrufen
$service = new Service();
$post = $service->requestItems($query);

$gen = new StreamGenerator("post");
$gen->addProperty("id", $post[0]->ID);
$gen->generateStreamOutput();