<?php
/**
 * Schnittstelle zur Authentifizieurng durch eine Anwendung
 */
//Standard API Initialisieren
require '../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");

//Parameter auslesen
$user = $_REQUEST['user'];
$pass = $_REQUEST['password'];

// Anfragedaten hinterlegen
$jsgen = new JSONGenerator();
$jsgen->addQueryData("user", $user);
$jsgen->addQueryData("password", $pass);

//Login durchfuehren
$result = Login::getInstance()->processLogin($user, $pass);

//Ergebnis setzen
if ($result === true) {
	$jsgen->addResultData("status", "Erfolg");
	$jsgen->addResultData("message", "Anmeldung erfolgreich.");
	$jsgen->addResultData("sessionid", Login::getInstance()->getSessionID());
	$jsgen->addResultData("userid", Login::getInstance()->getCurrentUserObject()->get(User::PROPERTY_ID));
} else {
	$jsgen->addResultData("status", "Fehler");
	$jsgen->addResultData("message", $result);
}

//Ausgabe erzeugen
$jsgen->generateJSONOutput();