<?php

LibImporter::import("api/basic/class.AbstractPropertyAPI.php");
LibImporter::import("basic/entity/class.Group.php");
LibImporter::import("basic/util/class.Settings.php");
LibImporter::import("basic/util/internal/class.Message.php");
LibImporter::import("basic/util/class.Mail.php");

/**
 * Class ForumAPI
 * Verarbeitungslogik der Schnittstelle fuer eine Gruppe.
 *
 * @author Frederik Kirsch
 */
class GroupAPI extends AbstractPropertyAPI {

	private $group = null;

	private $userID = null;

	private $anfrage = null;

	private $activeUserID = null;

	public function __construct($groupID, $userID, $anfrage = null) {
		$this->group = new Group($groupID);
		$this->userID = $userID;
		$this->anfrage = $anfrage;

		$curUser = Login::getInstance()->getCurrentUserObject();
		$this->activeUserID = $curUser->get(User::PROPERTY_ID);

		$this->addQueryItem("gruppe", $this->group->getID());
		$this->addQueryItem("user", $this->userID);
		$this->addQueryItem("anfrage", $this->anfrage);
	}

	/**
	 * Prüft ob der Nutzer die Berechtigung hat eine Beitrittsanfrage an die Gruppe zu senden.
	 *
	 * @return bool|string
	 */
	protected function verifyRequest() {
		//Anwender authentifizieren
		if ($this->activeUserID == $this->userID && $this->group->getUserStatus($this->userID) == null) {
			return true;
		}
		return "Keine Berechtigung. Um eine Anfrage abzusenden dürfen sie nicht Teil der Gruppe sein.";
	}

	/**
	 * Führt eine Beitrittsanfrage durch.
	 *
	 * @return bool|string
	 */
	protected function performRequest() {
		if ($this->group->requestAccess($this->userID, $this->anfrage)) {
			return true;
		}
		return "Unbekanntes Problem.";
	}

	/**
	 * Prüft ob der Nutzer die Berechtigung hat eine Beitrittsanfrage an die Gruppe zu senden.
	 *
	 * @return bool|string
	 */
	protected function verifyAccept() {
		if ($this->group->getUserStatus($this->userID) != null) {
			return "Der Nutzer ist bereits Mitglied dieser Gruppe.";

		}
		$usrStatus = $this->group->getUserStatus($this->activeUserID);
		if ($usrStatus != Group::RANG_VERTRETER && $usrStatus != Group::RANG_VERWALTER) {
			return "Keine ausreichenden Berechtigungen für diese Aktion.";
		}
		return true;
	}

	/**
	 * Akzeptiert eine Beitrittsanfrage zu einer Gruppe.
	 *
	 * @return bool|string
	 */
	protected function performAccept() {
		if ($this->group->acceptRequest($this->userID, $this->activeUserID)) {
			return true;
		}
		return "Unbekanntes Problem.";
	}

	/**
	 * Prüft ob der Nutzer die Berechtigung hat eine Beitrittsanfrage an die Gruppe abzulehnen.
	 *
	 * @return bool|string
	 */
	protected function verifyDecline() {
		$usrStatus = $this->group->getUserStatus($this->activeUserID);
		if ($usrStatus != Group::RANG_VERTRETER && $usrStatus != Group::RANG_VERWALTER) {
			return "Keine ausreichenden Berechtigungen für diese Aktion.";
		}
		return true;
	}

	/**
	 * Lehnt eine Beitrittsanfrage zu einer Gruppe ab.
	 *
	 * @return bool|string
	 */
	protected function performDecline() {
		if ($this->group->declineRequest($this->userID, $this->activeUserID)) {
			return true;
		}
		return "Unbekanntes Problem.";
	}

	/**
	 * Prüft ob ein Nutzer zu einer Gruppe hinzugefügt werden kann.
	 *
	 * @return bool|string
	 */
	protected function verifyAdd() {
		if ($this->group->getUserStatus($this->userID) == null) {
			return true;
		}
		return "Der Nutzer ist bereits Mitglied dieser Gruppe.";
	}

	/**
	 * Fügt einen Nutzer zu einer Gruppe hinzu.
	 *
	 * @return bool|string
	 */
	protected function performAdd() {
		if ($this->group->addGroupMember($this->userID, $this->activeUserID)) {
			$this->addResultItem("userStatus", Group::RANG_MITGLIED);
			return true;
		}
		return "Unbekanntes Problem.";
	}

	/**
	 * Prüft ob der Rang eines Nutzers angehoben werden kann.
	 *
	 * @return bool|string
	 */
	protected function verifyUpgrade() {
		if ($this->group->getUserStatus($this->activeUserID) == null) {
			return "Sie sind nicht Mitglied der Gruppe.";
		}
		if ($this->group->getUserStatus($this->userID) == Group::RANG_VERWALTER) {
			return "Verwalter ist bereits der höchste Rang.";
		}
		return true;
	}

	/**
	 * Hebt den Rang eines Nutzers an.
	 *
	 * @return bool|string
	 */
	protected function performUpgrade() {
		if ($this->group->upgrade($this->userID, $this->activeUserID)) {
			$this->addResultItem("userStatus", $this->group->getUserStatus($this->userID));
			return true;
		}
		return "Unbekanntes Problem.";
	}

	/**
	 * Prüft ob der Rang eines Nutzers verringert werden kann.
	 *
	 * @return bool|string
	 */
	protected function verifyDowngrade() {
		if ($this->group->getUserStatus($this->activeUserID) == null) {
			return "Sie sind nicht Mitglied der Gruppe.";
		}
		if ($this->group->getUserStatus($this->userID) == Group::RANG_MITGLIED) {
			return "Mitglied ist bereits der niedrigste Rang.";
		}
		return true;
	}

	/**
	 * Verringert den Rang eines Nutzers.
	 *
	 * @return bool|string
	 */
	protected function performDowngrade() {
		if ($this->group->downgrade($this->userID, $this->activeUserID)) {
			$this->addResultItem("userStatus", $this->group->getUserStatus($this->userID));
			return true;
		}
		return "Unbekanntes Problem.";
	}

	/**
	 * Prüft ob ein Mitglied aus einer Gruppe entfernt werden kann.
	 *
	 * @return bool|string
	 */
	protected function verifyRemove() {
		$userStatus = $this->group->getUserStatus($this->userID);
		if ($userStatus == null) {
			return "Der Nutzer ist nicht Mitglied dieser Gruppe.";
		}
		if ($userStatus == Group::RANG_VERWALTER) {
			return "Verwalter können nicht entfernt werden.";
		}
		return true;
	}

	/**
	 * Entfernt ein Mitglied aus einer Gruppe
	 *
	 * @return bool|string
	 */
	protected function performRemove() {
		if ($this->group->removeGroupMember($this->userID, $this->activeUserID)) {
			return true;
		}
		return "Unbekanntes Problem.";
	}

}