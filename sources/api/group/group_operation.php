<?php
/**
 * Schnittstelle fuer Aktionen bezogen auf eine Gruppe.
 */

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/group/class.GroupAPI.php");
LibImporter::import("basic/entity/class.Group.php");
LibImporter::import("basic/util/class.Settings.php");
LibImporter::import("basic/util/internal/class.Message.php");
LibImporter::import("basic/util/class.Mail.php");

$action = Filter::text($_REQUEST['action']);
$userID = Filter::text($_REQUEST['user']);
$gruppeID = Filter::text($_REQUEST['group']);
$anfrage = Filter::text($_REQUEST['anfrage']);

$groupAPI = new GroupAPI($gruppeID, $userID, $anfrage);
$groupAPI->perform($action);
$groupAPI->createOutput();