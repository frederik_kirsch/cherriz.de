<?php

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.ForumQuery.php");
LibImporter::import("basic/entity/class.ForumItem.php");
LibImporter::import("basic/entity/class.User.php");

//Parameter abrufen
$start = FILTER::text($_REQUEST['start']);
$max = FILTER::text($_REQUEST['max']);
$deleted = FILTER::text($_REQUEST['deleted']);

//Quera aufbauen
$query = new ForumQuery();
$query->setDeleted(false);
$query->setRange($start, $max);

//Daten abrufen
$service = new Service();
$news = $service->requestItems($query);
$anz = $service->countItems($query);
//JSON erzeugen
$generator = new JSONGenerator();
$generator->addQueryData("start", $start);
$generator->addQueryData("max", $max);
$generator->addQueryData("deleted", $deleted);
$items = array();
foreach ($news as $item) {
	array_push($items, array("id" => $item->ID, "marker" => $item->Name));
}
$generator->addResultData("items", $items);
$generator->addResultData("count", $anz);

//JSON ausgeben
$generator->generateJSONOutput();