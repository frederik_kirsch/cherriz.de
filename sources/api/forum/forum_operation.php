<?php
/**
 * Schnittstelle fuer Aktionen bezogen auf ein Forum.
 */

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/forum/class.ForumAPI.php");

$action = Filter::text($_REQUEST['action']);
$forum = Filter::text($_REQUEST['forum']);

$forumAPI = new ForumAPI($forum);
$forumAPI->perform($action);
$forumAPI->createOutput();