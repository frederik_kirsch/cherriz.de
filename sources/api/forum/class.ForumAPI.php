<?php

LibImporter::import("api/basic/class.AbstractPropertyAPI.php");
//Zusätzlich Imports
LibImporter::import("basic/entity/class.User.php");
LibImporter::import("basic/util/class.Verify.php");
LibImporter::import("basic/util/class.Settings.php");

/**
 * Class ForumAPI
 * Verarbeitungslogik der Schnittstelle fuer ein Forum.
 *
 * @author Frederik Kirsch
 */
class ForumAPI extends AbstractPropertyAPI {

	private $forum = null;

	private $db = null;

	public function __construct($forum) {
		$this->db = DBConnect::getDBConnection();
		$this->forum = $forum;
		$this->addQueryItem("forum", $forum);
	}

	protected function verifyDelete() {
		$user = Login::getInstance()->getCurrentUserObject();
		if (Login::getInstance()->isLoggedIn() && $user->hasRight(Recht::FORUM_ADMIN)) {
			return true;
		}
		return "Fehlende Berechtigung.";
	}

	protected function performDelete() {
		if ($this->db->query("UPDATE Forum SET Deleted = 1 WHERE ID = $this->forum")) {
			return true;
		}
		return "Fehler bei der Kommunikation mit der Datenbank.";
	}

}