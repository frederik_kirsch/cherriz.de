<?php

//Standard API Initialisieren
require '../../basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("../../");
LibImporter::import("basic/globals.php");

//Zusätzlich Imports
LibImporter::import("api/basic/class.JSONGenerator.php");
LibImporter::import("basic/entity/class.Kommentar.php");
LibImporter::import("basic/entity/class.NewsItem.php");
LibImporter::import("service/class.Service.php");
LibImporter::import("service/class.CommentQuery.php");

//Parameter abrufen
$pNews = FILTER::text($_REQUEST['news']);
$pStart = FILTER::text($_REQUEST['start']);
$pMax = FILTER::text($_REQUEST['max']);

//Berechtigung prüfen
$hasAccess = false;
$news = NewsItem::createNewsFromDB($pNews);
if ($news->get(NewsItem::ATTRIBUTE_PUBLIC) === true && $news->get(NewsItem::ATTRIBUTE_PUBLISHED) === true) {
    $hasAccess = true;
} else {
    if (Login::getInstance()->isLoggedIn() && $news->get(NewsItem::ATTRIBUTE_PUBLISHED) === true) {
        $user = Login::getInstance()->getCurrentUserObject();
        $newsGroups = $news->get(NewsItem::ATTRIBUTE_GROUPS);
        $userGroups = $user->getGroups();
        foreach ($userGroups as $grp) {
            if (in_array($grp->getID(), $newsGroups)) {
                $hasAccess = true;
                break;
            }
        }
    }
}

$gen = new JSONGenerator();
$gen->addQueryData("start", $pStart);
$gen->addQueryData("max", $pMax);
$gen->addQueryData("news", $pNews);

if ($hasAccess === true) {
    //Query aufbauen
    $query = new CommentQuery($pNews);
	$query->setRange($pStart, $pMax);
    //Daten abrufen
    $service = new Service();
	$anz = $service->countItems($query);
    $news = $service->requestItems($query);
    //JSON erzeugen
	$items = array();
    foreach ($news as $item) {
	    array_push($items, array("id" => $item->ID, "marker" => strtotime($item->Date)));
    }
	$gen->addResultData("items", $items);
	$gen->addResultData("count", $anz);
} else {
    $gen->setError("Sie besitzen nicht die notwendigen Berechtigungen für diese Abfrage.");
}

//JSON ausgeben
$gen->generateJSONOutput();