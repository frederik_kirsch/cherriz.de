<?php
require 'sources/basic/util/class.LibImporter.php';
LibImporter::setExecutionPath("sources/");
LibImporter::import("basic/globals.php");

//Alle Includes für die Seite
LibImporter::import("basic/util/class.Settings.php");
LibImporter::import("basic/util/class.Toolkit.php");
LibImporter::import("basic/util/class.Verify.php");
LibImporter::import("basic/util/class.CookieInfo.php");
LibImporter::import("basic/entity/class.Group.php");
LibImporter::import("basic/entity/interface.UserKonstanten.php");

LibImporter::import("sites/class.Page.php");
LibImporter::import("sites/class.PageController.php");
LibImporter::import("sites/profile/class.Profile.php");
LibImporter::import("sidebar/class.SidebarBox.php");

//Objekte erzeugen
$VERIFY = new Verify();
$PROFILE = null;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>cherriz.de - the unique community blog</title>
	<meta charset="UTF-8" content="text/html"/>
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	<link rel="stylesheet" type="text/css" href="styles/lightbox/lightbox.css">
	<link rel="stylesheet" type="text/css" href="styles/dialog.css">
	<link rel="stylesheet" type="text/css" href="styles/highlight/idea.css">
	<link rel="stylesheet" type="text/css" href="styles/jquery-ui/jquery-ui-1.11.4.css">
	<link rel="stylesheet" type="text/css" href="styles/font-awesome/css/font-awesome.min.css"/>
	<script type='text/javascript' src='scripts/jquery/jquery-1.11.1.min.js'></script>
	<script type='text/javascript' src='scripts/jquery/jquery-ui-1.11.4.min.js'></script>
	<script type='text/javascript' src='scripts/lightbox/lightbox.min.js'></script>
	<script type="text/javascript" src="scripts/toolkit.js"></script>
	<script type="text/javascript" src="scripts/dialog.js"></script>
	<script type="text/javascript" src="scripts/highlight/highlight.pack.js"></script>
	<script type='text/javascript'>
		STORE.saveAssoc('UserID', <?php echo("'" . Login::getInstance()->getCurrentUserObject()->get(User::PROPERTY_ID) . "'") ?>);
		$(document).tooltip();
	</script>
</head>
<body>
<?php CookieInfo::showCookieBannerIfRequired(); ?>
<div id="main">
	<div id="content">
		<div id="head"></div>
		<div id="nav">
			<ul>
				<li><a href='index.php?page=<?php echo Menu::FEED ?>'>Feed</a></li>
				<li><a href='index.php?page=<?php echo Menu::FORUM ?>'>Forum</a></li>
				<li><a href='index.php?page=<?php echo Menu::GRUPPENUEBERSICHT ?>'>Gruppen</a></li>
			</ul>
		</div>
		<div id="logo"></div>
		<div id="publish">
			<?php
			$pageController = PageController::getInstance();
			$pageContent = $pageController->determineAndLoadPage();
			$pageController->showPage($pageContent);
			?>
		</div>
		<div id="sidebar">
			<?php
			$db = DBConnect::getDBConnection();
			$result = $db->query("SELECT ID, Class FROM Sidebar ORDER BY Pos");

			while ($box = $result->fetch_object()) {
				LibImporter::import($box->Class);
				/** @var SidebarBox $box */
				$box = new $box->ID($pageContent);
				$box->draw();
			}
			?>
		</div>
	</div>
	<div id="footer">
		<div id="footer_topref">
			Zum Seitenanfang
		</div>
	</div>
</div>
</body>
</html>