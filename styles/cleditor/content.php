<?php
	ini_set("display_errors", 1);
	require '../../sources/basic/util/class.LibImporter.php';
	LibImporter::setExecutionPath("../../sources/basic/util/");
	LibImporter::import("class.Filter.php");
	LibImporter::import("class.DBConnect.php");

	$filter = new Filter();
	$width = $filter->text($_GET["width"]);
	header("Content-Type: text/css; charset=utf-8");
?>
@import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,600);
@import url(https://fonts.googleapis.com/css?family=Playfair+Display:400,700);

* {
padding:     0;
margin:      0;
font-family: "Source Sans Pro", Arial;
color:       black;
}

body {
    float: left;
    color: #333;
    font-size: 14px;
    line-height: 1.5;
    text-align: justify;
    width: <?php echo($width - 25); ?>px;
}

body img {
    margin-left: -5px;
}

body h2 {
    font-size: 18px;
    color: #4D4D4D;
    padding-top: 7px;
    padding-bottom: 10px;
    clear: both;
}

body a {
    color: #2c881d;
    text-decoration: none;
}

body a:hover {
    color: #F40000;
}

body ul {
    list-style-type: none;
	padding-left: 14px;
}

body ul li {
    color: #000;
    text-decoration: none;
    padding-left: 21px;
    background: url(../../gfx/submenue.png) 1px 9px no-repeat;
}

body ol {
	padding-left: 30px;
}

body ol li {
	padding-left: 5px;
}

iframe {
	padding-top: 5px;
	width: <?php echo($width - 35); ?>px;
	height: 344px;
	border: none;
}

code{
	display: block;
	border:                   solid #333333 1px;
	background:               #eeeeee;
	margin-bottom:            10px;
	margin-top:               10px;
}